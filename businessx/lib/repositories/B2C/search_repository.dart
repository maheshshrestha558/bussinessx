import 'package:b2b_market/models/response/category_search_response.dart';
import 'package:b2b_market/models/response/product_search_response.dart';
import 'package:b2b_market/repositories/base_repository.dart';
import 'package:dio/dio.dart';

class SearchB2CRepository {
  late Dio dio;
  late String appUrl;

  SearchB2CRepository() {
    dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<ProductSearchResponse> searchB2CProducts(String keyword) async {
    try {
      final response = await dio.get('$appUrl/search?pdt=$keyword');
      return ProductSearchResponse.fromJson(response.data);
    } catch (error) {
      return ProductSearchResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<LeafCategorySearchResponse> getB2CLeafCategories(
    String keyword,
  ) async {
    try {
      final response = await dio.get('$appUrl/search-categories?cat=$keyword');
      return LeafCategorySearchResponse.fromJson(response.data);
    } catch (error) {
      return LeafCategorySearchResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
