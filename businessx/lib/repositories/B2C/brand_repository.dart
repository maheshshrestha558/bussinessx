import 'package:b2b_market/models/response/brand/all_brands_response.dart';
import 'package:b2b_market/models/response/brand/single_brand_response.dart';
import 'package:b2b_market/repositories/base_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class BrandRepository {
  late final String appUrl;
  late final Dio _dio;

  BrandRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<AllBrandsResponse> fetchAll() async {
    try {
      Response response = await _dio.get('$appUrl/brands');
      return AllBrandsResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return AllBrandsResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SingleBrandResponse> fetch(int brandID) async {
    try {
      Response response = await _dio.get('$appUrl/shop-by-brands/$brandID');
      return SingleBrandResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return SingleBrandResponse.withError(baseRepository.handleError(error));
    }
  }
}
