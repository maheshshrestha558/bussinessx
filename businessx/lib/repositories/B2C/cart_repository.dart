import 'package:b2b_market/common/enums.dart';
import 'package:b2b_market/common/type_defs.dart';
import 'package:b2b_market/models/response/add_to_cart_response.dart';
import 'package:b2b_market/models/response/total_price_response.dart';
import 'package:b2b_market/repositories/base_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../../models/response/cart_response.dart';
import '../../models/response/product/popular_product_response.dart';

class CartRepository {
  late final String appUrl;
  late final Dio _dio;

  CartRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<AddToCartResponse> addToCart(SingleProduct product) async {
    try {
      Response response = await _dio.post(
        '$appUrl/cart-items',
        data: {
          'product_id': product.id,
          'quantity': 1,
        },
      );
      return AddToCartResponse.withSuccess(response.statusCode == 200);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return AddToCartResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<AddToCartResponse> updateProductQuantity(
    int cartId,
    int productQuantity,
    ProductQuantityEvent event,
  ) async {
    if (event == ProductQuantityEvent.add) {
      productQuantity++;
    } else {
      productQuantity--;
    }
    try {
      Response response = await _dio.post(
        '$appUrl/cart-items/$cartId',
        queryParameters: {
          'quantity': productQuantity,
        },
      );
      return AddToCartResponse.withSuccess(response.statusCode == 200);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return AddToCartResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<TotalPriceResponse> getTotalPrice(ProductIds productIds) async {
    try {
      Response response = await _dio.get(
        '$appUrl/total-price',
        queryParameters: {
          'products[]': productIds,
        },
      );
      return TotalPriceResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return TotalPriceResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<CartResponse> getCart() async {
    try {
      Response response = await _dio.get(
        '$appUrl/cart-items',
      );
      return CartResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return CartResponse.withError(baseRepository.handleError(error));
    }
  }
}
