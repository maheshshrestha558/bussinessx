import 'package:dio/dio.dart';

import '../models/response/category_search_response.dart';
import '../models/response/product_search_response.dart';
import 'repositories.dart';

class SearchRepository {
  late Dio dio;
  late String appUrl;

  SearchRepository() {
    dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<ProductSearchResponse> searchProducts(String keyword) async {
    try {
      final response = await dio.get('$appUrl/search?pdt=$keyword');
      return ProductSearchResponse.fromJson(response.data);
    } catch (error) {
      return ProductSearchResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<LeafCategorySearchResponse> getLeafCategories(String keyword) async {
    try {
      final response = await dio.get('$appUrl/search-categories?cat=$keyword');
      return LeafCategorySearchResponse.fromJson(response.data);
    } catch (error) {
      return LeafCategorySearchResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
