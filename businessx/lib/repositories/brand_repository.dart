import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/response.dart';
import 'base_repository.dart';

class BrandRepository {
  late final String appUrl;
  late final Dio _dio;

  BrandRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<AllBrandsResponse> fetchAll() async {
    try {
      Response response = await _dio.get('$appUrl/brands');
      return AllBrandsResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return AllBrandsResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SingleBrandResponse> fetch(int brandID) async {
    try {
      Response response = await _dio.get('$appUrl/brands/$brandID');
      return SingleBrandResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return SingleBrandResponse.withError(baseRepository.handleError(error));
    }
  }
}
