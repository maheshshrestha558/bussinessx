import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/response/response.dart';
import 'base_repository.dart';

class BannerRepository {
  late Dio _dio;
  late String appUrl;

  BannerRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<BannersResponse> getBanner() async {
    try {
      Response response = await _dio.get('$appUrl/banners');
      return BannersResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return BannersResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<B2CBannerResponse> getB2CBanner() async {
    try {
      Response response = await _dio.get('$appUrl/retail-banners');
      return B2CBannerResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return B2CBannerResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<B2CBannerCategoryResponse> getB2CCategoryBanner(int categoryId) async {
    try {
      Response response =
          await _dio.get('$appUrl/category-banners/?cat=$categoryId');
      return B2CBannerCategoryResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return B2CBannerCategoryResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
