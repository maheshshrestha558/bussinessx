import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/response.dart';
import 'repositories.dart';

class PlatformDetailsRepository {
  late Dio _dio;
  late String appUrl;

  PlatformDetailsRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<PlatformDetailsResponse> getPlatformDetails() async {
    try {
      final response = await _dio.get('$appUrl/platform');
      return PlatformDetailsResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return PlatformDetailsResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}

final platformDetailsRepository = PlatformDetailsRepository();
