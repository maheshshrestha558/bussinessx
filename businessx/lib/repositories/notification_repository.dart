import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/response.dart';
import 'base_repository.dart';

class NotificationRepository {
  late final String appUrl;
  late final Dio _dio;

  NotificationRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<NotificationsResponse> getAllNotifications() async {
    try {
      Response response = await _dio.get('$appUrl/notifications');
      return NotificationsResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return NotificationsResponse.withError(baseRepository.handleError(error));
    }
  }

 void sendFirebaseToken(String firebasseToken, int userId)  {
    try {
        _dio.post(
        '$appUrl/store-token/$userId',
        data: {
          'device_token': firebasseToken,
        },
      );
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
    }
  }
}
