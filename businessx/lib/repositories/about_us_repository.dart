import '../models/response/about_us/about_us_response.dart';
import 'base_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AboutUsRepository {
  late Dio _dio;
  late String appUrl;

  AboutUsRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<AboutUsResponse> getAboutUs() async {
    try {
      Response response = await _dio.get('$appUrl/seller/about');
      return AboutUsResponse.fromJson(response.data);
    } catch (error) {
      return AboutUsResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<AboutUsResponse> getSellerAboutInfo(int businessID) async {
    try {
      Response response = await _dio.get('$appUrl/seller/about/$businessID');
      return AboutUsResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return AboutUsResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<AboutUsResponse> updateAboutUs(params) async {
    try {
      Response response =
          await _dio.post('$appUrl/seller/about', queryParameters: params);
      return AboutUsResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return AboutUsResponse.withError(baseRepository.handleError(error));
    }
  }
}
