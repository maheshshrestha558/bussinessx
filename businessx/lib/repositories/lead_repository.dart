import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/params/create_lead_params.dart';
import '../models/response/lead/view_leads_response.dart';
import '../models/response/response.dart';
import 'base_repository.dart';

class LeadRepository {
  late Dio _dio;
  late String appUrl;

  LeadRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<LeadResponse> createLead(CreateLeadParams params) async {
    try {
      final response =
          await _dio.post('$appUrl/leads', queryParameters: params.toJson());
      return LeadResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return LeadResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<ViewLeadsResponse> viewLeads(int page, int perPage) async {
    try {
      final response = await _dio.get(
        '$appUrl/chats',
        queryParameters: {'page': page, 'per_page': perPage},
      );
      return ViewLeadsResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return ViewLeadsResponse.withError(baseRepository.handleError(error));
    }
  }
}

LeadRepository activityLogRepository = LeadRepository();
