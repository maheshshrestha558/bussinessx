import 'package:b2b_market/models/place.dart';
import 'package:b2b_market/models/response/data_collection/data_collection_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:latlong2/latlong.dart';

import 'base_repository.dart';

class DataCollectionRepository {
  late final String appUrl;
  late final Dio _dio;

  DataCollectionRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<DataCollectionResponse> postDatacollect(
    DataCollectionAnswer dataCollectionAnswer,
  ) async {
    debugPrint('dataCollectionAnswer ${dataCollectionAnswer.toJson()}');
    try {
      Response response = await _dio.post(
        '$appUrl/market-answers',
        data: dataCollectionAnswer.toJson(),
      );
      return DataCollectionResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return DataCollectionResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<DataCollectionResponse> updateDatacollect(
    DataCollectionAnswer dataCollectionAnswer,
    int id,
  ) async {
    debugPrint('dataCollectionAnswer ${dataCollectionAnswer.toJson()}');
    try {
      Response response = await _dio.post(
        '$appUrl/market-answers/$id',
        data: dataCollectionAnswer.toJson(),
      );
      return DataCollectionResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return DataCollectionResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<DataCollection> getDatacollect() async {
    try {
      final response = await _dio.get('$appUrl/market-answers');
      return DataCollection.fromJson(response.data);
    } catch (error) {
      return DataCollection.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<Place> reverseSearch(LatLng latLng) async {
    try {
      final response = await _dio.get(
        'https://nominatim.openstreetmap.org/reverse',
        queryParameters: {
          'format': 'jsonv2',
          'lat': latLng.latitude.toString(),
          'lon': latLng.longitude.toString(),
        },
      );
      return Place.fromJson(response.data);
    } catch (error) {
      return Place.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
