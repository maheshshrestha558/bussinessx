import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/category/root_with_sub_categories_response.dart';
import '../models/response/response.dart';
import 'repositories.dart';

class CategoryRepository {
  late String appUrl;
  late Dio dio;

  CategoryRepository() {
    dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<RootWithSubCategoriesResponse> getRootWithSubCategories(
    int page,
    int perPage,
  ) async {
    try {
      final response = await dio.get(
        '$appUrl/categories?category_type=rootWithSubCategories',
        queryParameters: {'page': page, 'per_page': perPage},
      );
      return RootWithSubCategoriesResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return RootWithSubCategoriesResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<RootCategoriesResponse> getRootCategories(
    int page,
    int perPage,
  ) async {
    try {
      final response = await dio.get(
        '$appUrl/categories?category_type=root',
        queryParameters: {'page': page, 'per_page': perPage},
      );
      return RootCategoriesResponse.fromJson(response.data);
    } catch (error) {
      debugPrint('Error occured : ${error.toString()}');
      return RootCategoriesResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  // Future<B2CTestCategoriesResponse> getTestCategories(int categoryID) async {
  //   try {
  //     final response = await dio.get('$appUrl/show-categories/$categoryID');
  //     print('res from res ${response.data}');
  //     return B2CTestCategoriesResponse.fromJson(response.data);
  //   } catch (error, stackTrace) {
  //     debugPrint('error $error');
  //     debugPrint('stack trace $stackTrace');
  //     return B2CTestCategoriesResponse.withError(
  //       baseRepository.handleError(error),
  //     );
  //   }
  // }

  Future<SubCategoriesResponse> getSubCategories(int categoryID) async {
    try {
      final response = await dio.get('$appUrl/categories/$categoryID');
      return SubCategoriesResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error');
      debugPrint('stack trace $stackTrace');
      return SubCategoriesResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<ChildCategoriesWithProductsResponse> getChildCategoriesWithProducts(
    int businessID,
    int page,
    int perPage,
  ) async {
    try {
      final response = await dio.get(
        '$appUrl/categories',
        queryParameters: {
          'business_id': businessID,
          'category_type': 'leaf',
          'page': page,
          'per_page': perPage
        },
      );
      return ChildCategoriesWithProductsResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error stackTrace: $stackTrace');
      return ChildCategoriesWithProductsResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<ChildCategoryResponse> getChildCategories(int categoryID) async {
    try {
      final response = await dio.get('$appUrl/categories/$categoryID');
     
      return ChildCategoryResponse.fromJson(response.data);
    } catch (error) {
      return ChildCategoryResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<LeafResponse> getLeafCategories(int categoryID) async {
    try {
      final response = await dio.get('$appUrl/categories/$categoryID');
      return LeafResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error');
      debugPrint('stack $stackTrace');
      return LeafResponse.withError(baseRepository.handleError(error));
    }
  }
}
