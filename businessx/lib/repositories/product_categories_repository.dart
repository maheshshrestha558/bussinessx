// import 'package:dio/dio.dart';
// import 'package:flutter/foundation.dart';

// import '../models/response/response.dart';
// import 'repositories.dart';

// class ProductCategoriesRepository {
//   late Dio _dio;
//   late String appUrl;

//   ProductCategoriesRepository() {
//     _dio = baseRepository.dio;
//     appUrl = baseRepository.appUrl;
//   }

//   Future<ProductCategoriesResponse> getProductCategories() async {
//     try {
//       final response = await _dio.get('$appUrl/categories?category_type=leaf');
//       return ProductCategoriesResponse.fromJson(response.data);
//     } catch (error, stackTrace) {
//       debugPrint('Exception occured: $error stackTrace: $stackTrace');
//       return ProductCategoriesResponse.withError(
//           baseRepository.handleError(error));
//     }
//   }
// }

// ProductCategoriesRepository productCategoriesRepository =
//     ProductCategoriesRepository();
