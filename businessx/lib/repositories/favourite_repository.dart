import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/response/favorite/add_favorite_response.dart';
import '../models/response/favorite/delete_favorite_response.dart';
import '../models/response/favorite/favourite_response.dart';
import '../models/response/favorite/unfavorited_response.dart';
import 'base_repository.dart';

class FavouriteRepository {
  late Dio _dio;
  late String appUrl;

  FavouriteRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }
  Future<FavoriteResponse> getFavourite(int page, int perPage) async {
    try {
      final response = await _dio.get(
        '$appUrl/favourites',
        queryParameters: {'page': page, 'per_page': perPage},
      );

      return FavoriteResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return FavoriteResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<UnfavoriteResponse> getUnfavorite() async {
    try {
      final response = await _dio.get('$appUrl/unfavourated-sellers');
      return UnfavoriteResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return UnfavoriteResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<DeleteFavoriteResponse> deleteFavourite(int favoriteID) async {
    try {
      var response = await _dio.delete('$appUrl/favourites/$favoriteID');
      return DeleteFavoriteResponse.fromJson(response.data);
    } catch (error) {
      return DeleteFavoriteResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<AddFavoriteResponse> addToFavorite(int businessID) async {
    try {
      var response =
          await _dio.post('$appUrl/favourites?business_id=$businessID');
      return AddFavoriteResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return AddFavoriteResponse.withError(baseRepository.handleError(error));
    }
  }
}
