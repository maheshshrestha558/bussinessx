import 'package:b2b_market/common/type_defs.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/response.dart';
import 'base_repository.dart';

class AuthRepository {
  late final String appUrl;
  late final Dio _dio;

  AuthRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<LoginResponse> login(LoginCredentials credentials) async {
    try {
      Response response = await _dio.post(
        '$appUrl/login',
        queryParameters: credentials.toJson(),
      );

      return LoginResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return LoginResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SignUpResponse> signup(SignUpCredentials credentials) async {
    try {
      Response response = await _dio.post(
        '$appUrl/register',
        queryParameters: credentials.toJson(),
      );
      return SignUpResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return SignUpResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<OTPVerificationResponse> otpVerification(OTPParams otpParams) async {
    try {
      Response response = await _dio.post(
        '$appUrl/submitOtp',
        queryParameters: otpParams.toJson(),
      );
      return OTPVerificationResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return OTPVerificationResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<OTPResendResponse> otpResend(Map<String, dynamic> params) async {
    try {
      Response response = await _dio.post(
        '$appUrl/resendOtp',
        queryParameters: params,
      );
      return OTPResendResponse.fromJson(response.data);
    } catch (error) {
      return OTPResendResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<ResetPasswordResponse> resetPasswordRequest(
    KVPair data,
  ) async {
    try {
      Response response = await _dio.post(
        '$appUrl/reset-password-request',
        data: data,
      );
      return ResetPasswordResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');

      return ResetPasswordResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<ResetPasswordResponse> resetPassword(
    PasswordResetParams data,
  ) async {
    try {
      Response response =
          await _dio.post('$appUrl/reset-password', data: data.toJson());
      return ResetPasswordResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');

      return ResetPasswordResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SetNewPasswordResponse> setNewPassword(
    SetNewPasswordParams params,
  ) async {
    try {
      Response response = await _dio.post(
        '$appUrl/changePassword',
        queryParameters: params.toJson(),
      );
      return SetNewPasswordResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return SetNewPasswordResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<LogOutResponse> logOut() async {
    try {
      final response = await _dio.post('$appUrl/logOut');
      return LogOutResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return LogOutResponse.withError(baseRepository.handleError(error));
    }
  }
}
