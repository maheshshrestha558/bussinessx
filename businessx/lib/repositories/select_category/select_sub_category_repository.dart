import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../models/response/categories_response.dart';
import '../base_repository.dart';

class SelectSubCategoryRepository {
  late Dio _dio;
  late String appUrl;

  SelectSubCategoryRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<CategoriesResponse> getSubCategories({
    int parentId = 0,
    int page = 1,
  }) async {
    try {
      final response = await _dio.get(
        '$appUrl/product-categories',
        queryParameters: {'per_page': 20, 'parent_id': parentId, 'page': page},
      );
      return CategoriesResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      debugPrint('Exception occurred: $error stackTrace: $stacktrace');
      return CategoriesResponse.withError(baseRepository.handleError(error));
    }
  }
}
