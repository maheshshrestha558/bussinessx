import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/response.dart';
import 'repositories.dart';

class SpecificationsRepository {
  late Dio dio;
  late String appUrl;

  SpecificationsRepository() {
    dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<SpecificationsResponse> getSpecifications(int categoryId) async {
    try {
      final response = await dio.get('$appUrl/specifications?category_id=$categoryId');
      return SpecificationsResponse.fromJson(response.data);
    } catch (error,stacktrace) {
      debugPrint('Error: $error stackTrace: $stacktrace');
      return SpecificationsResponse.withError(baseRepository.handleError(error));
    }
  }
}

