import '../common/logger/logger.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/response.dart';
import 'base_repository.dart';

class ChatRepository {
  late Dio _dio;
  late String appUrl;

  final log = getLogger(ChatRepository);

  ChatRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<AllMessageResponse> fetchMessages(int leadId, int page) async {
    try {
      final response = await _dio.get(
        '$appUrl/chat-message/$leadId',
        queryParameters: {'per_page': 15, 'page': page},
      );
      return AllMessageResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return AllMessageResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SendMessageResponse> sendMessage(
    Map<String, dynamic> message,
    FormData? productPhotos,
  ) async {
    if (productPhotos != null) {
      for (var photo in productPhotos.files) {
        debugPrint('photo: ${photo.value.filename}');
      }
    }

    try {
      final response = await _dio.post(
        '$appUrl/lead-message',
        queryParameters: message,
        data: productPhotos,
        options: Options(contentType: 'multipart/form-data'),
      );
      return SendMessageResponse.fromJson(response.data);
    } catch (error) {
      log.e(error);
      return SendMessageResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<void> downloadImage(String urlPath, String savePath) async {
    final response = await _dio.download(urlPath, savePath);
    debugPrint('downloadImage: $response');
  }
}
