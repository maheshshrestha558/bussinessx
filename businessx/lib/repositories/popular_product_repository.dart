import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/response/product/popular_product_response.dart';
import 'base_repository.dart';

class PopularProductRepository {
  late String appUrl;
  late Dio dio;

  PopularProductRepository() {
    dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<PopularProductResponse> getPopularProducts() async {
    try {
      final response = await dio.get('$appUrl/popular-products');
      return PopularProductResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error  stack $stackTrace');
      return PopularProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<PopularProductResponse> getSellerPopularProducts(
    int businessID,
  ) async {
    try {
      final response = await dio.get(
        '$appUrl/popular-products',
        queryParameters: {'business_id': businessID},
      );
      return PopularProductResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error');
      debugPrint('stack $stackTrace');
      return PopularProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
