import 'package:dio/dio.dart';

import '../models/packages.dart';
import 'base_repository.dart';

class PackageRepository {
  late Dio _dio;
  late String appUrl;

  PackageRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<PackagesResponse> getPackages() async {
    try {
      final response = await _dio.get('$appUrl/packages');
      return PackagesResponse.fromJson(response.data);
    } catch (error) {
      return PackagesResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<PackageSubscriptionResponse> postPackages(int id) async {
    try {
      final response = await _dio
          .post('$appUrl/packages/subscribe-package?package_id=' '$id');
      return PackageSubscriptionResponse.fromJson(response.data);
    } catch (error) {
      return PackageSubscriptionResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
