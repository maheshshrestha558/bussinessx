import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../models/response/user/user_response.dart';
import '../models/user.dart';
import 'base_repository.dart';

class UsersRepository {
  late Dio _dio;
  late String appUrl;

  UsersRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<UserResponse> getSingleUser(int userID) async {
    try {
      final response = await _dio.get('$appUrl/users/$userID');
      return UserResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return UserResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<UserResponse> updateUser(User user, FormData? data) async {
    debugPrint('formdata : ${data?.fields}');

    try {
      final response = await _dio.post(
        '$appUrl/users/${user.id}',
        queryParameters: user.toJson(),
        data: data,
        options: Options(contentType: 'multipart/form-data'),
      );
      return UserResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return UserResponse.withError(baseRepository.handleError(error));
    }
  }
}
