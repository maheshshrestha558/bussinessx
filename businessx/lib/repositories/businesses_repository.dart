import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../models/response/business/businesses_response.dart';
import '../models/response/business/single_business_response.dart';
import '../models/response/business/store_business_response.dart';
import '../models/response/business/update_business_response.dart';
import '../models/response/business_type/all_business_type_response.dart';
import 'base_repository.dart';

class BusinessesRepository {
  late Dio _dio;
  late String appUrl;

  BusinessesRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<BusinessesResponse> getBusinesses() async {
    try {
      final response = await _dio.get('$appUrl/businesses');
      return BusinessesResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error' 'stackTrace $stackTrace');
      return BusinessesResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SingleBusinessResponse> getBusiness() async {
    try {
      final response = await _dio.get('$appUrl/business');
      return SingleBusinessResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrintStack(stackTrace: stackTrace);
      return SingleBusinessResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<SingleBusinessResponse> getFavouriteBusiness(int businessID) async {
    try {
      final response = await _dio.get('$appUrl/businesses/$businessID');
      return SingleBusinessResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('error $error  stack $stackTrace');
      return SingleBusinessResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<UpdateBusinessResponse> updateBusiness(
    int businessID,
    params,
    data,
  ) async {
    try {
      final response = await _dio.post(
        '$appUrl/businesses/$businessID',
        queryParameters: params,
        data: data,
        options: Options(contentType: 'multipart/form-data'),
      );
      return UpdateBusinessResponse.fromJson(response.data);
    } catch (error) {
      debugPrint(error.toString());
      return UpdateBusinessResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

// done
  Future<CreateBusinessResponse> createBusiness(params, FormData? data) async {
    try {
      final response = await _dio.post(
        '$appUrl/businesses',
        queryParameters: params,
        data: data,
        options: Options(contentType: 'multipart/form-data'),
      );
      return CreateBusinessResponse.fromJson(response.data);
    } catch (error) {
      return CreateBusinessResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<AllBusinessTypeResponse> getBusinessTypes() async {
    try {
      final response = await _dio.get('$appUrl/business-types');
      return AllBusinessTypeResponse.fromJson(response.data);
    } catch (error) {
      return AllBusinessTypeResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}
