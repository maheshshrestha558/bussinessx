import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/response/dashboard_response.dart';
import 'base_repository.dart';

class DashBoardRepository {
  late Dio dio;
  late String appUrl;

  DashBoardRepository() {
    dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<DashboardResponse> getDashboardInfo() async {
    try {
      final response = await dio.get('$appUrl/seller-dashboard');
      return DashboardResponse.fromJson(response.data);
    } catch (error) {
      debugPrint('Error occured : ${error.toString()}');
      return DashboardResponse.withError(baseRepository.handleError(error));
    }
  }
}
