import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../bloc/auth_bloc.dart';

class BaseRepository {
  // static const String _baseUrl = 'http://192.168.0.123:8000';

  // static const String _baseUrl = 'https://businessx.com.np';

  static const String _baseUrl = 'https://devbusinessx.sidhasewa.com';

  static const String _appUrl = '$_baseUrl/api';

  String get baseUrl => _baseUrl;

  String get appUrl => _appUrl;

  late Dio _dio;
  get dio => _dio;

  bool isUserLoggedIn = false;
  String? socketId;

  BaseRepository() {
    BaseOptions options = BaseOptions(
      receiveTimeout: const Duration(milliseconds: 20000),
      connectTimeout: const Duration(milliseconds: 30000),
    );
    _dio = Dio(options);
    _dio.interceptors.addAll(
      [
        QueuedInterceptorsWrapper(
          onRequest: (options, handler) async {
            options.headers[HttpHeaders.acceptHeader] = ContentType.json;
            var token = await authBloc.getToken();
            if (token != null) {
              options.headers[HttpHeaders.authorizationHeader] =
                  'Bearer $token';
              options.headers[HttpHeaders.userAgentHeader] =
                  'eeeinnovation369@gmail.com';
            }
            if (socketId != null) {
              options.headers['X-Socket-ID'] = socketId;
            }

            handler.next(options);
          },
        ),
        if (kDebugMode)
          LogInterceptor(
            requestBody: true,
            responseBody: true,
          ),
      ],
    );
  }

  String handleError(error) {
    String? errorDescription;
    if (error is DioError) {
      DioError dioError = error;
      switch (dioError.type) {
        case DioErrorType.cancel:
          errorDescription = 'Request to API server was cancelled';
          break;
        case DioErrorType.connectionTimeout:
          errorDescription = 'Connection timeout with API server';
          break;
        case DioErrorType.unknown:
          errorDescription = 'No internet connection';
          break;
        case DioErrorType.receiveTimeout:
          errorDescription = 'Receive timeout in connection with API server';
          break;
        case DioErrorType.badResponse:
          errorDescription =
              'Received invalid status code: ${dioError.response?.statusCode}';
          switch (dioError.response?.statusCode) {
            case 401:
              errorDescription = 'Unauthenticated';
              break;
            case 422:
              if (dioError.response?.data['error'] != null) {
                var errors = json.encode(dioError.response?.data['error']);
                errorDescription = json
                    .decode(errors)
                    .values
                    .toList()
                    .map((v) => v.join('\n'))
                    .join('\n');
              } else if (dioError.response?.data['message'] != null) {
                errorDescription = dioError.response?.data['message'];
              } else {
                errorDescription = dioError.response?.statusMessage;
              }
              break;
            case 500:
              if (dioError.response?.data['error'] != null) {
                errorDescription = dioError.response?.data['error'];
              } else {
                errorDescription = 'Something went wrong on server';
              }
              break;
          }
          break;
        case DioErrorType.sendTimeout:
          errorDescription = 'Send timeout in connection with API server';
          break;
        case DioErrorType.badCertificate:
          break;
        case DioErrorType.connectionError:
          errorDescription = 'Send timeout in connection with API server';

          break;
      }
    } else {
      errorDescription = 'Unexpected error occurred';
    }
    return errorDescription!;
  }
}

final BaseRepository baseRepository = BaseRepository();
