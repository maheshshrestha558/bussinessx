import 'package:b2b_market/models/response/product/popular_product_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/response/response.dart';
import 'repositories.dart';

class ProductRepository {
  late Dio _dio;
  late String appUrl;

  ProductRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<ProductsResponse> getProducts(int childCategoryId) async {
    try {
      final response = await _dio.get('$appUrl/categories/$childCategoryId');

      return ProductsResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return ProductsResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<CreateProductResponse> createProduct(
    createProductParams,
    FormData? productPhotos,
  ) async {
    debugPrint('product photos from repository: $productPhotos');
    try {
      final response = await _dio.post(
        '$appUrl/products',
        queryParameters: createProductParams,
        data: productPhotos,
        options: Options(contentType: 'multipart/form-data'),
      );
      return CreateProductResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return CreateProductResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<CreateProductResponse> updateProduct(
    int productId,
    createProductParams,
    FormData? productPhotos,
  ) async {
    try {
      final response = await _dio.post(
        '$appUrl/products/$productId',
        queryParameters: createProductParams,
        data: productPhotos,
        options: Options(contentType: 'multipart/form-data'),
      );
      return CreateProductResponse.fromJson(response.data);
    } catch (error, stackTrace) {
      debugPrint('Exception occured: $error stackTrace: $stackTrace');
      return CreateProductResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SingleProductResponse> getSingleProduct(int productId) async {
    try {
      final response = await _dio.get('$appUrl/products/$productId');
      return SingleProductResponse.fromJson(response.data);
    } catch (error) {
      return SingleProductResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<B2CSingleProductResponse> getB2CSingleProduct(int productId) async {
    try {
      final response =
          await _dio.get('$appUrl/single-product-details/$productId');
      return B2CSingleProductResponse.fromJson(response.data);
    } catch (error) {
      return B2CSingleProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<ProductStatusResponse> setStatusOfProduct(
    int productId,
    int status,
  ) async {
    try {
      var response = await _dio.post(
        '$appUrl/products/status-update/$productId',
        data: {'status': status},
      );
      return ProductStatusResponse.fromJson(response.data);
    } catch (error) {
      return ProductStatusResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<SellerProductsResponse> fetchSellerProducts() async {
    try {
      var response = await _dio.get('$appUrl/products');
      return SellerProductsResponse.fromJson(response.data);
    } catch (error) {
      return SellerProductsResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<ProductDeleteResponse> deleteProduct(int productId) async {
    try {
      var response = await _dio.delete('$appUrl/products/$productId');
      return ProductDeleteResponse.fromJson(response.data);
    } catch (error) {
      return ProductDeleteResponse.withError(baseRepository.handleError(error));
    }
  }

  Future<B2CProductResponse> fetchNewArrivalProducts() async {
    try {
      var response = await _dio.get('$appUrl/new-arrivals');

      return B2CProductResponse.fromJson(response.data);
    } catch (error) {
      return B2CProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<B2CProductResponse> fetchAllProducts() async {
    try {
      var response = await _dio.get('$appUrl/all-products');

      return B2CProductResponse.fromJson(response.data);
    } catch (error) {
      return B2CProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<B2CProductResponse> fetchSuggestedProducts() async {
    try {
      var response = await _dio.get('$appUrl/suggested-products');

      return B2CProductResponse.fromJson(response.data);
    } catch (error) {
      return B2CProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<B2CProductResponse> fetchFeaturedProducts() async {
    try {
      var response = await _dio.get('$appUrl/featured-products');

      return B2CProductResponse.fromJson(response.data);
    } catch (error) {
      return B2CProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }

  Future<B2CProductResponse> fetchCategoryWiseProducts(int categoryId) async {
    try {
      final response = await _dio.get(
        '$appUrl/category-products',
        queryParameters: {
          'category_id': categoryId,
          'per_page': 10,
        },
      );
      return B2CProductResponse.fromJson(response.data);
    } catch (error) {
      return B2CProductResponse.withError(
        baseRepository.handleError(error),
      );
    }
  }
}

ProductRepository allProductRepository = ProductRepository();
