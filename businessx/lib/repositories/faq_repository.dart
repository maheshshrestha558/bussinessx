import '../models/response/faq/faq_response.dart';
import 'package:dio/dio.dart';

import 'base_repository.dart';

class FaqRepository {
  late Dio _dio;
  late String appUrl;

  FaqRepository() {
    _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  Future<FaqResponse> getFaq() async {
    try {
      final response = await _dio.get('$appUrl/faqs');
      return FaqResponse.fromJson(response.data);
    } catch (error) {
      return FaqResponse.withError(baseRepository.handleError(error));
    }
  }
}
