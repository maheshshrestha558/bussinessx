import 'dart:async';

import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../common/logger/logger.dart';

mixin NoInternetMixin<T extends StatefulWidget> on State<T> {
  late StreamSubscription connectivityStream;
  late bool showInternetAlert = false;
  bool check = true;
  final _logger = getLogger(NoInternetMixin);

  @override
  initState() {
    super.initState();
    connectivityStream = InternetConnectionChecker().onStatusChange.listen(
      (InternetConnectionStatus result) {
        if (result == InternetConnectionStatus.disconnected &&
            !showInternetAlert &&
            check) {
          _logger.i(result);

          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Icon(
                    Icons.error_outline,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text('No internet connection'),
                ],
              ),
              behavior: SnackBarBehavior.floating,
            ),
          );
          showInternetAlert = true;
        }
        if (result == InternetConnectionStatus.connected &&
            showInternetAlert &&
            check) {
          _logger.i(result);

          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Icon(
                    Icons.check_circle_outline,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text('Internet connection available'),
                ],
              ),
              behavior: SnackBarBehavior.floating,
              backgroundColor: Colors.green,
            ),
          );
          showInternetAlert = false;
        }
      },
    );
  }

  @override
  void dispose() {
    connectivityStream.cancel();
    super.dispose();
  }
}
