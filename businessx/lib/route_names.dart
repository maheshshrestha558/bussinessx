class RouteName {
  RouteName._();
  // auth routes

  static const String loginOptionsScreen = '/loginOptionsScreen';
  static const String passwordResetConfirmationScreen =
      '/passwordResetConfirmationScreen';
  static const String otpVerificationScreen = 'otpVerificationScreen';

  static const String navigationScreen = '/navigationScreen';
  static const String dashBoardScreen = '/dashBoardScreen';
  static const String sellerProductsListScreen = '/sellerProductsListScreen';

  static const String sellerHomePage = '/sellerHomePage';
  static const String changePasswordScreen = '/changePasswordScreen';
  static const String settingScreen = '/settingScreen';

  static const String checkConditionPage = '/checkConditionPage';
  static const String editSellerProfileScreen = '/editSellerProfileScreen';
  static const String editAboutUsScreen = '/editAboutUsScreen';
  static const String editAdditionalInfoScreen = '/editAdditionalInfoScreen';
  static const String editStutoryProfileScreen = '/editStutoryProfileScreen';
  static const String editPackagingInfoScreen = '/editPackagingInfoScreen';
  static const String editContactUsScreen = '/editContactUsScreen';
  static const String termsAndConditionScreen = '/termsAndConditionScreen';
  static const String helpAndSupportScreen = '/helpAndSupportScreen';
  static const String checkFavouriteScreen = 'checkFavouriteScreen';
  static const String aboutUsScreen = 'aboutUsScreen';
  static const String searchScreen = 'searchScreen';

  static const String mainDataCollectionPage = 'mainDataCollectionPage';

  static const String generalPresenceQuestionScreen =
      'generalPresenceQuestionScreen';
  static const String b2bPainPointsQuestionScreen =
      'b2bPainPointsQuestionScreen';
  static const String benefitsQuestionScreen = 'benefitsQuestionScreen';
  static const String buyingPatternQuestionScreen =
      'buyingPatternQuestionScreen';
  static const String willingnessAboutOurPlatformQuestionScreen =
      'willingnessAboutOurPlatformQuestionScreen';

  static const String createSellerProfileFirstScreen =
      'createSellerProfileFirstScreen';
  static const String viewLeadScreen = 'viewLeadScreen';
  static const String faqScreen = 'faqScreen';

  // category screens
  static const String leafCategoryScreen = 'leafCategoryScreen';
  static const String subCategoryScreen = 'subCategoryScreen';
  static const String childCategoryScreen = 'childCategoryScreen';

  // select category routes

  static const String selectRootCategoryScreen = '/selectCategoryScreen';
  static const String selectSubCategoryScreen = '/selectSubCategoryScreen';
  static const String selectChildCategoryScreen = '/selectChildCategoryScreen';
  static const String selectLeafCategoryScreen = '/selectLeafCategoryScreen';

  // add/edit product routes
  static const String addProductScreen = 'addProductScreen';
  static const String editProductScreen = 'editProductScreen';

  // notification
  static const String notificationScreen = 'notificationScreen';

  // contact_us
  static const String contactUs = 'contactUs';

  // edit_profile_screen
  static const String editProfileScreen = 'editProfileScreen';

  //brand produt screen
  static const String brandProductScreen = 'brandProductScreen';

  static const String submitRequirementScreen = 'submitRequirementScreen';

  static const String businessProfileScreen = 'businessProfileScreen';

  static const String storePageScreen = 'storePageScreen';
  static const String editBusinessProfileScreen = 'editBusinessProfileScreen';

  static const String productsScreen = 'productsScreen';
  static const String productDetailScreen = 'productDetailScreen';
  static const String sellerPopularProductsScreen =
      'sellerPopularProductsScreen';
  static const String singlePackageSubscriptionScreen =
      'singlePackageSubscriptionScreen';
  // buyer category
  static const String buyerCategoryScreen = 'buyerCategoryScreen';
  static const String buyerSubCategoryScreen = 'buyerSubCategoryScreen';
  static const String buyerChildCategoryScreen = 'buyerChildCategoryScreen';
  static const String buyerLeafCategoryScreen = 'buyerLeafCategoryScreen';

  static const String buyerProductsScreen = 'buyerProductsScreen';
  static const String buyerSingleProductsScreen = 'buyerSingleProductsScreen';
  static const String searchB2CScreen = 'searchB2CScreen';

  static const String buyerSuggestionScreen = 'buyerSuggestionScreen';
  static const String buyerfeatureScreen = 'buyerfeatureScreen';

  static const String editPageScreen = 'editPageScreen';

  static const String shopdetailsScreen = 'ShopdetailsScreen';
}
