import 'package:rxdart/rxdart.dart';

import '../../models/response/categories_response.dart';
import '../../repositories/repositories.dart';

class SelectSubCategoryBloc {
  final _repository = SelectSubCategoryRepository();

  final _subCategories = BehaviorSubject<List<Category>>();

  int? _lastPage;
  final List<Category> _subCategoryList = [];

  int? get lastPage => _lastPage;

  void getSubCategories(int parentId, int page) async {
    final response =
        await _repository.getSubCategories(parentId: parentId, page: page);
    if (response.categoryData != null) {
      _lastPage = response.categoryData!.meta.lastPage;
      _subCategoryList.addAll(response.categoryData!.categories!);
      _subCategories.sink.add(_subCategoryList);
    }
    if (response.error != null) {
      _subCategories.sink.addError(response.error!);
    }
  }

  void dispose() {
    _subCategoryList.clear();
  }

  BehaviorSubject<List<Category>> get subCategories => _subCategories;
}

final selectSubCategoryBloc = SelectSubCategoryBloc();
