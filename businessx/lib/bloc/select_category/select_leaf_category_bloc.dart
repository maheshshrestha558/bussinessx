import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../../models/response/categories_response.dart';
import '../../repositories/repositories.dart';

class SelectLeafCategoryBloc {
  final _repository = SelectLeafCategoryRepository();

  final _leafCategories = BehaviorSubject<List<Category>>();

  final categoryNotifier = ValueNotifier<String>('');

  int? _lastPage;
  final List<Category> _leafCategoryList = [];

  int? get lastPage => _lastPage;

  bool _isEditMode = false;

  bool get isEditMode => _isEditMode;

  void setEditMode(bool value) {
    _isEditMode = value;
  }

  void getLeafCategories(int parentId, int page) async {
    final response =
        await _repository.getLeafCategories(parentId: parentId, page: page);
    if (response.categoryData != null) {
      _lastPage = response.categoryData!.meta.lastPage;
      _leafCategoryList.addAll(response.categoryData!.categories!);
      _leafCategories.sink.add(_leafCategoryList);
    }
    if (response.error != null) {
      _leafCategories.sink.addError(response.error!);
    }
  }

  BehaviorSubject<List<Category>> get leafCategories => _leafCategories;

  void dispose() {
    _leafCategoryList.clear();
  }
}

final selectLeafCategoryBloc = SelectLeafCategoryBloc();
