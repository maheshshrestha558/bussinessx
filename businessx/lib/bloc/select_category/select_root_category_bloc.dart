import 'package:rxdart/rxdart.dart';

import '../../models/response/categories_response.dart';
import '../../repositories/repositories.dart';

class SelectRootCategoryBloc {
  final _repository = SelectRootCategoryRepository();

  final _rootCategories = BehaviorSubject<List<Category>>();

  int? _lastPage;
  final List<Category> _rootCategoryList = [];

  int? get lastPage => _lastPage;

  void getRootCategories(int parentId, int page) async {
    final response =
        await _repository.getRootCategories(parentId: parentId, page: page);
    if (response.categoryData != null) {
      _lastPage = response.categoryData!.meta.lastPage;
      _rootCategoryList.addAll(response.categoryData!.categories!);
      _rootCategories.sink.add(_rootCategoryList);
    }
    if (response.error != null) {
      _rootCategories.sink.addError(response.error!);
    }
  }

  void dispose() {
    _rootCategoryList.clear();
  }

  BehaviorSubject<List<Category>> get rootCategories => _rootCategories;
}

final selectRootCategoryBloc = SelectRootCategoryBloc();
