import 'package:rxdart/rxdart.dart';

import '../../models/response/categories_response.dart';
import '../../repositories/repositories.dart';

class SelectChildCategoryBloc {
  final _repository = SelectChildCategoryRepository();

  final _childCategories = BehaviorSubject<List<Category>>();

  int? _lastPage;
  final List<Category> _childCategoryList = [];

  int? get lastPage => _lastPage;

  void getChildCategories(int parentId, int page) async {
    final response =
        await _repository.getChildCategories(parentId: parentId, page: page);
    if (response.categoryData != null) {
      _lastPage = response.categoryData!.meta.lastPage;
      _childCategoryList.addAll(response.categoryData!.categories!);
      _childCategories.sink.add(_childCategoryList);
    }
    if (response.error != null) {
      _childCategories.sink.addError(response.error!);
    }
  }

  void dispose() {
    _childCategoryList.clear();
  }

  BehaviorSubject<List<Category>> get childCategories => _childCategories;
}

final selectChildCategoryBloc = SelectChildCategoryBloc();
