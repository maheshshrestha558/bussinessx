import 'package:rxdart/rxdart.dart';

import '../models/response/response.dart';
import '../repositories/repositories.dart';

class BrandBloc {
  final BrandRepository _repository = BrandRepository();
  final _allBrandsResponse = BehaviorSubject<AllBrandsResponse>();
  final _brandResponse = BehaviorSubject<SingleBrandResponse>();

  Future<AllBrandsResponse> fetchAll() async {
    AllBrandsResponse response = await _repository.fetchAll();
    _allBrandsResponse.sink.add(response);
    return response;
  }

  Future<SingleBrandResponse> fetch(int brandID) async {
    SingleBrandResponse response = await _repository.fetch(brandID);
    _brandResponse.sink.add(response);
    return response;
  }

  BehaviorSubject<AllBrandsResponse> get allBrandsResponse =>
      _allBrandsResponse;
  BehaviorSubject<SingleBrandResponse> get brandResponse => _brandResponse;
}

final BrandBloc brandBloc = BrandBloc();
