import 'package:b2b_market/bloc/bloc.dart';
import 'package:b2b_market/common/enums.dart';
import 'package:b2b_market/common/type_defs.dart';
import 'package:b2b_market/screens/buyer_screens/buyer_navigation_screen.dart';
import 'package:b2b_market/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/logger/logger.dart';
import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/response/response.dart';
import '../repositories/repositories.dart';
import '../route_names.dart';

class AuthBloc {
  final AuthRepository _repository = AuthRepository();
  final _logger = getLogger(AuthBloc);

  Future<bool> isUserAuthenticated() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('token') != null ? true : false;
  }

  Future<String?> getToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('token');
  }

  Future<String?> getName() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('name');
  }

  Future<String?> getContact() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('contact');
  }

  Future<bool> isDataCollector() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool('isDataCollector') != null;
  }

  Future<bool> userIsBuyer() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    _logger.i('user is buyer ${pref.get(UserType.buyer.type)}');
    return pref.get(UserType.buyer.type) == true;
  }

  void login(
    BuildContext context,
    LoginCredentials credentials,
  ) async {
    showCustomProgressDialog(context, 'Logging in...');

    final LoginResponse response = await _repository.login(credentials);

    // pop custom_progress_dialog
    Navigator.pop(context);

    if (response.error == null && response.userData?.user == null) {
      ShowToast.success(response.message);
      await Future.delayed(const Duration(seconds: 1));
      Navigator.pushNamed(
        context,
        RouteName.otpVerificationScreen,
        arguments: credentials,
      );
    }

    if (response.error == null && response.userData?.user != null) {
      String? token = response.userData?.token;
      int? userID = response.userData?.user!.id;
      String? name = response.userData?.user!.enName;
      String? contact = response.userData?.user!.contactNumber;
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', token!);
      prefs.setInt('userID', userID!);
      prefs.setString('name', name!);
      prefs.setString('contact', contact!);
      baseRepository.isUserLoggedIn = true;
      notificationBloc.sendFirebaseToken();
      ShowToast.success('Login Successful');
      if (response.userData!.permissions.contains('market-detail-create')) {
        prefs.setBool('isDataCollector', true);
      }
      if (response.userData!.roles.contains(UserType.buyer.type)) {
        _logger
            .i('user type ${response.userData!.roles}  ${UserType.buyer.type}');
        prefs.setBool(UserType.buyer.type, true);
        Navigator.pushReplacementNamed(
          context,
          BuyerNavigationScreen.routeName,
        );
      } else {
        Navigator.pushReplacementNamed(
          context,
          RouteName.navigationScreen,
        );
      }
    }

    if (response.error != null) {
      ShowToast.error(response.error);
    }
  }

  void logOut(
    BuildContext context,
  ) async {
    showCustomProgressDialog(context, 'Logging Out...');

    final response = await _repository.logOut();

    // pop custom_progress_dialog
    Navigator.pop(context);

    if (response.error == null) {
      baseRepository.isUserLoggedIn = false;
      ShowToast.success(response.message);
      SharedPreferences pref = await SharedPreferences.getInstance();
      pref.clear();

      Navigator.pushNamedAndRemoveUntil(
        context,
        RouteName.loginOptionsScreen,
        (Route<dynamic> route) => false,
      );
    } else {
      ShowToast.error(response.error);
    }
  }

  void signUp(BuildContext context, SignUpCredentials credentials) async {
    _logger.i(credentials);

    showCustomProgressDialog(context, 'Signing Up...');

    SignUpResponse response = await _repository.signup(credentials);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.message);
      Navigator.pushNamed(
        context,
        LoginScreen.routeName,
        arguments: credentials.userType,
      );
    } else {
      ShowToast.error(response.error);
    }
  }

  void otpVerification(
    BuildContext context,
    OTPParams params,
  ) async {
    showCustomProgressDialog(context, 'Verifying OTP...');

    final OTPVerificationResponse response =
        await _repository.otpVerification(params);

    Navigator.pop(context);
    if (response.userData?.user != null) {
      String? token = response.userData?.token;
      int? userID = response.userData?.user!.id;
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', token!);
      prefs.setInt('userID', userID!);
      baseRepository.isUserLoggedIn = true;
      ShowToast.success('Login Successful');
       if (response.userData!.roles.contains(UserType.buyer.type)) {
        _logger
            .i('user type ${response.userData!.roles}  ${UserType.buyer.type}');
        prefs.setBool(UserType.buyer.type, true);
        Navigator.pushReplacementNamed(
          context,
          BuyerNavigationScreen.routeName,
        );
      } else {
        Navigator.pushReplacementNamed(
          context,
          RouteName.navigationScreen,
        );
      }
     
    } else {
      ShowToast.success(response.error);
    }
  }

  void resendOtp(BuildContext context, Map<String, dynamic> params) async {
    showCustomProgressDialog(context, 'Sending OTP...');

    OTPResendResponse response = await _repository.otpResend(params);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.message);
    } else {
      ShowToast.error(response.error);
    }
  }

  void resetPasswordRequest(
    BuildContext context,
    KVPair data,
  ) async {
    showCustomProgressDialog(context, 'Veryfing contact number...');

    final response = await _repository.resetPasswordRequest(data);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.message);
      Navigator.pushNamed(
        context,
        PasswordResetScreen.routeName,
        arguments: data['contact_number'],
      );
    } else {
      ShowToast.error(response.error);
    }
  }

  void resetPassword(
    BuildContext context,
    PasswordResetParams data,
  ) async {
    showCustomProgressDialog(context, 'Changing Password...');

    final response = await _repository.resetPassword(data);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.message);
      Navigator.pushNamed(context, LoginScreen.routeName);
    } else {
      ShowToast.error(response.error);
    }
  }

  void setNewPassword(
    BuildContext context,
    SetNewPasswordParams params,
  ) async {
    showCustomProgressDialog(context, 'Requesting...');

    SetNewPasswordResponse response = await _repository.setNewPassword(params);

    Navigator.pop(context);
    if (response.status == 200) {
      ShowToast.success(response.message);
      authBloc.logOut(context);
    } else {
      ShowToast.error(response.error);
    }
  }
}

final AuthBloc authBloc = AuthBloc();
