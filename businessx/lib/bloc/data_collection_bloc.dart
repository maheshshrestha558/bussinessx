import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:latlong2/latlong.dart';


import '../common/widgets/custom_progress_dialog.dart';
import '../common/widgets/custom_toast.dart';
import '../models/place.dart';
import '../models/response/data_collection/data_collection_response.dart';
import '../repositories/data_collection_repository.dart';
import '../route_names.dart';

class DataCollectionBloc {
  final DataCollectionRepository _repository = DataCollectionRepository();

  final _dataCollectionResponse = BehaviorSubject<DataCollection>();

  final locationNotifier = ValueNotifier<String>('');

  void getDatacollect() async {
    final response = await _repository.getDatacollect();
    _dataCollectionResponse.sink.add(response);
  }

  void postDatacollect(
    BuildContext context,
    DataCollectionAnswer datacollectionAnswer,
  ) async {
    showCustomProgressDialog(context, 'Requesting...');

    DataCollectionResponse response =
        await _repository.postDatacollect(datacollectionAnswer);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.message);
      datacollectionAnswer = DataCollectionAnswer();
      Navigator.of(context)
        ..pushNamedAndRemoveUntil(
          RouteName.navigationScreen,
          (route) => false,
        )
        ..pushNamed(RouteName.mainDataCollectionPage);
    } else {
      ShowToast.error(response.error);
    }
  }

  void updateDatacollect(
    BuildContext context,
    DataCollectionAnswer datacollectionAnswer,
    int id,
  ) async {
    showCustomProgressDialog(context, 'Requesting...');

    DataCollectionResponse response =
        await _repository.updateDatacollect(datacollectionAnswer, id);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.message);
      datacollectionAnswer = DataCollectionAnswer();
      Navigator.of(context)
        ..pushNamedAndRemoveUntil(
          RouteName.navigationScreen,
          (route) => false,
        )
        ..pushNamed(RouteName.mainDataCollectionPage);
    } else {
      ShowToast.error(response.error);
    }
  }

  Future<Place> reverseSearch(LatLng latLng) async {
    return _repository.reverseSearch(latLng);
  }

  BehaviorSubject<DataCollection> get dataCollection => _dataCollectionResponse;
}

final DataCollectionBloc dataCollectionBloc = DataCollectionBloc();
