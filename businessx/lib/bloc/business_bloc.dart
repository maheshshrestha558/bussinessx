import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/response/business/businesses_response.dart';
import '../models/response/business/single_business_response.dart';
import '../models/response/business/store_business_response.dart';
import '../models/response/business/update_business_response.dart';
import '../models/response/business_type/all_business_type_response.dart';
import '../repositories/businesses_repository.dart';
import '../route_names.dart';

class BusinessesBloc {
  final BusinessesRepository _repository = BusinessesRepository();

  final _businesses = BehaviorSubject<BusinessesResponse>();
  final _business = BehaviorSubject<SingleBusinessResponse>();

  final _businessTypes = BehaviorSubject<AllBusinessTypeResponse>();

  void createSeller(
    BuildContext context,
    Map<String, dynamic> params,
    File? image,
  ) async {
    FormData? data;

    if (image != null) {
      data = FormData.fromMap({
        'image': await MultipartFile.fromFile(
          image.path,
          filename: image.path.split('/').last,
        )
      });
    }

    showCustomProgressDialog(context, 'Creating Business');

    CreateBusinessResponse response =
        await _repository.createBusiness(params, data);

    Navigator.pop(context);

    if (response.status == 200) {
      ShowToast.success('Success');

      Navigator.pop(context);
      Navigator.pushReplacementNamed(
        context,
        RouteName.checkConditionPage,
      );
    } else {
      ShowToast.error(response.error);
    }
  }

  void updateBusiness(
    BuildContext context,
    int businessID,
    Map<String, dynamic> params,
    File? image,
  ) async {
    FormData? data;

    if (image != null) {
      data = FormData.fromMap({
        'company_logo': await MultipartFile.fromFile(
          image.path,
          filename: image.path.split('/').last,
        )
      });
    }

    showCustomProgressDialog(context, 'Updating User');

    UpdateBusinessResponse response =
        await _repository.updateBusiness(businessID, params, data);

    Navigator.pop(context);

    if (response.error == null) {
      ShowToast.success('Success');
      Navigator.of(context)
        ..pushNamedAndRemoveUntil(
          RouteName.navigationScreen,
          (route) => false,
        )
        ..pushNamed(RouteName.checkConditionPage);
    } else {
      ShowToast.error(response.error);
    }
  }

  Future<BusinessesResponse> getBusinesses() async {
    BusinessesResponse businessesResponse = await _repository.getBusinesses();
    _businesses.sink.add(businessesResponse);
    return businessesResponse;
  }

  Future<SingleBusinessResponse> getBusiness() async {
    SingleBusinessResponse singleBusinessResponse =
        await _repository.getBusiness();
    _business.sink.add(singleBusinessResponse);
    return singleBusinessResponse;
  }

  Future<SingleBusinessResponse> getFavoriteBusiness(int businessID) async {
    SingleBusinessResponse singleBusinessResponse =
        await _repository.getFavouriteBusiness(businessID);
    _business.sink.add(singleBusinessResponse);
    return singleBusinessResponse;
  }

  Future<AllBusinessTypeResponse> getBusinessTypes() async {
    AllBusinessTypeResponse allBusinessTypeResponse =
        await _repository.getBusinessTypes();
    _businessTypes.sink.add(allBusinessTypeResponse);
    return allBusinessTypeResponse;
  }

  BehaviorSubject<SingleBusinessResponse> get business => _business;
  BehaviorSubject<BusinessesResponse> get businesses => _businesses;
  BehaviorSubject<AllBusinessTypeResponse> get allBusinessTypes =>
      _businessTypes;
}

BusinessesBloc businessesBloc = BusinessesBloc();
