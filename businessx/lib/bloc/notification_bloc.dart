import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/response/response.dart';
import '../repositories/repositories.dart';

class NotificationBloc {
  final _repository = NotificationRepository();
  final _notificationResponse = BehaviorSubject<NotificationsResponse>();

  String? _firebaseToken;

  String? get firebaseToken => _firebaseToken;

  set firebaseToken(String? value) {
    if (value != null) {
      _firebaseToken = value;
    }
  }

  Future<NotificationsResponse> getAllNotifications() async {
    final response = await _repository.getAllNotifications();
    _notificationResponse.sink.add(response);
    return response;
  }

  void sendFirebaseToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var userID = pref.getInt('userID');
    if (userID != null && firebaseToken != null) {
      _repository.sendFirebaseToken(firebaseToken!, userID);
    }
  }

  BehaviorSubject<NotificationsResponse> get notificationResponse =>
      _notificationResponse;
}

final notificationBloc = NotificationBloc();
