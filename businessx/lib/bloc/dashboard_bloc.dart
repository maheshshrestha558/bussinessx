import 'package:rxdart/rxdart.dart';

import '../models/response/dashboard_response.dart';
import '../repositories/dashboard_repository.dart';

class DashBoardBloc {
  final DashBoardRepository _repository = DashBoardRepository();

  final _dashBoardInfo = BehaviorSubject<DashboardResponse>();

  Future<DashboardResponse> getDashboardInfo() async {
    DashboardResponse dashboardResponse = await _repository.getDashboardInfo();
    _dashBoardInfo.sink.add(dashboardResponse);
    return dashboardResponse;
  }

  BehaviorSubject<DashboardResponse> get dashboardInfo => _dashBoardInfo;
}

DashBoardBloc dashBoardBloc = DashBoardBloc();
