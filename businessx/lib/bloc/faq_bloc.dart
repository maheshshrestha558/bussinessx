import 'package:rxdart/rxdart.dart';

import '../models/response/faq/faq_response.dart';
import '../repositories/faq_repository.dart';

class FaqBloc {
  final FaqRepository _repository = FaqRepository();
  final _faqResponse = BehaviorSubject<FaqResponse>();

  Future<FaqResponse> getFaq() async {
    FaqResponse response = await _repository.getFaq();
    _faqResponse.sink.add(response);
    return response;
  }

  BehaviorSubject<FaqResponse> get faq => _faqResponse;
}

FaqBloc faqBloc = FaqBloc();
