import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/response/favorite/add_favorite_response.dart';
import '../models/response/favorite/delete_favorite_response.dart';
import '../models/response/favorite/favourite_response.dart';
import '../models/response/favorite/unfavorited_response.dart';
import '../repositories/favourite_repository.dart';

class FavouriteBloc {
  final FavouriteRepository _repository = FavouriteRepository();
  final _favoriteResponse = BehaviorSubject<FavoriteResponse>();
  final _unFavoriteResponse = BehaviorSubject<UnfavoriteResponse>();
  final _favorite = BehaviorSubject<List<Favorite>>();

  int? _lastPage;
  List<Favorite> _favoriteList = [];

  Future<FavoriteResponse> getFavourites({
    int page = 1,
    int perPage = 5,
  }) async {
    FavoriteResponse response = await _repository.getFavourite(page, perPage);
    _lastPage = response.favorites?.meta?.lastPage;
    var favorites = response.favorites?.favorite;
    if (favorites != null) {
      _favoriteList.addAll(favorites);
      _favorite.sink.add(_favoriteList);
      _favoriteResponse.sink.add(response);
    }
    return response;
  }

  Future<UnfavoriteResponse> getUnfavorite() async {
    UnfavoriteResponse response = await _repository.getUnfavorite();
    _unFavoriteResponse.sink.add(response);
    return response;
  }

  Future<DeleteFavoriteResponse> deleteFavourite(
    BuildContext context,
    int favoriteId,
  ) async {
    showCustomProgressDialog(context, 'Deleting...');
    final response = await _repository.deleteFavourite(favoriteId);
    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success('Deleted successfully');
      _favoriteList.removeWhere((x) => x.id == favoriteId);
      _favorite.sink.add(_favoriteList);
    } else {
      ShowToast.error(response.error);
    }
    return response;
  }

  Future<AddFavoriteResponse> addToFavorite(
    BuildContext context,
    int businessID,
  ) async {
    showCustomProgressDialog(context, 'Adding...');
    final response = await _repository.addToFavorite(businessID);
    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success('Added successfully');
      _favoriteList.add(response.favoriteData!.favorite!);
      _favorite.sink.add(_favoriteList);
    } else {
      ShowToast.error(response.error);
    }
    return response;
  }

  void drainFavorite() {
    _favoriteResponse.drain();
    _favorite.value = [];
    _favoriteList = [];
  }

  int? get lastPage => _lastPage;
  BehaviorSubject<List<Favorite>> get favoriteList => _favorite;
  BehaviorSubject<FavoriteResponse> get favorites => _favoriteResponse;
  BehaviorSubject<UnfavoriteResponse> get unfavorite => _unFavoriteResponse;
}

FavouriteBloc favouriteBloc = FavouriteBloc();
