import '../models/response/response.dart';
import '../repositories/repositories.dart';
import 'package:rxdart/rxdart.dart';

class PlatformDetailsBloc {
  final _platformDetails = BehaviorSubject<PlatformDetailsResponse>();

  Future<PlatformDetailsResponse> getPlatformDetails() async {
    final response = await platformDetailsRepository.getPlatformDetails();
    _platformDetails.sink.add(response);
    return response;
  }

  BehaviorSubject<PlatformDetailsResponse> get platformDetails =>
      _platformDetails;
}

final platformDetailsBloc = PlatformDetailsBloc();
