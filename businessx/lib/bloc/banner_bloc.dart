import 'package:rxdart/rxdart.dart';

import '../models/response/banner/banner_response.dart';
import '../repositories/banner_repository.dart';

class BannerBloc {
  final BannerRepository _repository = BannerRepository();

  final _banner = BehaviorSubject<BannersResponse>();
  final _b2cBanner = BehaviorSubject<B2CBannerResponse>();
  final _b2cCategoryBanner = BehaviorSubject<B2CBannerCategoryResponse>();

  Future<BannersResponse> getBanner() async {
    BannersResponse bannerResponse = await _repository.getBanner();
    _banner.sink.add(bannerResponse);
    return bannerResponse;
  }

  Future<B2CBannerResponse> getB2CBanner() async {
    B2CBannerResponse bannerResponse = await _repository.getB2CBanner();
    _b2cBanner.sink.add(bannerResponse);

    return bannerResponse;
  }

  Future<B2CBannerCategoryResponse> getB2CCategoryBanner(int categoryId) async {
    B2CBannerCategoryResponse bannerResponse =
        await _repository.getB2CCategoryBanner(categoryId);
    _b2cCategoryBanner.sink.add(bannerResponse);
    return bannerResponse;
  }

  BehaviorSubject<BannersResponse> get banner => _banner;
  BehaviorSubject<B2CBannerResponse> get b2cBanner => _b2cBanner;
  BehaviorSubject<B2CBannerCategoryResponse> get b2cCategoryBanner =>
      _b2cCategoryBanner;
}

BannerBloc bannerBloc = BannerBloc();
