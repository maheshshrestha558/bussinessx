import 'package:rxdart/rxdart.dart';

import '../models/response/product/popular_product_response.dart';
import '../repositories/popular_product_repository.dart';

class PopularProductBloc {
  final PopularProductRepository _repository = PopularProductRepository();
  final _popularProductResponse = BehaviorSubject<PopularProductResponse>();
  final _sellerPopularProduct = BehaviorSubject<PopularProductResponse>();

  Future<PopularProductResponse> getPopularProduct() async {
    PopularProductResponse response = await _repository.getPopularProducts();

    _popularProductResponse.sink.add(response);
    return response;
  }

  Future<PopularProductResponse> getSellerPopularProduct(int businessID) async {
    PopularProductResponse response =
        await _repository.getSellerPopularProducts(businessID);
    _sellerPopularProduct.sink.add(response);
    return response;
  }

  BehaviorSubject<PopularProductResponse> get popularProduct =>
      _popularProductResponse;
  BehaviorSubject<PopularProductResponse> get sellerPopularProduct =>
      _sellerPopularProduct;
}

PopularProductBloc popularProductBloc = PopularProductBloc();
