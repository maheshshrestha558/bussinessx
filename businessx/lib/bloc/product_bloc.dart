import 'dart:io';

import 'package:b2b_market/models/response/product/popular_product_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/models.dart';
import '../models/response/response.dart';
import '../repositories/repositories.dart';
import '../route_names.dart';

class ProductBloc {
  final ProductRepository _repository = ProductRepository();
  // final ProductRepository _b2cRepository = ProductRepository();

  final _product = BehaviorSubject<SingleProductResponse>();
  final _productsResponse = BehaviorSubject<ProductsResponse>();
  final _sellerProducts = BehaviorSubject<List<SellerProduct>>();
  final _newArrivalProducts = BehaviorSubject<List<Product>>();
  final _allProducts = BehaviorSubject<List<Product>>();
  final _suggestedProducts = BehaviorSubject<List<Product>>();
  final _featuredProducts = BehaviorSubject<List<Product>>();
  final _categoryWiseProducts = BehaviorSubject<List<Product>>();
  final _subCategoryWiseProducts = BehaviorSubject<List<Product>>();
  final _childCategoryWiseProducts = BehaviorSubject<List<Product>>();
  final _leafCategoryWiseProducts = BehaviorSubject<List<Product>>();

  final _b2cProduct = BehaviorSubject<B2CSingleProductResponse>();
  late SellerProductsResponse _sellerProductsResponse;
  late B2CProductResponse _allProductsResponse;

  String? _productNameEnglish;
  String? _unit;
  String? _description;
  int? _brandId;
  int? _categoryId;
  String? _price;
  List<Map<String, dynamic>>? _specifications;

  void setProductNameEnglish(String? value) {
    _productNameEnglish = value;
  }

  void setUnit(String? value) {
    _unit = value;
  }

  void setDescription(String? value) {
    _description = value;
  }

  void setBrandId(int? value) {
    _brandId = value;
  }

  void setCategoryId(int? value) {
    _categoryId = value;
  }

  void setPrice(String? value) {
    _price = value;
  }

  void setSpecifications(List<Map<String, dynamic>>? specifications) {
    _specifications = specifications;
  }

  get productNameEnglish => _productNameEnglish;
  get unit => _unit;
  get description => _description;
  get brandId => _brandId;
  get categoryId => _categoryId;
  get price => _price;
  get specifications => _specifications;

  getProductParams() {
    return {
      'en_name': productNameEnglish,
      'unit': unit,
      'description': description,
      'brand': brandId,
      'category': categoryId,
      'price': price,
      'specifications': specifications,
    };
  }

  void createProduct(context, images) async {
    showCustomProgressDialog(context, 'Adding...');
    FormData? productPhotos;
    if (images.isNotEmpty) {
      productPhotos = FormData();
      for (var image in images) {
        if (image != null) {
          productPhotos.files.add(
            MapEntry(
              'files[${images.indexOf(image)}]',
              await MultipartFile.fromFile(
                image?.path,
              ),
            ),
          );
        }
      }
    }

    final response =
        await _repository.createProduct(getProductParams(), productPhotos);

    // pop custom_progress_dialog
    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success('Product added successfully');
      Navigator.of(context)
        ..pushNamedAndRemoveUntil(
          RouteName.navigationScreen,
          (route) => false,
        )
        ..pushNamed(RouteName.checkConditionPage);
    } else {
      ShowToast.error(response.error);
    }
  }

  void updateProduct(context, int productId, List<dynamic> imagePaths) async {
    showCustomProgressDialog(context, 'Updating...');
    FormData? productPhotos = FormData();
    if (imagePaths.isNotEmpty) {
      for (var imagePath in imagePaths) {
        if (imagePath is File) {
          productPhotos.files.add(
            MapEntry(
              'files[${imagePaths.indexOf(imagePath)}]',
              await MultipartFile.fromFile(
                imagePath.path,
              ),
            ),
          );
        }
        if (imagePath is String) {
          productPhotos.files.add(
            MapEntry(
              'files[${imagePaths.indexOf(imagePath)}]',
              MultipartFile.fromString(
                imagePath,
              ),
            ),
          );
        }
      }
    }

    final response = await _repository.updateProduct(
      productId,
      getProductParams(),
      productPhotos,
    );

    // pop custom_progress_dialog
    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success();
      Navigator.of(context)
        ..pushNamedAndRemoveUntil(
          RouteName.navigationScreen,
          (route) => false,
        )
        ..pushNamed(RouteName.sellerProductsListScreen);
    } else {
      ShowToast.error(response.error);
    }
  }

  Future<ProductsResponse> getProducts(int childCategoryId) async {
    ProductsResponse response = await _repository.getProducts(childCategoryId);
    _productsResponse.sink.add(response);
    return response;
  }

  Future<ProductStatusResponse> setStatusOfProduct(
    BuildContext context,
    int productId,
    int status,
  ) async {
    showCustomProgressDialog(context, 'Requesting...');
    final response = await _repository.setStatusOfProduct(productId, status);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success(response.data);
    } else if (response.error != null) {
      ShowToast.error(response.error);
    }
    return response;
  }

  Future<ProductDeleteResponse> deleteProduct(
    BuildContext context,
    int productId,
  ) async {
    showCustomProgressDialog(context, 'Requesting...');
    final response = await _repository.deleteProduct(productId);

    Navigator.pop(context);
    if (response.error == null) {
      _sellerProductsResponse.sellerProducts
          .removeWhere((x) => x.id == productId);
      ShowToast.success('Product deleted successfully');
      _sellerProducts.sink.add(_sellerProductsResponse.sellerProducts);
    } else if (response.error != null) {
      ShowToast.error(response.error);
    }
    return response;
  }

  Future<SingleProductResponse> getProduct(int productId) async {
    SingleProductResponse singleProductResponse =
        await _repository.getSingleProduct(productId);
    _product.sink.add(singleProductResponse);
    return singleProductResponse;
  }

  Future<B2CSingleProductResponse> getB2CProduct(int productId) async {
    B2CSingleProductResponse singleProductResponse =
        await _repository.getB2CSingleProduct(productId);
    _b2cProduct.sink.add(singleProductResponse);
    return singleProductResponse;
  }

  Future<SellerProductsResponse> fetchSellerProducts() async {
    _sellerProductsResponse = await _repository.fetchSellerProducts();

    if (_sellerProductsResponse.error.isEmpty) {
      _sellerProducts.sink.add(_sellerProductsResponse.sellerProducts);
    }

    if (_sellerProductsResponse.error.isNotEmpty) {
      _sellerProducts.sink.addError(_sellerProductsResponse.error);
    }
    return _sellerProductsResponse;
  }

  Future<B2CProductResponse> fetchNewArrivalProducts() async {
    _allProductsResponse = await _repository.fetchNewArrivalProducts();

    _newArrivalProducts.sink.add(_allProductsResponse.products);

    return _allProductsResponse;
  }

  Future<B2CProductResponse> fetchAllProducts() async {
    _allProductsResponse = await _repository.fetchAllProducts();

    _allProducts.sink.add(_allProductsResponse.products);

    return _allProductsResponse;
  }

  Future<B2CProductResponse> fetchFeaturedProducts() async {
    _allProductsResponse = await _repository.fetchFeaturedProducts();

    // if (_allProductsResponse.error.isEmpty) {
    _featuredProducts.sink.add(_allProductsResponse.products);
    // }

    // if (_allProductsResponse.error.isNotEmpty) {
    //   _newArrivalProducts.sink.addError(_allProductsResponse.error.toString());
    // }
    return _allProductsResponse;
  }

  Future<B2CProductResponse> fetchSuggestedProducts() async {
    _allProductsResponse = await _repository.fetchSuggestedProducts();

    _suggestedProducts.sink.add(_allProductsResponse.products);

    return _allProductsResponse;
  }

  Future<void> fetchCategoryWiseProducts(int categoryId) async {
    _allProductsResponse =
        await _repository.fetchCategoryWiseProducts(categoryId);

    _categoryWiseProducts.sink.add(_allProductsResponse.products);
  }

  Future<void> fetchSubCategoryWiseProducts(int categoryId) async {
    _allProductsResponse =
        await _repository.fetchCategoryWiseProducts(categoryId);

    _subCategoryWiseProducts.sink.add(_allProductsResponse.products);
  }

  Future<void> fetchChildCategoryWiseProducts(int categoryId) async {
    _allProductsResponse =
        await _repository.fetchCategoryWiseProducts(categoryId);

    _childCategoryWiseProducts.sink.add(_allProductsResponse.products);
  }

  Future<void> fetchLeafCategoryWiseProducts(int categoryId) async {
    _allProductsResponse =
        await _repository.fetchCategoryWiseProducts(categoryId);

    _leafCategoryWiseProducts.sink.add(_allProductsResponse.products);
  }

  BehaviorSubject<SingleProductResponse> get product => _product;

  BehaviorSubject<ProductsResponse> get productsResponse => _productsResponse;

  ValueStream<List<SellerProduct>> get sellerProducts => _sellerProducts.stream;
  ValueStream<List<Product>> get newArrivalProducts =>
      _newArrivalProducts.stream;
  ValueStream<List<Product>> get allProducts => _allProducts.stream;
  ValueStream<List<Product>> get suggestedProducts => _suggestedProducts.stream;

  ValueStream<List<Product>> get featuredProducts => _featuredProducts.stream;
  ValueStream<List<Product>> get categoryWiseProducts =>
      _categoryWiseProducts.stream;
  ValueStream<List<Product>> get subCategoryWiseProducts =>
      _subCategoryWiseProducts.stream;
  ValueStream<List<Product>> get childCategoryWiseProducts =>
      _childCategoryWiseProducts.stream;
  ValueStream<List<Product>> get leafCategoryWiseProducts =>
      _leafCategoryWiseProducts.stream;
}

ProductBloc productBloc = ProductBloc();
