import 'package:rxdart/rxdart.dart';

import '../models/response/category/root_with_sub_categories_response.dart';
import '../models/response/response.dart';
import '../repositories/category_repository.dart';

class CategoryBloc {
  final CategoryRepository _repository = CategoryRepository();
  final _rootWithSubCategoriesRespone =
      BehaviorSubject<RootWithSubCategoriesResponse>();

  final _rootCategoriesResponse = BehaviorSubject<RootCategoriesResponse>();

  final _subCategoriesResponse = BehaviorSubject<SubCategoriesResponse>();
  // final _testCategoriesResponse = BehaviorSubject<B2CTestCategoriesResponse>();
  final _sellerSubCategoriesResponse =
      BehaviorSubject<RootWithSubCategoriesResponse>();

  final _childCategoriesResponse = BehaviorSubject<ChildCategoryResponse>();

  final _leafCategoriesResponse = BehaviorSubject<LeafResponse>();

  final _childCategoriesWithProductsResponse =
      BehaviorSubject<ChildCategoriesWithProductsResponse>();

  int? _lastPageRoot;
  int? _lastPageRootWithSub;
  int? _lastPageChildWithProduct;

  final _rootCategory = BehaviorSubject<List<RootCategory>>();
  final _rootWithSubCategory = BehaviorSubject<List<RootWithSubCategory>>();
  final _childWithProduct = BehaviorSubject<List<ChildCategory>>();
  List<RootCategory> _rootCategoryList = [];
  List<RootWithSubCategory> _rootWithSubCategoryList = [];
  List<ChildCategory> _childWithProductList = [];

  Future<RootCategoriesResponse> getRootCategories({
    int page = 1,
    int perPage = 25,
  }) async {
    RootCategoriesResponse response =
        await _repository.getRootCategories(page, perPage);
    _lastPageRoot = response.rootCategories?.meta?.lastPage;
    var rootCategories = response.rootCategories?.rootCategory;
    if (rootCategories != null) {
      _rootCategoryList.addAll(rootCategories);
      _rootCategory.sink.add(_rootCategoryList);
      _rootCategoriesResponse.sink.add(response);
    }

    return response;
  }

  Future<RootWithSubCategoriesResponse> getRootWithSubCategories({
    int page = 1,
    int perPage = 10,
  }) async {
    RootWithSubCategoriesResponse response =
        await _repository.getRootWithSubCategories(page, perPage);
    _lastPageRootWithSub = response.rootCategories?.meta?.lastPage;
    var rootCategories = response.rootCategories?.rootWithSubCategory;
    if (rootCategories != null) {
      _rootWithSubCategoryList.clear();
      _rootWithSubCategoryList.addAll(rootCategories);
      _rootWithSubCategory.sink.add(_rootWithSubCategoryList);
      _rootWithSubCategoriesRespone.sink.add(response);
    }
    return response;
  }

  // Future<B2CTestCategoriesResponse> getTestCategories(int categoryId) async {
  //   B2CTestCategoriesResponse response =
  //       await _repository.getTestCategories(categoryId);
  //   _testCategoriesResponse.sink.add(response);
  //   print('gg bloc  ${response.data}');
  //   return response;
  // }

  Future<SubCategoriesResponse> getSubCategories(int categoryId) async {
    SubCategoriesResponse response =
        await _repository.getSubCategories(categoryId);
    _subCategoriesResponse.sink.add(response);
    return response;
  }

  Future<ChildCategoriesWithProductsResponse> getChildCategoriesWithProducts(
    int businessID, {
    int page = 1,
    int perPage = 10,
  }) async {
    ChildCategoriesWithProductsResponse response = await _repository
        .getChildCategoriesWithProducts(businessID, page, perPage);
    _lastPageChildWithProduct = response.childCategories?.meta?.lastPage;
    var childWithProduct = response.childCategories?.childCategory;
    if (childWithProduct != null) {
      _childWithProductList.addAll(childWithProduct);
      _childWithProduct.sink.add(_childWithProductList);
      _childCategoriesWithProductsResponse.sink.add(response);
    }

    return response;
  }

  Future<ChildCategoryResponse> getChildCategories(int categoryId) async {
    ChildCategoryResponse response =
        await _repository.getChildCategories(categoryId);
    _childCategoriesResponse.sink.add(response);
    return response;
  }

  Future<LeafResponse> getLeafCategories(int categoryId) async {
    LeafResponse response = await _repository.getLeafCategories(categoryId);
    _leafCategoriesResponse.sink.add(response);
    return response;
  }

  void drainRootCategory() {
    _rootCategoriesResponse.drain();
    _rootCategory.value = [];
    _rootCategoryList = [];
  }

  void drainRootWithSubCategory() {
    _rootWithSubCategoriesRespone.drain();
    _rootWithSubCategory.value = [];
    _rootWithSubCategoryList = [];
  }

  void drainChildWithProduct() {
    _childCategoriesWithProductsResponse.drain();
    _childWithProduct.value = [];
    _childWithProductList = [];
  }

  int? get lastPage => _lastPageRoot;
  int? get lastPageRootWithSub => _lastPageRootWithSub;
  int? get lastPageChildWithProduct => _lastPageChildWithProduct;

  BehaviorSubject<RootWithSubCategoriesResponse> get rootWithSubCategories =>
      _rootWithSubCategoriesRespone;

  BehaviorSubject<RootCategoriesResponse> get rootCategories =>
      _rootCategoriesResponse;
  BehaviorSubject<SubCategoriesResponse> get subCategories =>
      _subCategoriesResponse;
  // BehaviorSubject<B2CTestCategoriesResponse> get b2cTestCategories =>
  //     _testCategoriesResponse;
  BehaviorSubject<RootWithSubCategoriesResponse> get sellerSubCategories =>
      _sellerSubCategoriesResponse;

  BehaviorSubject<ChildCategoryResponse> get childCategories =>
      _childCategoriesResponse;

  BehaviorSubject<LeafResponse> get leafCategories => _leafCategoriesResponse;

  BehaviorSubject<ChildCategoriesWithProductsResponse>
      get childCategoriesWithProducts => _childCategoriesWithProductsResponse;

  BehaviorSubject<List<RootCategory>> get rootCategory => _rootCategory;
  BehaviorSubject<List<RootWithSubCategory>> get rootWithSubCategory =>
      _rootWithSubCategory;
  BehaviorSubject<List<ChildCategory>> get childWithProduct =>
      _childWithProduct;
}

CategoryBloc categoryBloc = CategoryBloc();
