import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/response/about_us/about_us_response.dart';
import '../repositories/about_us_repository.dart';
import '../route_names.dart';

class AboutUsBloc {
  final _repository = AboutUsRepository();

  final _aboutUsInfo = BehaviorSubject<AboutUsResponse>();
  final _sellerAboutUsInfo = BehaviorSubject<AboutUsResponse>();

  void updateAboutUsInfo(
    BuildContext context,
    Map<String, dynamic> params,
  ) async {
    showCustomProgressDialog(context, 'Updating Info');

    AboutUsResponse response = await _repository.updateAboutUs(params);

    Navigator.pop(context);

    if (response.error == null) {
      ShowToast.success('Updated Successfully');
      Navigator.of(context)
        ..pushNamedAndRemoveUntil(
          RouteName.navigationScreen,
          (route) => false,
        )
        ..pushNamed(RouteName.checkConditionPage);
    } else {
      ShowToast.error(response.error);
    }
  }

  Future<AboutUsResponse> getAboutUsInfo() async {
    AboutUsResponse response = await _repository.getAboutUs();
    _aboutUsInfo.sink.add(response);
    return response;
  }

  Future<AboutUsResponse> getSellerAboutInfo(int businessID) async {
    AboutUsResponse response = await _repository.getSellerAboutInfo(businessID);
    _sellerAboutUsInfo.sink.add(response);
    return response;
  }

  BehaviorSubject<AboutUsResponse> get aboutUsInfo => _aboutUsInfo;
  BehaviorSubject<AboutUsResponse> get sellerAboutUsInfo => _sellerAboutUsInfo;
}

AboutUsBloc aboutUsBloc = AboutUsBloc();
