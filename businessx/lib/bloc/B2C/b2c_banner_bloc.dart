import 'package:b2b_market/models/response/response.dart';
import 'package:b2b_market/repositories/B2C/brand_repository.dart';
import 'package:rxdart/rxdart.dart';

class B2CBrandBloc {
  final BrandRepository _repository = BrandRepository();
  final _allBrandsResponse = BehaviorSubject<AllBrandsResponse>();
  final _brandResponse = BehaviorSubject<SingleBrandResponse>();

  Future<AllBrandsResponse> fetchAll() async {
    AllBrandsResponse response = await _repository.fetchAll();
    _allBrandsResponse.sink.add(response);
    return response;
  }

  Future<SingleBrandResponse> fetch(int brandID) async {
    SingleBrandResponse response = await _repository.fetch(brandID);
    _brandResponse.sink.add(response);
    return response;
  }

  BehaviorSubject<AllBrandsResponse> get allBrandsResponse =>
      _allBrandsResponse;
  BehaviorSubject<SingleBrandResponse> get brandResponse => _brandResponse;
}

final B2CBrandBloc b2cBrandBloc = B2CBrandBloc();
