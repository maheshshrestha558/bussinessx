import 'package:b2b_market/models/response/category_search_response.dart';
import 'package:b2b_market/models/response/product_search_response.dart';
import 'package:b2b_market/repositories/B2C/search_repository.dart';
// import 'package:b2b_market/repositories/search_repository.dart';
import 'package:rxdart/rxdart.dart';

class SearchB2CBloc {
  final SearchB2CRepository _repository = SearchB2CRepository();

  final _leafCategorySearchResponse =
      BehaviorSubject<LeafCategorySearchResponse?>();

  final _productSearchResponse = BehaviorSubject<ProductSearchResponse>();

  final _query = PublishSubject<String>();
  Sink<String> get querySink => _query.sink;

  SearchB2CBloc() {
    _query.debounceTime(const Duration(seconds: 1)).switchMap((query) async* {
      if (query.length > 2) {
       
        yield await _repository.getB2CLeafCategories(query);
      }
      if (query.length <= 2) {
        _leafCategorySearchResponse.value = null;
      }
    }).listen((response) async {
      _leafCategorySearchResponse.add(response);
    });
  }

  void searchProducts(String query) async {
    final response = await _repository.searchB2CProducts(query);
    _productSearchResponse.add(response);
  }

  BehaviorSubject<ProductSearchResponse> get productSearchResponse =>
      _productSearchResponse;

  BehaviorSubject<LeafCategorySearchResponse?> get leafCategorySearchResult =>
      _leafCategorySearchResponse;

  dispose() {
    _leafCategorySearchResponse.close();
    _query.close();
  }
}
