import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/models/response/add_to_cart_response.dart';
import 'package:b2b_market/models/response/product/popular_product_response.dart';
import 'package:b2b_market/repositories/B2C/cart_repository.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../../common/enums.dart';
import '../../common/logger/logger.dart';
import '../../common/type_defs.dart';
import '../../models/response/cart_response.dart';
import '../../models/response/total_price_response.dart';

class CartBloc {
  final _repository = CartRepository();
  final _addToCartResponse = BehaviorSubject<AddToCartResponse>();
  final _cartResponse = BehaviorSubject<CartResponse>();
  final _totalPriceResponse = BehaviorSubject<TotalPriceResponse>();

  final _logger = getLogger(CartBloc);

  /// User selected productIds to checkout
  List<int> productIds = [];

  Future<AddToCartResponse> addToCart(
    SingleProduct product,
    BuildContext context,
  ) async {
    AddToCartResponse response = await _repository.addToCart(product);
    if (response.error == null) {
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Added to cart')));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
    _addToCartResponse.sink.add(response);
    return response;
  }

  Future<AddToCartResponse> updateProductQuantity(
    int cartId,
    int productQuantity,
    ProductQuantityEvent event,
    BuildContext context,
  ) async {
    showCustomProgressDialog(context, 'updating');
    AddToCartResponse response =
        await _repository.updateProductQuantity(cartId, productQuantity, event);
    Navigator.pop(context);
    if (response.error != null) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
    return response;
  }

  Future<void> getCart() async {
    CartResponse response = await _repository.getCart();
    _logger.i(response.data.toString());

    _cartResponse.sink.add(response);
  }

  Future<void> getTotalPrice(ProductIds productIds) async {
    _logger.i(productIds);
    TotalPriceResponse response = await _repository.getTotalPrice(productIds);
    _logger.i(response.totalPrice);
    _totalPriceResponse.sink.add(response);
  }

  void dispose() {
    cartBloc.productIds.clear();
    _totalPriceResponse.valueOrNull?.totalPrice = 0;
  }

  BehaviorSubject<TotalPriceResponse> get totalPriceResponse =>
      _totalPriceResponse;

  BehaviorSubject<CartResponse> get cartResponse => _cartResponse;

  BehaviorSubject<AddToCartResponse> get addToCartResponse =>
      _addToCartResponse;
}

final cartBloc = CartBloc();
