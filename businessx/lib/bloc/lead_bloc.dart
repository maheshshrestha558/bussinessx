import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/params/create_lead_params.dart';
import '../models/response/lead/view_leads_response.dart';
import '../models/response/response.dart';
import '../repositories/repositories.dart';
import '../route_names.dart';

class LeadBloc {
  final LeadRepository _repository = LeadRepository();
  final _lead = BehaviorSubject<List<Lead>>();

  int? _lastPage;
  List<Lead> _leadList = [];

  void createLead(
    BuildContext context,
    CreateLeadParams params,
  ) async {
    showCustomProgressDialog(context, 'Requesting...');

    LeadResponse response = await _repository.createLead(params);

    Navigator.pop(context);
    if (response.error == null) {
      ShowToast.success('Lead created successfully');

      showDialog(
        context: context,
        builder: (_) => BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            color: Colors.white.withOpacity(0.5),
            margin: const EdgeInsets.fromLTRB(18, 20, 18, 18),
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Column(
              children: [
                const Spacer(),
                Center(
                  child: Text(
                    'Congratulations !',
                    style: GoogleFonts.rubik(
                      decoration: TextDecoration.none,
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: const Color(0xff1D4ED8),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text(
                    'Requirement Submitted Successfully',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text(
                    'Our team will get in touch with you soon.Please be patient. ',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    'Meanwhile browse our other products that might pick your interest.',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
                const SizedBox(
                  height: 65,
                ),
                SizedBox(
                  height: 50,
                  width: double.maxFinite,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          const Color(0xff1D4ED8),
                        ),
                        padding: MaterialStateProperty.all(
                          const EdgeInsets.symmetric(
                            vertical: 10,
                            horizontal: 10,
                          ),
                        ),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      onPressed: () => Navigator.pushNamed(
                        context,
                        RouteName.navigationScreen,
                      ),
                      child: Text(
                        'Browse Products',
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                const Spacer()
              ],
            ),
          ),
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (_) => BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Container(
            color: Colors.white.withOpacity(0.5),
            margin: const EdgeInsets.fromLTRB(18, 20, 18, 18),
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: Column(
              children: [
                const Spacer(),
                Center(
                  child: Text(
                    '! Error !',
                    style: GoogleFonts.rubik(
                      decoration: TextDecoration.none,
                      fontSize: 30,
                      fontWeight: FontWeight.normal,
                      color: const Color(0xffDC2626),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text(
                    'Unknown Error Occured',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Text(
                    'Your request could not be proceesed right now. Please try again in few minutes.We apologize for the inconvenience caused. ',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    'Meanwhile browse our other products that might pick your interest. ',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
                const SizedBox(
                  height: 47,
                ),
                CustomOutlinedButton(
                  onPressed: () => Navigator.pop(context),
                  title: 'Try Again',
                  color: const Color(0xffDC2626),
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 50,
                  width: double.maxFinite,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          const Color(0xff1D4ED8),
                        ),
                        padding: MaterialStateProperty.all(
                          const EdgeInsets.symmetric(
                            vertical: 10,
                            horizontal: 10,
                          ),
                        ),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                      onPressed: () => Navigator.pushNamed(
                        context,
                        RouteName.navigationScreen,
                      ),
                      child: Text(
                        'Browse Products',
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                const Spacer()
              ],
            ),
          ),
        ),
      );
      ShowToast.error(response.error);
    }
  }

  Future<ViewLeadsResponse> viewLead({int page = 1, int perPage = 10}) async {
    ViewLeadsResponse response = await _repository.viewLeads(page, perPage);
    _lastPage = response.leads?.meta?.lastPage;
    var leads = response.leads?.lead;
    if (leads != null) {
      _leadList.addAll(leads);
      _lead.sink.add(_leadList);
    }
    return response;
  }

  void drainLead() {
    _lead.value = [];
    _leadList = [];
  }

  int? get lastPage => _lastPage;
  BehaviorSubject<List<Lead>> get lead => _lead;
}

final leadBloc = LeadBloc();
