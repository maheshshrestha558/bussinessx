import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../models/packages.dart';
import '../repositories/package_repository.dart';

class PackageBloc {
  final _repository = PackageRepository();
  final _packagesResponse = BehaviorSubject<PackagesResponse>();
  final _packagesSubscriptionResponse =
      BehaviorSubject<PackageSubscriptionResponse>();

  void getPackages() async {
    final response = await _repository.getPackages();
    _packagesResponse.sink.add(response);
  }

  void postPackages(int id) async {
    final response = await _repository.postPackages(id);

    if (response.error == null) {
      ShowToast.success(response.message);
    } else {
      ShowToast.error(response.error);
    }
    _packagesSubscriptionResponse.sink.add(response);
  }

  BehaviorSubject<PackagesResponse> get packagesResponse => _packagesResponse;

  BehaviorSubject<PackageSubscriptionResponse> get packagesSubscribeResponse =>
      _packagesSubscriptionResponse;
}

final packageBloc = PackageBloc();
