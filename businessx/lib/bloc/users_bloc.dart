import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/response/response.dart';
import '../models/user.dart';
import '../repositories/users_repository.dart';

class UsersBloc {
  final UsersRepository _repository = UsersRepository();
  final _user = BehaviorSubject<UserResponse>();

  Future<void> getUser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var userID = pref.getInt('userID');
    if (userID != null) {
      var response = await _repository.getSingleUser(userID);
      _user.sink.add(response);
    }
  }

  void updateUser(
    BuildContext context,
    User user,
    File? image,
  ) async {
    FormData? data;

    if (image != null) {
      data = FormData.fromMap({
        'profile_image': await MultipartFile.fromFile(
          image.path,
          filename: image.path.split('/').last,
        )
      });
    }

    showCustomProgressDialog(context, 'Updating User');

    UserResponse response = await _repository.updateUser(user, data);

    Navigator.pop(context);

    if (response.error == null) {
      ShowToast.success('Updated Successfully');
      usersBloc.getUser();
      Navigator.pop(context);
    } else {
      ShowToast.error(response.error);
    }
  }

  BehaviorSubject<UserResponse> get user => _user;

  dispose() {
    if (_user.hasValue) {
      _user.value.user = null;
    }
  }
}

UsersBloc usersBloc = UsersBloc();
