import 'package:rxdart/rxdart.dart';

import '../models/response/category_search_response.dart';
import '../models/response/product_search_response.dart';
import '../repositories/search_repository.dart';

class SearchBloc {
  final SearchRepository _repository = SearchRepository();

  final _leafCategorySearchResponse =
      BehaviorSubject<LeafCategorySearchResponse?>();

  final _productSearchResponse = BehaviorSubject<ProductSearchResponse>();

  final _query = PublishSubject<String>();
  Sink<String> get querySink => _query.sink;

  SearchBloc() {
    _query.debounceTime(const Duration(seconds: 1)).switchMap((query) async* {
      if (query.length > 2) {
        yield await _repository.getLeafCategories(query);
      }
      if (query.length <= 2) {
        _leafCategorySearchResponse.value = null;
      }
    }).listen((response) async {
      _leafCategorySearchResponse.add(response);
    });
  }

  void searchProducts(String query) async {
    final response = await _repository.searchProducts(query);
    _productSearchResponse.add(response);
  }

  BehaviorSubject<ProductSearchResponse> get productSearchResponse =>
      _productSearchResponse;

  BehaviorSubject<LeafCategorySearchResponse?> get leafCategorySearchResult =>
      _leafCategorySearchResponse;

  dispose() {
    _leafCategorySearchResponse.close();
    _query.close();
  }
}
