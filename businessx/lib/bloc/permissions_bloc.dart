import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionsBloc {
  Future<bool> requestPermissions() async {
    var permissionStatus = await Permission.storage.status;

    if (permissionStatus.isDenied) {
      await Permission.storage.request();
      permissionStatus = await Permission.storage.status;
    }

    return permissionStatus == PermissionStatus.granted;
  }

  Future<Directory> getDownloadDirectory() async {
    return await getApplicationDocumentsDirectory();
  }
}

final permissionsBloc = PermissionsBloc();
