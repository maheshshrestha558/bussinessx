import '../models/models.dart';
import 'package:rxdart/rxdart.dart';

import '../repositories/repositories.dart';

class SpecificationsBloc {
  final SpecificationsRepository _repository = SpecificationsRepository();

  final _specifications = BehaviorSubject<List<Specification>>();

  void getSpecifications(int categoryId) async {
    final response = await _repository.getSpecifications(categoryId);
    if (response.error.isEmpty) {
      // _log.i(response.specifications);
      _specifications.sink.add(response.specifications);
    }

    if (response.error.isNotEmpty) {
      _specifications.sink.addError(response.error);
    }
  }

  void dispose() {
    _specifications.value = [];
    _specifications.drain();
  }

  BehaviorSubject<List<Specification>> get specifications => _specifications;
}

SpecificationsBloc specificationsBloc = SpecificationsBloc();
