import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rxdart/rxdart.dart';

import '../common/widgets/custom_toast.dart';
import '../common/widgets/widgets.dart';
import '../models/message_entity.dart';
import '../models/response/lead/view_leads_response.dart';
import '../models/response/message/all_message_response.dart';
import '../models/response/message/send_message_response.dart';
import '../repositories/repositories.dart';

class ChatBloc {
  final _repository = ChatRepository();
  final _messageEntities = BehaviorSubject<List<MessageEntity>>();

  late AllMessageResponse _allMessageResponse;
  final List<MessageEntity> _messageEntitiesList = [];

  int? get lastPage => _allMessageResponse.data?.meta.lastPage;


  Lead? _lead;
  String? _messageText;
  List<XFile>? _images;
  int? _userId;

  void setLead(Lead? value) {
    if (value != null) {
      _lead = value;
    }
  }

  void setUserId(int? value) {
    if (value != null) {
      _userId = value;
    }
  }

  void setMessage(String? value) {
    _messageText = value;
  }

  void setImages(List<XFile>? value) {
    if (value != null) {
      _images = value;
    }
  }

  get _getLead => _lead;
  get _getMessageText => _messageText;
  get _imageList => _images;
  get _getUserId => _userId;

  Map<String, dynamic> messageParams() {
    var message = {
      'new_message': _getMessageText,
      'chat_id': _getLead?.id,
      'seller_id': _getLead?.sellerId,
      'buyer_id': _getLead?.buyerId,
      'sender_id': _getUserId,
    };
    return message;
  }

  Future<AllMessageResponse> fetchMessages(int leadId, int page) async {
    _allMessageResponse = await _repository.fetchMessages(leadId, page);
    if (_allMessageResponse.error.isEmpty &&
        _allMessageResponse.data?.messageEntities != null) {
      var messageEntities = _allMessageResponse.data!.messageEntities;
      _messageEntitiesList.addAll(messageEntities);
      _messageEntities.sink.add(_messageEntitiesList);
    }
    if (_allMessageResponse.error.isNotEmpty) {
      _messageEntities.sink.addError(_allMessageResponse.error);
    }
    return _allMessageResponse;
  }

  Future<SendMessageResponse> sendMessage(context) async {
    if (messageParams()['new_message'] == null) {
      showCustomProgressDialog(context, 'Sending...');
    }
    FormData? productPhotos;
    if (_imageList != null && _imageList.isNotEmpty) {
      productPhotos = FormData();
      for (var image in _imageList) {
        if (image != null) {
          productPhotos.files.add(
            MapEntry(
              'files[${(_imageList.indexOf(image))}]',
              await MultipartFile.fromFile(
                image.path,
                filename: image.name,
              ),
            ),
          );
        }
      }
    }

    final response =
        await _repository.sendMessage(messageParams(), productPhotos);
    if (messageParams()['new_message'] == null) {
      Navigator.pop(context);
      if (response.error.isNotEmpty) {
        ShowToast.error(response.error);
      }
    }
    return response;
  }

  void addMessage(MessageEntity messageEntity) {
    _messageEntitiesList.add(messageEntity);
    _messageEntities.sink.add(_messageEntitiesList);
  }

  Future<void> downloadImage(String url, String savePath) async {
    await _repository.downloadImage(url, savePath);
  }

  void dispose() {
    _messageEntitiesList.clear();
  }

  ValueStream<List<MessageEntity>> get messages => _messageEntities.stream;
}

final chatBloc = ChatBloc();
