enum NotificationType {
  quotationRequest,
  productUpload,
  packageStatus,
  businessStatus,
  support
}

enum UserType {
  wholesaler('Wholesaler'),
  retailer('Retailer'),
  buyer('Buyer');

  final String type;

  const UserType(this.type);
}

enum ProductQuantityEvent { add, remove }
