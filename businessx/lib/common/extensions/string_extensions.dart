extension EllipsisExtension on String {
  String showEllipsis() {
    return length > 15 ? '${substring(0, 15)}...' : this;
  }
}

extension StringExtension on String? {
  String showDashIfNull() {
    return this ?? '- - - - -';
  }

  /// If not null returns only the first letter as capitalized else returns empty string
  String getCapitalizedFirstLetter() {
    return this != null ? this![0].toUpperCase() : '';
  }

  /// If not null returns the word with first letter capitalized else returns null
  String? capitalizeFirstLetter() {
    return (this != null && this!.isNotEmpty)
        ? '${this![0].toUpperCase()}${this!.substring(1).toLowerCase()}'
        : null;
  }

  String returnEmptyStringIfNull() {
    return this ?? '';
  }
}
