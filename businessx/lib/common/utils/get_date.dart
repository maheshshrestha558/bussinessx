import 'package:intl/intl.dart';

class Utils {
  static getDate(String dateTime) {
    var newString =
        '${dateTime.substring(0, 10)} ${dateTime.substring(11, 23)}';

    final dt = DateTime.parse(newString);
    return DateFormat('EEE, d MMM').format(dt);
  }
}
