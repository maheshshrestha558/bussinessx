import 'package:flutter/material.dart';

BoxDecoration appBarDecoration = const BoxDecoration(
  gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    tileMode: TileMode.clamp,
    colors: [Color(0xff7CD0FF), Color(0xff6AFD93)],
  ),
);
