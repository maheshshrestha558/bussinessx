import 'package:b2b_market/repositories/base_repository.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../bloc/bloc.dart';
import '../../models/params/create_lead_params.dart';
import '../../models/response/response.dart';
import '../../models/user.dart';
import 'widgets.dart';

class SubmitRequirementFormBuilder extends StatefulWidget {
  const SubmitRequirementFormBuilder({Key? key}) : super(key: key);

  @override
  State<SubmitRequirementFormBuilder> createState() =>
      _SubmitRequirementFormBuilderState();
}

class _SubmitRequirementFormBuilderState
    extends State<SubmitRequirementFormBuilder> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserResponse>(
      stream: usersBloc.user.stream,
      builder: (context, snapshot) {
        var user = snapshot.data?.user;
        return SubmitRequirementForm(
          key: UniqueKey(),
          user: user,
        );
      },
    );
  }
}

class SubmitRequirementForm extends StatefulWidget {
  const SubmitRequirementForm({
    Key? key,
    this.user,
  }) : super(key: key);

  final User? user;

  @override
  State<SubmitRequirementForm> createState() => SubmitRequirementFormState();
}

class SubmitRequirementFormState extends State<SubmitRequirementForm> {
  late bool _termsAgreed;

  final _nameController = TextEditingController();
  final _contactController = TextEditingController();
  final _productNameController = TextEditingController();
  final _descriptionController = TextEditingController();

  final requirementFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _termsAgreed = false;
    _nameController.text = widget.user?.name ?? '';
    _contactController.text = widget.user?.contactNumber ?? '';
  }

  @override
  void dispose() {
    _nameController.dispose();
    _contactController.dispose();
    _productNameController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 16),
      decoration: const BoxDecoration(
        color: Color.fromARGB(225, 124, 207, 255),
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Form(
        key: requirementFormKey,
        // autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            CustomTextFormField(
              controller: _nameController,
              decoration: _inputDecoration('Your Full Name'),
              // height: 0,
              enabled: !baseRepository.isUserLoggedIn,
              validator: RequiredValidator(errorText: 'Name is required'),
            ),
            CustomTextFormField(
              controller: _contactController,
              decoration: _inputDecoration('Your Contact Number'),
              textInputType: TextInputType.phone,
              height: 0,
              enabled: !baseRepository.isUserLoggedIn,
              validator: MultiValidator(
                [
                  RequiredValidator(errorText: 'Contact Number is required'),
                  MinLengthValidator(
                    10,
                    errorText: 'Contact must be 10 digits',
                  ),
                  MaxLengthValidator(
                    10,
                    errorText: 'Contact must be 10 digits',
                  ),
                ],
              ),
            ),
            CustomTextFormField(
              controller: _productNameController,
              decoration: _inputDecoration('Product Name'),
              height: 0,
              validator:
                  RequiredValidator(errorText: 'Product name is required'),
            ),
            CustomTextFormField(
              controller: _descriptionController,
              decoration: _inputDecoration('Product Description'),
              maxLines: 8,
              height: 0,
              validator: MultiValidator([
                RequiredValidator(errorText: 'Description is required'),
                MinLengthValidator(
                  20,
                  errorText: 'Description must be at least 20 characters',
                ),
              ]),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25.0),
              child: StatefulBuilder(
                builder: (context, setState) => CheckboxListTile(
                  checkColor: Colors.green,
                  controlAffinity: ListTileControlAffinity.leading,
                  title: const Text(
                    'I agree to terms and conditions',
                    style: TextStyle(
                      fontSize: 14,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  selected: _termsAgreed,
                  value: _termsAgreed,
                  onChanged: (value) {
                    _termsAgreed = value!;
                    setState(() {});
                  },
                ),
              ),
            ),
            SizedBox(
              height: 50,
              width: 330,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    const Color.fromARGB(255, 249, 250, 252),
                  ),
                  padding: MaterialStateProperty.all(
                    const EdgeInsets.symmetric(vertical: 5, horizontal: 25),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                onPressed: () {
                  if (requirementFormKey.currentState!.validate()) {
                    var params = CreateLeadParams(
                      name: _nameController.text,
                      contact: _contactController.text,
                      productName: _productNameController.text,
                      productId: null,
                      description: _descriptionController.text,
                      agree: _termsAgreed,
                    );

                    leadBloc.createLead(context, params);
                  }
                },
                child: const Text(
                  'Submit Requirement',
                  style: TextStyle(
                    color: Color.fromARGB(225, 124, 207, 255),
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

_inputDecoration(String hintText) => InputDecoration(
      hintText: hintText,
      hintStyle: const TextStyle(
        color: Color(0xFFC4C4C4),
        fontSize: 14,
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
        ),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
        ),
      ),
      border: const OutlineInputBorder(),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
        ),
      ),
      focusedErrorBorder: const OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
        ),
      ),
      disabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
        ),
      ),
      fillColor: Colors.white,
      filled: true,
    );
