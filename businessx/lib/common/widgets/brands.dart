import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../bloc/brand_bloc.dart';
import '../../models/models.dart';
import '../../models/response/brand/all_brands_response.dart';
import '../../route_names.dart';
import '../../screens/chat/components/components.dart';
import 'widgets.dart';

class BrandCard extends StatelessWidget {
  final Brand brand;

  const BrandCard({
    Key? key,
    required this.brand,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await brandBloc.fetch(brand.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.brandProductScreen,
            arguments: brand.id,
          );
        }
        if (response.error != null) {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Column(
        children: [
          SizedBox(
            height: 60,
            width: 60,
            child: CircleAvatar(
              backgroundColor: Colors.white,
              maxRadius: 30,
              child: MyCachedNetworkImage(
                filePath: brand.imageThumbnail,
              ),
            ),
          ),
          const SizedBox(height: 3),
          SizedBox(
            width: 60,
            child: Text(
              brand.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
        ],
      ),
    );
  }
}

class Brands extends StatelessWidget {
  const Brands({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, top: 10, right: 16, bottom: 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Brands',
                style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Rubik',
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold,
                ),
              ),
              IconButton(
                icon: const Icon(
                  Icons.arrow_forward,
                  color: Color.fromARGB(244, 29, 79, 216),
                ),
                onPressed: () => Navigator.pushReplacementNamed(
                  context,
                  RouteName.navigationScreen,
                  arguments: 1,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 90,
            child: StreamBuilder<AllBrandsResponse>(
              stream: brandBloc.allBrandsResponse.stream,
              builder: (context, snapshot) {
                var brands = snapshot.data?.brands;
                if (brands != null && brands.isNotEmpty) {
                  brands.sort((a, b) => a.name.compareTo(b.name.toUpperCase()));
                  return ListView.separated(
                    separatorBuilder: (context, index) => const SizedBox(
                      width: 10,
                    ),
                    scrollDirection: Axis.horizontal,
                    itemCount: brands.length,
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      var brand = brands[index];
                      return BrandCard(brand: brand);
                    },
                  );
                }
                return const BrandShimmer();
              },
            ),
          ),
        ],
      ),
    );
  }
}
