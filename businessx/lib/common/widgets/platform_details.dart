import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../bloc/bloc.dart';
import '../../models/response/response.dart';

class PlatformDetails extends StatelessWidget {
  const PlatformDetails({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlatformDetailsResponse>(
      stream: platformDetailsBloc.platformDetails.stream,
      builder: (context, snapshot) {
        var platformDetails = snapshot.data?.platformDetails;
        return Container(
          decoration: const BoxDecoration(color: Color(0xff68E7CA)),
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 20),
          child: Column(
            children: [
              const SizedBox(
                height: 15,
              ),
              Text(
                platformDetails?.enName ?? 'Business X',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                platformDetails?.address ??
                    'Block 24, Trade Street, Kathmandu, Province 3, Nepal',
                style: Theme.of(context).textTheme.displaySmall,
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    platformDetails?.email ?? 'www.businessx.com.np',
                    style: GoogleFonts.rubik(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: const Color(0xff0077b6),
                    ),
                  ),
                  Text(
                    platformDetails?.contact ?? '+977-9800100110',
                    style: GoogleFonts.rubik(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: const Color(0xff0077b6),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'Designed & Developed by EEE Innovation Ghar Pvt. Ltd',
                style: GoogleFonts.rubik(
                  fontSize: 12,
                  // fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
              const SizedBox(
                height: 25,
              ),
            ],
          ),
        );
      },
    );
  }
}
