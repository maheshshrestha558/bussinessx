import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class CircularGridCategoryShimmer extends StatelessWidget {
  const CircularGridCategoryShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: GridView.builder(
        primary: false,
        itemCount: 4,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          mainAxisExtent: 115,
        ),
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) => const Column(
          children: [
            SizedBox(height: 4),
            ShimmerWidget.circular(
              height: 60,
              width: 60,
            ),
            SizedBox(height: 8),
            ShimmerWidget.rectangular(
              height: 14,
              width: 51,
            )
          ],
        ),
      ),
    );
  }
}
