import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class MessageShimmer extends StatelessWidget {
  const MessageShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: 5,
      scrollDirection: Axis.vertical,
      separatorBuilder: (BuildContext context, int index) => const Divider(),
      itemBuilder: (context, index) => const Padding(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: ListTile(
          leading: ShimmerWidget.circular(width: 50, height: 50),
          title: Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: ShimmerWidget.rectangular(
                height: 20,
                width: 100,
              ),
            ),
          ),
          subtitle: Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 20,
              width: 200,
            ),
          ),
        ),
      ),
    );
  }
}
