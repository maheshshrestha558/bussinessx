import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class ProductAndLeadStatsShimmer extends StatelessWidget {
  const ProductAndLeadStatsShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Column(
          children: [
            Align(
              alignment: Alignment.center,
              child: ShimmerWidget.rectangular(
                height: 36,
                width: 39,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Align(
              alignment: Alignment.center,
              child: ShimmerWidget.rectangular(
                height: 17,
                width: 60,
              ),
            ),
          ],
        ),
        Column(
          children: [
            Align(
              alignment: Alignment.center,
              child: ShimmerWidget.rectangular(
                height: 36,
                width: 39,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Align(
              alignment: Alignment.center,
              child: ShimmerWidget.rectangular(
                height: 17,
                width: 60,
              ),
            ),
          ],
        ),
        Column(
          children: [
            Align(
              alignment: Alignment.center,
              child: ShimmerWidget.rectangular(
                height: 36,
                width: 39,
              ),
            ),
            SizedBox(
              height: 4,
            ),
            Align(
              alignment: Alignment.center,
              child: ShimmerWidget.rectangular(
                height: 17,
                width: 60,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
