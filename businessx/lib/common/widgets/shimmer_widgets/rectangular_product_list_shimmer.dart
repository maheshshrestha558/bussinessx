import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class RectangularProductListShimmer extends StatelessWidget {
  const RectangularProductListShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(width: 10),
      padding: const EdgeInsets.all(10),
      itemCount: 10,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) => const Column(
        children: [
          SizedBox(height: 4),
          ShimmerWidget.rectangular(
            height: 100,
            width: 100,
          ),
          SizedBox(height: 8),
          ShimmerWidget.rectangular(
            height: 10,
            width: 51,
          ),
          SizedBox(height: 8),
          ShimmerWidget.rectangular(
            height: 10,
            width: 40,
          )
        ],
      ),
    );
  }
}
