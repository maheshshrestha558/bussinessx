import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class RectangularProductShimmer extends StatelessWidget {
  const RectangularProductShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: 6,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        mainAxisExtent: (MediaQuery.of(context).size.width / 2.2),
        crossAxisCount: 2,
      ),
      scrollDirection: Axis.horizontal,
      itemBuilder: (_, __) => const Padding(
        padding: EdgeInsets.all(3.0),
        child: ShimmerWidget.rectangular(
          height: 175,
        ),
      ),
    );
  }
}
