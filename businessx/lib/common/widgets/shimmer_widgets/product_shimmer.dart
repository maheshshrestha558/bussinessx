import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class ProductShimmer extends StatelessWidget {
  const ProductShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: 4,
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index) => const Card(
        elevation: 0,
        child: SizedBox(
          height: 150,
          width: double.maxFinite,
          child: Row(
            children: [
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: ShimmerWidget.rectangular(
                    height: 120,
                    width: 120,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 15.0,
                    top: 15.0,
                    bottom: 15.0,
                    right: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ShimmerWidget.rectangular(
                          height: 20,
                          width: 200,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ShimmerWidget.rectangular(
                          height: 15,
                          width: 150,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ShimmerWidget.rectangular(
                          height: 12,
                          width: 100,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ShimmerWidget.rectangular(
                          height: 14,
                          width: 200,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
