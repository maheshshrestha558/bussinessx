import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class CircularCategoryShimmer extends StatelessWidget {
  const CircularCategoryShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(width: 25),
      padding: const EdgeInsets.all(10),
      itemCount: 10,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) => const Column(
        children: [
          SizedBox(height: 4),
          ShimmerWidget.circular(
            height: 60,
            width: 60,
          ),
          SizedBox(height: 8),
          ShimmerWidget.rectangular(
            height: 14,
            width: 60,
          )
        ],
      ),
    );
  }
}

class BrandShimmer extends StatelessWidget {
  const BrandShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      separatorBuilder: (context, index) => const SizedBox(width: 10),
      padding: const EdgeInsets.all(10),
      itemCount: 3,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) => const ShimmerWidget.rectangular(
        height: 120,
        width: 120,
      ),
    );
  }
}
