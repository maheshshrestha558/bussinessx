import 'package:flutter/material.dart';

import '../../shimmer_widget.dart';

class NoAvatarProfileShimmer extends StatelessWidget {
  const NoAvatarProfileShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 28.0, right: 28.0),
      child: ListView(
        children: const [
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 17,
              width: 90,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.center,
            child: ShimmerWidget.rectangular(
              height: 45,
              width: 320,
            ),
          ),
          SizedBox(height: 100),
        ],
      ),
    );
  }
}
