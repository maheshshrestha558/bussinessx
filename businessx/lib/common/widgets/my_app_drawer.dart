import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../bloc/auth_bloc.dart';
import '../../models/response/data_collection/data_collection_response.dart';
import '../../route_names.dart';
import 'widgets.dart';

class MyAppDrawer extends StatefulWidget {
  const MyAppDrawer({
    Key? key,
  }) : super(key: key);

  @override
  State<MyAppDrawer> createState() => _MyAppDrawerState();
}

class _MyAppDrawerState extends State<MyAppDrawer> {
  bool isDataCollector = false;

  @override
  void initState() {
    _checkDataCollector();
    super.initState();
  }

  void _checkDataCollector() async {
    isDataCollector = await authBloc.isDataCollector();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        decoration: const BoxDecoration(
          color: Color.fromARGB(207, 188, 236, 202),

          // gradient: LinearGradient(
          //   begin: Alignment.topCenter,
          //   end: Alignment.bottomCenter,
          //   tileMode: TileMode.clamp,
          //   colors: [
          //     // Colors.white,
          //     Color(0x886AFD93),
          //     Color(0xfffafafa),
          //   ],
          // ),
        ),
        child: Padding(
          padding: EdgeInsets.zero,
          child: Column(
            children: [
              const CustomDrawerUserName(),
              ListTile(
                dense: true,
                leading: const Icon(
                  Icons.home_outlined,
                  color: Colors.black87,
                ),
                title: Transform.translate(
                  offset: const Offset(-16, 0),
                  child: Text(
                    'Home',
                    style: GoogleFonts.rubik(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
                trailing: const Icon(
                  Icons.arrow_forward_ios,
                  size: 12,
                ),
                onTap: () => Navigator.popAndPushNamed(
                  context,
                  RouteName.navigationScreen,
                ),
              ),
              const Divider(
                indent: 40,
                endIndent: 40,
              ),
              DrawerListTile(
                imgName: 'sellerprofile',
                title: 'Business Profile',
                onPress: () {
                  Navigator.of(context)
                    ..pushNamedAndRemoveUntil(
                      RouteName.navigationScreen,
                      (route) => false,
                    )
                    ..pushNamed(RouteName.checkConditionPage);
                },
              ),
              const Divider(
                indent: 40,
                endIndent: 40,
              ),
              DrawerListTile(
                imgName: 'favorite',
                title: 'Favorites',
                onPress: () {
                  Navigator.of(context)
                    ..pushNamedAndRemoveUntil(
                      RouteName.navigationScreen,
                      (route) => false,
                    )
                    ..pushNamed(RouteName.checkFavouriteScreen);
                },
              ),
              const Visibility(
                child: Divider(
                  indent: 40,
                  endIndent: 40,
                ),
              ),
              Visibility(
                visible: isDataCollector,
                child: Column(
                  children: [
                    DrawerListTile(
                      imgName: 'favorite',
                      title: 'Data Collection',
                      onPress: () {
                        dataCollectionAnswer = DataCollectionAnswer();
                        Navigator.of(context)
                          ..pushNamedAndRemoveUntil(
                            RouteName.navigationScreen,
                            (route) => false,
                          )
                          ..pushNamed(RouteName.mainDataCollectionPage);
                      },
                    ),
                    const Divider(
                      indent: 40,
                      endIndent: 40,
                    ),
                  ],
                ),
              ),
              DrawerListTile(
                imgName: 'support',
                title: 'Help & Support',
                onPress: () {
                  Navigator.of(context)
                    ..pushNamedAndRemoveUntil(
                      RouteName.navigationScreen,
                      (route) => false,
                    )
                    ..pushNamed(RouteName.helpAndSupportScreen);
                },
              ),
              const Divider(
                indent: 40,
                endIndent: 40,
              ),
              DrawerListTile(
                imgName: 'terms',
                title: 'Terms & Conditions',
                onPress: () {
                  Navigator.of(context)
                    ..pushNamedAndRemoveUntil(
                      RouteName.navigationScreen,
                      (route) => false,
                    )
                    ..pushNamed(RouteName.termsAndConditionScreen);
                },
              ),
              const Divider(
                indent: 40,
                endIndent: 40,
              ),
              DrawerListTile(
                imgName: 'about',
                title: 'About Us',
                onPress: () {
                  Navigator.of(context)
                    ..pushNamedAndRemoveUntil(
                      RouteName.navigationScreen,
                      (route) => false,
                    )
                    ..pushNamed(RouteName.aboutUsScreen);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
