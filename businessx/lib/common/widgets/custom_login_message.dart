import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../screens/theme.dart';

class CustomLoginMessage extends StatelessWidget {
  const CustomLoginMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RichText(
        text: TextSpan(
          text: 'Already have an account? ',
          style: B2BTheme.textTheme.headlineMedium,
          children: <TextSpan>[
            TextSpan(
              text: 'Sign In',
              style: GoogleFonts.rubik(
                color: const Color(0xff0077b6),
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Navigator.pop(
                    context,
                  );
                },
            )
          ],
        ),
      ),
    );
  }
}
