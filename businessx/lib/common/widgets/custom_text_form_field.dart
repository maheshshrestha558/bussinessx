import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'custom_label_text.dart';

class CustomTextFormField extends StatelessWidget {
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final TextEditingController? controller;
  final String? labelText;
  final String? hintText;
  final void Function()? onTap;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final int? maxLines;
  final String? Function(String?)? validator;
  final void Function(String)? onFieldSubmitted;
  final void Function(String)? onChanged;
  final bool obscureText;
  final double? textFieldHeight;
  final double? height;
  final bool? enabled;
  final String? initialValue;
  final int? maxLenght;
  final InputDecoration? decoration;
  final bool readOnly;
  final List<TextInputFormatter>? textInputFormatterList;

  const CustomTextFormField({
    Key? key,
    this.textInputAction = TextInputAction.next,
    this.controller,
    this.validator,
    this.suffixIcon,
    this.labelText,
    this.onFieldSubmitted,
    this.obscureText = false,
    this.hintText,
    this.prefixIcon,
    this.enabled = true,
    this.height = 5,
    this.initialValue,
    this.maxLines = 1,
    this.textFieldHeight = 55,
    this.textInputType = TextInputType.text,
    this.onTap,
    this.onChanged,
    this.maxLenght,
    this.decoration,
    this.readOnly = false,
    this.textInputFormatterList = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomLabelText(labelText: labelText ?? ''),
          SizedBox(
            height: height,
          ),
          TextFormField(
            maxLength: maxLenght,
            onChanged: onChanged,
            decoration: decoration ??
                InputDecoration(
                  errorMaxLines: 3,
                  contentPadding: const EdgeInsets.only(
                    left: 10.0,
                    right: 5.0,
                    top: 10,
                    bottom: 10,
                  ),
                  filled: true,
                  suffixIcon: suffixIcon,
                  prefixIcon: prefixIcon,
                  fillColor: const Color(0xffffffff),
                  hintText: hintText,
                  hintStyle: const TextStyle(
                    color: Color(0xFFC4C4C4),
                    fontSize: 14,
                  ),
                  border: border,
                  focusedBorder: focusedBorder,
                  enabledBorder: enabledBorder,
                ),
            textInputAction: textInputAction,
            keyboardType: textInputType,
            enabled: enabled,
            readOnly: readOnly,
            maxLines: maxLines,
            initialValue: initialValue,
            obscureText: obscureText,
            textAlign: TextAlign.left,
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.black),
            onTap: onTap,
            controller: controller,
            validator: validator,
            onFieldSubmitted: onFieldSubmitted,
            inputFormatters: textInputFormatterList,
          ),
          SizedBox(
            height: height,
          ),
        ],
      ),
    );
  }
}

const OutlineInputBorder border = OutlineInputBorder(
  borderRadius: BorderRadius.all(
    Radius.circular(4),
  ),
  borderSide: BorderSide(
    color: Color.fromRGBO(218, 216, 216, 1),
    width: 1,
  ),
);

const OutlineInputBorder focusedBorder = OutlineInputBorder(
  borderRadius: BorderRadius.all(Radius.circular(5)),
  borderSide: BorderSide(
    color: Color(0xFFDAD8D8),
    width: 1,
  ),
);

const OutlineInputBorder enabledBorder = OutlineInputBorder(
  borderRadius: BorderRadius.all(Radius.circular(5)),
  borderSide: BorderSide(
    // style: BorderStyle.solid,
    color: Color(0xFFDAD8D8),
    width: 1,
  ),
);
