import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '/common/extensions/string_extensions.dart';
import 'widgets.dart';

class DescriptionField extends StatelessWidget {
  const DescriptionField({
    Key? key,
    required this.labelText,
    required this.description,
  }) : super(key: key);

  final String labelText;
  final String? description;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: CustomLabelText(
            labelText: labelText,
            textStyle: GoogleFonts.ubuntu(
              fontSize: 16,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 12.0),
          child: CustomLabelText(
            labelText: description.showDashIfNull(),
            textStyle: GoogleFonts.ubuntu(
              fontSize: 18,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        const SizedBox(
          height: 3,
        ),
        Divider(
          indent: 8,
          endIndent: 8,
          color: Colors.grey.shade400,
          height: 1,
          thickness: 1,
        ),
        const SizedBox(
          height: 12,
        ),
      ],
    );
  }
}
