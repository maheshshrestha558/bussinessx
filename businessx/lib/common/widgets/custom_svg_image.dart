import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

abstract class SvgImagePath {
  SvgImagePath._();
  static const String welcome = 'assets/images/welcome.svg';
  static const String login = 'assets/images/login.svg';
  static const String singup = 'assets/images/signup.svg';
  static const String passwordReset = 'assets/images/reset_password.svg';
}

class CustomSvgImage extends StatelessWidget {
  final String svgImagePath;
  const CustomSvgImage({Key? key, required this.svgImagePath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      svgImagePath,
      height: 150,
      width: 150,
    );
  }
}
