import 'package:flutter/material.dart';

class CustomFlatButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback onPressed;
  final Color? fillColor, textColor;
  const CustomFlatButton({
    Key? key,
    required this.buttonText,
    required this.onPressed,
    this.fillColor,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 20,
      height: 48,
      child: RawMaterialButton(
        onPressed: onPressed,
        shape: const StadiumBorder(),
        fillColor: fillColor ?? Colors.white,
        child: Text(
          buttonText,
          style: TextStyle(
            color: textColor ?? Colors.green,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
