import 'package:flutter/material.dart';

class ProductThumbnail extends StatelessWidget {
  final String productName;
  const ProductThumbnail({
    Key? key,
    required this.productName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Expanded(
            child: ClipRRect(
              child: Placeholder(),
            ),
          ),
          const SizedBox(height: 10),
          Text(
            productName,
            maxLines: 1,
            style: Theme.of(context).textTheme.bodySmall,
          ),
        ],
      ),
    );
  }
}
