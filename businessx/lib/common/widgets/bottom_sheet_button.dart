import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BottomSheetButton extends StatelessWidget {
  final Color backgroundColor;
  final void Function()? onPressed;
  final String buttonLabel;
  const BottomSheetButton({
    Key? key,
    required this.backgroundColor,
    required this.onPressed,
    required this.buttonLabel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 150,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            backgroundColor,
          ),
          padding: MaterialStateProperty.all(
            const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 10,
            ),
          ),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
        onPressed: onPressed,
        child: Text(
          buttonLabel,
          style: GoogleFonts.rubik(
            color:
                backgroundColor == Colors.white ? Colors.black : Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}