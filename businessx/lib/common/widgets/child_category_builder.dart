import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../bloc/bloc.dart';
import '../../models/models.dart';
import '../../models/response/category/child_category_respose.dart';
import '../../route_names.dart';
import '../../screens/chat/components/components.dart';
import 'widgets.dart';

class ChildCategoryBuilder extends StatelessWidget {
  final String title;

  const ChildCategoryBuilder({
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        PageTitle(title: title),
        Expanded(
          child: StreamBuilder<ChildCategoryResponse>(
            stream: categoryBloc.childCategories.stream,
            builder: (context, snapshot) {
              var childCategories =
                  snapshot.data?.childCategoryData?.childCategories;
              if (childCategories != null) {
                childCategories.sort((a, b) => a.name.compareTo(b.name));
                return GridView.builder(
                  itemCount: childCategories.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    mainAxisExtent: 115,
                  ),
                  physics: const BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    var childCategory = childCategories[index];
                    return ChildCategoryCard(
                      childCategory: childCategory,
                    );
                  },
                );
              }
              return const CircularGridCategoryShimmer();
            },
          ),
        ),
      ],
    );
  }
}

class ChildCategoryCard extends StatelessWidget {
  final ChildCategory childCategory;

  const ChildCategoryCard({Key? key, required this.childCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getLeafCategories(childCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.leafCategoryScreen,
            arguments: childCategory.name,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Column(
        children: [
          Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45),
            ),
            shadowColor: const Color.fromRGBO(0, 0, 0, 0.25),
            child: SizedBox(
              height: 60,
              width: 60,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ClipOval(
                  child: MyCachedNetworkImage(
                    filePath: childCategory.imageThumbnail,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 5),
          SizedBox(
            width: 70,
            child: Text(
              childCategory.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: const TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
