import 'package:flutter/material.dart';

import '../../route_names.dart';
import '../decoration.dart';
import 'widgets.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
    this.showBellIcon = true,
  }) : super(key: key);
  final bool showBellIcon;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      flexibleSpace: Container(
        decoration: appBarDecoration,
      ),
      title: const CustomSearchBox(),
      titleSpacing: 0.2,
      actions: <Widget>[
        Visibility(
          visible: showBellIcon,
          child: IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {
              Navigator.pushNamed(context, RouteName.notificationScreen);
            },
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
