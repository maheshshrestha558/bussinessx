// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../route_names.dart';

class BecomeSellerMessage extends StatelessWidget {
  const BecomeSellerMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            'Become A Seller Today ',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Center(
          child: Text(
            '3 Easy Steps',
            style: Theme.of(context).textTheme.headlineSmall,
          ),
        ),
        const SizedBox(
          height: 25,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                SvgPicture.asset('assets/images/setup.svg'),
                const SizedBox(
                  height: 12,
                ),
                Text(
                  'Setup',
                  style: GoogleFonts.rubik(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: const Color(0xff1D4ED8),
                  ),
                )
              ],
            ),
            Column(
              children: [
                SvgPicture.asset('assets/images/cart.svg'),
                const SizedBox(
                  height: 12,
                ),
                Text(
                  'Sell',
                  style: GoogleFonts.rubik(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: const Color(0xff29DE92),
                  ),
                )
              ],
            ),
            Column(
              children: [
                SvgPicture.asset(
                  'assets/images/wallet.svg',
                  color: const Color(0xff7CD0FF),
                ),
                const SizedBox(
                  height: 12,
                ),
                Text(
                  'Earn',
                  style: GoogleFonts.rubik(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: const Color(0xff7CD0FF),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 25,
        ),
        SizedBox(
          height: 50,
          width: double.maxFinite,
          child: Padding(
            padding: const EdgeInsets.only(left: 28, right: 28),
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(const Color(0xff29DE92)),
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  RouteName.checkConditionPage,
                );
              },
              child: const Text('Become A Seller'),
            ),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
