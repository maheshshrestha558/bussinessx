import 'package:flutter/material.dart';

class PageTitle extends StatelessWidget {
  final String title;
  final bool showBackButton;
  final bool showTopNavButton;
  final Function? onBackPressed;
  final Function()? ontopNavButtonPressed;
  final String navbuttontext;
  const PageTitle({
    Key? key,
    required this.title,
    this.showBackButton = true,
    this.onBackPressed,
    this.showTopNavButton = false,
    this.ontopNavButtonPressed,
    this.navbuttontext = 'Button',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // padding: const EdgeInsets.fromLTRB(15.0, 10.0, 0.0, 15.0),
      width: double.maxFinite,
      child: Row(
        children: [
          Visibility(
            visible: showBackButton,
            child: Positioned(
              left: 0,
              child: IconButton(
                onPressed: () {
                  if (onBackPressed != null) {
                    onBackPressed!();
                  }
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back_ios),
              ),
            ),
          ),
          Text(
            title,
            style: Theme.of(context).textTheme.headlineLarge,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
      //  Stack(
      //   // alignment: Alignment.center,
      //   children: [
      //     Visibility(
      //       visible: showBackButton,
      //       child: Positioned(
      //         left: 0,
      //         child: IconButton(
      //           onPressed: () {
      //             if (onBackPressed != null) {
      //               onBackPressed!();
      //             }
      //             Navigator.pop(context);
      //           },
      //           icon: const Icon(Icons.arrow_back_ios),
      //         ),
      //       ),
      //     ),
      //     SizedBox(

      //       width: MediaQuery.of(context).size.width - 100,
      //       child: FittedBox(

      //         alignment: Alignment.center,
      //         fit: BoxFit.scaleDown,
      //         child: Padding(
      //           padding: const EdgeInsets.all(5.0),
      //           child: Padding(
      //             padding: showTopNavButton
      //                 ? const EdgeInsets.only(right: 30, left: 0)
      //                 : const EdgeInsets.only(right: 10, left: 0),
      //             child: Text(
      //               title,
      //               style: Theme.of(context).textTheme.headlineLarge,
      //               maxLines: 1,
      //               overflow: TextOverflow.ellipsis,
      //             ),
      //           ),
      //         ),
      //       ),
      //     ),
      //     Visibility(
      //       visible: showTopNavButton,
      //       child: Positioned(
      //         right: 5,
      //         child: TextButton(
      //           style: const ButtonStyle(
      //             splashFactory: NoSplash.splashFactory,
      //           ),
      //           onPressed: ontopNavButtonPressed,
      //           child: DecoratedBox(

      //             decoration: BoxDecoration(
      //               border: Border.all(
      //                 width: 1,
      //                 color: Colors.grey,
      //               ),
      //               borderRadius: BorderRadius.circular(10),
      //               color: Colors.lightGreen,
      //             ),
      //             child: Padding(
      //               padding: const EdgeInsets.symmetric(
      //                 horizontal: 10,
      //                 vertical: 5,
      //               ),
      //               child: Text(
      //                 navbuttontext,
      //                 style: const TextStyle(color: Colors.white),
      //               ),
      //             ),
      //           ),
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}

class PageTitleForAuth extends StatelessWidget {
  final String title;
  const PageTitleForAuth({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.maxFinite,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            left: 10,
            child: IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(Icons.arrow_back_ios),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width - 100,
            child: FittedBox(
              alignment: Alignment.center,
              fit: BoxFit.scaleDown,
              child: Text(
                title,
                style: Theme.of(context).textTheme.displayMedium,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
