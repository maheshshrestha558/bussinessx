import 'package:flutter/material.dart';

import '../../route_names.dart';
import '../../screens/screens.dart';

class SubmitRequirementButtons extends StatelessWidget {
  const SubmitRequirementButtons({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final ProductDetailScreen widget;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SizedBox(
            height: 40,
            width: double.maxFinite,
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 5),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(const Color(0xff29DE92)),
                  padding: MaterialStateProperty.all(
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(
                    context,
                    RouteName.submitRequirementScreen,
                    arguments: widget.product,

                    // SlideRightRoute(
                    //   page: SubmitRequirementScreen(
                    //     product: widget.product,
                    //   ),
                    // ),
                  );
                },
                child: const Text(
                  'Contact Seller',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            height: 40,
            width: double.maxFinite,
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(const Color(0xff1D4ED8)),
                  padding: MaterialStateProperty.all(
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(
                    context,
                    RouteName.submitRequirementScreen,
                    arguments: widget.product,
                  );
                },
                child: const Text(
                  'Email Seller',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
