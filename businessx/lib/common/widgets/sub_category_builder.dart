import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../bloc/category_bloc.dart';
import '../../models/response/category/sub_categories_response.dart';
import '../../route_names.dart';
import '../../screens/chat/components/my_cached_network_image.dart';
import 'widgets.dart';

class SubCategoryBuilder extends StatelessWidget {
  const SubCategoryBuilder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: StreamBuilder<SubCategoriesResponse>(
        stream: categoryBloc.subCategories.stream,
        builder: (context, snapshot) {
          var subCategories = snapshot.data?.data?.subCategories;
          if (subCategories != null && subCategories.isNotEmpty) {
            subCategories.sort((a, b) => a.name.compareTo(b.name));
            return GridView.builder(
              itemCount: subCategories.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisExtent: 115,
              ),
              physics: const BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                var subCategory = subCategories[index];
                return SubCategoryCard(subCategory: subCategory);
              },
            );
          }
          return const CircularGridCategoryShimmer();
        },
      ),
    );
  }
}

class SubCategoryCard extends StatelessWidget {
  final SubCategory subCategory;

  const SubCategoryCard({Key? key, required this.subCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getChildCategories(subCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.childCategoryScreen,
            arguments: subCategory.name,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Column(
        children: [
          Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45),
            ),
            shadowColor: const Color.fromRGBO(0, 0, 0, 0.25),
            child: SizedBox(
              height: 60,
              width: 60,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ClipOval(
                  child: MyCachedNetworkImage(
                    filePath: subCategory.imageThumbnail,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 5),
          SizedBox(
            width: 70,
            child: Text(
              subCategory.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: const TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
          //     Column(
          //   children: [
          //     SizedBox(
          //       height: 50,
          //       width: 50,
          //       child: CircleAvatar(
          //         backgroundColor: Colors.white,
          //         radius: 30,
          //         child: MyCachedNetworkImage(
          //           filePath: rootCategory.iconImageThumbnail,
          //         ),
          //       ),
          //     ),
          //     SizedBox(
          //       width: 60,
          //       child: Text(
          //         rootCategory.name,
          //         textAlign: TextAlign.center,
          //         overflow: TextOverflow.ellipsis,
          //         maxLines: 2,
          //         style: Theme.of(context).textTheme.titleSmall,
          //       ),
          //     ),
          //   ],
          // ),
        ],
      ),
    );
  }
}
