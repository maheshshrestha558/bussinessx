import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'widgets.dart';

Future<bool> showExitPopup(context) async {
  return await showModalBottomSheet(
        context: context,
        builder: (context) => const CustomExitPopUp(),
      ) ??
      false;
}

class CustomExitPopUp extends StatefulWidget {
  const CustomExitPopUp({
    Key? key,
  }) : super(key: key);

  @override
  State<CustomExitPopUp> createState() => _CustomExitPopUpState();
}

class _CustomExitPopUpState extends State<CustomExitPopUp> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: Container(
        height: 150,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            tileMode: TileMode.clamp,
            colors: [
              Color(0x886AFD93),
              Color(0x887CD0FF),
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              'Exit App',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Are you sure you want to Exit app?',
              style: Theme.of(context).textTheme.bodySmall,
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                BottomSheetButton(
                  buttonLabel: 'Exit',
                  backgroundColor: Colors.red,
                  onPressed: () => SystemNavigator.pop(),
                ),
                BottomSheetButton(
                  buttonLabel: 'Cancel',
                  backgroundColor: Colors.white,
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
