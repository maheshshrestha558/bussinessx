import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingsOptions extends StatefulWidget {
  final String title;
  const SettingsOptions({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  State<SettingsOptions> createState() => _SettingsOptionsState();
}

class _SettingsOptionsState extends State<SettingsOptions> {
  bool value = false;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.title, style: const TextStyle(fontSize: 14)),
      trailing: CupertinoSwitch(
        value: value,
        onChanged: (toggleValue) {
          setState(() {
            value = toggleValue;
          });
        },
      ),
      onTap: null,
    );
  }
}
