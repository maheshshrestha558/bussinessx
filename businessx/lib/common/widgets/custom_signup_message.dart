import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../screens/screens.dart';
import '../../screens/theme.dart';
import '../enums.dart';

class CustomSignupMessage extends StatelessWidget {
  final UserType userType;
  const CustomSignupMessage({
    Key? key,
    required this.userType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RichText(
        text: TextSpan(
          text: 'Don\'t have an account?',
          style: B2BTheme.textTheme.headlineMedium,
          children: <TextSpan>[
            TextSpan(
              text: ' Sign up',
              style: GoogleFonts.rubik(
                color: const Color(0xff0077b6),
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () {
                  Navigator.pushNamed(
                    context,
                    SignUpScreen.routeName,
                    arguments: userType,
                  );
                },
            )
          ],
        ),
      ),
    );
  }
}
