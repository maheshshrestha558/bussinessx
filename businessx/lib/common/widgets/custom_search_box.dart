import 'package:flutter/material.dart';

import '../../route_names.dart';

class CustomSearchBox extends StatelessWidget {
  final ValueChanged<String>? onChanged;
  final bool? enabled;
  const CustomSearchBox({
    Key? key,
    this.onChanged,
    this.enabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final FocusNode inputFocusNode = FocusNode();
    return Container(
      height: 40,
      width: MediaQuery.of(context).size.width / 1.25,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Colors.grey.withOpacity(0.32),
        ),
      ),
      child: GestureDetector(
        onTap: () => Navigator.pushNamed(
          context,
          RouteName.searchScreen,
        ),
        child: TextField(
          autofocus: false,
          textAlign: TextAlign.center,
          focusNode: inputFocusNode,
          enabled: enabled,
          decoration: InputDecoration(
            suffixIcon: IconButton(
              icon: const Icon(Icons.search),
              onPressed: () {},
            ),
            contentPadding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
            border: InputBorder.none,
            hintText: 'Search Product/ Service',
            hintStyle: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
      ),
    );
  }
}
