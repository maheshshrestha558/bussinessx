import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class DrawerListTile extends StatelessWidget {
  final String imgName;
  final String title;
  final void Function()? onPress;
  const DrawerListTile({
    Key? key,
    required this.imgName,
    required this.title,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      leading: SizedBox(
        height: 19.5,
        width: 19.5,
        child: SvgPicture.asset('assets/images/$imgName.svg'),
      ),
      title: Transform.translate(
        offset: const Offset(-16, 0),
        child: Text(
          title,
          style: GoogleFonts.rubik(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Colors.black,
          ),
        ),
      ),
      trailing: const Icon(
        Icons.arrow_forward_ios,
        size: 12,
      ),
      onTap: onPress,
    );
  }
}
