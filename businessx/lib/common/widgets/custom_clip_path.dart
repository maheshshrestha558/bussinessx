import 'package:flutter/material.dart';

import 'widgets.dart';

class CustomClipPath extends StatelessWidget {
  final double height;

  const CustomClipPath({
    Key? key,
    required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WaveClipper(),
      child: Container(
        height: height,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment(-1.0, -4.0),
            end: Alignment(1.0, 4.0),
            tileMode: TileMode.clamp,
            colors: [Color(0xff7CD0FF), Color(0xff6AFD93)],
          ),
        ),
      ),
    );
  }
}
