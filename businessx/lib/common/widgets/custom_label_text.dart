import 'package:flutter/material.dart';

import '../../screens/theme.dart';

class CustomLabelText extends StatelessWidget {
  final String labelText;
  final TextStyle? textStyle;
  const CustomLabelText({
    Key? key,
    required this.labelText,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      labelText,
      style: textStyle ?? B2BTheme.textTheme.headlineMedium,
    );
  }
}
