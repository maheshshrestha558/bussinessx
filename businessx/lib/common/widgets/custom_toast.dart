import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ShowToast {
  ShowToast._();

  static void success([String? message]) => Fluttertoast.showToast(
        toastLength: Toast.LENGTH_LONG,
        msg: message ?? 'Success',
        backgroundColor: Colors.green,
        textColor: Colors.white,
        fontSize: 16.0,
      );

  static void error([String? message]) => Fluttertoast.showToast(
        toastLength: Toast.LENGTH_LONG,
        msg: message ?? 'Error Occurred',
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0,
      );
}
