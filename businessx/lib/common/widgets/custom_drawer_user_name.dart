import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../bloc/bloc.dart';
import '../../models/response/response.dart';

class CustomDrawerUserName extends StatelessWidget {
  const CustomDrawerUserName({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserResponse>(
      stream: usersBloc.user.stream,
      builder: (context, snapshot) {
        var name = snapshot.data?.user?.enName;
        return DrawerHeader(
          child: Center(
            child: RichText(
              text: TextSpan(
                text: 'Hello, ',
                style: GoogleFonts.rubik(
                  fontSize: 25,
                  fontWeight: FontWeight.w400,
                  color: const Color(0xff1D4ED8),
                ),
                children: [
                  TextSpan(
                    text: name ?? 'User',
                    style: GoogleFonts.rubik(
                      fontSize: 25,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
