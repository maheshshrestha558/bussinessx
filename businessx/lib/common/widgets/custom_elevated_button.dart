import 'package:flutter/material.dart';

class CustomElevatedButton extends StatelessWidget {
  final void Function()? onPressed;
  final String buttonText;
  final Color? color;
  final TextStyle? style;
  final double leftPadding, rightPadding;

  const CustomElevatedButton({
    Key? key,
    this.onPressed,
    required this.buttonText,
    this.leftPadding = 28,
    this.rightPadding = 28,
    this.color = const Color(0xff1D4ED8),
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.maxFinite,
      child: Padding(
        padding: EdgeInsets.only(left: leftPadding, right: rightPadding),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(color),
            padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
            ),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            ),
          ),
          onPressed: onPressed,
          child: Text(
            buttonText,
            style: const TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
