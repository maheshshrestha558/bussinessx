import 'package:flutter/material.dart';

class ComplaintStatusCard extends StatelessWidget {
  const ComplaintStatusCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.21),
            spreadRadius: 10,
            blurRadius: 19,
            offset: Offset(0, 10),
          )
        ],
      ),
      child: const ListTile(
        minVerticalPadding: 8,
        isThreeLine: true,
        leading: Text(
          '12 \n Dec, 21',
          textAlign: TextAlign.center,
        ),
        title: Text(
          'Defective Product sent by seller and refusing to take return',
          textAlign: TextAlign.justify,
          style: TextStyle(
            fontSize: 14,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 6,
            ),
            Text(
              '#01198',
              style: TextStyle(color: Colors.black),
            ),
            SizedBox(
              height: 6,
            ),
            Text('Status:Assigned'),
          ],
        ),
      ),
    );
  }
}
