class PlatformDetails {
  String? enName;
  String? address;
  String? email;
  String? contact;
  String? establishedDate;

  PlatformDetails({
    this.enName,
    this.address,
    this.email,
    this.contact,
    this.establishedDate,
  });

  PlatformDetails.fromJson(Map<String, dynamic> json)
      : enName = json['en_name'],
        address = json['address'],
        email = json['email'],
        contact = json['contact'],
        establishedDate = json['established_date'];
}
