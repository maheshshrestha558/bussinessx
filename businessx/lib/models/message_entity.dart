import 'models.dart';

class MessageEntity {
  int id;
  int? buyerId;
  int? sellerId;
  int? senderId;
  String? message;
  List<FileElement> fileElements; 

  MessageEntity.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        buyerId = json['buyer_id'],
        sellerId = json['seller_id'],
        senderId = json['sender_id'],
        message = json['message'],
        fileElements = json['files'] != null
            ? (json['files'] as List)
                .map((i) => FileElement.fromJson(i))
                .toList()
            : [];
}
