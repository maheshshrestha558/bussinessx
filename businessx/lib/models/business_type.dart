class BusinessType {
  BusinessType({
    required this.id,
    required this.enName,
    required this.npName,
    required this.slug,
    required this.name,
  });
  late final int id;
  late final String enName;
  late final String npName;
  String? slug;
  late final String name;

  BusinessType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    enName = json['en_name'];
    slug = json['slug'];
    name = json['name'];
  }
}
