import 'package:equatable/equatable.dart';

import 'models.dart';
import 'product_image.dart';

class SellerProduct extends Equatable {
  int id;
  String imageThumbnail;
  String enName;
  String name;
  dynamic price;
  String unit;
  String stackId;
  int status;
  int isVerified;
  String description;
  int views;
  ProductCategory productCategory;
  List<Specification> specifications;
  Brand brand;
  List<ProductImage>? productImages;

  SellerProduct.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imageThumbnail = json['image_thumbnail'],
        enName = json['en_name'],
        name = json['name'],
        price = json['price'],
        unit = json['unit'],
        stackId = json['stack_id'],
        status = json['status'],
        isVerified = json['is_verified'],
        description = json['description'],
        views = json['views'],
        brand = Brand.fromJson(json['brand']),
        productCategory = ProductCategory.fromJson(json['category']),
        specifications = (json['specifications'] as List)
            .map((specification) => Specification.fromJson(specification))
            .toList(),
        productImages = (json['files'] as List)
            .map((productImage) => ProductImage.fromJson(productImage))
            .toList();

  @override
  List<Object?> get props => [
        id,
        imageThumbnail,
        enName,
        name,
        price,
        unit,
        stackId,
        status,
        isVerified,
        description,
        views,
        productCategory,
        specifications,
        brand,
      ];
}
