class ChildCategory {
  int id;
  String imageThumbnail;
  String name;

  ChildCategory.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imageThumbnail = json['image_thumbnail'],
        name = json['name'];
}
