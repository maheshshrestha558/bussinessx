class Banner {
  final String imageThumbnail;

  Banner.fromJson(Map<String, dynamic> json)
      : imageThumbnail = json['image_thumbnail'];
}
