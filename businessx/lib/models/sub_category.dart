class SubCategory {
  int? id;
  String? imageThumbnail;
  String? name;

  SubCategory({this.id, this.imageThumbnail, this.name});

  SubCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
  }
}