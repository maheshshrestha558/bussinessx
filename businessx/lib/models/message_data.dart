import 'models.dart';

class MessageData {
  List<MessageEntity> messageEntities = [];
  Meta meta;

  MessageData.fromJson(Map<String, dynamic> json)
      : messageEntities = List<MessageEntity>.from(
          json['data'].map((x) => MessageEntity.fromJson(x)),
        ),
        meta = Meta.fromJson(json['meta']);
}
