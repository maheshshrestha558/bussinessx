import 'business_type.dart';

class UnfavoriteBusiness {
  UnfavoriteBusiness({
    required this.id,
    required this.userId,
    required this.businessTypeId,
    required this.slug,
    required this.enName,
    required this.ceoName,
    this.contactNumber,
    this.alternateContactNumber,
    this.pan,
    this.importExportCode,
    required this.companyLogo,
    this.establishedDate,
    this.websiteUrl,
    this.facebookUrl,
    this.instagramUrl,
    this.whatsappNumber,
    required this.createdAt,
    required this.updatedAt,
    required this.imageSize,
    required this.imageThumbnail,
    required this.name,
    required this.businessType,
  });
  late final int id;
  int? userId;
  late final int businessTypeId;
  String? slug;
  late final String enName;
  late final String ceoName;
  late final int? contactNumber;
  late final int? alternateContactNumber;
  late final int? pan;
  late final int? importExportCode;
  String? companyLogo;
  late final String? establishedDate;
  late final String? websiteUrl;
  late final String? facebookUrl;
  late final String? instagramUrl;
  late final int? whatsappNumber;
  String? createdAt;
  String? updatedAt;
  String? imageSize;
  late final String imageThumbnail;
  late final String name;
  late final BusinessType businessType;

  UnfavoriteBusiness.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    businessTypeId = json['business_type_id'];
    slug = json['slug'];
    enName = json['en_name'];
    ceoName = json['ceo_name'];
    contactNumber = null;
    alternateContactNumber = null;
    pan = null;
    importExportCode = null;
    companyLogo = json['company_logo'];
    establishedDate = null;
    websiteUrl = null;
    facebookUrl = null;
    instagramUrl = null;
    whatsappNumber = null;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    imageSize = json['image_size'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    businessType = BusinessType.fromJson(json['business_type']);
  }
}
