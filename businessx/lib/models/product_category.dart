import 'package:equatable/equatable.dart';

class ProductCategory extends Equatable {
  final int id;
  final String name;

  ProductCategory.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [id, name];

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
