import 'models.dart';

class Buyer {
  int id;
  String name;
  int verified;
  String imageThumbnail;
  String? gender;
  String? designation;
  String? address;
  String? email;
  String? contactNumber;
  String? alternateContactNumber;
  Business? business;

  @override
  String toString() {
    return 'Buyer{id: $id, name: $name, verified: $verified, designation: $designation, address: $address, email: $email, contactNumber: $contactNumber, alternateContactNumber: $alternateContactNumber, business: $business}';
  }

  Buyer.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        verified = json['verified'],
        imageThumbnail = json['image_thumbnail'],
        gender = json['gender'],
        designation = json['designation'],
        address = json['address'],
        email = json['email'],
        contactNumber = json['contact_number'],
        alternateContactNumber = json['alternate_contact_number'],
        business = null;
}
