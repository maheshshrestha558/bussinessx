class ProductImage {
  int? id;
  String? type;
  String? name;
  String? path;

  ProductImage({this.id, this.type, this.name, this.path});

  ProductImage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    name = json['name'];
    path = json['path'];
  }
}