class Lead {
  Lead({
    required this.id,
    required this.name,
    required this.contact,
    required this.source,
  });
  late final int id;
  late final String name;
  late final String contact;
  late final String source;

  Lead.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    contact = json['contact'];
    source = json['source'];
  }
}
