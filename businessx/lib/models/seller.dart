import 'models.dart';

class Seller {
  int id;
  String name;
  int verified;
  Business business;
  String? designation;
  String imageThumbnail;

  Seller.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        verified = json['verified'],
        business = Business.fromJson(json['business']),
        designation = json['designation'],
        imageThumbnail = json['image_thumbnail'];
}
