class FileElement {
  int? id;
  int? leadMessageId;
  String? type;
  String? file;
  String? path;
  String? createdAt;
  String? updatedAt;
  String? imageThumbnail;

  FileElement({
    this.id,
    this.leadMessageId,
    this.type,
    this.file,
    this.path,
    this.createdAt,
    this.updatedAt,
    this.imageThumbnail,
  });

  FileElement.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    leadMessageId = json['lead_message_id'];
    type = json['type'];
    file = json['file'];
    path = json['path'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    imageThumbnail = json['image_thumbnail'];
  }
}
