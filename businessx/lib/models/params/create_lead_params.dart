class CreateLeadParams {
  String? name, contact, description, productName;
  int? productId;
  bool? agree;
  CreateLeadParams({
    this.name,
    this.contact,
    this.description,
    this.productId,
    this.productName,
    this.agree,
  });

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['contact'] = contact;
    data['name'] = name;
    data['product_id'] = productId;
    data['agree'] = agree;
    data['description'] = description;
    data['product_name'] = productName;
    data.removeWhere((key, value) => value == null || value.toString().isEmpty);
    return data;
  }
}
