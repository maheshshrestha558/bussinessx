class User {
  final int id;
  String? enName;
  String? gender;
  String? designation;
  String? address;
  String? email;
  String? contactNumber;
  String? alternateContactNumber;
  String? profileImage;
  String? imageThumbnail;
  String? name;

  User({
    required this.id,
    this.enName,
    this.gender,
    this.designation,
    this.address,
    this.email,
    this.contactNumber,
    this.alternateContactNumber,
    this.profileImage,
    this.imageThumbnail,
    this.name,
  });

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        enName = json['en_name'],
        gender = json['gender'],
        designation = json['designation'],
        address = json['address'],
        email = json['email'],
        contactNumber = json['contact_number'],
        alternateContactNumber = json['alternate_contact_number'],
        profileImage = json['profile_image'],
        imageThumbnail = json['image_thumbnail'],
        name = json['name'];

  User copyWith({
    int? id,
    String? enName,
    String? gender,
    String? designation,
    String? address,
    String? email,
    String? contactNumber,
    String? alternateContactNumber,
    
  }) {
    return User(
      id: id ?? this.id,
      enName: enName ?? this.enName,
      gender: gender ?? this.gender,
      designation: designation ?? this.designation,
      address: address ?? this.address,
      email: email ?? this.email,
      contactNumber: contactNumber ?? contactNumber,
      alternateContactNumber:
          alternateContactNumber ?? this.alternateContactNumber,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'en_name': enName,
      'contact_number': contactNumber,
      'alternate_contact_number': alternateContactNumber,
      'gender': gender,
      'address': address,
      'email': email,
      'designation': designation
    };
  }
}
