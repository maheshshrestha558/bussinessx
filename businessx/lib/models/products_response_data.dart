import 'models.dart';

class ProductsResponseData {
  int? id;
  String? imageThumbnail;
  String? name;
  List<Product>? products;

  ProductsResponseData({
    this.id,
    this.imageThumbnail,
    this.name,
    this.products,
  });

  ProductsResponseData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    if (json['products'] != null) {
      products = <Product>[];
      json['products'].forEach((v) {
        products?.add(Product.fromJson(v));
      });
    }
  }
}
