class Place {
  double? lat;
  double? lon;
  String? displayName;
  Map<String, dynamic>? address;
  Map<String, dynamic>? nameDetails;
  String? error;

  Place.fromJson(Map<String, dynamic> json)
      : lat = double.parse(json['lat']),
        lon = double.parse(json['lon']),
        displayName = json['display_name'],
        address = json['address'] != null
            ? json['address'] as Map<String, dynamic>
            : null,
        nameDetails = json['namedetails'] != null
            ? json['namedetails'] as Map<String, dynamic>
            : null;

  Place.withError(String errorValue) : error = errorValue;
}
