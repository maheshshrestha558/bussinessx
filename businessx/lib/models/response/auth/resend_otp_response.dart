class OTPResendResponse {
  OTPResendResponse({
    required this.message,
  });
  late final String message;
  String? error;

  OTPResendResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
  }

  OTPResendResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }
}
