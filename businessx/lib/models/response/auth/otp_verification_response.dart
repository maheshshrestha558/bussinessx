import 'package:b2b_market/models/response/response.dart';

class OTPVerificationResponse {
  UserData? userData;
  String? error;

  OTPVerificationResponse.fromJson(Map<String, dynamic> json)
      : userData = UserData.fromJson(json['data']);
  OTPVerificationResponse.withError(String errorValue) : error = errorValue;
}

class OTPParams {
  OTPParams({
    required this.otp,
    required this.contactNumber,
  });
  final String otp;
  final String contactNumber;

  Map<String, dynamic> toJson() {
    return {
      'otp': otp,
      'contact_number': contactNumber,
    };
  }
}
