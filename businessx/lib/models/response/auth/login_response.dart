import '../../user.dart';

class LoginResponse {
  UserData? userData;
  String? error;
  String? message;

  LoginResponse.fromJson(Map<String, dynamic> json)
      : userData =
            json['data'] != null ? UserData.fromJson(json['data']) : null,
        message = json['message'];

  LoginResponse.withError(String errorValue) : error = errorValue;
}

class UserData {
  UserData({
    required this.token,
    this.user,
  });
  late final String token;
  User? user;
  late List<dynamic> permissions;
  late List<dynamic> roles;

  UserData.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    user = User.fromJson(json['user']);
    permissions = json['permissions'];
    roles = json['roles'];
  }
}

class LoginCredentials {
  final String contactNumber;
  final String password;
  final String? hashKey;
  final String type;

  LoginCredentials({
    required this.contactNumber,
    required this.password,
    required this.type,
    this.hashKey,
  });

  Map<String, dynamic> toJson() {
    return {
      'contact_number': contactNumber,
      'password': password,
      'hashKey': hashKey,
      'type':type
    };
  }
}
