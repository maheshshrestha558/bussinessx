class LogOutResponse {
  LogOutResponse({
    required this.message,
  });
  String? message;
  String? error;

  LogOutResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
  }

  LogOutResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['message'] = message;
    return data;
  }
}
