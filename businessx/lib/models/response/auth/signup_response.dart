import 'package:b2b_market/common/type_defs.dart';
import 'package:equatable/equatable.dart';

import '../../../common/enums.dart';

class SignUpResponse {
  String? message;
  late final String? error;

  SignUpResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    error = null;
  }

  SignUpResponse.withError(String errorValue) : error = errorValue;
}

class SignUpCredentials extends Equatable {
  final String enName, contact, password, confirmPassword;
  final UserType userType;

  const SignUpCredentials({
    required this.enName,
    required this.contact,
    required this.password,
    required this.confirmPassword,
    required this.userType,
  });

  KVPair toJson() {
    return {
      'en_name': enName,
      'contact': contact,
      'password': password,
      'confirm_password': confirmPassword,
      'type': userType.type,
    };
  }

  @override
  List<Object?> get props => [
        enName,
        contact,
        password,
        confirmPassword,
        userType.type,
      ];
}
