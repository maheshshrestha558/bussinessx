import 'package:b2b_market/common/type_defs.dart';

class TotalPriceResponse {
  int? totalPrice;
  String? error;

  TotalPriceResponse.fromJson(KVPair json) : totalPrice = json['total_price'];

  TotalPriceResponse.withError(String errorValue) : error = errorValue;
}
