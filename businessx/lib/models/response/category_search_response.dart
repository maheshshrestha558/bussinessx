import 'category/leaf_category_response.dart';

class LeafCategorySearchResponse {
  List<LeafCategory>? leafCategories;
  String? error;


  LeafCategorySearchResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      leafCategories = <LeafCategory>[];
      json['data'].forEach((v) {
        leafCategories!.add(LeafCategory.fromJson(v));
      });
    }
  }

  LeafCategorySearchResponse.withError(String errorValue) : error = errorValue;
}
