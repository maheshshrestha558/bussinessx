class AboutUsResponse {
 
  int? status;
  AboutUsInfo? aboutUsInfo;
  String? error;

  AboutUsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    aboutUsInfo = AboutUsInfo.fromJson(json['data']);
  }

  AboutUsResponse.withError(String errorValue) : error = errorValue;
}

class AboutUsInfo {
  int? id;
  String? profileHeading;
  String? profileDescription;
  int? numberOfEmployees;
  String? annualTurnover;
  String? packagingMode;
  String? paymentMode;
  String? shipmentMode;
  String? legalStatus;
  int? businessId;
  String? createdAt;
  String? updatedAt;

  AboutUsInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    profileHeading = json['profile_heading'];
    profileDescription = json['profile_description'];
    numberOfEmployees = json['number_of_employees'];
    annualTurnover = json['annual_turnover'];
    packagingMode = json['packaging_mode'];
    paymentMode = json['payment_mode'];
    shipmentMode = json['shipment_mode'];
    legalStatus = json['legal_status'];
    businessId = json['business_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
}
