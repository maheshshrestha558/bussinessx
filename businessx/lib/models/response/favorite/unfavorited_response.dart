class UnfavoriteResponse {
  UnfavoriteResponse({this.status, this.unfavoriteBusiness, this.error});
  int? status;
  List<UnfavoriteBusiness>? unfavoriteBusiness;
  String? error;

  UnfavoriteResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    unfavoriteBusiness = List.from(json['data'])
        .map((e) => UnfavoriteBusiness.fromJson(e))
        .toList();
  }

  UnfavoriteResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['data'] = unfavoriteBusiness?.map((e) => e.toJson()).toList();
    return data;
  }
}

class UnfavoriteBusiness {
  UnfavoriteBusiness({
    required this.id,
    required this.name,
  });
  late final int id;
  late final String name;

  UnfavoriteBusiness.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
