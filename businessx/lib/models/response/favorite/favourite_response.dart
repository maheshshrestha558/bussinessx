import '../../meta.dart';

class FavoriteResponse {
  int? status;
  Favorites? favorites;
  String? error;
  FavoriteResponse({
    required this.status,
    required this.favorites,
    this.error,
  });

  FavoriteResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    favorites = Favorites.fromJson(json['data']);
  }

  FavoriteResponse.withError(String errorValue) : error = errorValue;
}

class Favorites {
  Favorites({
    required this.favorite,
    this.meta,
  });
  List<Favorite>? favorite;
  Meta? meta;

  Favorites.fromJson(Map<String, dynamic> json) {
    favorite =
        List.from(json['data']).map((e) => Favorite.fromJson(e)).toList();
    meta = Meta.fromJson(json['meta']);
  }
}

class Favorite {
  final int id;
  final int sellerId;
  final int userId;
  final int businessId;
  final Seller seller;

  Favorite.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        sellerId = json['seller_id'],
        userId = json['user_id'],
        businessId = json['business_id'],
        seller = Seller.fromJson(json['seller']);
}

class Seller {
  int id;
  int verified;
  String imageThumbnail;
  String name;
  String enName;
  String ceoName;
  String contactNumber;
  String address;
  String email;
  String? alternateContactNumber;
  int pan;
  int? importExportCode;
  String establishedDate;
  String? websiteUrl;
  String? facebookUrl;
  String? instagramUrl;
  String? whatsappNumber;
  int businessTypeId;

  Seller.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        verified = json['verified'],
        imageThumbnail = json['image_thumbnail'],
        name = json['name'],
        enName = json['en_name'],
        ceoName = json['ceo_name'],
        contactNumber = json['contact_number'],
        address = json['address'],
        email = json['email'],
        alternateContactNumber = json['alternate_contact_number'],
        pan = json['pan'],
        importExportCode = json['import_export_code'],
        establishedDate = json['established_date'],
        websiteUrl = json['website_url'],
        facebookUrl = json['facebook_url'],
        instagramUrl = json['instagram_url'],
        whatsappNumber = json['whatsapp_number'],
        businessTypeId = json['business_type_id'];
}
