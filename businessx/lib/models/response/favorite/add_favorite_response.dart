import 'favourite_response.dart';

class AddFavoriteResponse {
  AddFavoriteResponse({
    required this.status,
    required this.favoriteData,
    this.error,
  });
  int? status;
  FavoriteData? favoriteData;
  String? error;

  AddFavoriteResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    favoriteData = FavoriteData.fromJson(json['data']);
  }

  AddFavoriteResponse.withError(String errorValue) : error = errorValue;
}

class FavoriteData {
  FavoriteData({
    required this.favorite,
  });
  Favorite? favorite;

  FavoriteData.fromJson(Map<String, dynamic> json) {
    favorite = Favorite.fromJson(json['data']);
  }
}
