class DeleteFavoriteResponse {
  DeleteFavoriteResponse({
    this.status,
    this.data,
    this.error,
  });
  int? status;
  bool? data;
  String? error;

  DeleteFavoriteResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'];
  }

  DeleteFavoriteResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['data'] = data;
    return data;
  }
}
