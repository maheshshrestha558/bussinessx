import '../../banner.dart';

class BannersResponse {
  List<Banner> banners = [];
  String? error;

  BannersResponse.fromJson(Map<String, dynamic> json)
      : banners =
            List.from(json['data']).map((e) => Banner.fromJson(e)).toList();

  BannersResponse.withError(String errorValue) : error = errorValue;
}

class B2CBannerResponse {
  B2CBannerResponse({
    this.homeSlidingBanner,
    this.homeBannerFirst,
  });
  String? error;
  List<Banner>? homeSlidingBanner;
  Banner? homeBannerFirst;

  factory B2CBannerResponse.fromJson(Map<String, dynamic> json) =>
      B2CBannerResponse(
        homeSlidingBanner: List<Banner>.from(
          json['home_sliding_banner'].map((x) => Banner.fromJson(x)),
        ),
        homeBannerFirst: Banner.fromJson(json['home_banner_first']),
      );
  B2CBannerResponse.withError(String errorValue) : error = errorValue;
}

class B2CBannerCategoryResponse {
  B2CBannerCategoryResponse({
    this.categoryBanners,
  });
  String? error;
  List<Banner>? categoryBanners;
  // Banner? homeBannerFirst;

  factory B2CBannerCategoryResponse.fromJson(Map<String, dynamic> json) =>
      B2CBannerCategoryResponse(
        categoryBanners: List<Banner>.from(
          json['category_banners'].map((x) => Banner.fromJson(x)),
        ),
      );
  B2CBannerCategoryResponse.withError(String errorValue) : error = errorValue;
}
