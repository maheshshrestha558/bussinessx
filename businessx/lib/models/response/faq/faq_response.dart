class FaqResponse {
  FaqResponse({required this.status, required this.faqs, this.error});
  int? status;
  Faqs? faqs;
  String? error;

  FaqResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    faqs = Faqs.fromJson(json['data']);
  }

  FaqResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['data'] = faqs?.toJson();
    return data;
  }
}

class Faqs {
  Faqs({
    required this.faq,
  });
  late final List<Faq> faq;

  Faqs.fromJson(Map<String, dynamic> json) {
    faq = List.from(json['data']).map((e) => Faq.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['faq'] = faq.map((e) => e.toJson()).toList();
    return data;
  }
}

class Faq {
  Faq({
    // required this.id,
    required this.enName,
    required this.enDescription,
    required this.npDescription,
    // required this.status,
    // required this.position,
    // this.createdAt,
    // this.updatedAt,
    required this.name,
  });
  // late final int id;
  String? enName;
  String? enDescription;
  String? npDescription;
  // late final String status;
  // late final String position;
  // late final Null createdAt;
  // late final Null updatedAt;
  String? name;

  Faq.fromJson(Map<String, dynamic> json) {
    // id = json['id'];
    enName = json['en_name'];
    enDescription = json['en_description'];
    npDescription = json['np_description'];
    // status = json['status'];
    // position = json['position'];
    // createdAt = null;
    // updatedAt = null;
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    // _data['id'] = id;
    data['en_name'] = enName;
    data['en_description'] = enDescription;
    data['np_description'] = npDescription;
    // _data['status'] = status;
    // _data['position'] = position;
    // _data['created_at'] = createdAt;
    // _data['updated_at'] = updatedAt;
    data['name'] = name;
    return data;
  }
}
