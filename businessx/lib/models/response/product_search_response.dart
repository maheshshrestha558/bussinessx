import '../product.dart';

class ProductSearchResponse {
  ProductSearchResponse({
    required this.products,
  });
  List<Product>? products;
  String? error;  

  ProductSearchResponse.fromJson(Map<String, dynamic> json) {
    products = List.from(json['data']).map((e) => Product.fromJson(e)).toList();
  }

  ProductSearchResponse.withError(String errorValue) : error = errorValue;
}
