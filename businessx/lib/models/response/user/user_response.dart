import '../../models.dart';

class UserResponse {
  User? user;
  String? error;

  UserResponse.fromJson(Map<String, dynamic> json) {
    user = User.fromJson(json['data']);
  }

  UserResponse.withError(String errorValue) : error = errorValue;
}
