import '../models.dart';

class CategoriesResponse {
  CategoryData? categoryData;
  String? error;

  CategoriesResponse.fromJson(Map<String, dynamic> json)
      : categoryData = CategoryData.fromJson(json['data']);

  CategoriesResponse.withError(String errorValue) : error = errorValue;
}

class CategoryData {
  Meta meta;
  List<Category>? categories;

  CategoryData.fromJson(Map<String, dynamic> json)
      : meta = Meta.fromJson(json['meta']),
        categories =
            List<Category>.from(json['data'].map((x) => Category.fromJson(x)));
}

class Category {
  int id;
  String enName;
  String imageThumbnail;
  String iconImageThumbnail;
  String name;
  String? qrCode;
  List<Specification> specifications;

  Category.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        enName = json['en_name'],
        imageThumbnail = json['image_thumbnail'],
        iconImageThumbnail = json['icon_image_thumbnail'],
        name = json['name'],
        qrCode = json['qr_code'],
        specifications = json['specifications'] != null
            ? (json['specifications'] as List)
                .map((i) => Specification.fromJson(i))
                .toList()
            : [];
}
