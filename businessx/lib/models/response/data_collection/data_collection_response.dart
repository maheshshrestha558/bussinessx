class DataCollectionResponse {
  String? message;
  late final String? error;

  DataCollectionResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    error = null;
  }

  DataCollectionResponse.withError(String errorValue) : error = errorValue;
}

class DataCollection {
  DataCollection({
    this.status,
    this.totalDetails,
    this.submittedDetails,
    this.verifiedDetails,
    this.rejectedDetails,
    this.updatedDetails,
    this.data,
  });

  int? status;
  int? totalDetails;
  int? submittedDetails;
  int? verifiedDetails;
  int? rejectedDetails;
  int? updatedDetails;
  List<DataCollectionAnswer>? data;
  String? error;

  factory DataCollection.fromJson(Map<String, dynamic> json) => DataCollection(
        status: json['status'],
        totalDetails: json['total_details'],
        submittedDetails: json['submitted_details'],
        verifiedDetails: json['verified_details'],
        rejectedDetails: json['rejected_details'],
        updatedDetails: json['updated_details'],
        data: List<DataCollectionAnswer>.from(
          json['data'].map((x) => DataCollectionAnswer.fromJson(x)),
        ),
      );

  DataCollection.withError(String errorValue) : error = errorValue;
}

class DataCollectionAnswer {
  int? id;
  String? shopName;
  String? location;
  String? ceoName;
  String? contactNumber;
  String? panNumber;
  String? status;
  String? remarks;
  List<dynamic>? answers = List.filled(36, '');

  DataCollectionAnswer({
    this.id,
    this.shopName,
    this.location,
    this.ceoName,
    this.panNumber,
    this.contactNumber,
    this.status,
    this.remarks,
    this.answers,
  });
  DataCollectionAnswer.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        shopName = json['shop_name'],
        location = json['location'],
        ceoName = json['ceo_name'],
        panNumber = json['pan_number'],
        contactNumber = json['contact_number'],
        status = json['status'],
        remarks = json['remarks'],
        answers = json['answers'] != null
            ? List<String>.from(json['answers'].map((x) => x))
            : List.filled(36, '');

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'shop_name': shopName,
      'location': location,
      'ceo_name': ceoName,
      'contact_number': contactNumber,
      'pan_number': panNumber,
      'answers': answers,
    };
  }
}

var dataCollectionAnswer = DataCollectionAnswer();
