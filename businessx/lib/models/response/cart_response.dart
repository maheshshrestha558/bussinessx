import 'package:equatable/equatable.dart';

import '../product.dart';

class CartResponse {
  List<ProductData> data = [];
  late int totalPrice;
  String? error;

  CartResponse.fromJson(Map<String, dynamic> json)
      : data = List<ProductData>.from(
          json['data'].map((x) => ProductData.fromJson(x)),
        ),
        totalPrice = json['total_price'];

  CartResponse.withError(String errorValue) : error = errorValue;

  @override
  String toString() {
    return 'CartResponse{data: $data, totalPrice: $totalPrice, error: $error}';
  }
}

class ProductData extends Equatable {
  final Product product;
  final int quantity;
  final int cartId;

  const ProductData({
    required this.product,
    required this.quantity,
    required this.cartId,
  });

  factory ProductData.fromJson(Map<String, dynamic> json) {
    return ProductData(
      product: Product.fromJson(json['product']),
      quantity: json['quantity'],
      cartId: json['id'],
    );
  }

  @override
  List<Object?> get props => [cartId, quantity];
}
