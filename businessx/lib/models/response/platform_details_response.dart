import '../models.dart';

class PlatformDetailsResponse {
  PlatformDetails? platformDetails;
  String? error;

  PlatformDetailsResponse({required this.platformDetails});

  PlatformDetailsResponse.fromJson(Map<String, dynamic> json)
      : platformDetails = PlatformDetails.fromJson(json['data']);

  PlatformDetailsResponse.withError(String errorValue) : error = errorValue;
}
