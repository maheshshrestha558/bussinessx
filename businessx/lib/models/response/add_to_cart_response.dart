class AddToCartResponse {
  String? error;
  bool? success;

  AddToCartResponse.withSuccess(bool value) : success = value;

  AddToCartResponse.withError(String errorValue) : error = errorValue;
}
