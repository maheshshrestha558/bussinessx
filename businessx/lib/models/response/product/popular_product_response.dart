import '../../models.dart';

/// this response is used in B2b and b2c for fetching products

class PopularProductResponse {
  PopularProductResponse({
    required this.products,
  });
  List<Product> products = [];
  String? error;

  PopularProductResponse.fromJson(Map<String, dynamic> json) {
    products = List.from(json['data']).map((e) => Product.fromJson(e)).toList();
  }

  PopularProductResponse.withError(String errorValue) : error = errorValue;
}

class B2CProductResponse {
  B2CProductResponse({
    required this.products,
    error,
  });
  List<Product> products = [];
  late final String error;

  B2CProductResponse.fromJson(Map<String, dynamic> json) {
    products = List.from(json['data']).map((e) => Product.fromJson(e)).toList();
    // error = null;
  }

  B2CProductResponse.withError(String errorValue) : error = errorValue;
}

class B2CSingleProductResponse {
  B2CSingleProductResponse({
    required this.product,
    this.error,
  });
  SingleProduct? product;
  String? error;

  B2CSingleProductResponse.fromJson(Map<String, dynamic> json)
      : product = SingleProduct.fromJson(json['data']);

  B2CSingleProductResponse.withError(String errorValue) : error = errorValue;
}
// To parse this JSON data, do
//
//     final b2CSingleProductResponse = b2CSingleProductResponseFromJson(jsonString);

// import 'dart:convert';

// B2CSingleProductResponse b2CSingleProductResponseFromJson(String str) => B2CSingleProductResponse.fromJson(json.decode(str));

// String b2CSingleProductResponseToJson(B2CSingleProductResponse data) => json.encode(data.toJson());

// class B2CSingleProductResponse {
//     B2CSingleProductResponse({
//         this.singleProduct,
//     });

//     SingleProduct singleProduct;

//     factory B2CSingleProductResponse.fromJson(Map<String, dynamic> json) => B2CSingleProductResponse(
//         singleProduct: SingleProduct.fromJson(json["SingleProduct"]),
//     );

//     Map<String, dynamic> toJson() => {
//         "SingleProduct": singleProduct.toJson(),
//     };
// }

class SingleProduct {
  SingleProduct({
    required this.id,
    this.imageThumbnail,
    required this.enName,
    required this.npName,
    required this.name,
    required this.price,
    required this.unit,
    required this.stackId,
    required this.status,
    this.description,
    this.views,
    required this.isVerified,
    required this.seller,
    required this.specifications,
    this.files,
    required this.categoriesId,
  });

  int id;
  String? imageThumbnail;
  String enName;
  String npName;
  String name;
  int price;
  String unit;
  String stackId;
  int status;
  String? description;
  int? views;
  int isVerified;
  Seller seller;
  List<Specification>? specifications;
  List<FileElement>? files;
  List<int> categoriesId;

  factory SingleProduct.fromJson(Map<String, dynamic> json) => SingleProduct(
        id: json['id'],
        imageThumbnail: json['image_thumbnail'],
        enName: json['en_name'],
        npName: json['np_name'],
        name: json['name'],
        price: json['price'],
        unit: json['unit'],
        stackId: json['stack_id'],
        status: json['status'],
        description: json['description'],
        views: json['views'],
        isVerified: json['is_verified'],
        seller: Seller.fromJson(json['seller']),
        specifications: List<Specification>.from(
          json['specifications'].map((x) => Specification.fromJson(x)),
        ),
        files: List<FileElement>.from(
          json['files'].map((x) => FileElement.fromJson(x)),
        ),
        categoriesId: List<int>.from(json['categories_id'].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'image_thumbnail': imageThumbnail,
        'en_name': enName,
        'np_name': npName,
        'name': name,
        'price': price,
        'unit': unit,
        'stack_id': stackId,
        'status': status,
        'description': description,
        'views': views,
        'is_verified': isVerified,
        'seller': seller.toJson(),
        'specifications':
            List<dynamic>.from(specifications!.map((x) => x.toJson())),
        'files': List<dynamic>.from(files!.map((x) => x.toJson())),
        'categories_id': List<dynamic>.from(categoriesId.map((x) => x)),
      };
}

class FileElement {
  FileElement({
    this.id,
    this.type,
    this.name,
    this.path,
  });

  int? id;
  String? type;
  String? name;
  String? path;

  factory FileElement.fromJson(Map<String, dynamic> json) => FileElement(
        id: json['id'],
        type: json['type'],
        name: json['name'],
        path: json['path'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'type': type,
        'name': name,
        'path': path,
      };
}

class Seller {
  Seller({
    required this.id,
    required this.enName,
    required this.npName,
    required this.name,
    required this.verified,
    this.imageThumbnail,
    this.gender,
    this.designation,
    this.address,
    this.email,
    this.contactNumber,
    this.alternateContactNumber,
    this.business,
  });

  int id;
  String enName;
  String npName;
  String name;
  int verified;
  String? imageThumbnail;
  String? gender;
  dynamic designation;
  String? address;
  String? email;
  String? contactNumber;
  dynamic alternateContactNumber;
  SingleProdBusiness? business;

  factory Seller.fromJson(Map<String, dynamic> json) => Seller(
        id: json['id'],
        enName: json['en_name'],
        npName: json['np_name'],
        name: json['name'],
        verified: json['verified'],
        imageThumbnail: json['image_thumbnail'],
        gender: json['gender'],
        designation: json['designation'],
        address: json['address'],
        email: json['email'],
        contactNumber: json['contact_number'],
        alternateContactNumber: json['alternate_contact_number'],
        business: SingleProdBusiness.fromJson(json['business']),
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'en_name': enName,
        'np_name': npName,
        'name': name,
        'verified': verified,
        'image_thumbnail': imageThumbnail,
        'gender': gender,
        'designation': designation,
        'address': address,
        'email': email,
        'contact_number': contactNumber,
        'alternate_contact_number': alternateContactNumber,
        'business': business!.toJson(),
      };
}

class SingleProdBusiness {
  SingleProdBusiness({
    required this.id,
    this.verified,
    this.imageThumbnail,
    this.name,
    this.enName,
    this.npName,
    this.ceoName,
    this.contactNumber,
    this.address,
    this.email,
    this.alternateContactNumber,
    this.pan,
    this.importExportCode,
    // this.establishedDate,
    this.websiteUrl,
    this.facebookUrl,
    this.instagramUrl,
    this.whatsappNumber,
    this.businessTypeId,
  });

  int id;
  int? verified;
  String? imageThumbnail;
  String? name;
  String? enName;
  String? npName;
  String? ceoName;
  String? contactNumber;
  String? address;
  String? email;
  String? alternateContactNumber;
  int? pan;
  int? importExportCode;
  // DateTime establishedDate;
  String? websiteUrl;
  String? facebookUrl;
  String? instagramUrl;
  String? whatsappNumber;
  int? businessTypeId;

  factory SingleProdBusiness.fromJson(Map<String, dynamic> json) =>
      SingleProdBusiness(
        id: json['id'],
        verified: json['verified'],
        imageThumbnail: json['image_thumbnail'],
        name: json['name'],
        enName: json['en_name'],
        npName: json['np_name'],
        ceoName: json['ceo_name'],
        contactNumber: json['contact_number'],
        address: json['address'],
        email: json['email'],
        alternateContactNumber: json['alternate_contact_number'],
        pan: json['pan'],
        importExportCode: json['import_export_code'],
        // establishedDate: DateTime.parse(json['established_date']),
        websiteUrl: json['website_url'],
        facebookUrl: json['facebook_url'],
        instagramUrl: json['instagram_url'],
        whatsappNumber: json['whatsapp_number'],
        businessTypeId: json['business_type_id'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'verified': verified,
        'image_thumbnail': imageThumbnail,
        'name': name,
        'en_name': enName,
        'np_name': npName,
        'ceo_name': ceoName,
        'contact_number': contactNumber,
        'address': address,
        'email': email,
        'alternate_contact_number': alternateContactNumber,
        'pan': pan,
        'import_export_code': importExportCode,
        // 'established_date':
        //     "${establishedDate.year.toString().padLeft(4, '0')}-${establishedDate.month.toString().padLeft(2, '0')}-${establishedDate.day.toString().padLeft(2, '0')}",
        'website_url': websiteUrl,
        'facebook_url': facebookUrl,
        'instagram_url': instagramUrl,
        'whatsapp_number': whatsappNumber,
        'business_type_id': businessTypeId,
      };
}

class Specification {
  Specification({
    this.id,
    this.name,
    this.value,
  });

  int? id;
  String? name;
  String? value;

  factory Specification.fromJson(Map<String, dynamic> json) => Specification(
        id: json['id'],
        name: json['name'],
        value: json['value'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'value': value,
      };
}
