class CreateProductResponse {
  int? status;
  String? error;

  CreateProductResponse({
    this.status,
  });

  CreateProductResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
  }

  CreateProductResponse.withError(String errorValue) : error = errorValue;
}
