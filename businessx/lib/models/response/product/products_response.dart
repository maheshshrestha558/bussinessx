import '../../models.dart';

class ProductsResponse {
  int? status;
  ProductsResponseData? productResponseData;
  String? error;

  ProductsResponse({this.status, this.productResponseData, this.error});

  ProductsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    productResponseData = json['data'] != null
        ? ProductsResponseData.fromJson(json['data'])
        : null;
  }

  ProductsResponse.withError(String errorValue) : error = errorValue;
}

class ProductStatusResponse {
  String? data;
  // ProductsResponseData? productResponseData;
  String? error;

  ProductStatusResponse({this.data, this.error});

  ProductStatusResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'];
  }

  ProductStatusResponse.withError(String errorValue) : error = errorValue;
}
