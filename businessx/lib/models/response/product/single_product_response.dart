import '../../product.dart';

class SingleProductResponse {
  SingleProductResponse({
    required this.product,
    this.error,
  });
    Product? product;
    String? error;

  SingleProductResponse.fromJson(Map<String, dynamic> json)
      :  product = Product.fromJson(json['data']);

  SingleProductResponse.withError(String errorValue) : error = errorValue;
}
