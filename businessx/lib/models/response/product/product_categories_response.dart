import '../../models.dart';

class ProductCategoriesResponse {
  ProductCategoriesResponse({
    this.status,
    this.productCategory,
    this.error,
  });
  int? status;
  ProductCategories? productCategory;
  String? error;

  ProductCategoriesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    productCategory = ProductCategories.fromJson(json['data']);
  }

  ProductCategoriesResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['data'] = productCategory?.toJson();
    return data;
  }
}

class ProductCategories {
  ProductCategories({
    this.productCategories,
  });
  List<ProductCategory>? productCategories;

  ProductCategories.fromJson(Map<String, dynamic> json) {
    productCategories = List.from(json['data'])
        .map((e) => ProductCategory.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['productCategory'] =
        productCategories?.map((e) => e.toJson()).toList();
    return data;
  }
}
