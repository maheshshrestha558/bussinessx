class ProductDeleteResponse {
  int? status;
  bool? data;
  String? error;

  

  ProductDeleteResponse.fromJson(Map<String,dynamic> json) {
    status = json['status'];
    data = json['data'];
  }

  ProductDeleteResponse.withError(String errorValue) : error = errorValue;
}
