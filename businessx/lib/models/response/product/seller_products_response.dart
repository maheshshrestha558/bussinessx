import '../../models.dart';

class SellerProductsResponse {
  List<SellerProduct> sellerProducts;
  String error;

  SellerProductsResponse.fromJson(Map<String, dynamic> json)
      : sellerProducts = (json['data'] as List)
            .map((e) => SellerProduct.fromJson(e))
            .toList(),
        error = '';

  SellerProductsResponse.withError(String errorValue)
      : error = errorValue,
        sellerProducts = [];
}
