import '../../models.dart';

class RootCategoriesResponse {
  RootCategoriesResponse({
    required this.status,
    required this.rootCategories,
    this.error,
  });
  int? status;
  RootCategories? rootCategories;
  String? error;

  RootCategoriesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    rootCategories = RootCategories.fromJson(json['data']);
  }

  RootCategoriesResponse.withError(String errorValue) : error = errorValue;
}

class RootCategories {
  RootCategories({
    required this.rootCategory,
    this.meta,
  });
  List<RootCategory>? rootCategory;
  Meta? meta;

  RootCategories.fromJson(Map<String, dynamic> json) {
    rootCategory =
        List.from(json['data']).map((e) => RootCategory.fromJson(e)).toList();
    meta = Meta.fromJson(json['meta']);
  }
}

class RootCategory {
  RootCategory({
    required this.id,
    required this.imageThumbnail,
    required this.iconImageThumbnail,
    required this.name,
  });
  int id;
  String imageThumbnail;
  String iconImageThumbnail;
  String name;

  factory RootCategory.fromJson(Map<String, dynamic> json) {
    return RootCategory(
      id: json['id'],
      imageThumbnail: json['image_thumbnail'],
      iconImageThumbnail: json['icon_image_thumbnail'],
      name: json['name'],
    );
  }
}

class B2CTestCategoriesResponse {
  B2CTestCategoriesResponse({
    this.status,
    required this.data,
  });
  int? status;
  CategoryData? data;
  String? error;

  B2CTestCategoriesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? CategoryData.fromJson(json['data']) : null;
  }

  B2CTestCategoriesResponse.withError(String errorValue) : error = errorValue;
}

class CategoryData {
  int? id;
  String? imageThumbnail;
  String? name;
  List<RootCategory>? rootCategories;
  int? activeSubCategory;

  CategoryData({
    this.id,
    this.imageThumbnail,
    this.name,
    this.rootCategories,
    this.activeSubCategory,
  });

  CategoryData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    rootCategories = List.from(json['sub_category'])
        .map((e) => RootCategory.fromJson(e))
        .toList();
    // if (json['sub_category'] != null) {
    //   rootCategories = <RootCategory>[];
    //   json['sub_category'].forEach((v) {
    //     rootCategories?.add(RootCategory.fromJson(v));
    //   });
    // }
    activeSubCategory = json['active_sub_category'];
  }
}

// class SubCategory {
//   int id;
//   String imageThumbnail;
//   String name;

//   SubCategory({
//     required this.id,
//     required this.imageThumbnail,
//     required this.name,
//   });

//   factory SubCategory.fromJson(Map<String, dynamic> json) {
//     return SubCategory(
//       id: json['id'],
//       imageThumbnail: json['image_thumbnail'],
//       name: json['name'],
//     );
//   }
// }
