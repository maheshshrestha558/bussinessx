class SubCategoriesResponse {
  int? status;
  Datas? data;
  String? error;

  SubCategoriesResponse({this.status, this.data});

  SubCategoriesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? Datas.fromJson(json['data']) : null;
  }

  SubCategoriesResponse.withError(String errorValue) : error = errorValue;
}

class Datas {
  int? id;
  String? imageThumbnail;
  String? name;
  List<SubCategory>? subCategories;
  int? activeSubCategory;

  Datas({
    this.id,
    this.imageThumbnail,
    this.name,
    this.subCategories,
    this.activeSubCategory,
  });

  Datas.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    if (json['sub_category'] != null) {
      subCategories = <SubCategory>[];
      json['sub_category'].forEach((v) {
        subCategories?.add(SubCategory.fromJson(v));
      });
    }
    activeSubCategory = json['active_sub_category'];
  }
}

class SubCategory {
  int id;
  String imageThumbnail;
  String name;

  SubCategory({
    required this.id,
    required this.imageThumbnail,
    required this.name,
  });

  factory SubCategory.fromJson(Map<String, dynamic> json) {
    return SubCategory(
      id: json['id'],
      imageThumbnail: json['image_thumbnail'],
      name: json['name'],
    );
  }
}
