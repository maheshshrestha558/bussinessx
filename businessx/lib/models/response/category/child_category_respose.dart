import '../../models.dart';

class ChildCategoryResponse {
  int? status;
  ChildCategoryData? childCategoryData;
  String? error;

  @override
  toString() {
    return 'child Category data $childCategoryData';
  }

  ChildCategoryResponse({this.status, this.childCategoryData, this.error});

  ChildCategoryResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    childCategoryData =
        json['data'] != null ? ChildCategoryData.fromJson(json['data']) : null;
  }

  ChildCategoryResponse.withError(String errorValue) : error = errorValue;
}
