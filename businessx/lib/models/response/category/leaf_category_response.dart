class LeafResponse {
  LeafResponse({required this.status, required this.leaf, this.error});
  int? status;
  Leaf? leaf;
  String? error;

  LeafResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    leaf = Leaf.fromJson(json['data']);
  }

  LeafResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['data'] = leaf?.toJson();
    return data;
  }
}

class Leaf {
  Leaf({
    required this.id,
    required this.imageThumbnail,
    required this.iconImageThumbnail,
    required this.name,
    required this.enName,
    required this.npName,
    required this.categoryId,
    required this.leafCategory,
    required this.status,
  });
  late final int id;
  late final String imageThumbnail;
  late final String iconImageThumbnail;
  late final String name;
  late final String enName;
  late final String npName;
  late final int categoryId;
  late final List<LeafCategory> leafCategory;
  late final int status;

  Leaf.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    iconImageThumbnail = json['icon_image_thumbnail'];
    name = json['name'];
    enName = json['en_name'];
    categoryId = json['category_id'];
    leafCategory = List.from(json['leaf_category'])
        .map((e) => LeafCategory.fromJson(e))
        .toList();
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['image_thumbnail'] = imageThumbnail;
    data['icon_image_thumbnail'] = iconImageThumbnail;
    data['name'] = name;
    data['en_name'] = enName;
    data['category_id'] = categoryId;
    data['leaf_category'] = leafCategory.map((e) => e.toJson()).toList();
    data['status'] = status;
    return data;
  }
}

class LeafCategory {
  LeafCategory({
    required this.id,
    required this.imageThumbnail,
    required this.iconImageThumbnail,
    required this.name,
    required this.enName,
    required this.categoryId,
    required this.status,
  });
  late final int id;
  late final String imageThumbnail;
  late final String iconImageThumbnail;
  late final String name;
  late final String enName;
  late final int categoryId;
  late final int status;

  LeafCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    iconImageThumbnail = json['icon_image_thumbnail'];
    name = json['name'];
    enName = json['en_name'];
    categoryId = json['category_id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['image_thumbnail'] = imageThumbnail;
    data['icon_image_thumbnail'] = iconImageThumbnail;
    data['name'] = name;
    data['en_name'] = enName;
    data['category_id'] = categoryId;
    data['status'] = status;
    return data;
  }
}
