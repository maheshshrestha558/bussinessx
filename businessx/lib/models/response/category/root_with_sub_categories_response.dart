import '../../models.dart';

class RootWithSubCategoriesResponse {
  RootWithSubCategoriesResponse({
    required this.status,
    required this.rootCategories,
    this.error,
  });
  int? status;
  RootCategories? rootCategories;
  String? error;

  RootWithSubCategoriesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    rootCategories = RootCategories.fromJson(json['data']);
  }

  RootWithSubCategoriesResponse.withError(String errorValue)
      : error = errorValue;
}

class RootCategories {
  RootCategories({
    required this.rootWithSubCategory,
    required this.meta,
  });
  List<RootWithSubCategory>? rootWithSubCategory;
  Meta? meta;

  RootCategories.fromJson(Map<String, dynamic> json) {
    rootWithSubCategory = List.from(json['data'])
        .map((e) => RootWithSubCategory.fromJson(e))
        .toList();
    meta = Meta.fromJson(json['meta']);
  }
}

class RootWithSubCategory {
  late final int id;
  late final String imageThumbnail;
  late final String iconImageThumbnail;
  late final String name;
  List<SubCategory> subCategories;

  RootWithSubCategory.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imageThumbnail = json['image_thumbnail'],
        iconImageThumbnail = json['icon_image_thumbnail'],
        name = json['name'],
        subCategories = List.from(json['sub_category'])
            .map((e) => SubCategory.fromJson(e))
            .toList();
}

class SubCategory {
  int id;
  String? imageThumbnail;
  String iconImageThumbnail;
  String name;
  int categoryId;

  SubCategory.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imageThumbnail = json['image_thumbnail'],
        iconImageThumbnail = json['icon_image_thumbnail'],
        name = json['name'],
        categoryId = json['category_id'];
}
