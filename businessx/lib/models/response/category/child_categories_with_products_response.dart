import '../../models.dart';

class ChildCategoriesWithProductsResponse {
  ChildCategoriesWithProductsResponse({
    this.status,
    this.childCategories,
    this.error,
  });
  int? status;
  ChildCategories? childCategories;
  String? error;

  ChildCategoriesWithProductsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    childCategories = ChildCategories.fromJson(json['data']);
  }

  ChildCategoriesWithProductsResponse.withError(String errorValue)
      : error = errorValue;
}

class ChildCategories {
  ChildCategories({
    this.childCategory,
    this.meta,
  });
  List<ChildCategory>? childCategory;
  Meta? meta;

  ChildCategories.fromJson(Map<String, dynamic> json) {
    childCategory =
        List.from(json['data']).map((e) => ChildCategory.fromJson(e)).toList();
    meta = Meta.fromJson(json['meta']);
  }
}

class ChildCategory {
  int id;
  String imageThumbnail;
  String iconImageThumbnail;
  String name;
  String enName;
  int categoryId;
  List<Product> products;
  int status;

  ChildCategory.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imageThumbnail = json['image_thumbnail'],
        iconImageThumbnail = json['icon_image_thumbnail'],
        name = json['name'],
        enName = json['en_name'],
        categoryId = json['category_id'],
        products = List.from(json['products'])
            .map((e) => Product.fromJson(e))
            .toList(),
        status = json['status'];
}
