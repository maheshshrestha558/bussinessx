import '../../business_type.dart';

class AllBusinessTypeResponse {
  AllBusinessTypeResponse({this.status, this.businessTypes, this.error});
  int? status;
  List<BusinessType>? businessTypes;
  String? error;

  AllBusinessTypeResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    businessTypes =
        List.from(json['data']).map((e) => BusinessType.fromJson(e)).toList();
  }

  AllBusinessTypeResponse.withError(String errorValue) : error = errorValue;
}
