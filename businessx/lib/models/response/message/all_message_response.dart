import '../../models.dart';

class AllMessageResponse {
  int? status;
  MessageData? data;
  String error;

  AllMessageResponse.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        data = MessageData.fromJson(json['data']),
        error = '';

  AllMessageResponse.withError(String errorValue) : error = errorValue;
}
