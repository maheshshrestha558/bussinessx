import '../../models.dart';

class SendMessageResponse {
  int? status;
  String error = '';
  MessageEntity? messageEntity;

  SendMessageResponse.fromJson(Map<String, dynamic> json)
      : status = json['status'],
        messageEntity = MessageEntity.fromJson(json['data']);

  SendMessageResponse.withError(String errorValue) {
    error = errorValue;
  }
}
