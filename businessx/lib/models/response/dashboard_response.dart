class DashboardResponse {
  DashboardResponse({
    required this.dashboard,
    required this.status,
  });
  Dashboard? dashboard;
  int? status;
  String? error;

  DashboardResponse.fromJson(Map<String, dynamic> json) {
    dashboard = Dashboard.fromJson(json['data']);
    status = json['status'];
  }

  DashboardResponse.withError(String errorValue) : error = errorValue;

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['data'] = dashboard!.toJson();
    data['status'] = status;
    return data;
  }
}

class Dashboard {
  Dashboard({
    required this.leadsCount,
    required this.productsViewCount,
    required this.productsCount,
  });
  late final int leadsCount;
  late final int productsViewCount;
  late final int productsCount;

  Dashboard.fromJson(Map<String, dynamic> json) {
    leadsCount = json['leads_count'];
    productsViewCount = json['Products_viewed_count'];
    productsCount = json['products_count'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['leads_count'] = leadsCount;
    data['Products_viewed_count'] = productsViewCount;
    data['products_count'] = productsCount;
    return data;
  }
}
