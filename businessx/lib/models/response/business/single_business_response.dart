
import '../../models.dart';

class SingleBusinessResponse {
  SingleBusinessResponse({
    required this.status,
    required this.business,
  });
  late final int status;
  Business? business;
  String? error;

  SingleBusinessResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    business = Business.fromJson(json['data']);
  }

  SingleBusinessResponse.withError(String errorValue) : error = errorValue;
}
