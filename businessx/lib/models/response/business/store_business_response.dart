class CreateBusinessResponse {
  CreateBusinessResponse({
    required this.status,
  });
  int? status;
  String? error;

  CreateBusinessResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
  }

  CreateBusinessResponse.withError(String errorValue) : error = errorValue;
}
