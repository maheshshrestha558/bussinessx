import '../../businesses.dart';

class BusinessesResponse {
  BusinessesResponse({
    required this.status,
    required this.businesses,
  });
  int? status;
  List<UnfavoriteBusiness>? businesses;
  String? error;

  BusinessesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    businesses = List.from(json['data'])
        .map((e) => UnfavoriteBusiness.fromJson(e))
        .toList();
  }

  BusinessesResponse.withError(String errorValue) : error = errorValue;
}
