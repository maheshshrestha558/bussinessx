class UpdateBusinessResponse {
  UpdateBusinessResponse({
    required this.status,
  });
  int? status;
  String? error;

  UpdateBusinessResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
  }

  UpdateBusinessResponse.withError(String errorValue) : error = errorValue;
}
