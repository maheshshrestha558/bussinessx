import '../../models.dart';

class BrandResponse {
  BrandResponse({
    required this.status,
    required this.brand,
  });
  late final int status;
  late final Brand brand;
  String? error;

  BrandResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    brand = Brand.fromJson(json['data']);
  }

  BrandResponse.withError(String errorValue) : error = errorValue;

 
}
