import '../../models.dart';

class AllBrandsResponse {
  AllBrandsResponse({
    required this.status,
    required this.brands,
  });
  late final int status;
  List<Brand>? brands;
  String? error;

  AllBrandsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    brands = List.from(json['data']).map((e) => Brand.fromJson(e)).toList();
  }

  AllBrandsResponse.withError(String errorValue) : error = errorValue;
}
