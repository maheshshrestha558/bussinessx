import '../../models.dart';

class SingleBrandResponse {
  SingleBrandResponse({
    required this.status,
    required this.brand,
  });
  int? status;
  Brand? brand;
  String? error;

  SingleBrandResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    brand = Brand.fromJson(json['data']);
  }

  SingleBrandResponse.withError(String errorValue) : error = errorValue;
}

// class SingleBrand {
//   final int id;
//   final String imageThumbnail;
//   final String name;
//   final int views;
//   List<Product> products = [];

//   SingleBrand.fromJson(Map<String, dynamic> json)
//       : id = json['id'],
//         imageThumbnail = json['image_thumbnail'],
//         name = json['name'],
//         views = json['views'],
//         products = List.from(json['products'])
//             .map((e) => Product.fromJson(e))
//             .toList();
// }
