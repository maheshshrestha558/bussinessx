class NotificationsResponse {
  int? status;
  LeadNotificationData? leadNotificationData;
  GeneralNotificationData? generalNotificationData;
  String? error;

  NotificationsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    leadNotificationData =
        LeadNotificationData.fromJson(json['lead_notifications']);
    generalNotificationData =
        GeneralNotificationData.fromJson(json['general_notifications']);
  }

  NotificationsResponse.withError(String errorValue) : error = errorValue;
}

class LeadNotificationData {
  LeadNotificationData({
    required this.leadNotifications,
    required this.total,
  });
  late final List<AppNotification> leadNotifications;
  late final int total;

  LeadNotificationData.fromJson(Map<String, dynamic> json) {
    leadNotifications =
        List.from(json['data']).map((e) => AppNotification.fromJson(e)).toList();
    total = json['total'];
  }
}

class AppNotification {
  AppNotification({
    required this.id,
    required this.type,
    required this.notifiableType,
    required this.notifiableId,
    required this.message,
    required this.createdAt,
  });
  late final String message;
  late final String id;
  late final String type;
  late final String notifiableType;
  late final int notifiableId;
   late final String createdAt;

  AppNotification.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    notifiableType = json['notifiable_type'];
    notifiableId = json['notifiable_id'];
    message = json['data']['message'];
    createdAt = json['created_at'];
    // data = Notification.fromJson(json['data']);
  }
}

class GeneralNotificationData {
  GeneralNotificationData({
    required this.generalNotifications,
    required this.total,
  });
  late final List<AppNotification> generalNotifications;
  late final int total;

  GeneralNotificationData.fromJson(Map<String, dynamic> json) {
    generalNotifications =
        List.from(json['data']).map((e) => AppNotification.fromJson(e)).toList();
    total = json['total'];
  }
}
