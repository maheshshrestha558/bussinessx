class SetNewPasswordResponse {
  int? status;
  String? message;
  String? error;

  SetNewPasswordResponse({this.message, this.error, this.status});

  SetNewPasswordResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
  }

  SetNewPasswordResponse.withError(String errorValue) : error = errorValue;
}
