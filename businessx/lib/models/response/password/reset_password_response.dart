import 'package:b2b_market/common/type_defs.dart';

class ResetPasswordResponse {
  ResetPasswordResponse({required this.message, this.error});
  late final String message;
  String? error;

  ResetPasswordResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
  }

  ResetPasswordResponse.withError(String errorValue) : error = errorValue;

 
}

class PasswordResetParams {
  final String otp, password, contactNumber;
  PasswordResetParams({
    required this.otp,
    required this.password,
    required this.contactNumber,
  });

  KVPair toJson() {
    return {
      'otp': otp,
      'password': password,
      'contact_number': contactNumber,
    };
  }
}
