class SetNewPasswordParams {
  String currentPassword, newPassword, passwordConfirmation;

  SetNewPasswordParams(
    this.currentPassword,
    this.newPassword,
    this.passwordConfirmation,
  );

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['email'] = 'cejebi6962@wolfpat.com';
    data['current_password'] = currentPassword;
    data['new_password'] = newPassword;
    data['password_confirmation'] = passwordConfirmation;
    return data;
  }
}
