import '../models.dart';

class SpecificationsResponse {
  List<Specification> specifications;
  String error;

  SpecificationsResponse.fromJson(Map<String, dynamic> json)
      : specifications = json['data'] != null
            ? (json['data'] as List)
                .map((i) => Specification.fromJson(i))
                .toList()
            : [],
        error = '';

  SpecificationsResponse.withError(String errorValue)
      : error = errorValue,
        specifications = [];
}
