import '../../models.dart';

class LeadResponse {
  LeadResponse({
    required this.status,
    required this.lead,
  });
  late final int status;
  late final Lead lead;
  String? error;

  LeadResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    lead = Lead.fromJson(json['data']);
  }

  LeadResponse.withError(String errorValue) : error = errorValue;
}
