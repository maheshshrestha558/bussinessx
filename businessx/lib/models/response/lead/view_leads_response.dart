import '../../models.dart';

class ViewLeadsResponse {
  int? status;
  Leads? leads;
  String? error;

  ViewLeadsResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    leads = Leads.fromJson(json['data']);
  }

  ViewLeadsResponse.withError(String errorValue) : error = errorValue;
}

class Leads {
  Leads({
    required this.lead,
    required this.meta,
  });
  List<Lead>? lead;
  Meta? meta;

  Leads.fromJson(Map<String, dynamic> json) {
    lead = List.from(json['data']).map((e) => Lead.fromJson(e)).toList();
    meta = Meta.fromJson(json['meta']);
  }
}

class Lead {
  Lead({
    this.id,
    this.buyerId,
    this.sellerId,
    this.productId,
    this.productName,
    this.name,
    this.email,
    this.contact,
    this.source,
    this.buyer,
    this.seller,
    this.product,
    this.leadType,
    this.leadSender,
    this.chatMessage,
  });
  int? id;
  int? buyerId;
  int? sellerId;
  int? productId;
  String? productName;
  String? name;
  String? email;
  String? contact;
  String? source;
  Buyer? buyer;
  Seller? seller;
  Product? product;
  String? leadType;
  String? leadSender;
  String? chatMessage;

  Lead.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    buyerId = json['buyer_id'];
    sellerId = json['seller_id'];
    productId = json['product_id'];
    productName = json['product_name'];
    name = json['name'];
    email = json['email'];
    contact = null;
    source = json['source'];
    buyer = json['buyer'] != null ? Buyer.fromJson(json['buyer']) : null;
    seller = Seller.fromJson(json['seller']);
    product =
        json['product'] != null ? Product.fromJson(json['product']) : null;
    leadType = json['lead_type'];
    leadSender = json['lead_sender'];
    chatMessage = json['chat_message'];
  }
}
