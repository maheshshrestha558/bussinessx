class SellerCategory {
  int? id;
  String? imageThumbnail;
  String? name;
  String? enName;
  int? categoryId;
  int? activeSubCategory;
  int? status;
  String? views;
  String? stackId;

  SellerCategory({
    this.id,
    this.imageThumbnail,
    this.name,
    this.enName,
    this.categoryId,
    this.activeSubCategory,
    this.status,
    this.views,
    this.stackId,
  });

  SellerCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    enName = json['en_name'];
    categoryId = json['category_id'];
    activeSubCategory = json['active_sub_category'];
    status = json['status'];
    views = json['views'];
    stackId = json['stack_id'];
  }
}
