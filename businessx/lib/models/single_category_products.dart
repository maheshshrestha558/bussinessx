// class SingleCategoryProducts {
//   SingleCategoryProducts({
//     required this.id,
//     required this.enName,
//     required this.npName,
//     required this.categoryId,
//     required this.userId,
//     required this.brandId,
//     required this.slug,
//     required this.status,
//     required this.price,
//     required this.unit,
//     required this.description,
//     required this.views,
//     required this.createdAt,
//     required this.updatedAt,
//     required this.name,
//     required this.imageSize,
//     required this.user,
//     required this.imageThumbnail,
//   });
//   late final int id;
//   late final String enName;
//   late final String npName;
//   late final String categoryId;
//   late final String userId;
//   late final String brandId;
//   late final String slug;
//   late final String status;
//   late final String price;
//   late final String unit;
//   late final String description;
//   late final String views;
//   late final String createdAt;
//   late final String updatedAt;
//   late final String name;
//   late final int imageSize;
//   SingleCategoryUser? user;
//   late final String imageThumbnail;

//   SingleCategoryProducts.fromJson(Map<String, dynamic> json) {
//     // id = json['id'];
//     enName = json['en_name'];
//     npName = json['np_name'];
//     // categoryId = json['category_id'];
//     // userId = json['user_id'];
//     // brandId = json['brand_id'];
//     // slug = json['slug'];
//     // status = json['status'];
//     price = json['price'];
//     unit = json['unit'];
//     description = json['description'];
//     views = json['views'];
//     // createdAt = json['created_at'];
//     // updatedAt = json['updated_at'];
//     name = json['name'];
//     user = SingleCategoryUser.fromJson(json['user']);
//     // imageSize = json['image_size'];
//     imageThumbnail = json['image_thumbnail'];
//   }

//   Map<String, dynamic> toJson() {
//     final _data = <String, dynamic>{};
//     // _data['id'] = id;
//     _data['en_name'] = enName;
//     _data['np_name'] = npName;
//     // _data['category_id'] = categoryId;
//     // _data['user_id'] = userId;
//     // _data['brand_id'] = brandId;
//     // _data['slug'] = slug;
//     // _data['status'] = status;
//     _data['price'] = price;
//     _data['unit'] = unit;
//     _data['description'] = description;
//     _data['views'] = views;
//     // _data['created_at'] = createdAt;
//     // _data['updated_at'] = updatedAt;
//     _data['name'] = name;
//     _data['user'] = user!.toJson();
//     // _data['image_size'] = imageSize;
//     _data['image_thumbnail'] = imageThumbnail;
//     return _data;
//   }
// }

// class SingleCategoryUser {
//   SingleCategoryUser({
//     this.id,
//     this.enName,
//     this.npName,
//     this.slug,
//     this.gender,
//     this.designation,
//     this.address,
//     this.email,
//     this.contactNumber,
//     this.alternateContactNumber,
//     // required this.profileImage,
//     // this.emailVerifiedAt,
//     // this.googleId,
//     // required this.verified,
//     // this.verificationOtp,
//     // this.verificationExpiryDate,
//     required this.createdAt,
//     required this.updatedAt,
//     // required this.imageSize,
//     this.imageThumbnail,
//     this.name,
//     required this.business,
//   });
//   int? id;
//   String? enName;
//   String? npName;
//   String? slug;
//   String? gender;
//   String? designation;
//   String? address;
//   String? email;
//   int? contactNumber;
//   int? alternateContactNumber;
//   // late final String profileImage;
//   // late final Null emailVerifiedAt;
//   // late final Null googleId;
//   // late final int verified;
//   // late final Null verificationOtp;
//   // late final Null verificationExpiryDate;
//   String? createdAt;
//   String? updatedAt;
//   // late final int imageSize;
//   String? imageThumbnail;
//   String? name;
//   Business? business;

//   SingleCategoryUser.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     enName = json['en_name'];
//     npName = json['np_name'];
//     slug = json['slug'];
//     gender = json['gender'];
//     designation = json['designation'];
//     address = json['address'];
//     email = json['email'];
//     contactNumber = json['contact_number'];
//     alternateContactNumber = json['alternate_contact_number'];
//     // profileImage = json['profile_image'];
//     // emailVerifiedAt = null;
//     // googleId = null;
//     // verified = json['verified'];
//     // verificationOtp = null;
//     // verificationExpiryDate = null;
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//     // imageSize = json['image_size'];
//     imageThumbnail = json['image_thumbnail'];
//     name = json['name'];
//     business = Business.fromJson(json['business']);
//   }

//   Map<String, dynamic> toJson() {
//     final _data = <String, dynamic>{};
//     _data['id'] = id;
//     _data['en_name'] = enName;
//     _data['np_name'] = npName;
//     _data['slug'] = slug;
//     _data['gender'] = gender;
//     _data['designation'] = designation;
//     _data['address'] = address;
//     _data['email'] = email;
//     _data['contact_number'] = contactNumber;
//     _data['alternate_contact_number'] = alternateContactNumber;
//     // _data['profile_image'] = profileImage;
//     // _data['email_verified_at'] = emailVerifiedAt;
//     // _data['google_id'] = googleId;
//     // _data['verified'] = verified;
//     // _data['verification_otp'] = verificationOtp;
//     // _data['verification_expiry_date'] = verificationExpiryDate;
//     _data['created_at'] = createdAt;
//     _data['updated_at'] = updatedAt;
//     // _data['image_size'] = imageSize;
//     _data['image_thumbnail'] = imageThumbnail;
//     _data['name'] = name;
//     _data['business'] = business!.toJson();
//     return _data;
//   }
// }

// class Business {
//   Business({
//     this.id,
//     this.userId,
//     this.businessTypeId,
//     this.slug,
//     required this.enName,
//     required this.npName,
//     this.ceoName,
//     this.contactNumber,
//     this.alternateContactNumber,
//     this.address,
//     this.email,
//     this.pan,
//     this.importExportCode,
//     this.companyLogo,
//     this.establishedDate,
//     this.websiteUrl,
//     this.facebookUrl,
//     this.instagramUrl,
//     this.whatsappNumber,
//     this.verified,
//     this.createdAt,
//     this.updatedAt,
//     // required this.imageSize,
//     required this.imageThumbnail,
//     required this.name,
//   });
//   int? id;
//   int? userId;
//   int? businessTypeId;
//   String? slug;
//   late final String enName;
//   late final String npName;
//   String? ceoName;
//   int? contactNumber;
//   int? alternateContactNumber;
//   String? address;
//   String? email;
//   int? pan;
//   int? importExportCode;
//   String? companyLogo;
//   String? establishedDate;
//   String? websiteUrl;
//   String? facebookUrl;
//   String? instagramUrl;
//   int? whatsappNumber;
//   int? verified;
//   String? createdAt;
//   String? updatedAt;
//   // late final int imageSize;
//   String? imageThumbnail;
//   String? name;

//   Business.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     userId = json['user_id'];
//     businessTypeId = json['business_type_id'];
//     slug = json['slug'];
//     enName = json['en_name'];
//     npName = json['np_name'];
//     ceoName = json['ceo_name'];
//     contactNumber = json['contact_number'];
//     alternateContactNumber = json['alternate_contact_number'];
//     address = json['address'];
//     email = json['email'];
//     pan = json['pan'];
//     importExportCode = json['import_export_code'];
//     companyLogo = json['company_logo'];
//     establishedDate = json['established_date'];
//     websiteUrl = json['website_url'];
//     facebookUrl = json['facebook_url'];
//     instagramUrl = json['instagram_url'];
//     whatsappNumber = json['whatsapp_number'];
//     verified = json['verified'];
//     createdAt = json['created_at'];
//     updatedAt = json['updated_at'];
//     // imageSize = json['image_size'];
//     imageThumbnail = json['image_thumbnail'];
//     name = json['name'];
//   }

//   Map<String, dynamic> toJson() {
//     final _data = <String, dynamic>{};
//     // _data['id'] = id;
//     // _data['user_id'] = userId;
//     // _data['business_type_id'] = businessTypeId;
//     // _data['slug'] = slug;
//     _data['en_name'] = enName;
//     _data['np_name'] = npName;
//     // _data['ceo_name'] = ceoName;
//     // _data['contact_number'] = contactNumber;
//     // _data['alternate_contact_number'] = alternateContactNumber;
//     _data['address'] = address;
//     // _data['email'] = email;
//     // _data['pan'] = pan;
//     // _data['import_export_code'] = importExportCode;
//     // // _data['company_logo'] = companyLogo;
//     // _data['established_date'] = establishedDate;
//     _data['website_url'] = websiteUrl;
//     // _data['facebook_url'] = facebookUrl;
//     // _data['instagram_url'] = instagramUrl;
//     // _data['whatsapp_number'] = whatsappNumber;
//     // _data['verified'] = verified;
//     // _data['created_at'] = createdAt;
//     // _data['updated_at'] = updatedAt;
//     // _data['image_size'] = imageSize;
//     _data['image_thumbnail'] = imageThumbnail;
//     _data['name'] = name;
//     return _data;
//   }
// }
