import 'models.dart';

class Category {
  int? id;
  String? imageThumbnail;
  String? name;
  List<SubCategory>? subCategories;

  Category({this.id, this.imageThumbnail, this.name, this.subCategories});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    if (json['sub_category'] != null) {
      subCategories = <SubCategory>[];
      json['sub_category'].forEach((v) {
        subCategories?.add(SubCategory.fromJson(v));
      });
    }
  }
}