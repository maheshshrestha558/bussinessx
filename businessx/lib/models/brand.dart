import 'package:equatable/equatable.dart';

import 'models.dart';

class Brand extends Equatable {
  final int id;
  final String imageThumbnail;
  final String name;
  final int views;
  final List<Product>? products;

  const Brand({
    required this.id,
    required this.imageThumbnail,
    required this.name,
    required this.views,
    this.products,
  });

  @override
  String toString() {
    return 'Brand(id: $id, imageThumbnail: $imageThumbnail, name: $name, views: $views, products: $products)';
  }

  factory Brand.fromJson(Map<String, dynamic> json) {
    return Brand(
      id: json['id'],
      imageThumbnail: json['image_thumbnail'],
      name: json['name'],
      views: json['views'],
      products: json['products'] != null
          ? (json['products'] as List).map((e) => Product.fromJson(e)).toList()
          : [],
    );
  }

  @override
  List<Object?> get props => [id, imageThumbnail, name, views, products];
}
