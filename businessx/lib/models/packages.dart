class PackagesResponse {
  int? status;
  List<SubscribedPackage> subscribedPackagesData = [];
  List<AllPackages> allPackages = [];
  String? error;

  PackagesResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];

    if (json['subscribed_packages'] != null) {
      subscribedPackagesData = <SubscribedPackage>[];
      json['subscribed_packages'].forEach((v) {
        subscribedPackagesData.add(SubscribedPackage.fromJson(v));
      });
    }
    if (json['all_packages'] != null) {
      allPackages = <AllPackages>[];
      json['all_packages'].forEach((v) {
        allPackages.add(AllPackages.fromJson(v));
      });
    }
  }

  PackagesResponse.withError(String errorValue) : error = errorValue;
}

class SubscribedPackage {
  int? id;
  int? packageId;
  int? userId;
  int? duration;
  int? subtotal;
  int? subscriptionTypeId;
  DateTime? startDate;
  DateTime? endDate;
  String? status;

  SubscriptionType? subscriptionType;

  

  SubscribedPackage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    packageId = json['package_id'];
    userId = json['user_id'];
    duration = json['duration'];
    subtotal = json['subtotal'];
    subscriptionTypeId = json['subscription_type_id'];
    startDate = DateTime.parse(json['start_date']);
    endDate = DateTime.parse(json['end_date']);
    status = json['status'];

    subscriptionType = json['subscription_type'] != null
        ? SubscriptionType.fromJson(json['subscription_type'])
        : null;
  }
}

class SubscriptionType {
  int? id;
  String? name;

  SubscriptionType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }
}


class AllPackages {
  int? id;
  String? imageThumbnail;
  String? enName;
  String? npName;
  String? name;
  int? packageLevel;
  int? status;
  String? ribbon;
  List<PackageFeatures>? packageFeatures;
  List<SubscriptionTypes>? subscriptionTypes;

  AllPackages({
    this.id,
    this.imageThumbnail,
    this.enName,
    this.npName,
    this.name,
    this.packageLevel,
    this.status,
    this.packageFeatures,
    this.ribbon,
    this.subscriptionTypes,
  });

  AllPackages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    enName = json['en_name'];
    name = json['name'];
    packageLevel = json['package_level'];
    status = json['status'];
    if (json['package_features'] != null) {
      packageFeatures = <PackageFeatures>[];
      json['package_features'].forEach((v) {
        packageFeatures!.add(PackageFeatures.fromJson(v));
      });
    }
    ribbon = json['ribbon'];
    if (json['subscription_types'] != null) {
      subscriptionTypes = <SubscriptionTypes>[];
      json['subscription_types'].forEach((v) {
        subscriptionTypes!.add(SubscriptionTypes.fromJson(v));
      });
    }
  }
}

class PackageFeatures {
  int? id;
  int? packageId;
  String? imageThumbnail;
  String? enName;
  String? enDescription;
  String? npDescription;
  String? name;

  PackageFeatures({
    this.id,
    this.packageId,
    this.imageThumbnail,
    this.enName,
    this.enDescription,
    this.npDescription,
    this.name,
  });

  PackageFeatures.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    packageId = json['package_id'];
    imageThumbnail = json['image_thumbnail'];
    enName = json['en_name'];
    enDescription = json['en_description'];
    npDescription = json['np_description'];
    name = json['name'];
  }
}

class SubscriptionTypes {
  int? id;
  String? name;
  String? value;

  SubscriptionTypes({this.id, this.name, this.value});

  SubscriptionTypes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['value'] = value;
    return data;
  }
}

class PackageSubscriptionResponse {
  int? status;
  String? message;
  String? error;

  PackageSubscriptionResponse({this.status, this.message});

  PackageSubscriptionResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['message'] = message;
    return data;
  }

  PackageSubscriptionResponse.withError(String errorValue) : error = errorValue;
}
