
import 'package:b2b_market/models/models.dart';


class ChildCategoryData {
  int? id;
  String? imageThumbnail;
  String? name;
  List<ChildCategory>? childCategories;

   @override
  toString() {
    return 'child Categories data $childCategories';
  }

  ChildCategoryData({this.id, this.imageThumbnail, this.name, this.childCategories});

  ChildCategoryData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageThumbnail = json['image_thumbnail'];
    name = json['name'];
    if (json['child_category'] != null) {
      childCategories = <ChildCategory>[];
      json['child_category'].forEach((v) {
        childCategories?.add(ChildCategory.fromJson(v));
      });
    }
  }
}