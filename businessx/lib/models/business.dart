class Business {
  int id;
  int verified;
  String imageThumbnail;
  String name;
  String enName;
  String ceoName;
  String contactNumber;
  String address;
  String email;
  String? alternateContactNumber;
  int? pan;
  int? importExportCode;
  String establishedDate;
  String? websiteUrl;
  String? facebookUrl;
  String? instagramUrl;
  String? whatsappNumber;
  int businessTypeId;

  

 

  Business.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        verified = json['verified'],
        imageThumbnail = json['image_thumbnail'],
        name = json['name'],
        enName = json['en_name'],
        ceoName = json['ceo_name'],
        contactNumber = json['contact_number'],
        address = json['address'],
        email = json['email'],
        alternateContactNumber = json['alternate_contact_number'],
        pan = json['pan'],
        importExportCode = json['import_export_code'],
        establishedDate = json['established_date'],
        websiteUrl = json['website_url'],
        facebookUrl = json['facebook_url'],
        instagramUrl = json['instagram_url'],
        whatsappNumber = json['whatsapp_number'],
        businessTypeId = json['business_type_id'];


       
}
