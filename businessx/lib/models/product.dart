import 'models.dart';
import 'product_image.dart';

class Product {
  final int id;
  final String imageThumbnail;
  final String name;
  final dynamic price;
  final String unit;
  final String? description;
  final Seller? seller;
  List<ProductImage> productImages = [];

  @override
  String toString() {
    return 'Product{id: $id, imageThumbnail: $imageThumbnail, name: $name, price: $price, unit: $unit, description: $description, seller: $seller, productImages: $productImages,}';
  }

  Product.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imageThumbnail = json['image_thumbnail'],
        name = json['name'],
        price = json['price'],
        unit = json['unit'],
        description = json['description'],
        seller =
            json['seller'] != null ? Seller.fromJson(json['seller']) : null,
        productImages = (json['files'] as List)
            .map((productImage) => ProductImage.fromJson(productImage))
            .toList();
}
