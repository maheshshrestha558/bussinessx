class Specification {
  int id;
  String name;
  String? value;

  Specification.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        value = json['value'];
}
