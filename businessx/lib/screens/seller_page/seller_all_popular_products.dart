import 'package:flutter/material.dart';

import '../../bloc/popular_product_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/product/popular_product_response.dart';
import '../product/components/components.dart';

class SellerPopularProductsScreen extends StatefulWidget {
  const SellerPopularProductsScreen({Key? key}) : super(key: key);

  @override
  State<SellerPopularProductsScreen> createState() =>
      _SellerPopularProductsScreenState();
}

class _SellerPopularProductsScreenState
    extends State<SellerPopularProductsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 5,
          ),
          const PageTitle(title: "Seller's Top Products"),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: StreamBuilder<PopularProductResponse>(
                stream: popularProductBloc.popularProduct.stream,
                builder: (context, snapshot) {
                  var products = snapshot.data?.products;
                  if (products != null && products.isNotEmpty) {
                    return ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: products.length,
                      itemBuilder: (context, index) {
                        var product = products[index];
                        return ProductListTile(
                          product: product,
                        );
                      },
                    );
                  }
                  if (snapshot.data?.error != null) {
                    return const Center(child: Text('No Products'));
                  }
                  return const ProductShimmer();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
