import 'package:flutter/material.dart';

import '/common/widgets/widgets.dart';
import '../../bloc/about_us_bloc.dart';
import '../../bloc/popular_product_bloc.dart';
import '../../common/logger/logger.dart';
import '../../models/product.dart';
import 'tabs/tabs.dart';

class SellerHomePage extends StatefulWidget {
  final Product product;

  const SellerHomePage({Key? key, required this.product}) : super(key: key);

  @override
  State<SellerHomePage> createState() => _SellerHomePageState();
}

class _SellerHomePageState extends State<SellerHomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  ScrollController? _scrollController;
  bool? _top;
  double? _expandH;
  double? _collapseH;
  final _logger = getLogger(_SellerHomePageState);
  @override
  void initState() {
    super.initState();
    popularProductBloc
        .getSellerPopularProduct(widget.product.seller!.business.id);
    aboutUsBloc.getSellerAboutInfo(widget.product.seller!.business.id);
    _tabController = TabController(length: 4, vsync: this);
    _collapseH = 200;
    _expandH = 200;
    _top = false;
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController!.offset == 0 && !_top!) {
          _logger.e('top true');

          setState(() {
            _top = true;
            _scrollController!.position.correctPixels(_expandH! - _collapseH!);
          });
        } else if (_top! &&
            (_scrollController!.offset > _expandH! - _collapseH!)) {
          _logger.e('top false');

          setState(() {
            _top = false;
            _scrollController!.position.correctPixels(0);
          });
        }
      });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: NestedScrollView(
        physics: const BouncingScrollPhysics(),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              expandedHeight: _top! ? _expandH : _collapseH,
              floating: true,
              pinned: false,
              leading: Padding(
                padding: const EdgeInsets.only(left: 28.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey.shade200,
                  radius: 55,
                  child: IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      color: Colors.black,
                      size: 22,
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ),
              ),
              backgroundColor: Colors.white,
              elevation: 0,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                        widget.product.seller?.imageThumbnail ?? '',
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
            ),
            SliverPersistentHeader(
              delegate: _SliverAppBarDelegate(
                TabBar(
                  controller: _tabController,
                  labelColor: Colors.black,
                  labelStyle: Theme.of(context).textTheme.headlineMedium,
                  tabs: const [
                    Tab(text: 'Home'),
                    Tab(text: 'Products'),
                    Tab(text: 'About'),
                    Tab(text: 'Contact'),
                  ],
                ),
              ),
              pinned: true,
            ),
          ];
        },
        body: TabBarView(
          controller: _tabController,
          children: [
            HomeTab(
              businessID: widget.product.seller!.business.id,
            ),
            ProductsTab(
              businessID: widget.product.seller!.business.id,
            ),
            AboutTab(
              product: widget.product,
            ),
            ContactTab(product: widget.product),
          ],
        ),
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;
  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    return Container(
      color: Colors.white,
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}
