import 'package:flutter/material.dart';

import '../../../common/widgets/widgets.dart';
import '../../../models/product.dart';
import '../../../route_names.dart';

class ContactTab extends StatelessWidget {
  final Product? product;

  const ContactTab({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.fromLTRB(20, 5, 20, 0),
      physics: const BouncingScrollPhysics(),
      children: [
        const SizedBox(height: 5),
        const Text(
          'Reach us',
          style: TextStyle(
            fontSize: 20,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w700,
            // color: Color(0xff393939),
          ),
        ),
        const SizedBox(height: 5),
        const Text(
          'Registered Address:',
          style: TextStyle(
            fontSize: 16,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w600,
            // color: Color(0xff393939),
          ),
        ),
        const SizedBox(height: 5),
        Text(
          product?.seller?.business.address ?? '',
          style: const TextStyle(
            fontSize: 14,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w500,
            color: Color(0xff393939),
          ),
        ),
        const SizedBox(height: 20),
        const Text(
          'Office Address:',
          style: TextStyle(
            fontSize: 16,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w600,
            // color: Color(0xff393939),
          ),
        ),
        const SizedBox(height: 5),
        Text(
          product?.seller?.business.address ?? '',
          style: const TextStyle(
            fontSize: 14,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w500,
            color: Color(0xff393939),
          ),
        ),
        const SizedBox(height: 20),
        Text(
          product?.seller?.business.ceoName ?? '',
          style: const TextStyle(
            decoration: TextDecoration.underline,
            fontSize: 18,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w700,
            // color: Color(0xff393939),
          ),
        ),
        const Text(
          'Chief Executive Officer',
          style: TextStyle(
            fontSize: 14,
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.w400,
            color: Color.fromARGB(255, 3, 3, 3),
          ),
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              child: SizedBox(
                // height: 50,
                width: double.maxFinite,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(const Color(0xff29DE92)),
                    padding: MaterialStateProperty.all(
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 8),
                    ),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(
                      context,
                      RouteName.submitRequirementScreen,
                      arguments: product!,
                    );
                  },
                  child: const Text(
                    'Contact Seller',
                    style: TextStyle(
                      fontSize: 14,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w500,
                      color: Color.fromARGB(255, 247, 244, 244),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: SizedBox(
                // height: 40,
                width: double.maxFinite,
                child: Padding(
                  padding: const EdgeInsets.only(left: 5, right: 5),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(const Color(0xff1D4ED8)),
                      padding: MaterialStateProperty.all(
                        const EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 8,
                        ),
                      ),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(
                        context,
                        RouteName.submitRequirementScreen,
                        arguments: product!,
                      );
                    },
                    child: const Text(
                      'Email Seller',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color.fromARGB(255, 243, 240, 240),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(height: 10),
        Text(
          'Contact Us for More Details',
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .headlineLarge!
              .copyWith(color: const Color(0xff393939)),
        ),
        const SizedBox(height: 10),
        const SubmitRequirementFormBuilder(),
        const SizedBox(height: 20),
      ],
    );
  }
}
