// import 'package:b2b_market/models/child_category.dart';
import 'package:b2b_market/models/response/category/child_categories_with_products_response.dart';
import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../common/shimmer_widget.dart';
import '../../../common/widgets/widgets.dart';
// import '../../../models/response/category/child_categories_with_products_response.dart';
import '../../../route_names.dart';
import '../../home/components/product_grid_card.dart';

class ProductsTab extends StatefulWidget {
  final int businessID;

  const ProductsTab({
    Key? key,
    required this.businessID,
  }) : super(key: key);

  @override
  State<ProductsTab> createState() => _ProductsTabState();
}

class _ProductsTabState extends State<ProductsTab> {
  int page = 1;

  bool? _top;
  double? _expandH;
  double? _collapseH;

  ScrollController? _scrollController;

  @override
  void initState() {
    categoryBloc.getChildCategoriesWithProducts(widget.businessID);
    super.initState();
    _collapseH = 200;
    _expandH = 200;
    _top = false;
    _scrollController = ScrollController()
      ..addListener(() {
        if (_scrollController!.offset == 0 && !_top!) {
          setState(() {
            _top = true;
            _scrollController!.position.correctPixels(_expandH! - _collapseH!);
          });
          if (_scrollController?.position.pixels ==
              _scrollController?.position.maxScrollExtent) {
            if (page != categoryBloc.lastPageChildWithProduct) {
              page++;
              categoryBloc.getChildCategoriesWithProducts(
                widget.businessID,
                page: page,
              );
            }
          }
        } else if (_top! &&
            (_scrollController!.offset > _expandH! - _collapseH!)) {
          setState(() {
            _top = false;
            _scrollController!.position.correctPixels(0);
          });
          if (_scrollController!.position.pixels ==
              _scrollController?.position.maxScrollExtent) {
            if (page != categoryBloc.lastPageChildWithProduct) {
              page++;
              categoryBloc.getChildCategoriesWithProducts(
                widget.businessID,
                page: page,
              );
            }
          }
        }
      });
  }

  @override
  void dispose() {
    _scrollController!.dispose();
    categoryBloc.drainChildWithProduct();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.only(top: 10),
      physics: const BouncingScrollPhysics(),
      controller: _scrollController,
      children: [
        const SizedBox(height: 10),
        StreamBuilder<List<ChildCategory>>(
          stream: categoryBloc.childWithProduct.stream,
          builder: (context, snapshot) {
            var childCategories = snapshot.data;
            if (childCategories != null && childCategories.isNotEmpty) {
              return ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                shrinkWrap: true,
                itemCount: childCategories.length,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  var childCategory = childCategories[index];
                  var products = childCategory.products;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              childCategory.name,
                              style: Theme.of(context).textTheme.headlineLarge,
                            ),
                          ),
                          TextButton(
                            child: Text(
                              'See all',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleLarge!
                                  .copyWith(
                                    color: const Color.fromRGBO(29, 78, 216, 1),
                                  ),
                            ),
                            onPressed: () {
                              productBloc.getProducts(childCategory.id);
                              Navigator.pushNamed(
                                context,
                                RouteName.productsScreen,
                                arguments: childCategory.name,
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: products.length >= 2 ? 360 : 180,
                        child: GridView.builder(
                          physics: const BouncingScrollPhysics(),
                          scrollDirection: Axis.horizontal,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: products.length >= 2 ? 2 : 1,
                            mainAxisExtent:
                                MediaQuery.of(context).size.width / 2,
                          ),
                          itemCount: products.length,
                          itemBuilder: (context, index) {
                            var product = products[index];
                            return ProductGridCard(product);
                          },
                        ),
                      ),
                    ],
                  );
                },
              );
            }
            return const Padding(
              padding: EdgeInsets.only(left: 15.0, right: 15.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: ShimmerWidget.rectangular(
                          height: 15,
                          width: 80,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: ShimmerWidget.rectangular(height: 15, width: 40),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 350,
                    child: RectangularProductShimmer(),
                  ),
                ],
              ),
            );
          },
        )
      ],
    );
  }
}
