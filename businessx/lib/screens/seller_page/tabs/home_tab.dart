import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../route_names.dart';
import 'seller_top_product_builder.dart';

class HomeTab extends StatefulWidget {
  final int businessID;

  const HomeTab({
    Key? key,
    required this.businessID,
  }) : super(key: key);

  @override
  State<HomeTab> createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  @override
  void initState() {
    popularProductBloc.getSellerPopularProduct(widget.businessID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 0),
      physics: const BouncingScrollPhysics(),
      children: [
        const SizedBox(height: 10),
        Text(
          "Seller's Top Products",
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        const SizedBox(height: 5),
        SellerTopProductBuilder(businessID: widget.businessID),
        const SizedBox(height: 15),
        CustomElevatedButton(
          buttonText: 'View All Products',
          color: const Color(0xff29ed92),
          onPressed: () {
            Navigator.pushNamed(
              context,
              RouteName.sellerPopularProductsScreen,
            );
          },
        ),
        const SizedBox(height: 10),
        Text(
          'Tell us your Buy Requirements',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                color: const Color(0xff393939),
              ),
        ),
        const SizedBox(height: 10),
        const SubmitRequirementFormBuilder(),
        const SizedBox(height: 20),
      ],
    );
  }
}
