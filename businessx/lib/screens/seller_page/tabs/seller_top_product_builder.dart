import 'package:flutter/material.dart';

import '../../../bloc/popular_product_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/product/popular_product_response.dart';
import '../../home/components/product_grid_card.dart';

class SellerTopProductBuilder extends StatefulWidget {
  const SellerTopProductBuilder({Key? key, required this.businessID})
      : super(key: key);

  final int businessID;

  @override
  State<SellerTopProductBuilder> createState() =>
      _SellerTopProductBuilderState();
}

class _SellerTopProductBuilderState extends State<SellerTopProductBuilder> {
  @override
  void initState() {
    popularProductBloc.getSellerPopularProduct(widget.businessID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 4,
      child: StreamBuilder<PopularProductResponse>(
        stream: popularProductBloc.sellerPopularProduct.stream,
        builder: (context, snapshot) {
          var products = snapshot.data?.products;
          if (products != null && products.isNotEmpty) {
            return GridView.builder(
              itemCount: products.length,
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1,
                childAspectRatio: 1.12,
              ),
              itemBuilder: (context, index) {
                var product = products[index];
                return ProductGridCard(product);
              },
            );
          }
          if (snapshot.data?.error != null) {
            return const Center(
              child: Text('No Products'),
            );
          }
          return const RectangularProductListShimmer();
        },
      ),
    );
  }
}
