import 'package:flutter/material.dart';

import '../../../bloc/about_us_bloc.dart';
import '../../../models/product.dart';
import '../../../models/response/about_us/about_us_response.dart';

class AboutTab extends StatelessWidget {
  final Product? product;

  const AboutTab({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const rowSpacer = TableRow(
      children: [
        SizedBox(
          height: 8,
        ),
        SizedBox(
          height: 8,
        ),
      ],
    );

    return StreamBuilder<AboutUsResponse>(
      stream: aboutUsBloc.sellerAboutUsInfo.stream,
      builder: (context, AsyncSnapshot<AboutUsResponse> snapshot) {
        var info = snapshot.data?.aboutUsInfo;
        return ListView(
          physics: const BouncingScrollPhysics(),
          padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
          children: [
            const SizedBox(height: 10),
            Text(
              info?.profileDescription ?? 'No Description Available',
              textAlign: TextAlign.justify,
              style: const TextStyle(
                fontSize: 16,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'Additional Information',
              style: TextStyle(
                fontSize: 18,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Table(
              children: [
                TableRow(
                  children: [
                    const Text(
                      'Nature of Business:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      product?.seller?.designation ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    const Text(
                      'Company CEO:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      product?.seller?.business.ceoName ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    const Text(
                      'Registered Address:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      product?.seller?.business.address ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    const Text(
                      'Number of Employee:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      info?.numberOfEmployees.toString() ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    const Text(
                      'Years of Establishment:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      product?.seller?.business.establishedDate ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    const Text(
                      'Annual Turnover:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      info?.annualTurnover ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Stuatory Profile',
              style: TextStyle(
                fontSize: 18,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w600,
                // color: Color(0xff393939),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Table(
              children: [
                TableRow(
                  children: [
                    const Text(
                      'EXIM Code:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      product?.seller?.business.importExportCode.toString() ??
                          '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    const Text(
                      'Pan No:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      product?.seller?.business.pan.toString() ?? '',
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              'Packaging, Payment & Shipment',
              style: TextStyle(
                fontSize: 18,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w600,
                // color: Color(0xff393939),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Table(
              children: [
                TableRow(
                  children: [
                    const Text(
                      'Packaging Mode:',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                    Text(
                      info?.packagingMode ?? "As Per Customer's Choice",
                      style: const TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                        color: Color(0xff393939),
                      ),
                    ),
                  ],
                ),
                rowSpacer,
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        );
      },
    );
  }
}
