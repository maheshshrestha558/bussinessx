class BottomNavigationBarItemLabel {
  BottomNavigationBarItemLabel._();
  static String home = 'Home';
  static String category = 'Category';
  static String message = 'Message';
  static String profile = 'Profile';
}
