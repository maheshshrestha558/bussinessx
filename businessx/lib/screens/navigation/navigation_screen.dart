import 'package:b2b_market/mixin/no_internet_mixin.dart';
import 'package:flutter/material.dart';
import 'package:upgrader/upgrader.dart';

import '../../bloc/business_bloc.dart';
import '../../common/utils/faded_indexed_stack.dart';
import '../../common/widgets/widgets.dart';
import '../../repositories/repositories.dart';
import '../../route_names.dart';
import '../screens.dart';
import 'components/bottom_navigation_bar_item_label.dart';

class NavigationScreen extends StatefulWidget {
  final int selectedIndex;

  const NavigationScreen({Key? key, this.selectedIndex = 0}) : super(key: key);

  @override
  State<NavigationScreen> createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen>
    with NoInternetMixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  int selectedIndex = 0;

  @override
  initState() {
    super.initState();
    selectedIndex = widget.selectedIndex;
    businessesBloc.getBusiness();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      check = true;
    } else {
      check = false;
    }
  }

  final screens = [
    const HomeScreen(),
    const RootCategoryScreen(),
    if (baseRepository.isUserLoggedIn) const MessageListScreen(),
    const ProfileBuilder(),
  ];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (selectedIndex == 0 && key.currentState!.isDrawerOpen != true) {
          showExitPopup(context);
        } else {
          Navigator.pushNamed(
            context,
            RouteName.navigationScreen,
          );
          return Future.value(false);
        }
        return Future.value(true);
      },
      child: Scaffold(
        key: key,
        drawer: const MyAppDrawer(),
        appBar: const CustomAppBar(),
        body: UpgradeAlert(
          upgrader: Upgrader(
            debugLogging: true,
            canDismissDialog: true,
            shouldPopScope: () => true,
            onIgnore: () => true,
            onLater: () => true,
            onUpdate: () => true,
            showLater: true,
            showIgnore: true,
          ),
          child: FadeIndexedStack(
            index: selectedIndex,
            children: [...screens],
          ),
        ),
        bottomNavigationBar: Theme(
          data: Theme.of(context)
              .copyWith(canvasColor: const Color.fromARGB(255, 169, 238, 215)),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            selectedItemColor: const Color.fromARGB(255, 24, 116, 87),
            unselectedItemColor: const Color.fromARGB(255, 89, 92, 88),
            selectedLabelStyle: Theme.of(context).textTheme.displayLarge,
            unselectedLabelStyle: Theme.of(context).textTheme.displayLarge,
            currentIndex: selectedIndex,
            showUnselectedLabels: true,
            onTap: (int index) {
              setState(() {
                selectedIndex = index;
                FocusScope.of(context).requestFocus(FocusNode());
              });
            },
            items: [
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.home_outlined,
                ),
                activeIcon: const Icon(
                  Icons.home,
                ),
                label: BottomNavigationBarItemLabel.home,
              ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.category_outlined,
                ),
                activeIcon: const Icon(
                  Icons.category,
                ),
                label: BottomNavigationBarItemLabel.category,
              ),
              if (baseRepository.isUserLoggedIn)
                BottomNavigationBarItem(
                  icon: const Icon(
                    Icons.message_outlined,
                  ),
                  activeIcon: const Icon(
                    Icons.message,
                  ),
                  label: BottomNavigationBarItemLabel.message,
                ),
              BottomNavigationBarItem(
                icon: const Icon(
                  Icons.account_circle_outlined,
                ),
                activeIcon: const Icon(
                  Icons.account_circle,
                ),
                label: BottomNavigationBarItemLabel.profile,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
