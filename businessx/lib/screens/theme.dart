import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class B2BTheme {
  static TextTheme textTheme = TextTheme(
    displayLarge: GoogleFonts.rubik(
      fontSize: 10,
      fontWeight: FontWeight.w500,
      color: Colors.black,
    ),
    displayMedium: GoogleFonts.rubik(
      fontSize: 25,
      fontWeight: FontWeight.w500,
      color: Colors.black,
    ),
    displaySmall: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      color: const Color(0xff0077b6),
    ),
    headlineMedium: GoogleFonts.rubik(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      color: const Color(0xff393939),
    ),
    headlineSmall: GoogleFonts.rubik(
      fontSize: 16,
      fontWeight: FontWeight.w500,
      color: Colors.black,
    ),
    headlineLarge: GoogleFonts.rubik(
      fontSize: 20,
      fontWeight: FontWeight.w500,
      color: Colors.black,
    ),
    titleMedium: GoogleFonts.rubik(
      fontSize: 12,
      fontWeight: FontWeight.w500,
      color: Colors.black,
      decoration: TextDecoration.none,
    ),
    titleSmall: GoogleFonts.rubik(
      fontSize: 12,
      fontWeight: FontWeight.w400,
      color: Colors.black,
      decoration: TextDecoration.none,
    ),
    labelLarge: GoogleFonts.rubik(
      fontSize: 20,
      fontWeight: FontWeight.w500,
      color: Colors.white,
    ),
    bodyLarge: GoogleFonts.rubik(
      fontSize: 14,
      color: Colors.black,
      fontWeight: FontWeight.w500,
      decoration: TextDecoration.none,
    ),
    bodyMedium: GoogleFonts.rubik(
      fontSize: 20,
      fontWeight: FontWeight.normal,
      color: Colors.black,
      decoration: TextDecoration.none,
    ),
  );

  static ThemeData light() {
    return ThemeData(
      brightness: Brightness.light,
      checkboxTheme: CheckboxThemeData(
        fillColor: MaterialStateColor.resolveWith(
          (states) {
            return Colors.white;
          },
        ),
      ),
      colorScheme:
          ColorScheme.fromSwatch().copyWith(secondary: const Color(0xff29ed92)),
      inputDecorationTheme: const InputDecorationTheme(
          // border: OutlineInputBorder(),
          ),
      scrollbarTheme: ScrollbarThemeData(
        thumbColor: MaterialStateProperty.all<Color>(Colors.green.shade300),
        crossAxisMargin: 2,
        interactive: true,
        thickness: MaterialStateProperty.all(8),
        trackColor: MaterialStateProperty.all<Color>(Colors.red.shade300),
        trackBorderColor:
            MaterialStateProperty.all<Color>(Colors.yellow.shade300),
        radius: const Radius.circular(5),
      ),
      textTheme: textTheme,
    );
  }
}
