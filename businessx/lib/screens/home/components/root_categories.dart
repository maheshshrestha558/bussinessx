import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/category/root_with_sub_categories_response.dart';
import 'components.dart';

class RootCategories extends StatelessWidget {
  const RootCategories({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      height: 86,
      child: StreamBuilder<RootWithSubCategoriesResponse>(
        stream: categoryBloc.rootWithSubCategories.stream,
        builder: (context, snapshot) {
          var rootCategories =
              snapshot.data?.rootCategories?.rootWithSubCategory;
          if (rootCategories != null && rootCategories.isNotEmpty) {
            rootCategories.sort((a, b) => a.name.compareTo(b.name));
            return ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(width: 10),
              itemCount: rootCategories.length,
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                final rootCategory = rootCategories[index];
                return RootWithSubCategoryCard(
                  rootCategory: rootCategory,
                );
              },
            );
          }
          return const CircularCategoryShimmer();
        },
      ),
    );
  }
}
