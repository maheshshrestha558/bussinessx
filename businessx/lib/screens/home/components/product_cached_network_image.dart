import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../models/models.dart';

class ProductCachedNetworkImage extends StatelessWidget {
  const ProductCachedNetworkImage({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: product.id,
      child: CachedNetworkImage(
        placeholder: (context, _) => ErrorAndPlaceholder(
          product.name,
        ),
        imageUrl: product.imageThumbnail,
        imageBuilder: (context, imageProvider) => Container(
          height: 120,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4),
              topRight: Radius.circular(4),
            ),
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        errorWidget: (context, url, error) => ErrorAndPlaceholder(
          product.name,
        ),
      ),
    );
  }
}

class ErrorAndPlaceholder extends StatelessWidget {
  const ErrorAndPlaceholder(
    this.productName, {
    Key? key,
  }) : super(key: key);

  final String productName;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Center(
        child: Text(productName[0]),
      ),
    );
  }
}

class B2CErrorAndPlaceholder extends StatelessWidget {
  const B2CErrorAndPlaceholder(
    this.productName, {
    Key? key,
  }) : super(key: key);

  final String productName;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 75,
      child: Center(
        child: Text(productName[0]),
      ),
    );
  }
}
