import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../common/shimmer_widget.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/category/root_with_sub_categories_response.dart';
import '../../../route_names.dart';
import '../../chat/components/my_cached_network_image.dart';

class FeaturedCategories extends StatefulWidget {
  const FeaturedCategories({
    Key? key,
  }) : super(key: key);

  @override
  State<FeaturedCategories> createState() => _FeaturedCategoriesState();
}

class _FeaturedCategoriesState extends State<FeaturedCategories> {
  final _scrollController = ScrollController();

  int page = 1;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (page != categoryBloc.lastPageRootWithSub) {
          page++;
          categoryBloc.getRootWithSubCategories(page: page);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<RootWithSubCategory>>(
      stream: categoryBloc.rootWithSubCategory.stream,
      builder: (context, snapshot) {
        var rootCategories = snapshot.data;
        if (rootCategories != null && rootCategories.isNotEmpty) {
          return ListView.builder(
            shrinkWrap: true,
            itemCount: rootCategories.length,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              var rootCategory = rootCategories[index];
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RootCategoryTitle(
                    title: rootCategory.name,
                    onPressed: () async {
                      Feedback.forTap(context);
                      showCustomProgressDialog(context, 'Loading...');
                      var response =
                          await categoryBloc.getSubCategories(rootCategory.id);
                      Navigator.of(context).pop();
                      if (response.error == null) {
                        Navigator.pushNamed(
                          context,
                          RouteName.subCategoryScreen,
                          arguments: rootCategory.name,
                        );
                      } else {
                        ShowToast.error(
                          response.error,
                        );
                      }
                    },
                  ),
                  SizedBox(
                    height: 95,
                    child: ListView.separated(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      separatorBuilder: (_, __) => const SizedBox(
                        width: 10,
                      ),
                      scrollDirection: Axis.horizontal,
                      itemCount: rootCategory.subCategories.length,
                      itemBuilder: (_, index) {
                        var subCategories = rootCategory.subCategories;
                        if (subCategories.isNotEmpty) {
                          subCategories
                              .sort((a, b) => a.name.compareTo(b.name));
                          var subCategory = subCategories[index];
                          return SubCategoryCard(subCategory);
                        }
                        return const Text('No sub category');
                      },
                    ),
                  ),
                ],
              );
            },
          );
        }
        return const Padding(
          padding: EdgeInsets.only(left: 16.0, right: 16.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: ShimmerWidget.rectangular(
                      height: 15,
                      width: 80,
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: ShimmerWidget.rectangular(height: 15, width: 40),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 130,
                child: CircularCategoryShimmer(),
              ),
            ],
          ),
        );
      },
    );
  }
}

class RootCategoryTitle extends StatelessWidget {
  const RootCategoryTitle({
    required this.title,
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  final String title;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      title: FittedBox(
        alignment: Alignment.centerLeft,
        fit: BoxFit.scaleDown,
        child: Text(
          title,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: const TextStyle(
            fontSize: 18,
            fontFamily: 'Rubik',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.bold,

            // color: Color.fromRGBO(57, 57, 57, 1),
          ),
        ),
      ),
      trailing: TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.zero,
          minimumSize: Size.zero,
          alignment: Alignment.centerLeft,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
        ),
        onPressed: onPressed,
        child: const Padding(
          padding: EdgeInsets.only(
            left: 8,
            top: 8,
            bottom: 8,
          ),
          child: Text(
            'See all',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: Color.fromRGBO(29, 78, 216, 1),
            ),
          ),
        ),
      ),
    );
  }
}

class SubCategoryCard extends StatelessWidget {
  final SubCategory subCategory;

  const SubCategoryCard(
    this.subCategory, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getChildCategories(subCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.childCategoryScreen,
            arguments: subCategory.name,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Column(
        children: [
          SizedBox(
            height: 65,
            width: 65,
            child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 30,
              child: MyCachedNetworkImage(
                filePath: subCategory.iconImageThumbnail,
              ),
            ),
          ),
          const SizedBox(
            height: 3,
          ),
          SizedBox(
            width: 60,
            child: Text(
              subCategory.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
        ],
      ),
    );
  }
}
