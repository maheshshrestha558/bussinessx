import 'package:flutter/material.dart';

import '../../../route_names.dart';

class SeeAllCategories extends StatefulWidget {
  const SeeAllCategories({
    Key? key,
  }) : super(key: key);

  @override
  State<SeeAllCategories> createState() => _SeeAllCategoriesState();
}

class _SeeAllCategoriesState extends State<SeeAllCategories> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, top: 5, right: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'See All Categories',
            style: TextStyle(
              fontStyle: FontStyle.normal,
              fontSize: 20,
              fontWeight: FontWeight.bold,
              fontFamily: 'Rubik',
            ),
          ),
          IconButton(
            icon: const Icon(
              Icons.arrow_forward,
              color: Color.fromARGB(244, 29, 79, 216),
            ),
            onPressed: () => Navigator.pushReplacementNamed(
              context,
              RouteName.navigationScreen,
              arguments: 1,
            ),
          ),
        ],
      ),
    );
    // ListTile(
    //   title: const Text(
    //     'See All Categories',
    //     style: TextStyle(
    //       fontStyle: FontStyle.normal,
    //       fontSize: 20,
    //       fontWeight: FontWeight.bold,
    //       fontFamily: 'Rubik',
    //     ),
    //   ),
    //   trailing: IconButton(
    //     icon: const Icon(
    //       Icons.arrow_forward,
    //       color: Color(0xff29DE92),
    //     ),
    //     onPressed: () => Navigator.pushReplacementNamed(
    //       context,
    //       RouteName.navigationScreen,
    //       arguments: 1,
    //     ),
    //   ),
    // );
  }
}
