import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../models/response/category/root_with_sub_categories_response.dart';
import '../../../route_names.dart';
import '../../chat/components/my_cached_network_image.dart';

class RootWithSubCategoryCard extends StatelessWidget {
  final RootWithSubCategory rootCategory;

  const RootWithSubCategoryCard({Key? key, required this.rootCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getSubCategories(rootCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.subCategoryScreen,
            arguments: rootCategory.name,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Column(
        children: [
          SizedBox(
            height: 60,
            width: 60,
            child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 30,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: MyCachedNetworkImage(
                  filePath: rootCategory.iconImageThumbnail,
                ),
              ),
            ),
          ),
          SizedBox(
            width: 60,
            child: Text(
              rootCategory.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: const TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
