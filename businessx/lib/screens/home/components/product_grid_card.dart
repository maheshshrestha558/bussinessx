import 'package:flutter/material.dart';

import '../../../models/models.dart';
import '../../../route_names.dart';

class ProductGridCard extends StatelessWidget {
  const ProductGridCard(
    this.product, {
    Key? key,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(
            context,
            RouteName.productDetailScreen,
            arguments: product,
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.width / 2.25,
              width: MediaQuery.of(context).size.width / 2.2,
              child: ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                ),
                child: Image.network(
                  product.imageThumbnail,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            // ProductCachedNetworkImage(product: product),

            Text(
              product.name,
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                fontFamily: 'Rubik',
              ),
            ),
            Text(
              ' Rs.${product.price} onwards',
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                fontFamily: 'Rubik',
                color: Color(0xff393939),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
