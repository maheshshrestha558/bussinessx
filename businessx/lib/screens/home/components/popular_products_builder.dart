import 'package:flutter/material.dart';

import '../../../bloc/popular_product_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/product/popular_product_response.dart';
import 'components.dart';

class PopularProducts extends StatefulWidget {
  const PopularProducts({Key? key}) : super(key: key);

  @override
  State<PopularProducts> createState() => _PopularProductsState();
}

class _PopularProductsState extends State<PopularProducts> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              'Popular Products',
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontFamily: 'Rubik',
              ),
            ),
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height / 2,
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: StreamBuilder<PopularProductResponse>(
            stream: popularProductBloc.popularProduct.stream,
            builder: (context, snapshot) {
              var products = snapshot.data?.products;
              if (products != null && products.isNotEmpty) {
                return GridView.builder(
                  itemCount: products.length,
                  scrollDirection: Axis.horizontal,
                  physics: const BouncingScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 1.12,
                    // mainAxisExtent: MediaQuery.of(context).size.width / 2.2,
                    crossAxisCount: 2,
                  ),
                  itemBuilder: (context, index) {
                    var product = products[index];
                    return ProductGridCard(product);
                  },
                );
              }
              return const SizedBox(
                height: 360,
                child: RectangularProductShimmer(),
              );
            },
          ),
        ),
      ],
    );
  }
}
