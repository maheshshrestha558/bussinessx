import 'package:b2b_market/bloc/product_bloc.dart';
import 'package:b2b_market/models/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';

class Banners extends StatefulWidget {
  const Banners({
    Key? key,
  }) : super(key: key);

  @override
  State<Banners> createState() => _BannersState();
}

class _BannersState extends State<Banners> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: StreamBuilder<List<Product>>(
        stream: productBloc.newArrivalProducts,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var allProducts = snapshot.data;
            if (allProducts != null && allProducts.isNotEmpty) {
              return CarouselSlider.builder(
                enableAutoSlider: true,
                unlimitedMode: true,
                itemCount: allProducts.length,
                slideBuilder: (index) {
                  var product = allProducts.first;
                  return BannerCachedNetworkImage(
                    filePath: product.imageThumbnail,
                  );
                },
                slideIndicator: CircularSlideIndicator(
                  indicatorBackgroundColor: Colors.grey,
                  currentIndicatorColor: Colors.white,
                  padding: const EdgeInsets.only(bottom: 18),
                ),
                autoSliderDelay: const Duration(seconds: 5),
              );
            }
          }

          return Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: SizedBox(
              height: 250,
              width: double.infinity,
              child: Image.asset(
                'assets/homeimage/1.png',
                fit: BoxFit.fill,
              ),
            ),
          );
          // const Align(
          //   alignment: Alignment.centerLeft,
          //   child: ShimmerWidget.rectangular(
          //     height: 250,
          //     width: 550,
          //   ),
          // );
        },
      ),
    );
  }
}

class BannerCachedNetworkImage extends StatelessWidget {
  const BannerCachedNetworkImage({
    Key? key,
    required this.filePath,
  }) : super(key: key);

  final String filePath;

  @override
  Widget build(BuildContext context) {
    return Image.network(
      filePath,
      fit: BoxFit.cover,
    );
    // CachedNetworkImage(
    //   imageUrl: filePath,
    //   fit: BoxFit.cover,
    //   errorWidget: (context, url, error) {
    //     return const Align(
    //       alignment: Alignment.centerLeft,
    //       child: ShimmerWidget.rectangular(
    //         height: 150,
    //         width: 550,
    //       ),
    //     );
    //   },
    // );
  }
}
