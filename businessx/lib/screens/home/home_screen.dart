import 'package:b2b_market/bloc/business_bloc.dart';
import 'package:b2b_market/models/response/business/single_business_response.dart';
import 'package:flutter/material.dart';

import '../../bloc/bloc.dart';
import '../../bloc/package_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/user/user_response.dart';
import 'components/components.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final widgetKey = GlobalKey<SubmitRequirementFormState>();

  @override
  void initState() {
    super.initState();
    popularProductBloc.getPopularProduct();
  }

  Future<void> _onRefresh() async {
    [
      packageBloc.getPackages(),
      bannerBloc.getBanner(),
      brandBloc.fetchAll(),
      categoryBloc.getRootWithSubCategories(),
      platformDetailsBloc.getPlatformDetails(),
      popularProductBloc.getPopularProduct(),
      usersBloc.getUser(),
      widgetKey.currentState?.requirementFormKey.currentState?.reset()
    ];
  }

  @override
  void dispose() {
    super.dispose();
    usersBloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      color: const Color.fromRGBO(52, 211, 153, 1),
      onRefresh: _onRefresh,
      child: SingleChildScrollView(
        child: Column(
          children: [
            const Banners(),
            const SeeAllCategories(),
            const RootCategories(),
            const PopularProducts(),
            const Brands(),
            const FeaturedCategories(),
            StreamBuilder<SingleBusinessResponse>(
              stream: businessesBloc.business.stream,
              builder:
                  (context, AsyncSnapshot<SingleBusinessResponse> snapshot) {
                var business = snapshot.data?.business;
                if (business != null) {
                  return Container();
                }
                return const BecomeSellerMessage();
              },
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 28, horizontal: 10),
              child: StreamBuilder<UserResponse>(
                stream: usersBloc.user.stream,
                builder: (context, snapshot) {
                  var user = snapshot.data?.user;
                  if (user != null) {
                    return SubmitRequirementForm(
                      user: user,
                      key: UniqueKey(),
                    );
                  }
                  return SubmitRequirementForm(
                    key: widgetKey,
                  );
                },
              ),
            ),
            const PlatformDetails()
          ],
        ),
      ),
    );
  }
}
