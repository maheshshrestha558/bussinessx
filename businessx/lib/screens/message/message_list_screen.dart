import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../bloc/lead_bloc.dart';
import '../../common/widgets/shimmer_widgets/message_shimmer.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/lead/view_leads_response.dart';
import '../chat/chat_screen.dart';

class MessageListScreen extends StatefulWidget {
  const MessageListScreen({Key? key}) : super(key: key);

  @override
  State<MessageListScreen> createState() => _MessageListScreenState();
}

class _MessageListScreenState extends State<MessageListScreen> {
  late int? _userID;
  late String? _token;

  @override
  initState() {
    super.initState();
    _getToken();
    leadBloc
      ..drainLead()
      ..viewLead();
  }

  _getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _userID = prefs.getInt('userID');
    _token = prefs.getString('token');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 15.0,
        right: 15.0,
        top: 10,
      ),
      child: Column(
        children: [
          const PageTitle(
            title: 'Messages',
            showBackButton: false,
          ),
          const SizedBox(height: 10),
          Expanded(
            child: StreamBuilder<List<Lead>>(
              stream: leadBloc.lead.stream,
              builder: (context, AsyncSnapshot<List<Lead>> snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const MessageShimmer();
                }
                var leads = snapshot.data;
                if (leads != null && leads.isNotEmpty == true) {
                  return ListView.separated(
                    physics: const BouncingScrollPhysics(),
                    separatorBuilder: (_, __) => const Divider(),
                    itemCount: leads.length,
                    itemBuilder: (context, index) {
                      var lead = leads[index];
                      return OpenContainer(
                        closedColor: Colors.transparent,
                        closedElevation: 0,
                        openElevation: 0,
                        transitionDuration: const Duration(milliseconds: 800),
                        closedBuilder: (context, action) => ListTile(
                          onTap: action,
                          leading: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: const Color(0xff77dce1),
                                width: 2,
                              ),
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                            child: CachedNetworkImage(
                              placeholder: (context, url) => Center(
                                child:
                                    Text(lead.seller?.business.name[0] ?? ''),
                              ),
                              imageUrl:
                                  lead.seller?.business.imageThumbnail ?? '',
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                padding: const EdgeInsets.all(10),
                                height: 70,
                                width: 70,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              errorWidget: (context, url, error) => Center(
                                child: Text(lead.seller!.business.name[0]),
                              ),
                            ),
                          ),
                          title: Text(
                            lead.seller?.business.name ?? '',
                            style: Theme.of(context).textTheme.headlineLarge,
                          ),
                          subtitle: Text(
                            lead.chatMessage ?? '',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        openBuilder: (context, _) => ChatScreen(
                          lead: lead,
                          userId: _userID!,
                          token: _token!,
                          title: lead.seller?.business.name ?? '',
                        ),
                      );
                    },
                  );
                }
                if (leads?.isEmpty == true) {
                  return Center(
                    child: Text(
                      'No Messages Found',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  );
                }
                return const MessageShimmer();
              },
            ),
          )
        ],
      ),
    );
  }
}
