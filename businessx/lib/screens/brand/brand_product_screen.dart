import 'package:flutter/material.dart';

import '../../bloc/brand_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/response.dart';
import '../product/components/product_list_tile.dart';

class BrandProductScreen extends StatefulWidget {
  final int brandID;

  const BrandProductScreen({Key? key, required this.brandID}) : super(key: key);

  @override
  State<BrandProductScreen> createState() => _BrandProductScreenState();
}

class _BrandProductScreenState extends State<BrandProductScreen> {
  @override
  void initState() {
    brandBloc.fetch(widget.brandID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          const PageTitle(title: 'Products'),
          Expanded(
            child: StreamBuilder<SingleBrandResponse>(
              stream: brandBloc.brandResponse.stream,
              builder: (context, AsyncSnapshot<SingleBrandResponse> snapshot) {
                var products = snapshot.data?.brand?.products;
                if (products != null && products.isNotEmpty) {
                  return ListView.builder(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 10,
                    ),
                    itemCount: products.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      var product = products[index];
                      return ProductListTile(product: product);
                    },
                  );
                }
                if (products != null && products.isEmpty) {
                  return Center(
                    child: Text(
                      'No Products Found',
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  );
                }
                return const ProductShimmer();
              },
            ),
          )
        ],
      ),
    );
  }
}
