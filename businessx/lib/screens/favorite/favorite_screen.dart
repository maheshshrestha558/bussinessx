import 'package:flutter/material.dart';

import '../../bloc/favourite_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/favorite/favourite_response.dart';
import '../../models/response/favorite/unfavorited_response.dart';
import 'components/favourite_seller_card.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  UnfavoriteBusiness? unfavoriteBusiness;

  final _scrollController = ScrollController();

  int page = 1;

  @override
  void initState() {
    favouriteBloc.getUnfavorite();
    favouriteBloc.getFavourites();
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (page != favouriteBloc.lastPage) {
          page++;
          favouriteBloc.getFavourites(page: page);
        }
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    favouriteBloc.drainFavorite();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        const PageTitle(
          title: 'Favorites',
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          'Choose Your Favorite',
          style: Theme.of(context).textTheme.bodySmall,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          padding: const EdgeInsets.only(left: 8),
          decoration: BoxDecoration(
            border: Border.all(
              color: Theme.of(context).dividerColor,
            ),
            borderRadius: BorderRadius.circular(4),
          ),
          child: StreamBuilder<UnfavoriteResponse>(
            stream: favouriteBloc.unfavorite.stream,
            builder: (context, snapshot) {
              var businesses = snapshot.data?.unfavoriteBusiness;
              if (businesses != null && businesses.isNotEmpty) {
                return ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButton<UnfavoriteBusiness>(
                    hint: const Text('Select One'),
                    underline: Container(),
                    elevation: 0,
                    icon: const Icon(Icons.arrow_drop_down),
                    value: unfavoriteBusiness,
                    onChanged: (value) {
                      unfavoriteBusiness = value;
                      FocusScope.of(context).unfocus();
                      setState(() {});
                    },
                    items: businesses.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value.name),
                        onTap: () =>
                            favouriteBloc.addToFavorite(context, value.id),
                      );
                    }).toList(),
                  ),
                );
              }

              return const SizedBox.shrink();
            },
          ),
        ),
        const SizedBox(
          height: 25,
        ),
        Text(
          'My Favorites',
          style: Theme.of(context).textTheme.bodySmall,
        ),
        const SizedBox(
          height: 5,
        ),
        StreamBuilder<List<Favorite>>(
          stream: favouriteBloc.favoriteList.stream,
          builder: (context, snapshot) {
            var favorites = snapshot.data;
            if (favorites != null && favorites.isNotEmpty) {
              return Expanded(
                child: ListView.builder(
                  controller: _scrollController,
                  itemCount: favorites.length,
                  physics: const BouncingScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    var seller = favorites[index].seller;
                    return FavouriteSellerCard(
                      seller: seller,
                      favouriteId: favorites[index].id,
                    );
                  },
                ),
              );
            }
            return const Center(child: Text('No Favourites Added'));
          },
        ),
      ],
    );
  }
}
