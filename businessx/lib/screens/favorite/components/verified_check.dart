import 'package:flutter/material.dart';

class VerifiedCheck extends StatelessWidget {
  final bool isVerified;

  const VerifiedCheck({
    Key? key,
    required this.isVerified,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.verified,
          size: 20,
          color: isVerified ? const Color(0xff29ed92) : Colors.grey,
        ),
        const SizedBox(
          width: 5,
        ),
        Text(
          isVerified ? 'Verified Seller' : 'Not Verified',
          style: Theme.of(context).textTheme.bodySmall,
        ),
      ],
    );
  }
}
