import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../bloc/favourite_bloc.dart';
import '../../../common/extensions/string_extensions.dart';
import '../../../models/response/favorite/favourite_response.dart';
import 'verified_check.dart';

class FavouriteSellerCard extends StatelessWidget {
  final Seller seller;
  final int favouriteId;
  const FavouriteSellerCard({
    Key? key,
    required this.seller,
    required this.favouriteId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Stack(
        children: [
          Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  Center(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: const Color(0xff77dce1),
                          width: 2,
                        ),
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: CachedNetworkImage(
                        placeholder: (context, url) => Center(
                          child: Text(seller.name[0]),
                        ),
                        imageUrl: seller.imageThumbnail,
                        imageBuilder: (context, imageProvider) => Container(
                          padding: const EdgeInsets.all(10),
                          height: 90,
                          width: 90,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Center(
                          child: Text(seller.name[0]),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    seller.name,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  VerifiedCheck(
                    isVerified: seller.verified == 1,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0),
                    child: Table(
                      children: [
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'CEO Name:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.ceoName,
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Contact Number:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.contactNumber,
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Alternate Contact Number:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.alternateContactNumber.showDashIfNull(),
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'WhatsApp Number:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.whatsappNumber.showDashIfNull(),
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Email:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.email,
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Address:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.address,
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Website:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.websiteUrl.showDashIfNull(),
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Facebook:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.facebookUrl.showDashIfNull(),
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                        TableRow(
                          children: [
                            Text(
                              'Instagram:',
                              style: Theme.of(context)
                                  .textTheme
                                  .titleSmall!
                                  .copyWith(
                                    color: const Color(
                                      0xff393939,
                                    ),
                                  ),
                            ),
                            Text(
                              seller.instagramUrl.showDashIfNull(),
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                          ],
                        ),
                        rowSpacer,
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: IconButton(
              icon: const Icon(
                Icons.close,
                color: Colors.red,
              ),
              onPressed: () {
                favouriteBloc.deleteFavourite(
                  context,
                  favouriteId,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

const rowSpacer = TableRow(
  children: [
    SizedBox(
      height: 5,
    ),
    SizedBox(
      height: 5,
    )
  ],
);
