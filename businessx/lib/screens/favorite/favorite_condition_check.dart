import 'package:flutter/material.dart';

import '../../common/widgets/widgets.dart';
import '../../repositories/repositories.dart';
import '../business_profile/components/no_seller_profile_page.dart';
import '../screens.dart';

class FavoriteConditionCheck extends StatefulWidget {
  const FavoriteConditionCheck({Key? key}) : super(key: key);

  @override
  State<FavoriteConditionCheck> createState() => _FavoriteConditionCheckState();
}

class _FavoriteConditionCheckState extends State<FavoriteConditionCheck> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: baseRepository.isUserLoggedIn
          ? const FavoriteScreen()
          : const NoSellerProfile(
              visible: true,
              additionalMessage:
                  'You need to have a seller profile to add favorites',
            ),
    );
  }
}
