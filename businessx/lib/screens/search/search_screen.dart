import 'package:flutter/material.dart';

import '../../bloc/search_bloc.dart';
import '../../common/decoration.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/category_search_response.dart';
import '../../models/response/product_search_response.dart';
import '../category/leaf_categories_screen/components/leaf_category_card.dart';
import '../product/components/product_list_tile.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final _searchController = TextEditingController();
  final _inputFocusNode = FocusNode();
  final _searchBloc = SearchBloc();
  bool _showCategories = true;

  @override
  void dispose() {
    _inputFocusNode.dispose();
    _searchController.dispose();
    _searchBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: Container(
            height: 40,
            width: MediaQuery.of(context).size.width / 1.25,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: Colors.grey.withOpacity(0.32),
              ),
            ),
            child: TextField(
              controller: _searchController,
              autofocus: true,
              focusNode: _inputFocusNode,
              textInputAction: TextInputAction.search,
              onSubmitted: (query) {
                if (query.trim().isNotEmpty && query.trim().length > 2) {
                  _searchBloc.searchProducts(query.trim());
                  setState(() {
                    _showCategories = false;
                  });
                }
              },
              onChanged: (query) {
                _searchBloc.querySink.add(query.trim());
                setState(() {});
                _showCategories = true;
              },
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: const Icon(Icons.search),
                  onPressed: () {
                    if (_searchController.text.trim().isNotEmpty &&
                        _searchController.text.trim().length > 2) {
                      _searchBloc.searchProducts(_searchController.text.trim());
                      setState(() {
                        _showCategories = false;
                      });
                    }
                  },
                ),
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
                border: InputBorder.none,
                hintText: 'Search Product/ Service',
                hintStyle: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
          ),
          titleSpacing: 0.2,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.notifications),
              onPressed: () {},
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          const PageTitle(
            title: 'Search Results',
          ),
          Visibility(
            visible: _showCategories,
            replacement: Expanded(
              child: StreamBuilder<ProductSearchResponse>(
                stream: _searchBloc.productSearchResponse.stream,
                builder: (context, snapshot) {
                  var products = snapshot.data?.products;
                  if (products != null && products.isEmpty) {
                    return const Center(
                      child: Text(
                        'No results found',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    );
                  }
                  if (products != null && products.isNotEmpty) {
                    return ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: products.length,
                      itemBuilder: (context, index) {
                        var product = products[index];
                        return ProductListTile(product: product);
                      },
                    );
                  }

                  return const Center(
                    child: Text('Enter keyword to search'),
                  );
                },
              ),
            ),
            child: Expanded(
              child: StreamBuilder<LeafCategorySearchResponse?>(
                stream: _searchBloc.leafCategorySearchResult,
                builder: (context, snapshot) {
                  var leafCategories = snapshot.data?.leafCategories;
                  if (leafCategories != null && leafCategories.isEmpty) {
                    return const Center(
                      child: Text(
                        'No results found',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    );
                  }
                  if (leafCategories != null && leafCategories.isNotEmpty) {
                    return ListView.separated(
                      physics: const BouncingScrollPhysics(),
                      separatorBuilder: (__, _) => const Divider(),
                      itemCount: leafCategories.length,
                      itemBuilder: (context, index) {
                        var leafCategory = leafCategories[index];
                        return LeafCategoryCard.listTile(
                          leafCategory: leafCategory,
                        );
                      },
                    );
                  }
                  return const Center(
                    child: Text('Enter keyword to search'),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
