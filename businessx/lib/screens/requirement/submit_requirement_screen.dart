import 'package:b2b_market/bloc/lead_bloc.dart';
import 'package:b2b_market/models/params/create_lead_params.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../bloc/users_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/product.dart';
import '../../models/response/user/user_response.dart';

class SubmitRequirementScreen extends StatefulWidget {
  final Product product;

  const SubmitRequirementScreen({Key? key, required this.product})
      : super(key: key);

  @override
  State<SubmitRequirementScreen> createState() =>
      _SubmitRequirementScreenState();
}

class _SubmitRequirementScreenState extends State<SubmitRequirementScreen> {
  final _nameController = TextEditingController();
  final _contactController = TextEditingController();
  final _requirementController = TextEditingController();
  final _requirementFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 5,
          ),
          PageTitle(
            title: widget.product.name,
          ),
          StreamBuilder<UserResponse>(
            stream: usersBloc.user.stream,
            builder: (context, snapshot) {
              var user = snapshot.data?.user;
              _nameController.text = user?.name ?? '';
              _contactController.text = user?.contactNumber ?? '';
              return Form(
                key: _requirementFormKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Expanded(
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    children: [
                      CachedNetworkImage(
                        placeholder: (context, url) => Center(
                          child: Text(
                            widget.product.name[0].toUpperCase(),
                          ),
                        ),
                        imageUrl: widget.product.imageThumbnail,
                        imageBuilder: (context, imageProvider) => Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: Image.network(
                              widget.product.imageThumbnail,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Center(
                          child: Text(
                            widget.product.name[0].toUpperCase(),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Container(
                          padding: const EdgeInsets.only(top: 10),
                          decoration: const BoxDecoration(
                            color: Color.fromARGB(225, 124, 207, 255),
                            borderRadius: BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                          child: Column(
                            children: [
                              CustomTextFormField(
                                labelText: 'Enter your Full Name',
                                hintText: 'Enter your Full Name',
                                controller: _nameController,
                                validator:
                                    RequiredValidator(errorText: 'Required'),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                labelText: 'Enter your Contact',
                                controller: _contactController,
                                hintText: 'Enter your Contact',
                                textInputType: TextInputType.phone,
                                validator: MultiValidator(
                                  [
                                    RequiredValidator(
                                      errorText: 'Contact Number is required',
                                    ),
                                    MinLengthValidator(
                                      10,
                                      errorText: 'Contact must be 10 digits',
                                    ),
                                    MaxLengthValidator(
                                      10,
                                      errorText: 'Contact must be 10 digits',
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                labelText: 'Requirement Details',
                                controller: _requirementController,
                                maxLines: 8,
                                validator: MultiValidator([
                                  RequiredValidator(
                                    errorText: 'Description is required',
                                  ),
                                  MinLengthValidator(
                                    15,
                                    errorText:
                                        'Description must be at least 20 characters',
                                  ),
                                ]),
                                onFieldSubmitted: (value) {},
                              ),
                              const SizedBox(
                                height: 70,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: SizedBox(
                                  height: 50,
                                  width: double.maxFinite,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      left: 20,
                                      right: 20,
                                    ),
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                          const Color(0xff1D4ED8),
                                        ),
                                        padding: MaterialStateProperty.all(
                                          const EdgeInsets.symmetric(
                                            vertical: 10,
                                            horizontal: 10,
                                          ),
                                        ),
                                        shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                        ),
                                      ),
                                      onPressed: () {
                                        if (_requirementFormKey.currentState!
                                            .validate()) {
                                          var params = CreateLeadParams(
                                            name: _nameController.text,
                                            contact: _contactController.text,
                                            productId: widget.product.id,
                                            description:
                                                _requirementController.text,
                                          );
                                          leadBloc.createLead(context, params);
                                        }
                                      },
                                      child: const Text(
                                        'Submit Requirements',
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.normal,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ),

      // bottomNavigationBar:
    );
  }
}
