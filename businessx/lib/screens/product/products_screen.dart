import 'package:flutter/material.dart';

import '../../bloc/bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/response.dart';
import 'components/components.dart';

class ProductsScreen extends StatefulWidget {
  final String title;

  const ProductsScreen({Key? key, required this.title}) : super(key: key);

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          PageTitle(title: widget.title),
          Expanded(
            child: StreamBuilder<ProductsResponse>(
              stream: productBloc.productsResponse.stream,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const ProductShimmer();
                }
                var products = snapshot.data?.productResponseData?.products;
                if (products != null && products.isNotEmpty) {
                  return ListView.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    physics: const BouncingScrollPhysics(),
                    itemCount: products.length,
                    itemBuilder: (context, index) {
                      var product = products[index];
                      return ProductListTile(product: product);
                    },
                  );
                }
                if (snapshot.data?.error == null) {
                  return Center(
                    child: Text(
                      'No Products Found',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  );
                }
                return const ProductShimmer();
              },
            ),
          )
        ],
      ),
    );
  }
}
