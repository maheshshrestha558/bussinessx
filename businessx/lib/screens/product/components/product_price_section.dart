import 'package:flutter/material.dart';

import '../../../models/models.dart';
import '../../../route_names.dart';

class ProductPriceSection extends StatelessWidget {
  const ProductPriceSection({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Feedback.forTap(context);
        Navigator.pushNamed(
          context,
          RouteName.sellerHomePage,
          arguments: product,
        );
      },
      child: Column(
        children: [
          Text(
            product.seller?.business.name ?? '',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            '${product.price} NPR / ${product.unit}',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              color: Color.fromARGB(255, 220, 38, 38),
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            'Visit Store>>',
            style: TextStyle(
              color: Color.fromARGB(255, 100, 177, 77),
              fontSize: 15,
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }
}
