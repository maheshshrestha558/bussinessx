import 'package:flutter/material.dart';

import '../../../common/extensions/string_extensions.dart';
import '../../../models/models.dart';

class Info extends StatelessWidget {
  const Info({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Price & Quantity',
            style: TextStyle(
              // color: Color.fromARGB(239, 29, 79, 216),
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            ),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2.5,
                child: const Text(
                  'Min Order Quantity',
                  style: TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Text(
                product.unit,
                style: const TextStyle(
                  color: Color.fromARGB(239, 29, 79, 216),
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          const SizedBox(height: 5),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2.5,
                child: const Text(
                  'Price',
                  style: TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Text(
                '${product.price} NPR',
                style: const TextStyle(
                  color: Color.fromARGB(239, 29, 79, 216),
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Text(
            'Seller Information',
            style: TextStyle(
              // color: Color.fromARGB(239, 29, 79, 216),
              fontSize: 18,
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.normal,
            ),
          ),
          const SizedBox(height: 5),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2.5,
                child: const Text(
                  'Seller Name',
                  style: TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  product.seller?.name ?? '',
                  style: const TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 5),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2.5,
                child: const Text(
                  'Company Name:',
                  style: TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  product.seller!.business.name,
                  style: const TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 5),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2.5,
                child: const Text(
                  'Company Address:',
                  style: TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  product.seller!.business.address,
                  style: const TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 5),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 2.5,
                child: const Text(
                  'Company Website:',
                  style: TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  product.seller!.business.websiteUrl.showDashIfNull(),
                  style: const TextStyle(
                    color: Color.fromARGB(239, 29, 79, 216),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
