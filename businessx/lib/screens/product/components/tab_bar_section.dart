import 'package:flutter/material.dart';

class TabBarSection extends StatelessWidget {
  const TabBarSection({
    Key? key,
    required TabController tabController,
  })  : _tabController = tabController,
        super(key: key);

  final TabController _tabController;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      child: TabBar(
        controller: _tabController,
        indicator: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: const Color.fromRGBO(29, 78, 216, 1),
        ),
        labelColor: Colors.white,
        unselectedLabelColor: Colors.black,
        tabs: const [
          Tab(
            height: 35,
            text: 'Description',
          ),
          Tab(
            height: 35,
            text: 'Info',
          ),
        ],
      ),
    );
  }
}
