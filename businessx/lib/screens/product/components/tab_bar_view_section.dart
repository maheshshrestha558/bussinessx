import 'package:flutter/material.dart';

import '../../../models/models.dart';
import 'components.dart';

class TabBarViewSection extends StatelessWidget {
  const TabBarViewSection({
    Key? key,
    required TabController tabController,
    required this.product,
  })  : _tabController = tabController,
        super(key: key);

  final TabController _tabController;
  final Product product;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 3.5,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
        ),
      ),
      child: TabBarView(
        controller: _tabController,
        physics: const FixedExtentScrollPhysics(),
        children: [
          Description(description: product.description ?? ''),
          Info(product: product),
        ],
      ),
    );
  }
}
