import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';

import '../../../common/shimmer_widget.dart';
import '../../../models/models.dart';

class ProductImageWidget extends StatelessWidget {
  const ProductImageWidget({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: ColoredBox(
        color: Colors.grey.shade200,
        child: CarouselSlider.builder(
          enableAutoSlider: false,
          unlimitedMode: false,
          itemCount: product.productImages.length,
          slideBuilder: (index) {
            var imageThumbnail = product.productImages[index].path;
            return Hero(
              tag: product.id,
              child: ProductImageCarousel(
                filePath: imageThumbnail ?? '',
              ),
            );
          },
          slideIndicator: CircularSlideIndicator(
            indicatorBackgroundColor: Colors.grey,
            currentIndicatorColor: Colors.white,
            padding: const EdgeInsets.only(bottom: 18),
          ),
        ),
      ),
    );
  }
}

class ProductImageCarousel extends StatelessWidget {
  const ProductImageCarousel({
    Key? key,
    required this.filePath,
  }) : super(key: key);

  final String filePath;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: filePath,
      fit: BoxFit.cover,
      errorWidget: (context, url, error) {
        return const Align(
          alignment: Alignment.centerLeft,
          child: ShimmerWidget.rectangular(
            height: 200,
            width: 320,
          ),
        );
      },
    );
  }
}
