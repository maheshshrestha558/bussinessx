import 'package:flutter/material.dart';

import '../../../common/widgets/submit_requirement_buttons.dart';
import '../product_detail_screen.dart';

class ContactSellerSection extends StatelessWidget {
  const ContactSellerSection({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final ProductDetailScreen widget;

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
      // backgroundColor: Colors.amber,
      enableDrag: false,
      elevation: 7,
      builder: (BuildContext context) => SizedBox(
        height: 70,
        width: double.infinity,
        child: SubmitRequirementButtons(widget: widget),
      ),
      onClosing: () {},
    );
  }
}
