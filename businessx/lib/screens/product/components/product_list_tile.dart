import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../models/models.dart';
import '../../../route_names.dart';
import '../../home/components/components.dart';

class ProductListTile extends StatelessWidget {
  const ProductListTile({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Feedback.forTap(context);
        Navigator.pushNamed(
          context,
          RouteName.productDetailScreen,
          arguments: product,
        );
      },
      child: Card(
        elevation: 3,
        color: Colors.grey.shade100,
        // shape: RoundedRectangleBorder(
        //   side: const BorderSide(color: Colors.white70, width: 1),
        //   borderRadius: BorderRadius.circular(0),
        // ),
        child: SizedBox(
          height: MediaQuery.of(context).size.width / 3,
          child: Row(
            children: [
              Container(
                height: MediaQuery.of(context).size.width / 3.5,
                width: MediaQuery.of(context).size.width / 3.5,
                margin: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                color: Colors.grey[200],
                child: ProductCachedNetworkImage(
                  product: product,
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      product.name,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                    Text(
                      '${product.price} NRS / ${product.unit}',
                      style: GoogleFonts.rubik(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: const Color(0xffDC2626),
                      ),
                    ),
                    const Text(
                      'Supplied By:',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      product.seller?.business.name ?? '',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
