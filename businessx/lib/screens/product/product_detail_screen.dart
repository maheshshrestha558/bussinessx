import 'package:flutter/material.dart';

import '../../common/widgets/widgets.dart';
import '../../models/models.dart';
import 'components/components.dart';

class ProductDetailScreen extends StatefulWidget {
  final Product product;

  const ProductDetailScreen({Key? key, required this.product})
      : super(key: key);

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: ContactSellerSection(widget: widget),
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          PageTitle(title: widget.product.name),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.width - 32,
                        width: MediaQuery.of(context).size.width,
                        child: ProductImageWidget(
                          product: widget.product,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ProductPriceSection(product: widget.product),
                          const SizedBox(
                            width: 6,
                          ),
                          const Icon(
                            Icons.info,
                            color: Color.fromARGB(255, 57, 57, 57),
                            size: 30,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                const Divider(
                  color: Color.fromARGB(255, 57, 57, 57),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      TabBarSection(tabController: _tabController),
                      const SizedBox(
                        height: 15,
                      ),
                      TabBarViewSection(
                        tabController: _tabController,
                        product: widget.product,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 80),
        ],
      ),
    );
  }
}
