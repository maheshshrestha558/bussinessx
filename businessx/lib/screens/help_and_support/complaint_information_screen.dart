import 'package:flutter/material.dart';

import '../../common/decoration.dart';
import '../../common/widgets/widgets.dart';

class ComplaintInformationScreen extends StatelessWidget {
  const ComplaintInformationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 10),
          ListTile(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ),
            title: Text(
              'Complaint Information',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Container(
                  width: double.maxFinite,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.21),
                        spreadRadius: 0,
                        blurRadius: 19,
                        offset: Offset(0, 10), // changes position of shadow
                      ),
                    ],
                  ),
                  child: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('12, Dec 2021'),
                      SizedBox(height: 5),
                      Text('Ticket #01198'),
                      SizedBox(height: 22),
                      Text(
                        'Defective Product sent by seller and refusing to take return',
                      ),
                      SizedBox(height: 20),
                      Text('Status:Opened'),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: double.maxFinite,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.21),
                        spreadRadius: 0,
                        blurRadius: 19,
                        offset: Offset(0, 10), // changes position of shadow
                      ),
                    ],
                  ),
                  child: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Description: '),
                      SizedBox(height: 20),
                      Text(
                        'Seller sent me defective product and refuses to take exchange saying it’s my own fault. I seek help in this matter and request authority to resolve it as soon as possible. Thank You. ',
                      ),
                      SizedBox(height: 20),
                      Text('Assigned To:'),
                      SizedBox(height: 10),
                      Text('Rishi Shakya'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
