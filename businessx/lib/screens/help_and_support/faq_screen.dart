import 'package:flutter/material.dart';

import '../../bloc/faq_bloc.dart';
import '../../common/decoration.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/faq/faq_response.dart';

class FaqScreen extends StatelessWidget {
  const FaqScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 10),
          const PageTitle(title: "FAQ'S"),
          const SizedBox(height: 10),
          Expanded(
            child: StreamBuilder(
              stream: faqBloc.faq.stream,
              builder: (context, AsyncSnapshot<FaqResponse> snapshot) {
                var faqs = snapshot.data?.faqs?.faq;
                if (faqs != null) {
                  return ListView.builder(
                    itemCount: faqs.length,
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      var faq = faqs[index];

                      return CustomExpansionTile(
                        title: faq.name ?? '',
                        content: faq.enDescription ?? '',
                      );
                    },
                  );
                }
                return const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xff29ed92),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
