import 'package:flutter/material.dart';

import '../../common/decoration.dart';
import '../../common/widgets/widgets.dart';
import '../screens.dart';

class ComplaintStatusScreen extends StatelessWidget {
  const ComplaintStatusScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          ListTile(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ),
            title: Text(
              'Complaint Status',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
          ),
          const SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const ComplaintInformationScreen(),
                    ),
                  ),
                  child: const ComplaintStatusCard(),
                );
              },
            ),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
