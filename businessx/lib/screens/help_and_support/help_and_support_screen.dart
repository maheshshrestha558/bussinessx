import 'package:flutter/material.dart';

import '../../bloc/faq_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../route_names.dart';

class HelpAndSupportScreen extends StatelessWidget {
  const HelpAndSupportScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          const SizedBox(height: 10),
          const PageTitle(title: 'Help & Support'),
          const SizedBox(height: 50),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: OutlinedButton(
              onPressed: () {
                faqBloc.getFaq();
                Navigator.pushNamed(
                  context,
                  RouteName.faqScreen,
                );
              },
              style: OutlinedButton.styleFrom(
                minimumSize: const Size(double.maxFinite, 45),
                foregroundColor: const Color.fromRGBO(41, 222, 146, 1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                side: const BorderSide(
                  color: Color.fromRGBO(41, 222, 146, 0.3),
                  width: 2,
                ),
              ),
              child: const Text("FAQ'S"),
            ),
          ),
          // const SizedBox(height: 20),
          // OutlinedButton(
          //   onPressed: () {
          //     Navigator.push(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => const ComplaintScreen()));
          //   },
          //   child: const Text("Raise a Complaint"),
          //   style: OutlinedButton.styleFrom(
          //     minimumSize: const Size(double.maxFinite, 45),
          //     foregroundColor: const Color.fromRGBO(29, 78, 216, 1),
          //     shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(10),
          //     ),
          //     side: const BorderSide(
          //         color: Color.fromRGBO(29, 78, 216, 0.3), width: 2),
          //   ),
          // ),
          // const SizedBox(height: 20),
          // OutlinedButton(
          //   onPressed: () {
          //     Navigator.push(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => const ComplaintStatusScreen()));
          //   },
          //   child: const Text(" Complaint Status"),
          //   style: OutlinedButton.styleFrom(
          //     minimumSize: const Size(double.maxFinite, 45),
          //     foregroundColor: const Color.fromRGBO(41, 222, 146, 1),
          //     shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(10),
          //     ),
          //     side: const BorderSide(
          //         color: Color.fromRGBO(41, 222, 146, 0.3), width: 2),
          //   ),
          // ),
          // const SizedBox(height: 20),
          // OutlinedButton(
          //   onPressed: () {
          //     Navigator.push(
          //         context,
          //         MaterialPageRoute(
          //             builder: (context) => const FeedBackScreen()));
          //   },
          //   child: const Text("Submit Feedback"),
          //   style: OutlinedButton.styleFrom(
          //     minimumSize: const Size(double.maxFinite, 45),
          //     foregroundColor: const Color.fromRGBO(29, 78, 216, 1),
          //     shape: RoundedRectangleBorder(
          //       borderRadius: BorderRadius.circular(10),
          //     ),
          //     side: const BorderSide(
          //         color: Color.fromRGBO(29, 78, 216, 0.3), width: 2),
          //   ),
          // ),
        ],
      ),
    );
  }
}
