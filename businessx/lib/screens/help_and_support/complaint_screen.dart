import 'dart:ui';

import 'package:flutter/material.dart';

import '../../common/decoration.dart';
import '../../common/widgets/widgets.dart';

class ComplaintScreen extends StatefulWidget {
  const ComplaintScreen({Key? key}) : super(key: key);

  @override
  State<ComplaintScreen> createState() => _ComplaintScreenState();
}

class _ComplaintScreenState extends State<ComplaintScreen> {
  String complaintType = 'Seller';
  String complaintCategory = 'Defective Product';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.symmetric(horizontal: 27, vertical: 10),
        children: [
          ListTile(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ),
            title: Text(
              'Raise A Complaint',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
          ),
          const SizedBox(height: 10),
          const Text('Select Complaint Type'),
          const SizedBox(height: 8),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.grey,
                width: 1,
              ),
            ),
            child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton(
                underline: Container(),
                isExpanded: true,
                borderRadius: BorderRadius.circular(10),
                alignment: AlignmentDirectional.center,
                value: complaintType,
                items: <String>['Seller', 'Two', 'Three', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    complaintType = value.toString();
                  });
                },
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Text('Select Complaint Category'),
          const SizedBox(height: 8),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.grey,
                width: 1,
              ),
            ),
            child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton(
                underline: Container(),
                isExpanded: true,
                borderRadius: BorderRadius.circular(10),
                alignment: AlignmentDirectional.center,
                value: complaintCategory,
                items: <String>['Defective Product', 'Two', 'Three', 'Four']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    complaintCategory = value.toString();
                  });
                },
              ),
            ),
          ),
          const SizedBox(height: 20),
          const Text('Additional Description'),
          const SizedBox(height: 8),
          TextField(
            maxLines: 5,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          const SizedBox(height: 45),
          OutlinedButton(
            onPressed: () {
              showcustomDialog(context);
            },
            style: OutlinedButton.styleFrom(
              minimumSize: const Size(double.maxFinite, 45),
              foregroundColor: Colors.white,
              backgroundColor: const Color.fromRGBO(29, 78, 216, 1),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            child: const Text('Submit Complaint'),
          ),
        ],
      ),
    );
  }
}

void showcustomDialog(context) {
  showDialog(
    useSafeArea: true,
    barrierColor: Colors.transparent,
    barrierDismissible: true,
    context: context,
    builder: (context) => Padding(
      padding: const EdgeInsets.all(8.0),
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 20, sigmaY: 20),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            backgroundBlendMode: BlendMode.darken,
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey.withOpacity(0.8),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height / 9,
              ),
              const DefaultTextStyle(
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.green,
                ),
                child: Text(
                  'Complaint Submitted \n Successfully',
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
              ),
              const DefaultTextStyle(
                style: TextStyle(
                  fontSize: 14,
                ),
                child: SizedBox(
                  width: 300,
                  child: Text(
                    'Our team has received your complaint, we’ll resolve your issue within 3-5 business days. Please be patient.',
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.1,
              ),
              const DefaultTextStyle(
                style: TextStyle(
                  fontSize: 14,
                  // color: Colors.green,
                ),
                child: SizedBox(
                  width: 300,
                  child: Text(
                    'You can track your complaint status from the Help & Support option. ',
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              OutlinedButton(
                onPressed: () {},
                style: OutlinedButton.styleFrom(
                  minimumSize: const Size(double.maxFinite, 45),
                  foregroundColor: const Color.fromRGBO(29, 78, 216, 1),
                  backgroundColor: Colors.white38,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  side: const BorderSide(
                    color: Color.fromRGBO(29, 78, 216, 1),
                    width: 2,
                  ),
                ),
                child: const Text('Track Complaint Status'),
              ),
              const SizedBox(height: 10),
              OutlinedButton(
                onPressed: () {
                  showcustomDialog(context);
                },
                style: OutlinedButton.styleFrom(
                  minimumSize: const Size(double.maxFinite, 45),
                  foregroundColor: Colors.white,
                  backgroundColor: Colors.green,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child: const Text('Go To Home'),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
