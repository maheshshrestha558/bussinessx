import 'package:flutter/material.dart';

import '../common/widgets/widgets.dart';

class TermsAndConditions extends StatelessWidget {
  const TermsAndConditions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          const PageTitle(
            title: 'Terms & Conditions',
          ),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 25.0,
                    right: 25.0,
                    bottom: 10,
                  ),
                  child: Text(
                    'Business X is the company doing business since 2021, and has been able to compete in the B2B market, marvellously. Lorem ipsum is the dummy text but i prefer not to use, as it does not look authentic while designing a product. Pro tip for the design is, never ever use a dummy text. I.E Lorem Ipsum, even if it’s a long paragraph like terms and conditions, privacy policy, or even a two line sentence. \n \nBusinessX does not take part in any actual transaction that takes place between the buyer and the seller and is not a party to any contract for sale negotiated between buyer and seller. The website acts as  a mere platform for our members to interact and negotiate for buying, selling and other business services by way of locating companies to trade with, through our on-line exchange. The agreement shall not be considered to create partnership or any joint business relationship between BusinessX  and the other party. \n \nThe company through its website provides hosting services to its users. Company neither originates nor initiates the transmission nor selects the sender and receiver of the transmission, nor modifies the information contained in the transmission. \n \nThis agreement applies to all BusinessX services offered on the website, collectively with any additional terms and condition that may be applicable to the specific service used/accessed by user(s). \n \nMost content and some of the features on the website are made available to users free of charge. However, company reserves the right to terminate access to certain areas or features of the website to users at any time with or without giving any reason, with or without notice. Company also reserves the universal right to deny access to particular users to any/all of its services and/or content without any prior notice/explanation in order to protect the interests of company and/or other visitors to the website. \n \nNeither the company nor its director’s, employees, officers or agents shall be liable to any user or otherwise, for any illegal or fraudulent interaction or transaction with organizations and/or individuals located on or through the Site.',
                    textAlign: TextAlign.justify,
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
