import 'package:b2b_market/screens/screens.dart';
import 'package:flutter/material.dart';

import '../../../common/widgets/widgets.dart';
import '../../theme.dart';

class PasswordResetConfirmationScreen extends StatelessWidget {
  const PasswordResetConfirmationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Transform.rotate(
        angle: 9.42,
        child: const CustomClipPath(
          height: 100,
        ),
      ),
      body: Column(
        children: [
          const CustomClipPath(
            height: 130,
          ),
          Expanded(
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 18, right: 17),
                  child: Stack(
                    children: [
                      Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(
                              38,
                              104,
                              38,
                              104,
                            ),
                            child: Text(
                              'Password has been Reset successfully. Please login with your new password. ',
                              textAlign: TextAlign.center,
                              style: B2BTheme.textTheme.bodySmall,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        top: 10,
                        left: MediaQuery.of(context).size.width - 100,
                        child: IconButton(
                          icon: const Icon(
                            Icons.close,
                            color: Colors.red,
                          ),
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(
                              context,
                              LoginScreen.routeName,
                              (Route<dynamic> route) => false,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 18, right: 17),
                  child: CustomOutlinedButton(
                    title: 'Go To Login Screen',
                    color: const Color(0xff7CD0FF),
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                        context,
                        LoginScreen.routeName,
                        (Route<dynamic> route) => false,
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
