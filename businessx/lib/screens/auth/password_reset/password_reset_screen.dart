import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../../bloc/auth_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/password/reset_password_response.dart';
import '../otp/timer_button.dart';

class PasswordResetScreen extends StatefulWidget {
  final String contactNumber;

  const PasswordResetScreen({Key? key, required this.contactNumber})
      : super(key: key);

  static const String routeName = 'password-reset';

  @override
  State<PasswordResetScreen> createState() => _PasswordResetScreenState();
}

class _PasswordResetScreenState extends State<PasswordResetScreen> {
  final _otpCodeController = TextEditingController();
  final _passwordController = TextEditingController();
  final _retypePasswordController = TextEditingController();
  final _resetFormKey = GlobalKey<FormState>();

  bool _obscurePassword = true;
  bool _obscureRetypedPassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _resetFormKey,
        child: Column(
          children: [
            const CustomClipPath(
              height: 120,
            ),
            const PageTitleForAuth(title: 'Reset Password'),
            Expanded(
              child: ListView(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Enter OTP sent in your number',
                    controller: _otpCodeController,
                    hintText: 'OTP CODE',
                    validator: RequiredValidator(
                      errorText: 'Please enter OTP code',
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Enter your password',
                    controller: _passwordController,
                    obscureText: _obscurePassword,
                    validator: MultiValidator([
                      RequiredValidator(
                        errorText: 'Please enter your password',
                      ),
                      MinLengthValidator(
                        8,
                        errorText: 'Password must be at least 8 digits long',
                      ),
                      PatternValidator(
                        r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$)',
                        errorText:
                            'Must contain at least one special character, one uppercase letter, one lowercase letter and one number',
                      )
                    ]),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {});
                        _obscurePassword = !_obscurePassword;
                      },
                      icon: Icon(
                        _obscurePassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Re-enter your password',
                    controller: _retypePasswordController,
                    obscureText: _obscureRetypedPassword,
                    validator: (_) => MatchValidator(
                      errorText: 'Passwords do not match',
                    ).validateMatch(
                      _passwordController.text,
                      _retypePasswordController.text,
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {});
                        _obscureRetypedPassword = !_obscureRetypedPassword;
                      },
                      icon: Icon(
                        _obscureRetypedPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ListView(
        shrinkWrap: true,
        children: [
          TimerButton(
            label: 'Resend OTP',
            onPressed: () async {
              await SmsAutoFill().listenForCode();
              final hashKey = await SmsAutoFill().getAppSignature;
              authBloc.resendOtp(context, {
                'contact': widget.contactNumber,
                'hashKey': hashKey,
              });
            },
            timeOutInSeconds: 60,
          ),
          const SizedBox(height: 10),
          CustomElevatedButton(
            buttonText: 'SUBMIT',
            onPressed: () {
              if (_resetFormKey.currentState!.validate()) {
                PasswordResetParams data = PasswordResetParams(
                  otp: _otpCodeController.text,
                  password: _passwordController.text,
                  contactNumber: widget.contactNumber,
                );

                authBloc.resetPassword(context, data);
              }
            },
          ),
          const SizedBox(
            height: 20,
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
