import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../../bloc/auth_bloc.dart';
import '../../../common/logger/logger.dart';
import '../../../common/widgets/widgets.dart';

class ForgotPasswordScreen extends StatelessWidget {
  ForgotPasswordScreen({Key? key}) : super(key: key);

  final _forgetPasswordFormkey = GlobalKey<FormState>();
  final _contactNumberController = TextEditingController();

  static const String routeName = 'forgot-password';

  final _logger = getLogger(ForgotPasswordScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const CustomClipPath(
            height: 130,
          ),
          const SizedBox(
            height: 20,
          ),
          const PageTitleForAuth(title: 'Forgot Password ?'),
          const SizedBox(
            height: 20,
          ),
          Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _forgetPasswordFormkey,
            child: CustomTextFormField(
              prefixIcon: const Padding(
                padding: EdgeInsets.only(top: 14, left: 8),
                child: Text(
                  '+977',
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              ),
              labelText: 'Enter your phone numbers',
              controller: _contactNumberController,
              textInputAction: TextInputAction.next,
              textInputType: TextInputType.number,
              textInputFormatterList: [FilteringTextInputFormatter.digitsOnly],
              maxLenght: 10,
              validator: MultiValidator(
                [
                  RequiredValidator(
                    errorText: 'Phone number is required',
                  ),
                  MinLengthValidator(
                    10,
                    errorText: 'Phone number must be 10 digits',
                  ),
                  MaxLengthValidator(
                    10,
                    errorText: 'Phone number must be 10 digits',
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          CustomElevatedButton(
            buttonText: 'SUBMIT',
            onPressed: () async {
              if (_forgetPasswordFormkey.currentState!.validate()) {
                final hashKey = await SmsAutoFill().getAppSignature;
                _logger.i(hashKey);
                final params = {
                  'contact_number': _contactNumberController.text,
                  'hashKey': hashKey,
                };
                authBloc.resetPasswordRequest(
                  context,
                  params,
                );
              }
            },
          ),
        ],
      ),
    );
  }
}
