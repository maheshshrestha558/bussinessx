import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../../bloc/auth_bloc.dart';
import '../../../common/enums.dart';
import '../../../common/logger/logger.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/response.dart';

class SignUpScreen extends StatefulWidget {
  final UserType userType;
  const SignUpScreen({Key? key, required this.userType}) : super(key: key);

  static const String routeName = 'signup';

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _enNameController = TextEditingController();
  final _contactNumberController = TextEditingController();
  final _passwordController = TextEditingController();
  final _retypePasswordController = TextEditingController();

  bool _obscurePassword = true;
  bool _obscureRetypedPassword = true;
  final _signupFormKey = GlobalKey<FormState>();

  final _logger = getLogger(SignUpScreen);

  @override
  void initState() {
    super.initState();
    _logger.i(widget.userType.type);
  }

  @override
  void dispose() {
    _contactNumberController.dispose();
    _enNameController.dispose();
    _passwordController.dispose();
    _retypePasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const CustomClipPath(
            height: 120,
          ),
          const PageTitleForAuth(title: 'Sign Up'),
          Form(
            key: _signupFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Expanded(
              child: ListView(
                children: [
                  CustomTextFormField(
                    labelText: 'Enter your name',
                    controller: _enNameController,
                    validator: RequiredValidator(
                      errorText: 'Please Enter Your Name',
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    prefixIcon: const Padding(
                      padding: EdgeInsets.only(top: 14, left: 8),
                      child: Text(
                        '+977',
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ),
                    labelText: 'Enter your phone number',
                    controller: _contactNumberController,
                    textInputAction: TextInputAction.next,
                    textInputType: TextInputType.number,
                    textInputFormatterList: [
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    maxLenght: 10,
                    validator: MultiValidator(
                      [
                        RequiredValidator(
                          errorText: 'Phone number is required',
                        ),
                        MinLengthValidator(
                          10,
                          errorText: 'Phone number must be 10 digits',
                        ),
                        MaxLengthValidator(
                          10,
                          errorText: 'Phone number must be 10 digits',
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Enter your password',
                    controller: _passwordController,
                    obscureText: _obscurePassword,
                    validator: MultiValidator([
                      RequiredValidator(
                        errorText: 'Please enter your password',
                      ),
                      MinLengthValidator(
                        8,
                        errorText: 'Password must be at least 8 digits long',
                      ),
                      PatternValidator(
                        r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$)',
                        errorText:
                            'Must contain at least one special character, one uppercase letter, one lowercase letter and one number',
                      )
                    ]),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {});
                        _obscurePassword = !_obscurePassword;
                      },
                      icon: Icon(
                        _obscurePassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Re-enter your password',
                    controller: _retypePasswordController,
                    obscureText: _obscureRetypedPassword,
                    validator: (_) => MatchValidator(
                      errorText: 'Passwords do not match',
                    ).validateMatch(
                      _passwordController.text,
                      _retypePasswordController.text,
                    ),
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {});
                        _obscureRetypedPassword = !_obscureRetypedPassword;
                      },
                      icon: Icon(
                        _obscureRetypedPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: [
          CustomElevatedButton(
            buttonText: 'SIGN UP',
            onPressed: () {
              if (_signupFormKey.currentState!.validate()) {
                final signUpCredentials = SignUpCredentials(
                  enName: _enNameController.text,
                  contact: _contactNumberController.text,
                  password: _passwordController.text,
                  confirmPassword: _retypePasswordController.text,
                  userType: widget.userType,
                );
                authBloc.signUp(
                  context,
                  signUpCredentials,
                );
              }
            },
          ),
          const SizedBox(
            height: 10,
          ),
          const CustomLoginMessage(),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
