import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../../bloc/bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/response.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final _currentPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _confirmNewPasswordController = TextEditingController();

  bool _obscurePassword = true;
  bool _obscureRetypedPassword = true;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(height: 10),
          PageTitle(
            title: 'Change Password',
            showTopNavButton: true,
            navbuttontext: 'Done',
            ontopNavButtonPressed: () {
              final params = SetNewPasswordParams(
                _currentPasswordController.text,
                _newPasswordController.text,
                _confirmNewPasswordController.text,
              );
              if (_formKey.currentState!.validate()) {
                authBloc.setNewPassword(context, params);
              }
            },
          ),
          Form(
            key: _formKey,
            child: Expanded(
              child: ListView(
                padding: const EdgeInsets.all(8),
                children: [
                  const SizedBox(height: 20),
                  CustomTextFormField(
                    labelText: 'Enter Current Password',
                    controller: _currentPasswordController,
                    validator: MultiValidator([
                      RequiredValidator(
                        errorText: 'Please enter your current password',
                      ),
                      MinLengthValidator(
                        8,
                        errorText: 'Password must be at least 8 digits long',
                      ),
                      PatternValidator(
                        r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$)',
                        errorText:
                            'Must contain at least one special character, one uppercase letter, one lowercase letter and one number',
                      )
                    ]),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Enter New Password',
                    controller: _newPasswordController,
                    obscureText: _obscurePassword,
                    validator: MultiValidator([
                      RequiredValidator(errorText: 'Please enter new password'),
                      MinLengthValidator(
                        8,
                        errorText: 'Password must be at least 8 digits long',
                      ),
                      PatternValidator(
                        r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$)',
                        errorText:
                            'Must contain at least one special character, one uppercase letter, one lowercase letter and one number',
                      )
                    ]),
                    suffixIcon: IconButton(
                      onPressed: () {
                        _obscurePassword = !_obscurePassword;
                        setState(() {});
                      },
                      icon: Icon(
                        _obscurePassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextFormField(
                    labelText: 'Re-enter your Password',
                    controller: _confirmNewPasswordController,
                    obscureText: _obscureRetypedPassword,
                    textInputAction: TextInputAction.go,
                    validator: (val) =>
                        MatchValidator(errorText: 'Passwords do not match')
                            .validateMatch(val!, _newPasswordController.text),
                    suffixIcon: IconButton(
                      onPressed: () {
                        _obscureRetypedPassword = !_obscureRetypedPassword;
                        setState(() {});
                      },
                      icon: Icon(
                        _obscureRetypedPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
