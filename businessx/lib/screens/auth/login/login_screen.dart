import 'package:b2b_market/common/enums.dart';
import 'package:b2b_market/screens/buyer_screens/buyer_navigation_screen.dart';
import 'package:b2b_market/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../../bloc/auth_bloc.dart';
import '../../../common/logger/logger.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/auth/login_response.dart';
import '../../theme.dart';

class LoginScreen extends StatefulWidget {
  final UserType userType;

  const LoginScreen({Key? key, required this.userType}) : super(key: key);

  static const String routeName = 'login';

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _logger = getLogger(LoginScreen);

  final _contactNumberController = TextEditingController();
  final _passwordController = TextEditingController();

  final _loginFormkey = GlobalKey<FormState>();

  bool _obscurePassword = true;

  @override
  void initState() {
    super.initState();
    _logger.i(widget.userType.type);
  }

  @override
  void dispose() {
    _contactNumberController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const CustomClipPath(
            height: 130,
          ),
          const PageTitleForAuth(title: 'Login'),
          Expanded(
            child: Center(
              child: Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: _loginFormkey,
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    CustomTextFormField(
                      prefixIcon: const Padding(
                        padding: EdgeInsets.only(top: 14, left: 8),
                        child: Text(
                          '+977',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ),
                      labelText: 'Enter your phone number',
                      controller: _contactNumberController,
                      textInputAction: TextInputAction.next,
                      textInputType: TextInputType.number,
                      textInputFormatterList: [
                        FilteringTextInputFormatter.digitsOnly
                      ],
                      maxLenght: 10,
                      validator: MultiValidator(
                        [
                          RequiredValidator(
                            errorText: 'Phone number is required',
                          ),
                          MinLengthValidator(
                            10,
                            errorText: 'Phone number must be 10 digits',
                          ),
                          MaxLengthValidator(
                            10,
                            errorText: 'Phone number must be 10 digits',
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    CustomTextFormField(
                      labelText: 'Password',
                      controller: _passwordController,
                      obscureText: _obscurePassword,
                      textInputAction: TextInputAction.go,
                      validator: RequiredValidator(
                        errorText: 'Required',
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {});
                          _obscurePassword = !_obscurePassword;
                        },
                        icon: Icon(
                          _obscurePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      ),
                      onFieldSubmitted: (value) async {
                        if (_loginFormkey.currentState!.validate()) {
                          await SmsAutoFill().listenForCode();
                          final hashKey = await SmsAutoFill().getAppSignature;
                          final loginCredentails = LoginCredentials(
                            contactNumber: _contactNumberController.text,
                            password: _passwordController.text,
                            hashKey: hashKey,
                            type: widget.userType.type,
                          );
                          authBloc.login(context, loginCredentails);
                        }
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 28.0),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          onPressed: () => Navigator.pushNamed(
                            context,
                            ForgotPasswordScreen.routeName,
                          ),
                          child: Text(
                            'Forgot Password?',
                            style: B2BTheme.textTheme.titleLarge,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 28),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Visibility(
                          visible: widget.userType.type == UserType.buyer.type,
                          child: TextButton(
                            onPressed: () => Navigator.pushReplacementNamed(
                              context,
                              BuyerNavigationScreen.routeName,
                            ),
                            child: Text(
                              'Skip',
                              style: GoogleFonts.rubik(
                                color: const Color(0xff0077b6),
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: ListView(
        shrinkWrap: true,
        children: [
          CustomElevatedButton(
            buttonText: 'LOGIN',
            onPressed: () async {
              if (_loginFormkey.currentState!.validate()) {
                await SmsAutoFill().listenForCode();
                final hashKey = await SmsAutoFill().getAppSignature;
                final loginCredentails = LoginCredentials(
                  contactNumber: _contactNumberController.text,
                  password: _passwordController.text,
                  hashKey: hashKey,
                  type: widget.userType.type,
                );
                authBloc.login(context, loginCredentails);
              }
            },
          ),
          const SizedBox(
            height: 20,
          ),
          CustomSignupMessage(
            userType: widget.userType,
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
