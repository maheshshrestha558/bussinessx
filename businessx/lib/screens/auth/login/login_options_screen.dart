import 'package:b2b_market/common/enums.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../common/widgets/widgets.dart';
import '../../screens.dart';

class LoginOptionsScreen extends StatefulWidget {
  const LoginOptionsScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginOptionsScreen> createState() => _LoginOptionsScreenState();
}

class _LoginOptionsScreenState extends State<LoginOptionsScreen> {
  final Shader _linearGradient = const LinearGradient(
    colors: <Color>[Color(0xff1D4ED8), Color(0xff29DE92)],
  ).createShader(const Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const CustomClipPath(
              height: 130,
            ),
            Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 57.0, right: 25.0),
                  child: SvgPicture.asset(
                    'assets/images/business_logo.svg',
                    height: 160,
                    width: 300,
                  ),
                ),
                Positioned.fill(
                  top: 100,
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'One Stop For All Business Purpose',
                      style: GoogleFonts.rubik(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w500,
                        foreground: Paint()..shader = _linearGradient,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Center(
              child: Text(
                'I AM',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            CustomOutlinedButton(
              title: UserType.wholesaler.type,
              color: const Color(0xff1D4ED8),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  LoginScreen.routeName,
                  arguments: UserType.wholesaler,
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            CustomOutlinedButton(
              title: UserType.retailer.type,
              color: const Color(0xff29DE92),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  LoginScreen.routeName,
                  arguments: UserType.retailer,
                );
              },
            ),
            const SizedBox(
              height: 20,
            ),
            CustomOutlinedButton(
              title: UserType.buyer.type,
              color: const Color.fromARGB(255, 144, 222, 41),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  LoginScreen.routeName,
                  arguments: UserType.buyer,
                );
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: const RotatedBox(
        quarterTurns: 2,
        child: CustomClipPath(
          height: 100,
        ),
      ),
    );
  }
}
