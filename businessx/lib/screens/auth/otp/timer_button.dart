import 'dart:async';

import 'package:flutter/material.dart';

class TimerButton extends StatefulWidget {
  final String label;
  final int timeOutInSeconds;
  final VoidCallback onPressed;
  final Color disabledColor;
  final TextStyle? activeTextStyle;
  final TextStyle disabledTextStyle;
  final bool resetTimerOnPressed;

  const TimerButton({
    Key? key,
    required this.label,
    required this.onPressed,
    required this.timeOutInSeconds,
    this.resetTimerOnPressed = true,
    this.disabledColor = Colors.grey,
    this.activeTextStyle,
    this.disabledTextStyle = const TextStyle(color: Colors.grey),
  }) : super(key: key);

  @override
  State<TimerButton> createState() => _TimerButtonState();
}

class _TimerButtonState extends State<TimerButton> {
  bool timeUpFlag = false;
  int timeCounter = 0;

  String get _timerText => '$timeCounter' 's';

  @override
  void initState() {
    super.initState();
    timeCounter = widget.timeOutInSeconds;
    _timerUpdate();
  }

  _timerUpdate() {
    Timer(const Duration(seconds: 1), () async {
      setState(() {
        timeCounter--;
      });
      if (timeCounter != 0) {
        _timerUpdate();
      } else {
        timeUpFlag = true;
      }
    });
  }

  _onPressed() {
    if (timeUpFlag) {
      setState(() {
        timeUpFlag = false;
      });
      timeCounter = widget.timeOutInSeconds;

      widget.onPressed();

      if (widget.resetTimerOnPressed) {
        _timerUpdate();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.only(left: 28, right: 28),
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(const Color(0xff1D4ED8)),
            padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
            ),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            ),
          ),
          onPressed: _onPressed,
          child: timeUpFlag
              ? Text(
                  widget.label,
                )
              : Text(
                  '${widget.label} | $_timerText',
                  style: widget.disabledTextStyle,
                ),
        ),
      ),
    );
  }
}
