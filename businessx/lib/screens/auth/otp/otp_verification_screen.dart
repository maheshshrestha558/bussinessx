import 'package:b2b_market/screens/auth/otp/timer_button.dart';
import 'package:flutter/material.dart';
import 'package:sms_autofill/sms_autofill.dart';

import '../../../bloc/auth_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/response.dart';
import '../../theme.dart';

class OTPVerificationScreen extends StatefulWidget {
  final LoginCredentials credentials;

  const OTPVerificationScreen({
    Key? key,
    required this.credentials,
  }) : super(key: key);

  @override
  State<OTPVerificationScreen> createState() => _OTPVerificationScreenState();
}

class _OTPVerificationScreenState extends State<OTPVerificationScreen> {
  late final TextEditingController _otpController;

  @override
  void initState() {
    super.initState();
    _otpController = TextEditingController();
  }

  @override
  dispose() {
    SmsAutoFill().unregisterListener();
    _otpController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const CustomClipPath(
            height: 130,
          ),
          const SizedBox(height: 10),
          const PageTitleForAuth(
            title: 'Verify OTP',
          ),
          Expanded(
            child: Center(
              child: ListView(
                shrinkWrap: true,
                children: [
                  Text(
                    'We’ve sent a verification code in your number.',
                    textAlign: TextAlign.center,
                    style: B2BTheme.textTheme.headlineMedium,
                  ),
                  const SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 28.0, right: 28.0),
                    child: PinFieldAutoFill(
                      controller: _otpController,
                      autoFocus: true,
                      cursor: Cursor(
                        color: Colors.black,
                        enabled: true,
                        width: 1,
                      ),
                      enableInteractiveSelection: true,
                    ),
                  ),
                  const SizedBox(height: 20),
                  CustomElevatedButton(
                    buttonText: 'Submit',
                    onPressed: () {
                      if (_otpController.text.isNotEmpty) {
                        final otpParams = OTPParams(
                          otp: _otpController.text,
                          contactNumber: widget.credentials.contactNumber,
                        );
                        authBloc.otpVerification(context, otpParams);
                      }
                    },
                  ),
                  const SizedBox(height: 10),
                  TimerButton(
                    label: 'Resend OTP',
                    onPressed: () async {
                      await SmsAutoFill().listenForCode();
                      authBloc.resendOtp(context, {
                        'contact': widget.credentials.contactNumber,
                        'hashKey': widget.credentials.hashKey,
                      });
                    },
                    timeOutInSeconds: 60,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
