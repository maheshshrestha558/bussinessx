import 'package:flutter/material.dart';

import '../common/widgets/widgets.dart';

class AboutUsScreen extends StatelessWidget {
  const AboutUsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          const PageTitle(
            title: 'About Us',
          ),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 25.0,
                    right: 25.0,
                    bottom: 10,
                  ),
                  child: Text(
                    'We had a dream that one day we will be able to contribute to Nepalese economy significantly. We had a dream that one day we will be able to take a step towards leading Nepal globally. BusinessX is an attempt to fulfill our dream. Online B2B marketplace is not a new crop in the world’s cultivation, but it surely is in Nepalese soil. This challenging opportunity and the hope of its possibility nourished our zeal to step into this new avenue. \n \nBusinessX is a user friendly online B2B marketplace for the medium and small enterprises to showcase themselves and their products and services in domestic market as well as global arena. We envision to provide Nepalese businesses worldwide visibility connecting them to millions of customers from within the country and outside. We offer a platform and tools to the suppliers to promote themselves to millions of buyers from all over the world. \n \nWe aim to generate profitable returns for the businesses through high quality business promotion services and solutions that enhances revenues, are cost effective and generate business and employment opportunities in the country. \n \nOur Mission \n \nTo become pioneer online business directory, and the largest one; \nTo take Nepalese businesses around the globe; \nTo advocate sustainable business ecosystem in Nepal; \nTo become one stop information source of Nepalese business; \nTo become a trust agent and a trusted agent for all buyers and suppliers.\n \nOur Principles \n \nPASSION to innovate and create new opportunities for Nepalese businesses \nDEDICATION in our work and our purpose \nEXPERTISE in data acquisition, web development and online promotion \nZEAL towards continuous learning and self-improving \nRESPONSIBILITY to provide accurate information \nAN EYE towards success and THE OTHER towards integrity',
                    textAlign: TextAlign.justify,
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
