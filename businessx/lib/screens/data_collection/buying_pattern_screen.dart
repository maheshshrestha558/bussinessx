import 'package:b2b_market/common/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../common/logger/logger.dart';
import '../../models/response/data_collection/data_collection_response.dart';
import '../../route_names.dart';

class BuyingPatternQuestionScreen extends StatefulWidget {
  const BuyingPatternQuestionScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<BuyingPatternQuestionScreen> createState() =>
      _BuyingPatternQuestionScreenState();
}

class _BuyingPatternQuestionScreenState
    extends State<BuyingPatternQuestionScreen>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final _logger = getLogger(BuyingPatternQuestionScreen);

  final _q18Controller = TextEditingController();
  final _q19Controller = TextEditingController();
  final _q20Controller = TextEditingController();
  final _q21Controller = TextEditingController();
  final _q22Controller = TextEditingController();
  final _q23Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _q18Controller.text = dataCollectionAnswer.answers?[17] ?? '';
    _q19Controller.text = dataCollectionAnswer.answers?[18] ?? '';
    _q20Controller.text = dataCollectionAnswer.answers?[19] ?? '';
    _q21Controller.text = dataCollectionAnswer.answers?[20] ?? '';
    _q22Controller.text = dataCollectionAnswer.answers?[21] ?? '';
    _q23Controller.text = dataCollectionAnswer.answers?[22] ?? '';
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
        child: Form(
          key: _formkey,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                PageTitle(
                  title: 'Buying Pattern',
                  navbuttontext: 'Next',
                  onBackPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  showTopNavButton: true,
                  ontopNavButtonPressed: () {
                    _logger.i(dataCollectionAnswer.toJson());
                    if (_formkey.currentState!.validate()) {
                      Navigator.pushNamed(
                        context,
                        RouteName.b2bPainPointsQuestionScreen,
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomTextFormField(
                          labelText:
                              '18. Does your company ever purchase goods /or manufacture yourself/ sells homemade goods?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q18Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[17] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '19. If so, who is responsible for the buying decision?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q19Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[18] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '20. Where do you go when you are looking for goods/products?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q20Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[19] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText: '21. How often do you purchase products?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q21Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[20] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '22. How long does it take you to make a buying decision?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q22Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[21] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '23. What is your typical budget for buying ? ( Daily, Monthly or Yearly)',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q23Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[22] = value;
                          },
                          hintText: '( Daily, Monthly or Yearly)',
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
