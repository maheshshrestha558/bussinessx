import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../common/logger/logger.dart';
import '../../common/widgets/custom_app_bar.dart';
import '../../common/widgets/custom_text_form_field.dart';
import '../../common/widgets/my_app_drawer.dart';
import '../../common/widgets/page_title.dart';
import '../../models/response/data_collection/data_collection_response.dart';
import '../../route_names.dart';

class B2BPainPointsQuestionScreen extends StatefulWidget {
  const B2BPainPointsQuestionScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<B2BPainPointsQuestionScreen> createState() =>
      _B2BPainPointsQuestionScreenState();
}

class _B2BPainPointsQuestionScreenState
    extends State<B2BPainPointsQuestionScreen>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final _logger = getLogger(B2BPainPointsQuestionScreen);

  final _q24Controller = TextEditingController();
  final _q25Controller = TextEditingController();
  final _q26Controller = TextEditingController();
  final _q27Controller = TextEditingController();
  final _q28Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _q24Controller.text = dataCollectionAnswer.answers?[23] ?? '';
    _q25Controller.text = dataCollectionAnswer.answers?[24] ?? '';
    _q26Controller.text = dataCollectionAnswer.answers?[25] ?? '';
    _q27Controller.text = dataCollectionAnswer.answers?[26] ?? '';
    _q28Controller.text = dataCollectionAnswer.answers?[27] ?? '';
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        child: Form(
          key: _formkey,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                PageTitle(
                  title: 'B2B Pain Points ',
                  onBackPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  showTopNavButton: true,
                  navbuttontext: 'Next',
                  ontopNavButtonPressed: () {
                    _logger.i(dataCollectionAnswer.toJson());
                    if (_formkey.currentState!.validate()) {
                      Navigator.pushNamed(
                        context,
                        RouteName.benefitsQuestionScreen,
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomTextFormField(
                          labelText:
                              '24. What is the biggest challenge you face in your line of operation?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q24Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[23] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '25. Of the following, what is your business lacking?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q25Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[24] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '26. What types of assets would help your business grow? Do you need financial help? If already taken a loan please specify.',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q26Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[25] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '27. Do you have any problems at all with finding a new wholesalers  ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q27Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[26] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '28. What are you hoping to accomplish by setting up a [enter your product/solution here]?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q28Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[27] = value;
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
