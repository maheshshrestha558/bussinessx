import 'package:flutter/material.dart';

import '../../bloc/data_collection_bloc.dart';
import '../../common/widgets/custom_app_bar.dart';
import '../../common/widgets/custom_elevated_button.dart';
import '../../common/widgets/my_app_drawer.dart';
import '../../common/widgets/page_title.dart';
import '../../models/response/data_collection/data_collection_response.dart';
import 'components/custom_chip.dart';
import 'introduction_question_screen.dart';

class MainDataCollectionPage extends StatefulWidget {
  const MainDataCollectionPage({
    Key? key,
  }) : super(key: key);

  @override
  State<MainDataCollectionPage> createState() => _MainDataCollectionPageState();
}

class _MainDataCollectionPageState extends State<MainDataCollectionPage> {
  @override
  void initState() {
    super.initState();
    dataCollectionBloc.getDatacollect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        child: SafeArea(
          child: Column(
            children: [
              const PageTitle(title: 'FORMS'),
              StreamBuilder<DataCollection>(
                stream: dataCollectionBloc.dataCollection,
                builder: ((context, snapshot) {
                  var dataCollection = snapshot.data;
                  if (dataCollection == null) {
                    return const Center(child: CircularProgressIndicator());
                  }
                  if (dataCollection.error != null) {
                    return Center(
                      child: Text(dataCollection.error!),
                    );
                  }

                  return Expanded(
                    child: Column(
                      children: [
                        Card(
                          elevation: 0.3,
                          child: SizedBox(
                            width: double.infinity,
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    CustomChip(
                                      dataCollectionValue: dataCollection
                                                  .totalDetails ==
                                              null
                                          ? 'Total '
                                          : 'Total ${dataCollection.totalDetails}',
                                      color: Colors.blue,
                                    ),
                                    CustomChip(
                                      dataCollectionValue: dataCollection
                                                  .submittedDetails ==
                                              null
                                          ? 'Submitted '
                                          : 'Submitted ${dataCollection.submittedDetails}',
                                      color: Colors.green,
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    CustomChip(
                                      dataCollectionValue: dataCollection
                                                  .verifiedDetails ==
                                              null
                                          ? 'Verified '
                                          : 'Verified ${dataCollection.verifiedDetails}',
                                      color: Colors.cyan,
                                    ),
                                    CustomChip(
                                      dataCollectionValue: dataCollection
                                                  .verifiedDetails ==
                                              null
                                          ? 'Updated '
                                          : 'Updated ${dataCollection.updatedDetails}',
                                      color: const Color.fromARGB(
                                        255,
                                        241,
                                        143,
                                        107,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Align(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: EdgeInsets.only(left: 20, bottom: 10),
                            child: Text(
                              'Rejected Forms',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 5,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ),
                        dataCollection.data!.isEmpty
                            ? const Card(
                                child: Text('No Any Rejected Data Found'),
                              )
                            : Expanded(
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: dataCollection.data!.length,
                                  itemBuilder: (context, index) {
                                    var rejectedData =
                                        dataCollection.data![index];
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 10,
                                        vertical: 10,
                                      ),
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.pushNamed(
                                            context,
                                            IntroductionQuestionScreen
                                                .routeName,
                                          );
                                          dataCollectionAnswer = rejectedData;
                                        },
                                        child: Card(
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Column(
                                              children: [
                                                Text(
                                                  'Shop Name : ${rejectedData.shopName}',
                                                  style: const TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                const Divider(thickness: 1.5),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  children: [
                                                    const Padding(
                                                      padding: EdgeInsets.only(
                                                        left: 5,
                                                      ),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            'CEO : ',
                                                            style: TextStyle(
                                                              fontSize: 15,
                                                            ),
                                                          ),
                                                          Text(
                                                            'Location : ',
                                                            style: TextStyle(
                                                              fontSize: 15,
                                                            ),
                                                          ),
                                                          Text(
                                                            'Contact : ',
                                                            style: TextStyle(
                                                              fontSize: 15,
                                                            ),
                                                          ),
                                                          Text(
                                                            'PAN : ',
                                                            style: TextStyle(
                                                              fontSize: 15,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          ' ${rejectedData.ceoName}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 15,
                                                          ),
                                                        ),
                                                        Text(
                                                          ' ${rejectedData.location}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 15,
                                                          ),
                                                        ),
                                                        Text(
                                                          ' ${rejectedData.contactNumber}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 15,
                                                          ),
                                                        ),
                                                        Text(
                                                          ' ${rejectedData.panNumber}',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 15,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                const SizedBox(
                                                  height: 8,
                                                ),
                                                Text(
                                                  rejectedData.remarks == null
                                                      ? 'Remarks : No Remarks Found ! Click Here To Edit'
                                                      : 'Remarks : ${rejectedData.remarks}',
                                                  textAlign: TextAlign.justify,
                                                  style: const TextStyle(
                                                    fontSize: 15,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                      ],
                    ),
                  );
                }),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 25),
        child: CustomElevatedButton(
          buttonText: 'Create',
          onPressed: () {
            Navigator.pushNamed(
              context,
              IntroductionQuestionScreen.routeName,
            );
            dataCollectionAnswer =
                DataCollectionAnswer(answers: List.filled(36, ''));
          },
        ),
      ),
    );
  }
}
