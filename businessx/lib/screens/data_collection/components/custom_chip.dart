import 'package:flutter/material.dart';

class CustomChip extends StatelessWidget {
  const CustomChip({
    Key? key,
    required this.dataCollectionValue,
    required this.color,
  }) : super(key: key);

  final String dataCollectionValue;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Chip(
      backgroundColor: color,
      label: SizedBox(
        width: 120,
        child: Center(
          child: Text(
            dataCollectionValue,
          ),
        ),
      ),
    );
  }
}
