import 'package:b2b_market/common/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../common/logger/logger.dart';
import '../../models/response/data_collection/data_collection_response.dart';
import '../../route_names.dart';

class BenefitsQuestionScreen extends StatefulWidget {
  const BenefitsQuestionScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<BenefitsQuestionScreen> createState() => _BenefitsQuestionScreenState();
}

class _BenefitsQuestionScreenState extends State<BenefitsQuestionScreen>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final _logger = getLogger(BenefitsQuestionScreen);

  final _q29Controller = TextEditingController();
  final _q30Controller = TextEditingController();
  final _q31Controller = TextEditingController();
  final _q32Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _q29Controller.text = dataCollectionAnswer.answers?[28] ?? '';
    _q30Controller.text = dataCollectionAnswer.answers?[29] ?? '';
    _q31Controller.text = dataCollectionAnswer.answers?[30] ?? '';
    _q32Controller.text = dataCollectionAnswer.answers?[31] ?? '';
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        child: Form(
          key: _formkey,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                PageTitle(
                  title: 'Benefit Question',
                  onBackPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  navbuttontext: 'Next',
                  ontopNavButtonPressed: () {
                    _logger.i(dataCollectionAnswer.toJson());
                    if (_formkey.currentState!.validate()) {
                      Navigator.pushNamed(
                        context,
                        RouteName.willingnessAboutOurPlatformQuestionScreen,
                      );
                    }
                  },
                  showTopNavButton: true,
                ),
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomTextFormField(
                          labelText:
                              '29. What features do you look for when you purchase ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q29Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[28] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '30. What benefits do you look for when you purchase?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q30Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[29] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '31. What problems motivate you to purchase ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q31Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[30] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '32. What needs are you trying to meet when you purchase ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q32Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[31] = value;
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
