import 'package:b2b_market/bloc/data_collection_bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../common/logger/logger.dart';
import '../../common/widgets/custom_app_bar.dart';
import '../../common/widgets/custom_text_form_field.dart';
import '../../common/widgets/custom_toast.dart';
import '../../common/widgets/my_app_drawer.dart';
import '../../common/widgets/page_title.dart';
import '../../models/response/data_collection/data_collection_response.dart';
import '../../route_names.dart';
import '../map_screen.dart';

class IntroductionQuestionScreen extends StatefulWidget {
  const IntroductionQuestionScreen({Key? key}) : super(key: key);

  static const String routeName = 'introductionQuestionScreen';

  @override
  State<IntroductionQuestionScreen> createState() =>
      _IntroductionQuestionScreenState();
}

class _IntroductionQuestionScreenState
    extends State<IntroductionQuestionScreen> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final _logger = getLogger(IntroductionQuestionScreen);

  final _ceoNameController = TextEditingController();
  final _panController = TextEditingController();
  final _shopNameController = TextEditingController();
  final _locationController = TextEditingController();
  final _contactNumberController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _ceoNameController.text = dataCollectionAnswer.ceoName ?? '';
    _panController.text = dataCollectionAnswer.panNumber ?? '';
    _shopNameController.text = dataCollectionAnswer.shopName ?? '';
    _locationController.text = dataCollectionAnswer.location ?? '';
    _contactNumberController.text = dataCollectionAnswer.contactNumber ?? '';
  }

  @override
  void dispose() {
    dataCollectionBloc.locationNotifier.value = '';
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        child: Form(
          key: _formkey,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                PageTitle(
                  title: 'Basic ',
                  onBackPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  showTopNavButton: true,
                  navbuttontext: 'Next',
                  ontopNavButtonPressed: () {
                    if (_formkey.currentState!.validate()) {
                      _logger.i(dataCollectionAnswer.toJson());
                      Navigator.pushNamed(
                        context,
                        RouteName.generalPresenceQuestionScreen,
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomTextFormField(
                          labelText: 'Enter Your Shop Name',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _shopNameController,
                          onChanged: (value) {
                            dataCollectionAnswer.shopName = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText: 'Name of CEO',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _ceoNameController,
                          onChanged: (value) {
                            dataCollectionAnswer.ceoName = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText: 'Your Contact Number',
                          validator: MultiValidator([
                            RequiredValidator(errorText: 'required'),
                            MinLengthValidator(
                              10,
                              errorText: 'Please Enter Correct Phone Number',
                            ),
                          ]),
                          maxLenght: 10,
                          textInputType: TextInputType.number,
                          textInputFormatterList: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          controller: _contactNumberController,
                          onChanged: (value) {
                            dataCollectionAnswer.contactNumber = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText: 'PAN Number of Your Shop',
                          validator: RequiredValidator(errorText: 'required'),
                          textInputType: TextInputType.number,
                          controller: _panController,
                          onChanged: (value) {
                            dataCollectionAnswer.panNumber = value;
                          },
                        ),
                        ColoredBox(
                          color: Colors.transparent,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: ValueListenableBuilder(
                                  valueListenable:
                                      dataCollectionBloc.locationNotifier,
                                  builder: (
                                    context,
                                    location,
                                    _,
                                  ) {
                                    var locationValue = location as String;
                                    if (locationValue.isNotEmpty) {
                                      _locationController.text = locationValue;
                                    }

                                    return CustomTextFormField(
                                      labelText: 'Where Does Your Shop Locate?',
                                      validator: RequiredValidator(
                                        errorText: 'required',
                                      ),
                                      controller: _locationController,
                                      onChanged: (value) {
                                        dataCollectionAnswer.location = value;
                                      },
                                    );
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: IconButton(
                                  onPressed: () async {
                                    var position = await getLocation();
                                    showCustomProgressDialog(
                                      context,
                                      'Loading...',
                                    );
                                    var place =
                                        await dataCollectionBloc.reverseSearch(
                                      LatLng(
                                        position.latitude,
                                        position.longitude,
                                      ),
                                    );
                                    Navigator.pop(context);
                                    if (place.error != null) {
                                      ShowToast.error(place.error);
                                    }

                                    if (place.error == null) {
                                      Navigator.pushNamed(
                                        context,
                                        MapScreen.route,
                                        arguments: place,
                                      );
                                    }
                                  },
                                  iconSize: 35,
                                  icon: const Icon(
                                    Icons.location_on,
                                    color: Colors.blue,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Future<Position> getLocation() async {
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    ShowToast.error('Location services are disabled turn on gps');
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      // openAppSettings();
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    openAppSettings();
    // Permissions are denied forever, handle appropriately.
    return Future.error(
      'Location permissions are permanently denied, we cannot request permissions.',
    );
  }

  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  return await Geolocator.getCurrentPosition();
}
