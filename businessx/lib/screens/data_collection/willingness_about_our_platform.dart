import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../bloc/data_collection_bloc.dart';
import '../../common/logger/logger.dart';
import '../../common/widgets/custom_app_bar.dart';
import '../../common/widgets/custom_text_form_field.dart';
import '../../common/widgets/my_app_drawer.dart';
import '../../common/widgets/page_title.dart';
import '../../models/response/data_collection/data_collection_response.dart';

class WillingnessAboutOurPlatformScreen extends StatefulWidget {
  const WillingnessAboutOurPlatformScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<WillingnessAboutOurPlatformScreen> createState() =>
      _WillingnessAboutOurPlatformScreenState();
}

class _WillingnessAboutOurPlatformScreenState
    extends State<WillingnessAboutOurPlatformScreen>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  final _logger = getLogger(WillingnessAboutOurPlatformScreen);
  final _q33Controller = TextEditingController();
  final _q34Controller = TextEditingController();
  final _q35Controller = TextEditingController();
  final _q36Controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    _q33Controller.text = dataCollectionAnswer.answers?[32] ?? '';
    _q34Controller.text = dataCollectionAnswer.answers?[33] ?? '';
    _q35Controller.text = dataCollectionAnswer.answers?[34] ?? '';
    _q36Controller.text = dataCollectionAnswer.answers?[35] ?? '';
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
        child: Form(
          key: _formkey,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                PageTitle(
                  title: 'Willingness ',
                  onBackPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  navbuttontext: 'Submit',
                  showTopNavButton: true,
                  ontopNavButtonPressed: () {
                    if (_formkey.currentState!.validate()) {
                      _logger.i(dataCollectionAnswer.toString());
                      if (dataCollectionAnswer.id == null) {
                        dataCollectionBloc.postDatacollect(
                          context,
                          dataCollectionAnswer,
                        );
                      } else {
                        int id = dataCollectionAnswer.id!.toInt();
                        dataCollectionBloc.updateDatacollect(
                          context,
                          dataCollectionAnswer,
                          id,
                        );
                      }
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomTextFormField(
                          labelText:
                              '33. Are they excited to add their business in our platform?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q33Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[32] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '34. Are they willing to become buyer of B2B? If No, why not?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q34Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[33] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '35. Are they willing to become a seller of B2C? If no, why not?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q35Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[34] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '36. Is our mobile application is user friendly? If no, please update feedback.',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q36Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[35] = value;
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
