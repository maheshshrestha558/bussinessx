import 'package:b2b_market/common/widgets/custom_app_bar.dart';
import 'package:b2b_market/common/widgets/custom_label_text.dart';
import 'package:b2b_market/common/widgets/my_app_drawer.dart';
import 'package:b2b_market/models/response/data_collection/data_collection_response.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../common/logger/logger.dart';
import '../../common/widgets/custom_text_form_field.dart';
import '../../common/widgets/page_title.dart';
import '../../route_names.dart';

class GeneralPresenceQuestionScreen extends StatefulWidget {
  const GeneralPresenceQuestionScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<GeneralPresenceQuestionScreen> createState() =>
      _GeneralPresenceQuestionScreenState();
}

class _GeneralPresenceQuestionScreenState
    extends State<GeneralPresenceQuestionScreen>
    with AutomaticKeepAliveClientMixin {
  final _logger = getLogger(GeneralPresenceQuestionScreen);

  int? _radioGroupValue = 0;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();
  final _q1Controller = TextEditingController();
  final _q2Controller = TextEditingController();
  final _q3Controller = TextEditingController();
  // final _q4Controller = TextEditingController();
  final _q5Controller = TextEditingController();
  final _q6Controller = TextEditingController();
  final _q7Controller = TextEditingController();
  final _q8Controller = TextEditingController();
  final _q9Controller = TextEditingController();
  final _q10Controller = TextEditingController();
  final _q11Controller = TextEditingController();
  final _q12Controller = TextEditingController();
  final _q13Controller = TextEditingController();
  final _q14Controller = TextEditingController();
  final _q15Controller = TextEditingController();
  final _q16Controller = TextEditingController();
  final _q17Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _q1Controller.text = dataCollectionAnswer.answers?[0] ?? '';
    _q2Controller.text = dataCollectionAnswer.answers?[1] ?? '';
    _q3Controller.text = dataCollectionAnswer.answers?[2] ?? '';
    _radioGroupValue = dataCollectionAnswer.answers?[3] == 'Yes' ? 1 : 2;
    _q5Controller.text = dataCollectionAnswer.answers?[4] ?? '';
    _q6Controller.text = dataCollectionAnswer.answers?[5] ?? '';
    _q7Controller.text = dataCollectionAnswer.answers?[6] ?? '';
    _q8Controller.text = dataCollectionAnswer.answers?[7] ?? '';
    _q9Controller.text = dataCollectionAnswer.answers?[8] ?? '';
    _q10Controller.text = dataCollectionAnswer.answers?[9] ?? '';
    _q11Controller.text = dataCollectionAnswer.answers?[10] ?? '';
    _q12Controller.text = dataCollectionAnswer.answers?[11] ?? '';
    _q13Controller.text = dataCollectionAnswer.answers?[12] ?? '';
    _q14Controller.text = dataCollectionAnswer.answers?[13] ?? '';
    _q15Controller.text = dataCollectionAnswer.answers?[14] ?? '';
    _q16Controller.text = dataCollectionAnswer.answers?[15] ?? '';
    _q17Controller.text = dataCollectionAnswer.answers?[16] ?? '';
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 0),
        child: Form(
          key: _formkey,
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                PageTitle(
                  title: 'General Presence',
                  onBackPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  showTopNavButton: true,
                  navbuttontext: 'Next',
                  ontopNavButtonPressed: () {
                    dataCollectionAnswer.answers?[3] =
                        _radioGroupValue == 1 ? 'Yes' : 'No';
                    _logger.i(dataCollectionAnswer.toJson());
                    if (_formkey.currentState!.validate()) {
                      Navigator.pushNamed(
                        context,
                        RouteName.buyingPatternQuestionScreen,
                      );
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomTextFormField(
                          labelText: '1. Nature of Business.',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q1Controller,
                          hintText:
                              'Grocery/Automobile/Construction/Electronics/Groceries',
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[0] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '2. Do you have access of mobile data or WiFi? ',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q2Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[1] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '3. What kind of Mobile phone do you you have?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q3Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[2] = value;
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 20.0,
                            right: 5.0,
                            top: 10,
                            bottom: 10,
                          ),
                          child: Column(
                            children: [
                              const CustomLabelText(
                                labelText:
                                    '4. Have you assigned/integrated with any digital Platform?',
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _radioGroupValue = 1;
                                        });
                                      },
                                      child: ListTile(
                                        minLeadingWidth: 5,
                                        contentPadding: const EdgeInsets.all(2),
                                        leading: Transform.translate(
                                          offset: const Offset(40, 0),
                                          child: const CustomLabelText(
                                            labelText: 'Yes',
                                          ),
                                        ),
                                        title: Radio(
                                          value: 1,
                                          groupValue: _radioGroupValue,
                                          onChanged: (int? value) {
                                            setState(() {
                                              _radioGroupValue = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          _radioGroupValue = 2;
                                        });
                                      },
                                      child: ListTile(
                                        minLeadingWidth: 5,
                                        leading: Transform.translate(
                                          offset: const Offset(40, 0),
                                          child: const CustomLabelText(
                                            labelText: 'No',
                                          ),
                                        ),
                                        title: Radio(
                                          value: 2,
                                          groupValue: _radioGroupValue,
                                          onChanged: (int? value) {
                                            setState(() {
                                              _radioGroupValue = value;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        CustomTextFormField(
                          labelText:
                              '5. If yes ? Please specify. If not? Take a reason from shop.',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q5Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[4] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '6. Are you satisfied with existing digital platform/delivery services?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q6Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[5] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '7. Do you have facebook/Instagram/Tiktok page?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q7Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[6] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '8. Do you have google my business profile listing?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q8Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[7] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '9. Do you  boost your products through Social Media like facebook/Instagram/Google Add?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q9Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[8] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText: '10. Do you have e-commerce site?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q10Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[9] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '11. Do you have promotional video of your store/product?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q11Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[10] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '12. Do your have logo, graphics and do you need branding?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q12Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[11] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '13. Do you need product promo video / social media page handelling?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q13Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[12] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '14. What is your company`s gross revenue?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q14Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[13] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '15. How many people does your company employ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q15Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[14] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '16. How often do you try to find new wholesalers ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q16Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[15] = value;
                          },
                        ),
                        CustomTextFormField(
                          labelText:
                              '17. What is your monthly Online advertising budget to finding a new wholesale  ?',
                          validator: RequiredValidator(errorText: 'required'),
                          controller: _q17Controller,
                          onChanged: (value) {
                            dataCollectionAnswer.answers?[16] = value;
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      
    );
  }

  @override
  bool get wantKeepAlive => true;
}
