import 'package:b2b_market/screens/buyer_screens/buyer_navigation_screen.dart';
import 'package:flutter/material.dart';

import '../bloc/bloc.dart';
import '../common/decoration.dart';
import '../common/logger/logger.dart';
import '../repositories/base_repository.dart';
import '../route_names.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  final _logger = getLogger(SplashScreen);

  @override
  void initState() {
    super.initState();
    [
      bannerBloc.getBanner(),
      brandBloc.fetchAll(),
      categoryBloc.getRootCategories(),
      categoryBloc.getRootWithSubCategories(),
      platformDetailsBloc.getPlatformDetails()
    ];

    _controller = AnimationController(
      vsync: this,
      lowerBound: 0.5,
      upperBound: 1,
      duration: const Duration(seconds: 3),
    );

    _controller.forward();

    _controller.addStatusListener((status) async {
      if (status == AnimationStatus.completed) {
        baseRepository.isUserLoggedIn = await authBloc.isUserAuthenticated();
        bool userIsBuyer = await authBloc.userIsBuyer();
        if (baseRepository.isUserLoggedIn && userIsBuyer) {
          _logger.i('user is buyer $userIsBuyer');

          Navigator.pushReplacementNamed(
            context,
            BuyerNavigationScreen.routeName,
          );
        }else
        if (baseRepository.isUserLoggedIn && (userIsBuyer == false)) {
          Navigator.pushReplacementNamed(
            context,
            RouteName.navigationScreen,
          );
        } 
        else {
          Navigator.pushReplacementNamed(
            context,
            RouteName.loginOptionsScreen,
          );
        }
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DecoratedBox(
        decoration: appBarDecoration,
        child: Center(
          child: ScaleTransition(
            scale: _controller,
            child: const Text(
              'BUSINESS X',
              style: TextStyle(
                fontSize: 28,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
