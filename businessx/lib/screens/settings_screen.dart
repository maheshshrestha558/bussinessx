import 'package:flutter/material.dart';

import '../common/decoration.dart';
import '../common/widgets/widgets.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  String dropdownValue = 'English';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 16),
          const PageTitle(title: 'Settings'),
          const SettingsOptions(title: 'Receive Notification'),
          const SettingsOptions(title: 'Login with finger Print or FaceID'),
          const SettingsOptions(title: 'Auto Update App'),
          ListTile(
            onTap: null,
            onLongPress: null,
            title: const Text('Language', style: TextStyle(fontSize: 14)),
            trailing: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton<String>(
                borderRadius: BorderRadius.circular(10),
                value: dropdownValue,
                elevation: 16,
                underline: Container(),
                onChanged: (String? newValue) {
                  setState(() {
                    dropdownValue = newValue!;
                  });
                },
                items: <String>['English', 'Nepali']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
