import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pusher_client/pusher_client.dart';

import '../../bloc/chat_bloc.dart';
import '../../common/decoration.dart';
import '../../common/widgets/widgets.dart';
import '../../models/message_entity.dart';
import '../../models/response/lead/view_leads_response.dart';
import '../../repositories/repositories.dart';
import 'components/components.dart';

class ChatScreen extends StatefulWidget {
  final Lead lead;
  final int userId;
  final String token;
  final String title;
  const ChatScreen({
    required this.lead,
    required this.userId,
    required this.token,
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  static const String appKey = '6b8a219c9e8d1ccbd4d0';
  static const String pusherCluster = 'ap1';

  final _scrollController = ScrollController();
  int _page = 1;

  late PusherClient pusher;
  bool? _isSentByMe;

  @override
  initState() {
    super.initState();
    chatBloc.fetchMessages(widget.lead.id!, _page);
    pusher = PusherClient(
      appKey,
      PusherOptions(
        host: baseRepository.baseUrl,
        cluster: pusherCluster,
        auth: PusherAuth(
          '${baseRepository.baseUrl}/broadcasting/auth',
          headers: {
            HttpHeaders.acceptHeader: 'application/json',
            'Authorization': 'Bearer ${widget.token}',
          },
        ),
      ),
      enableLogging: true,
    );

    pusher.connect();

    Channel channel = pusher.subscribe('private-chat.${widget.lead.id}');
    // Channel _notificationChannel =
    //     pusher.subscribe('private-App.models.User.${widget.userId}');

    pusher.onConnectionStateChange((state) {
      baseRepository.socketId = pusher.getSocketId();
    });

    pusher.onConnectionError((error) {
      if (error != null) {
        debugPrint('error: ${error.message.toString()}');
      }
    });

    channel.bind(
      'chat-message',
      (PusherEvent? event) {
        if (event?.data != null) {
          final data = jsonDecode(event!.data!);
          final messageEntity = MessageEntity.fromJson(data);
          chatBloc.addMessage(messageEntity);
        }
      },
    );

    // _notificationChannel.bind(
    //   'notification',
    //   (PusherEvent? event) {
    //     if (event?.data != null) {
    //       debugPrint('notification: ${event?.data}');
    //     }
    //   },
    // );
  }

  @override
  void didChangeDependencies() {
    _scrollController.addListener(() {
      double currentPosition = _scrollController.position.pixels;
      double maxScrollExtent = _scrollController.position.maxScrollExtent;
      if (currentPosition == maxScrollExtent) {
        if (chatBloc.lastPage != _page) {
          _page++;
          if (widget.lead.id != null) {
            chatBloc.fetchMessages(widget.lead.id!, _page);
          }
        }
      }
    });
    super.didChangeDependencies();
  }

  @override
  dispose() {
    _scrollController.dispose();
    chatBloc.dispose();
    baseRepository.socketId = null;
    pusher.disconnect();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 10),
          PageTitle(title: widget.title),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: StreamBuilder<List<MessageEntity>>(
                stream: chatBloc.messages,
                builder: (context, snapshot) {
                  var messageEntities = snapshot.data;
                  if (messageEntities != null && messageEntities.isNotEmpty) {
                    messageEntities.sort((a, b) => b.id.compareTo(a.id));
                    return ListView.separated(
                      controller: _scrollController,
                      reverse: true,
                      physics: const BouncingScrollPhysics(),
                      separatorBuilder: (context, index) {
                        return const SizedBox(
                          height: 8,
                        );
                      },
                      itemCount: messageEntities.length,
                      itemBuilder: (context, index) {
                        var messageEntity = messageEntities[index];
                        if (messageEntity.fileElements.isNotEmpty) {
                          _isSentByMe = widget.userId == messageEntity.senderId;
                          return ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: messageEntity.fileElements.length,
                            itemBuilder: (context, index) {
                              var filePath = messageEntity
                                  .fileElements[index].imageThumbnail;
                              return _isSentByMe!
                                  ? SentImageCard(filePath ?? '')
                                  : ReceivedImageCard(filePath ?? '');
                            },
                          );
                        } else {
                          return MessageCard(
                            messageEntity: messageEntity,
                            userId: widget.userId,
                          );
                        }
                      },
                    );
                  }
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Color(0xff29ed92),
                    ),
                  );
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: TypeMessageSection(
                widget.lead,
                widget.userId,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
