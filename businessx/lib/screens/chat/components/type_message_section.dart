import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import '../../../bloc/chat_bloc.dart';
import '../../../models/response/lead/view_leads_response.dart';

class TypeMessageSection extends StatefulWidget {
  final Lead lead;
  final int userId;
  const TypeMessageSection(
    this.lead,
    this.userId, {
    Key? key,
  }) : super(key: key);

  @override
  State<TypeMessageSection> createState() => _TypeMessageSectionState();
}

class _TypeMessageSectionState extends State<TypeMessageSection> {
  final _messageController = TextEditingController();

  final ImagePicker _picker = ImagePicker();
  List<XFile>? images = [];

  @override
  initState() {
    super.initState();
    chatBloc.setLead(widget.lead);
    chatBloc.setUserId(widget.userId);
  }

  Future _img() async {
    images?.clear();
    _messageController.clear();
    try {
      final pickedImages = await _picker.pickMultiImage(imageQuality: 95);
      if (pickedImages == null) return;
      images?.addAll(pickedImages.take(4));
      debugPrint('Image path: $images');
      if (images != null && images!.isNotEmpty) {
        chatBloc.setImages(images);
        chatBloc.setMessage(null);
        final response = await chatBloc.sendMessage(context);
        if (response.status == 200) {
          var messageEntity = response.messageEntity;
          if (messageEntity != null) {
            chatBloc.addMessage(messageEntity);
          }
          images?.clear();
        }
      }

      setState(() {});
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image $e');
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Photo Library'),
                subtitle: const Text(
                  'Max 4 photos',
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 12,
                  ),
                ),
                onTap: () {
                  _img();
                  Navigator.of(context).pop();
                },
              ),
              // ListTile(
              //   leading: const Icon(Icons.photo_camera),
              //   title: const Text('Camera'),
              //   onTap: () {
              //     _img();
              //     Navigator.of(context).pop();
              //   },
              // ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 10, bottom: 10, top: 10),
      height: 60,
      width: double.infinity,
      color: const Color.fromRGBO(41, 222, 146, 0.3),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () => _showPicker(context),
            child: const Icon(
              Icons.add_circle_outline_outlined,
              color: Colors.black,
              size: 30,
            ),
          ),
          const SizedBox(
            width: 15,
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.25),
                  spreadRadius: 0,
                  blurRadius: 4,
                  offset: Offset(0, 4), // changes position of shadow
                ),
              ],
            ),
            child: TextField(
              controller: _messageController,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(10),
                hintText: 'Type your message',
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(30),
                ),
                suffixIcon: TextButton(
                  onPressed: () async {
                    debugPrint('Image path: $images');

                    if (_messageController.text.isNotEmpty) {
                      chatBloc.setMessage(_messageController.text);

                      final response = await chatBloc.sendMessage(context);
                      if (response.status == 200) {
                        var messageEntity = response.messageEntity;
                        if (messageEntity != null) {
                          chatBloc.addMessage(messageEntity);
                        }
                        _messageController.clear();
                        chatBloc.setMessage(null);
                      }
                    }
                  },
                  child: const Text(
                    'send',
                    style: TextStyle(fontSize: 14, color: Colors.blue),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
