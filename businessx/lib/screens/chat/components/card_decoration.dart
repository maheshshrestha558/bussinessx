import 'package:flutter/material.dart';

const recieverColor = Color.fromRGBO(29, 78, 216, 0.3);
const circularRadius8 = Radius.circular(8);
const senderColor = Color.fromRGBO(41, 222, 146, 0.3);
const padding8All = EdgeInsets.all(8);
