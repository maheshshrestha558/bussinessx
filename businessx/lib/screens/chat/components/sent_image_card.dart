import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../screens.dart';
import 'card_decoration.dart';

class SentImageCard extends StatelessWidget {
  const SentImageCard(
    this.filePath, {
    Key? key,
  }) : super(key: key);
  final String filePath;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.6,
            height: MediaQuery.of(context).size.height * 0.3,
            padding: padding8All,
            decoration: const BoxDecoration(
              color: senderColor,
              borderRadius: BorderRadius.only(
                topLeft: circularRadius8,
                topRight: circularRadius8,
                bottomLeft: circularRadius8,
              ),
            ),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ViewImageScreen(filePath),
                  ),
                );
              },
              child: Hero(
                tag: filePath,
                child: CachedNetworkImage(
                  imageUrl: filePath,
                  fit: BoxFit.cover,
                  errorWidget: (context, url, error) {
                    return const Icon(Icons.error);
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}


