import '../../../models/models.dart';
import 'package:flutter/material.dart';

class MessageCard extends StatelessWidget {
  final MessageEntity messageEntity;
  final int userId;

  const MessageCard({
    Key? key,
    required this.messageEntity,
    required this.userId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return messageEntity.message != null
        ? Align(
            alignment: userId == messageEntity.senderId
                ? Alignment.centerRight
                : Alignment.centerLeft,
            child: Container(
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width * 0.7,
                ),
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: userId == messageEntity.senderId
                      ? const Color.fromRGBO(41, 222, 146, 0.3)
                      : const Color.fromRGBO(29, 78, 216, 0.3),
                  borderRadius: userId == messageEntity.senderId
                      ? const BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                          bottomLeft: Radius.circular(8),
                        )
                      : const BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                          bottomRight: Radius.circular(8),
                        ),
                ),
                child: Text(
                  messageEntity.message!,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),),
          )
        : const SizedBox();
  }
}
