import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MyCachedNetworkImage extends StatelessWidget {
  const MyCachedNetworkImage({
    Key? key,
    required this.filePath,
  }) : super(key: key);

  final String filePath;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      height: 45,
      imageUrl: filePath,
      fit: BoxFit.cover,
      errorWidget: (context, url, error) {
        return const Icon(Icons.error);
      },
    );
  }
}
