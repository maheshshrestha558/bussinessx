import '../../bloc/bloc.dart';
import '../../bloc/chat_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:open_file/open_file.dart';

class ViewImageScreen extends StatefulWidget {
  const ViewImageScreen(this.filePath, {Key? key}) : super(key: key);
  final String filePath;

  @override
  State<ViewImageScreen> createState() => _ViewImageScreenState();
}

class _ViewImageScreenState extends State<ViewImageScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    super.initState();
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(
      SystemUiMode.manual,
      overlays: SystemUiOverlay.values,
    );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      key: _scaffoldKey,
      body: GestureDetector(
        onVerticalDragDown: (_) {
          // Navigator.pop(context);
        },
        child: Stack(
          children: [
            Hero(
              tag: widget.filePath,
              child: CachedNetworkImage(
                height: MediaQuery.of(context).size.height,
                imageUrl: widget.filePath,
                errorWidget: (context, url, error) {
                  return const Icon(Icons.error);
                },
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Container(
                  height: 50,
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: const Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.file_download,
                          color: Colors.white,
                        ),
                        onPressed: () async {
                          var directory =
                              await permissionsBloc.getDownloadDirectory();
                          debugPrint(directory.path);

                          final isPermissionGranted =
                              await permissionsBloc.requestPermissions();
                          debugPrint(isPermissionGranted.toString());
                          const androidDownloadPath =
                              '/storage/emulated/0/Download';

                          await chatBloc.downloadImage(
                            widget.filePath,
                            '$androidDownloadPath/image.jpg',
                          );

                          var snackbar = SnackBar(
                            content: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const Text('Downloaded successfully'),
                                ElevatedButton(
                                  onPressed: () {
                                    OpenFile.open(
                                      '$androidDownloadPath/image.jpg',
                                    );
                                  },
                                  child: const Text(
                                    'Open',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.green,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            backgroundColor: Colors.green,
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackbar);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
