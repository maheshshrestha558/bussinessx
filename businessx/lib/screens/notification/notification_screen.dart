import 'package:flutter/material.dart';

import '../../bloc/bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/notifications_response.dart';
import 'components/notification_card.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    notificationBloc.getAllNotifications();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
        showBellIcon: false,
      ),
      drawer: const MyAppDrawer(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          const PageTitle(title: 'Notifications'),
          ColoredBox(
            color: Theme.of(context).canvasColor,
            child: TabBar(
              indicatorColor: Colors.transparent,
              controller: _tabController,
              labelColor: const Color.fromRGBO(52, 211, 153, 1),
              unselectedLabelColor: Colors.grey,
              tabs: const [
                Tab(
                  text: 'Lead Notifications',
                ),
                Tab(
                  text: 'General Notifications',
                ),
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              physics: const BouncingScrollPhysics(),
              children: [
                //Lead Notifications
                StreamBuilder<NotificationsResponse>(
                  stream: notificationBloc.notificationResponse.stream,
                  builder: (context, snapshot) {
                    var leadNotifications =
                        snapshot.data?.leadNotificationData?.leadNotifications;

                    if (snapshot.data?.error != null) {
                      var error = snapshot.data!.error;
                      return Center(
                        child: Text(error.toString()),
                      );
                    }
                    if (leadNotifications == null) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    if (leadNotifications.isEmpty) {
                      return const Center(
                        child: Text('No notifications'),
                      );
                    }
                    return ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: leadNotifications.length,
                      itemBuilder: (context, index) {
                        var leadNotification = leadNotifications[index];
                        return NotificationCard(
                          notification: leadNotification,
                        );
                      },
                    );
                  },
                ),

                //General Notifications
                StreamBuilder<NotificationsResponse>(
                  stream: notificationBloc.notificationResponse.stream,
                  builder: (context, snapshot) {
                    var generalNotifications = snapshot
                        .data?.generalNotificationData?.generalNotifications;
                    if (generalNotifications == null) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    if (snapshot.data?.error != null) {
                      var error = snapshot.data!.error;
                      return Center(
                        child: Text(error.toString()),
                      );
                    }
                    if (generalNotifications.isEmpty) {
                      return const Center(
                        child: Text('No notifications'),
                      );
                    }
                    return ListView.builder(
                      physics: const BouncingScrollPhysics(),
                      itemCount: generalNotifications.length,
                      itemBuilder: (context, index) {
                        var generalNotification = generalNotifications[index];
                        return NotificationCard(
                          notification: generalNotification,
                        );
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
