import 'dart:io';

import 'package:b2b_market/bloc/bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:image_picker/image_picker.dart';

import '../../common/extensions/string_extensions.dart';
import '../../common/widgets/widgets.dart';
import '../../models/user.dart';

class EditProfile extends StatefulWidget {
  final User user;

  const EditProfile({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final ImagePicker _picker = ImagePicker();
  final _editProfileFormKey = GlobalKey<FormState>();

  final List<String> _genders = ['Male', 'Female', 'Other'];
  String? _selectedGender;
  File? image;

  final _enNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _contactController = TextEditingController();
  final _addressController = TextEditingController();
  final _designationController = TextEditingController();
  final _alternateContactController = TextEditingController();

  @override
  void initState() {
    var user = widget.user;
    _enNameController.text = user.enName.returnEmptyStringIfNull();
    _emailController.text = user.email.returnEmptyStringIfNull();
    _contactController.text = user.contactNumber.returnEmptyStringIfNull();
    _alternateContactController.text =
        user.alternateContactNumber.returnEmptyStringIfNull();
    _designationController.text = user.designation.returnEmptyStringIfNull();
    _addressController.text = user.address.returnEmptyStringIfNull();
    _selectedGender = user.gender.capitalizeFirstLetter();
    super.initState();
  }

  @override
  dispose() {
    _enNameController.dispose();
    _emailController.dispose();
    _contactController.dispose();
    _addressController.dispose();
    _designationController.dispose();
    _alternateContactController.dispose();
    super.dispose();
  }

  Future _img(ImageSource source) async {
    try {
      final image = await _picker.pickImage(source: source, imageQuality: 95);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image $e');
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Photo Library'),
                onTap: () {
                  _img(ImageSource.gallery);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () {
                  _img(ImageSource.camera);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          PageTitle(
            title: 'Edit Profile',
            showTopNavButton: true,
            navbuttontext: 'Save',
            ontopNavButtonPressed: () {
              if (_editProfileFormKey.currentState!.validate()) {
                final user = widget.user.copyWith(
                  address: _addressController.text,
                  alternateContactNumber: _alternateContactController.text,
                  contactNumber: _contactController.text,
                  email: _emailController.text,
                  enName: _enNameController.text,
                  designation: _designationController.text,
                  gender: _selectedGender,
                );
                usersBloc.updateUser(
                  context,
                  user,
                  image,
                );
              }
            },
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: SingleChildScrollView(
                child: Form(
                  key: _editProfileFormKey,
                  child: Column(
                    children: [
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            _showPicker(context);
                          },
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                width: 2,
                                color: const Color(0xff77dce1),
                              ),
                            ),
                            child: image != null
                                ? ClipOval(
                                    child: Image.file(
                                      image!,
                                      width: 90,
                                      height: 90,
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : Center(
                                    child: Container(
                                      height: 90,
                                      width: 90,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: const Color(0xff77dce1),
                                          width: 2,
                                        ),
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                      ),
                                      child: CachedNetworkImage(
                                        placeholder: (context, url) => Center(
                                          child: Text(
                                            widget.user.enName
                                                .getCapitalizedFirstLetter(),
                                          ),
                                        ),
                                        imageUrl: widget.user.imageThumbnail
                                            .returnEmptyStringIfNull(),
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          padding: const EdgeInsets.all(10),
                                          height: 90,
                                          width: 90,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Center(
                                          child: Text(
                                            widget.user.enName
                                                .getCapitalizedFirstLetter(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 50.0, right: 50.0),
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            foregroundColor: const Color.fromRGBO(29, 78, 216, 1),
                            backgroundColor: Colors.white,
                            shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                            ),
                            side: const BorderSide(
                              color: Color.fromRGBO(29, 78, 216, 1),
                              width: 3,
                            ),
                          ),
                          onPressed: () => _showPicker(context),
                          child: const Text('Change Avatar'),
                        ),
                      ),
                      const SizedBox(
                        height: 35,
                      ),
                      CustomTextFormField(
                        controller: _enNameController,
                        labelText: 'Name',
                        validator: RequiredValidator(errorText: 'Required'),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomTextFormField(
                        controller: _contactController,
                        textInputType: TextInputType.phone,
                        labelText: 'Contact Number',
                        validator: RequiredValidator(errorText: 'Required'),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomTextFormField(
                        controller: _alternateContactController,
                        labelText: 'Alternate Number',
                        textInputType: TextInputType.phone,
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomTextFormField(
                        controller: _emailController,
                        labelText: 'Email',
                        validator: MultiValidator([
                          RequiredValidator(errorText: 'Required'),
                          EmailValidator(errorText: 'Invalid Email'),
                        ]),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomTextFormField(
                        controller: _addressController,
                        labelText: 'Address',
                        validator: RequiredValidator(errorText: 'Required'),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const CustomLabelText(labelText: 'Gender'),
                            const SizedBox(
                              height: 5.0,
                            ),
                            ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButtonHideUnderline(
                                child: DropdownButtonFormField<String>(
                                  decoration: const InputDecoration(
                                    errorMaxLines: 1,
                                    contentPadding: EdgeInsets.only(
                                      right: 5.0,
                                      top: 10,
                                      bottom: 10,
                                    ),
                                    filled: true,
                                    fillColor: Color(0xffffffff),
                                    hintStyle: TextStyle(
                                      color: Color(0xFFC4C4C4),
                                      fontSize: 14,
                                    ),
                                    border: border,
                                    focusedBorder: focusedBorder,
                                    enabledBorder: enabledBorder,
                                  ),
                                  hint: const Text('Select One'),
                                  validator: (value) {
                                    if (value == null) {
                                      return 'Required';
                                    }
                                    return null;
                                  },
                                  elevation: 0,
                                  isExpanded: true,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  value: _selectedGender,
                                  onChanged: (value) {
                                    _selectedGender = value;

                                    FocusScope.of(context).unfocus();

                                    setState(() {});
                                  },
                                  items: _genders.map((value) {
                                    return DropdownMenuItem(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        controller: _designationController,
                        labelText: 'Designation',
                        validator: RequiredValidator(errorText: 'Required'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
