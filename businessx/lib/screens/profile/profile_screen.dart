import 'package:flutter/material.dart';

import '../../bloc/bloc.dart';
import '../../models/response/response.dart';
import 'components/components.dart';


class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<UserResponse>(
      stream: usersBloc.user.stream,
      builder: (context, snapshot) {
        var user = snapshot.data?.user;
        if (user != null) {
          return ListView(
            children: [
              const SizedBox(
                height: 35,
              ),
              ProfilePictureSection(user: user),
              ProfileDetailsSection(user: user),
              ButtonsSection(user: user),
            ],
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(
              color: Color(0xff29ed92),
            ),
          );
        }
      },
    );
  }
}


