import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '/common/extensions/string_extensions.dart';
import '../../../common/widgets/custom_label_text.dart';
import '../../../models/user.dart';

class ProfileDetailsSection extends StatelessWidget {
  const ProfileDetailsSection({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Text(
            user.enName.showDashIfNull(),
            textAlign: TextAlign.center,
            style: GoogleFonts.ubuntu(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        Text(
          user.email.showDashIfNull(),
          textAlign: TextAlign.center,
          style: GoogleFonts.ubuntu(
            fontSize: 15,
            fontWeight: FontWeight.w300,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.1),
                  offset: Offset.zero,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  CustomLabelText(
                    labelText: 'Contact Number',
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  CustomLabelText(
                    labelText: user.contactNumber.showDashIfNull(),
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomLabelText(
                    labelText: 'Alternate Contact Number',
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  CustomLabelText(
                    labelText: user.alternateContactNumber.showDashIfNull(),
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomLabelText(
                    labelText: 'Address',
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  CustomLabelText(
                    labelText: user.address.showDashIfNull(),
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomLabelText(
                    labelText: 'Gender',
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  CustomLabelText(
                    labelText: user.gender.showDashIfNull(),
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomLabelText(
                    labelText: 'Designation',
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  CustomLabelText(
                    labelText: user.designation.showDashIfNull(),
                    textStyle: GoogleFonts.ubuntu(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
