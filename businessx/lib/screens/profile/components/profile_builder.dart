import '../../../bloc/bloc.dart';
import 'package:flutter/material.dart';

import '../../../common/widgets/widgets.dart';
import '../../../repositories/repositories.dart';
import 'create_profile_message.dart';

class ProfileBuilder extends StatefulWidget {
  const ProfileBuilder({
    Key? key,
  }) : super(key: key);

  @override
  State<ProfileBuilder> createState() => _ProfileBuilderState();
}

class _ProfileBuilderState extends State<ProfileBuilder> {
  @override
  void initState() {
    super.initState();
    usersBloc.getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: baseRepository.isUserLoggedIn
          ? const ProfileScreen()
          : const CreateProfileMessage(),
    );
  }
}
