import 'package:b2b_market/route_names.dart';
import 'package:flutter/material.dart';

import '../../../common/widgets/widgets.dart';

class CreateProfileMessage extends StatelessWidget {
  const CreateProfileMessage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 28.0, right: 28.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Looks like you don’t have a profile.',
            style: Theme.of(context).textTheme.headlineLarge,
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 30,
          ),
          CustomOutlinedButton(
            title: 'Create Profile',
            color: const Color(0xff29DE92),
            onPressed: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                RouteName.loginOptionsScreen,
                (route) => false,
              );
            },
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
