import 'dart:ui';

import 'package:flutter/material.dart';

import '../../../bloc/auth_bloc.dart';
import '../../../common/widgets/bottom_sheet_button.dart';
import '../../../models/models.dart';
import '../../../route_names.dart';

class ButtonsSection extends StatelessWidget {
  const ButtonsSection({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User? user;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MyListTileButton(
          title: 'Edit Profile',
          iconData: Icons.person_outline_outlined,
          onTap: () {
            Navigator.pushNamed(
              context,
              RouteName.editProfileScreen,
              arguments: user,
            );
          },
        ),
        const SizedBox(
          height: 10,
        ),
        MyListTileButton(
          title: 'Change Password',
          iconData: Icons.edit,
          onTap: () {
            Navigator.pushNamed(
              context,
              RouteName.changePasswordScreen,
            );
          },
        ),
        const SizedBox(
          height: 10,
        ),
        MyListTileButton(
          title: 'Logout',
          iconData: Icons.exit_to_app,
          onTap: () => showLogoutPopUp(context),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}

Future<dynamic> showLogoutPopUp(BuildContext context) {
  return showModalBottomSheet(
    context: context,
    builder: (newContext) => BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: Container(
        height: 150,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            tileMode: TileMode.clamp,
            colors: [
              Color(0x886AFD93),
              Color(0x887CD0FF),
            ],
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 10,
            ),
            Text(
              'Logout',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Are you sure you want to Logout ?',
              style: Theme.of(context).textTheme.bodySmall,
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                BottomSheetButton(
                  buttonLabel: 'Logout',
                  backgroundColor: Colors.red,
                  onPressed: () {
                    Navigator.pop(newContext);
                    authBloc.logOut(context);
                  },
                ),
                BottomSheetButton(
                  buttonLabel: 'Cancel',
                  backgroundColor: Colors.white,
                  onPressed: () => Navigator.pop(context),
                ),
              ],
            ),
          ],
        ),
      ),
    ),
  );
}

class MyListTileButton extends StatelessWidget {
  const MyListTileButton({
    Key? key,
    required this.title,
    required this.iconData,
    required this.onTap,
  }) : super(key: key);
  final String title;
  final IconData iconData;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: ListTile(
        leading: Icon(
          iconData,
          color: const Color(0xff1D4ED8),
        ),
        tileColor: Colors.white,
        onTap: onTap,
        title: Text(title),
      ),
    );
  }
}
