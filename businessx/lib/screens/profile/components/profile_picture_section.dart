import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../models/user.dart';
import '/common/extensions/string_extensions.dart';

class ProfilePictureSection extends StatelessWidget {
  const ProfilePictureSection({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 90,
        width: 90,
        decoration: BoxDecoration(
          border: Border.all(
            color: const Color(0xff77dce1),
            width: 2,
          ),
          shape: BoxShape.circle,
          color: Colors.white,
        ),
        child: CachedNetworkImage(
          placeholder: (context, url) => Center(
            child: Text(
              user.enName.getCapitalizedFirstLetter(),
            ),
          ),
          imageUrl: user.imageThumbnail.returnEmptyStringIfNull(),
          imageBuilder: (context, imageProvider) => Container(
            padding: const EdgeInsets.all(10),
            height: 90,
            width: 90,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.cover,
              ),
            ),
          ),
          errorWidget: (context, url, error) => Center(
            child: Text(
              user.enName.getCapitalizedFirstLetter(),
            ),
          ),
        ),
      ),
    );
  }
}