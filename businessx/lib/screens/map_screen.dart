import 'dart:async';

import 'package:b2b_market/bloc/data_collection_bloc.dart';
import 'package:b2b_market/common/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/place.dart';

class MapScreen extends StatefulWidget {
  final Place place;

  static const String route = 'mapScreen';

  const MapScreen({Key? key, required this.place}) : super(key: key);

  @override
  State<MapScreen> createState() {
    return _MapScreen();
  }
}

class _MapScreen extends State<MapScreen> {
  late final MapController mapController;
  late final StreamSubscription mapEventSubscription;
  final pointSize = 40.0;
  final pointY = 200.0;

  final locationController = TextEditingController();

  late LatLng latLng;

  @override
  void initState() {
    super.initState();
    mapController = MapController();
    latLng = LatLng(widget.place.lat!, widget.place.lon!);
    locationController.text = widget.place.displayName ?? '';

    mapEventSubscription = mapController.mapEventStream
        .listen((mapEvent) => onMapEvent(mapEvent, context));

    // Future.delayed(Duration.zero, () {
    //   mapController.onReady.then((_) => _updatePointLatLng(context));
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      drawer: const MyAppDrawer(),
      body: FlutterMap(
        mapController: mapController,
        options: MapOptions(
          center: LatLng(
            widget.place.lat!,
            widget.place.lon!,
          ),
          onMapReady: () {
            Future.delayed(Duration.zero, () {
              ((_) => _updatePointLatLng(context));
            });
            // ((_) => _updatePointLatLng(context));
          },
          // onMapCreated: (mapController) {
          //   mapController.onReady.then((_) => _updatePointLatLng(context));
          // },
          zoom: 18.0,
          minZoom: 3.0,
        ),
        nonRotatedChildren: const [],
        children: [
          TileLayer(
            urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            subdomains: const ['a', 'b', 'c'],
            // userAgentPackageName: 'dev.fleaflet.flutter_map.example',
          ),
          MarkerLayer(
            markers: [
              Marker(
                width: pointSize,
                height: pointSize,
                point: latLng,
                builder: (ctx) => const Icon(
                  Icons.location_on,
                  color: Colors.green,
                  size: 48,
                ),
              )
            ],
          ),
          // TileLayerWidget(
          //   options: TileLayerOptions(
          //     urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
          //     subdomains: ['a', 'b', 'c'],
          //   ),
          // ),
          // MarkerLayerWidget(
          //   options: MarkerLayerOptions(
          //     markers:
          //   ),
          // ),
          Positioned(
            top: 0,
            child: Container(
              height: 60,
              color: Colors.white,
              child: const PageTitle(
                title: 'title',
              ),
            ),
          ),
          Positioned(
            bottom: 131,
            right: 10,
            child: Container(
              color: Colors.white,
              child: GestureDetector(
                onTap: () async {
                  final url = Uri.parse('https://osm.org/copyright');
                  if (await canLaunchUrl(url)) {
                    await launchUrl(url);
                  }
                },
                child: const Text(
                  '© OpenStreetMap',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.blue,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              height: 130,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Column(
                children: [
                  CustomTextFormField(
                    controller: locationController,
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      dataCollectionBloc.locationNotifier.value =
                          locationController.text;

                      Navigator.of(context).pop();
                    },
                    style: const ButtonStyle(),
                    child: const Text(
                      'Confirm Location',
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> onMapEvent(MapEvent mapEvent, BuildContext context) async {
    debugPrint('mapEvent.source.name: ${mapEvent.source.name}');
    if (mapEvent.source.name == 'dragEnd') {
      _updatePointLatLng(context);
      var place = await dataCollectionBloc.reverseSearch(
        latLng,
      );
      if (place.error == null) {
        setState(() {
          locationController.text = place.displayName!;
        });
      }
    }
  }

  Future<void> _updatePointLatLng(context) async {
    final pointX = _getPointX(context);

    final latLng = mapController.pointToLatLng(CustomPoint(pointX, pointY));
    if (latLng != null) {
      setState(() {
        this.latLng = latLng;
      });
    }
  }

  double _getPointX(BuildContext context) {
    return MediaQuery.of(context).size.width / 2;
  }

  @override
  void dispose() {
    super.dispose();
    mapEventSubscription.cancel();
  }
}
