import 'package:flutter/material.dart';

import '../../../bloc/category_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/category/root_categories_response.dart';
import 'components/allcategorylist_screen.dart';

class RootCategoryScreen extends StatefulWidget {
  const RootCategoryScreen({Key? key}) : super(key: key);

  @override
  State<RootCategoryScreen> createState() => _RootCategoryScreenState();
}

class _RootCategoryScreenState extends State<RootCategoryScreen> {
  final _scrollController = ScrollController();

  int page = 1;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (page != categoryBloc.lastPage) {
          page++;
          categoryBloc.getRootCategories(page: page);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16.0,
        top: 20,
        right: 16,
      ),
      child: Column(
        children: [
          const Align(
            alignment: Alignment.topLeft,
            child: Text(
              'All Categories',
              style: TextStyle(
                fontStyle: FontStyle.normal,
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontFamily: 'Rubik',
              ),
            ),
          ),

          // const Align(
          //   alignment: Alignment.topLeft,
          //   child: PageTitle(
          //     title: 'All Categories',
          //     showBackButton: false,
          //   ),
          // ),
          Expanded(
            child: StreamBuilder<List<RootCategory>>(
              stream: categoryBloc.rootCategory.stream,
              builder: (context, AsyncSnapshot<List<RootCategory>> snapshot) {
                var rootCategories = snapshot.data;
                if (rootCategories != null && rootCategories.isNotEmpty) {
                  rootCategories.sort((a, b) => a.name.compareTo(b.name));
                  return ListView.builder(
                    controller: _scrollController,
                    scrollDirection: Axis.vertical,
                    itemCount: rootCategories.length,
                    itemBuilder: (context, index) {
                      var rootCategory = rootCategories[index];
                      return AllCategoryList(
                        rootCategory: rootCategory,
                      );
                    },
                  );
                  // GridView.builder(
                  // scrollDirection: Axis.vertical,
                  // itemCount: rootCategories.length,
                  //   controller: _scrollController,
                  //   gridDelegate:
                  //       const SliverGridDelegateWithFixedCrossAxisCount(
                  //     crossAxisCount: 4,
                  //     mainAxisExtent: 115,
                  //   ),
                  //   physics: const BouncingScrollPhysics(),
                  // itemBuilder: (context, index) {
                  //   var rootCategory = rootCategories[index];
                  //   return RootCategoryCard(
                  //     rootCategory: rootCategory,
                  //   );
                  //   },
                  // );
                }
                if (snapshot.error != null) {
                  return Center(
                    child: Text(
                      'No Categories Found',
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  );
                }

                return const CircularGridCategoryShimmer();
              },
            ),
          ),
        ],
      ),
    );
  }
}
