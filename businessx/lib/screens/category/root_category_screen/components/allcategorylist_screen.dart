import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../../../bloc/category_bloc.dart';
import '../../../../common/widgets/custom_progress_dialog.dart';
import '../../../../models/response/category/root_categories_response.dart';
import '../../../../route_names.dart';
import '../../../chat/components/my_cached_network_image.dart';

class AllCategoryList extends StatelessWidget {
  final RootCategory rootCategory;

  const AllCategoryList({Key? key, required this.rootCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getSubCategories(rootCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.subCategoryScreen,
            arguments: rootCategory.name,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0),
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Container(
                        height: 80,
                        width: 80,
                        decoration: BoxDecoration(
                          // color: Colors.blue,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: MyCachedNetworkImage(
                            filePath: rootCategory.iconImageThumbnail,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        rootCategory.name,
                        style: const TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Rubik',
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        rootCategory.name,
                        style: const TextStyle(
                          fontStyle: FontStyle.normal,
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          fontFamily: 'Rubik',
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              Divider(
                color: Colors.grey.shade300,
                thickness: 2,
                height: 2,
              ),
            ],
          ),
        ),
      ),
      // child: Column(
      //   children: [
      //     Card(
      //       elevation: 2,
      //       shape: RoundedRectangleBorder(
      //         borderRadius: BorderRadius.circular(45),
      //       ),
      //       shadowColor: const Color.fromRGBO(0, 0, 0, 0.25),
      // child: SizedBox(
      //   height: 70,
      //   width: 70,
      //   child: Padding(
      //     padding: const EdgeInsets.all(18.0),
      //     child: MyCachedNetworkImage(
      //       filePath: rootCategory.iconImageThumbnail,
      //     ),
      //   ),
      //       ),
      //     ),
      //     const SizedBox(height: 5),
      //     SizedBox(
      //       width: 80,
      //       child: Text(
      // rootCategory.name,
      //         textAlign: TextAlign.center,
      //         overflow: TextOverflow.ellipsis,
      //         maxLines: 2,
      //         style: Theme.of(context).textTheme.titleSmall,
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}
