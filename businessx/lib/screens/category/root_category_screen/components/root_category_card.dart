import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../../../bloc/category_bloc.dart';
import '../../../../common/widgets/custom_progress_dialog.dart';
import '../../../../models/response/category/root_categories_response.dart';
import '../../../../route_names.dart';
import '../../../chat/components/components.dart';

class RootCategoryCard extends StatelessWidget {
  final RootCategory rootCategory;

  const RootCategoryCard({Key? key, required this.rootCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getSubCategories(rootCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.subCategoryScreen,
            arguments: rootCategory.name,
          );
        } else {
          ShowToast.error(
           response.error,
          );
        }
      },
      child: Column(
        children: [
          Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45),
            ),
            shadowColor: const Color.fromRGBO(0, 0, 0, 0.25),
            child: SizedBox(
              height: 70,
              width: 70,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: MyCachedNetworkImage(
                  filePath: rootCategory.iconImageThumbnail,
                ),
              ),
            ),
          ),
          const SizedBox(height: 5),
          SizedBox(
            width: 80,
            child: Text(
              rootCategory.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: Theme.of(context).textTheme.titleSmall,
            ),
          ),
        ],
      ),
    );
  }
}
