import 'package:flutter/material.dart';

import '../../../bloc/category_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/category/leaf_category_response.dart';
import 'components/leaf_category_card.dart';

class LeafCategoryScreen extends StatefulWidget {
  final String title;

  const LeafCategoryScreen({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  State<LeafCategoryScreen> createState() => _LeafCategoryScreenState();
}

class _LeafCategoryScreenState extends State<LeafCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          PageTitle(title: widget.title),
          Expanded(
            child: StreamBuilder<LeafResponse>(
              stream: categoryBloc.leafCategories.stream,
              builder: (context, snapshot) {
                var leafCategories = snapshot.data?.leaf?.leafCategory;
                if (leafCategories != null) {
                  leafCategories.sort((a, b) => a.name.compareTo(b.name));
                  return GridView.builder(
                    itemCount: leafCategories.length,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      mainAxisExtent: 115,
                    ),
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      var leafCategory = leafCategories[index];
                      return LeafCategoryCard.circular(
                        leafCategory: leafCategory,
                      );
                    },
                  );
                }
                return const CircularGridCategoryShimmer();
              },
            ),
          ),
        ],
      ),
    );
  }
}


