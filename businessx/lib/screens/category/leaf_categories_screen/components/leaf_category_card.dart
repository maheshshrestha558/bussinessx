import 'package:flutter/material.dart';

import '../../../../bloc/product_bloc.dart';
import '../../../../common/widgets/custom_progress_dialog.dart';
import '../../../../models/response/category/leaf_category_response.dart';
import '../../../../route_names.dart';
import '../../../chat/components/my_cached_network_image.dart';

enum _CardType { circular, listTile }

class LeafCategoryCard extends StatelessWidget {
  final LeafCategory leafCategory;
  final _CardType _cardType;

  const LeafCategoryCard.circular({
    Key? key,
    required this.leafCategory,
  })  : _cardType = _CardType.circular,
        super(key: key);

  const LeafCategoryCard.listTile({
    Key? key,
    required this.leafCategory,
  })  : _cardType = _CardType.listTile,
        super(key: key);

  Widget _buildListTileLeafCategoryCard(BuildContext context) {
    return Column(
      children: [
        ListTile(
          trailing: const CircleAvatar(
            backgroundImage: AssetImage('assets/images/ah.jpg'),
            radius: 23,
          ),
          title: Text(
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            leafCategory.name,
            style: const TextStyle(
              color: Color.fromARGB(255, 1, 75, 145),
              fontSize: 16,
              fontWeight: FontWeight.w400,
            ),
          ),
          onTap: () async {
            Feedback.forTap(context);

            var response = await productBloc.getProducts(leafCategory.id);
            if (response.error == null) {
              Navigator.pushNamed(
                context,
                RouteName.productsScreen,
                arguments: leafCategory.name,
              );
            }
          },
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5),
          child: Divider(
            thickness: 3,
            color: Colors.grey.shade300,
          ),
        ),
      ],
    );
  }

  Widget _buildCircularLeafCategoryCard(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        var response = await productBloc.getProducts(leafCategory.id);
        Navigator.pop(context);
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.productsScreen,
            arguments: leafCategory.name,
          );
        }
      },
      child: Column(
        children: [
          Card(
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45),
            ),
            shadowColor: const Color.fromRGBO(0, 0, 0, 0.25),
            child: SizedBox(
              height: 60,
              width: 60,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ClipOval(
                  child: MyCachedNetworkImage(
                    filePath: leafCategory.imageThumbnail,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 5),
          SizedBox(
            width: 70,
            child: Text(
              leafCategory.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: const TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    switch (_cardType) {
      case _CardType.circular:
        return _buildCircularLeafCategoryCard(context);
      case _CardType.listTile:
        return _buildListTileLeafCategoryCard(context);
    }
  }
}
