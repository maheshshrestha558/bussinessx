import 'package:flutter/material.dart';

import '../../common/widgets/widgets.dart';

class SubCategoryScreen extends StatefulWidget {
  final String categoryName;
  const SubCategoryScreen({
    Key? key,
    required this.categoryName,
  }) : super(key: key);

  @override
  State<SubCategoryScreen> createState() => _SubCategoryScreenState();
}

class _SubCategoryScreenState extends State<SubCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 5,
          ),
          PageTitle(
            title: widget.categoryName,
          ),
          const SubCategoryBuilder(),
        ],
      ),
    );
  }
}
