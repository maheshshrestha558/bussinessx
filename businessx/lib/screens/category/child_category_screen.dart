import 'package:flutter/material.dart';

import '../../common/widgets/widgets.dart';

class ChildCategoryScreen extends StatefulWidget {
  final String title;

  const ChildCategoryScreen({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  State<ChildCategoryScreen> createState() => _ChildCategoryScreenState();
}

class _ChildCategoryScreenState extends State<ChildCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: ChildCategoryBuilder(
        title: widget.title,
      ),
    );
  }
}
