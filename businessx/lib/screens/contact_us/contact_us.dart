import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../common/widgets/widgets.dart';

class ContactUsScreen extends StatefulWidget {
  const ContactUsScreen({Key? key}) : super(key: key);

  @override
  State<ContactUsScreen> createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  final _formKey = GlobalKey<FormState>();

  late final TextEditingController _nameController;
  late final TextEditingController _emailController;
  late final TextEditingController _phoneNumberController;
  late final TextEditingController _subjectController;
  late final TextEditingController _companyNameController;
  late final TextEditingController _messageController;

  String? _dropdownValue;

  var requirementOptions = ['One', 'General Inquiry', 'Buy Requirement'];

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneNumberController = TextEditingController();
    _subjectController = TextEditingController();
    _companyNameController = TextEditingController();
    _messageController = TextEditingController();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _phoneNumberController.dispose();
    _subjectController.dispose();
    _companyNameController.dispose();
    _messageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      drawer: const CustomAppBar(),
      body: Column(
        children: [
          const PageTitle(title: 'Contact Us'),
          Expanded(
            child: Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: ListView(
                children: [
                  CustomTextFormField(
                    labelText: 'Name',
                    controller: _nameController,
                    validator: RequiredValidator(errorText: 'Required'),
                  ),
                  CustomTextFormField(
                    labelText: 'Email',
                    controller: _emailController,
                    validator: MultiValidator([
                      RequiredValidator(errorText: 'Required'),
                      EmailValidator(errorText: 'Invalid email'),
                    ]),
                  ),
                  CustomTextFormField(
                    labelText: 'Phone',
                    controller: _phoneNumberController,
                    textInputType: TextInputType.phone,
                    validator: RequiredValidator(errorText: 'Required'),
                  ),
                  CustomTextFormField(
                    labelText: 'Subject',
                    controller: _subjectController,
                    validator: RequiredValidator(errorText: 'Required'),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 27,
                      vertical: 16,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const CustomLabelText(labelText: 'Purpose'),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 8),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: const Color.fromRGBO(218, 216, 216, 1),
                            ),
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              hint: const Text('Select One'),
                              underline: Container(),
                              elevation: 0,
                              isExpanded: true,
                              icon: const Icon(Icons.arrow_drop_down),
                              value: _dropdownValue,
                              onChanged: (value) {
                                _dropdownValue = value!;

                                FocusScope.of(context).unfocus();

                                setState(() {});
                              },
                              items: requirementOptions.map((value) {
                                return DropdownMenuItem(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  CustomTextFormField(
                    labelText: 'Company Name',
                    controller: _companyNameController,
                  ),
                  CustomTextFormField(
                    labelText: 'Message',
                    controller: _messageController,
                    maxLines: 6,
                    validator: RequiredValidator(errorText: 'Required'),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CustomElevatedButton(
          buttonText: 'Send Message',
          onPressed: () {
            if (_formKey.currentState!.validate()) {}
          },
        ),
      ),
    );
  }
}
