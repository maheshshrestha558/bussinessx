import 'package:b2b_market/bloc/B2C/cart_bloc.dart';
import 'package:b2b_market/common/enums.dart';
import 'package:b2b_market/models/response/add_to_cart_response.dart';
import 'package:b2b_market/models/response/cart_response.dart';
import 'package:b2b_market/screens/home/components/components.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../common/logger/logger.dart';

final _logger = getLogger(CartPage);

class CartPage extends StatefulWidget {
  const CartPage({
    Key? key,
  }) : super(key: key);

  static const String routeName = '/cart';

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  bool isSelect = true;
  @override
  void initState() {
    super.initState();
    cartBloc.getCart();
  }

  @override
  void dispose() {
    cartBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.arrow_back_ios),
          color: const Color.fromARGB(255, 1, 75, 145),
        ),
        title: const Text(
          'Checkout',
          style: TextStyle(color: Colors.blue),
        ),
      ),
      bottomNavigationBar: Container(
        height: 52,
        width: MediaQuery.of(context).size.width,
        color: const Color.fromARGB(255, 217, 217, 217),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          'Total:',
                          style: TextStyle(
                            fontSize: 14,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Text(
                          'Rs.14,999',
                          style: TextStyle(
                            color: Color.fromARGB(255, 220, 38, 38),
                            fontSize: 14,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w400,
                          ),
                        )
                      ],
                    ),
                    Text(
                      'All Text Included',
                      style: TextStyle(
                        color: Color.fromARGB(255, 52, 50, 50),
                        fontSize: 12,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                  decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 100, 177, 77),
                  ),
                  height: 28,
                  width: 122,
                  child: const Center(
                    child: Text(
                      'Place Order',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(62, 210, 232, 203),
                    blurRadius: 2.0,
                    spreadRadius: 1.0,
                    offset: Offset(
                      -0.5,
                      2,
                    ),
                  )
                ],
              ),
              child: Card(
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  side: BorderSide(
                    color: Color.fromARGB(255, 210, 232, 203),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 1.5),
                            child: Text(
                              'Delivered To: Jhon Sapkota',
                              style: TextStyle(
                                fontSize: 16,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width - 60,
                            child: const Text(
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              'Syambhu,Kathmandu-Ring Road Near XYI Bank Buddha ParkOpposite to Bhudda Park',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 0),
                            child: Text(
                              '9849123456',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: Color.fromARGB(255, 63, 60, 60),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Center(
                        child: Icon(
                          size: 20,
                          Icons.arrow_forward_ios,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          // Center(
          //   child: StreamBuilder<CartResponse>(
          //     stream: cartBloc.cartResponse,
          //     builder: (context, snapshot) {
          //       List<ProductData>? productDataList = snapshot.data?.data;
          //       if (productDataList == null) {
          //         return const CircularProgressIndicator();
          //       }
          //       return ListView.builder(
          //         itemCount: productDataList.length,
          //         itemBuilder: (context, index) {
          //           ProductData productData = productDataList[index];
          //           _logger.i(productData);
          //           return ProductListTile(
          //             key: ValueKey(productData.quantity),
          //             productData: productData,
          //           );
          //         },
          //       );
          //     },
          //   ),
          // ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              child: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: 1,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(62, 210, 232, 203),
                              blurRadius: 2.0,
                              spreadRadius: 1.0,
                              offset: Offset(
                                -0.5,
                                2,
                              ),
                            )
                          ],
                        ),
                        child: Card(
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            side: BorderSide(
                              color: Color.fromARGB(255, 210, 232, 203),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  'Glam & Glitter',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                const Divider(
                                  color: Colors.green,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 8,
                                    right: 8,
                                    left: 8,
                                  ),
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: 1,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return GestureDetector(
                                        onTap: () {},
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    10,
                                                  ),
                                                  child: Image.asset(
                                                    'assets/images/ah.jpg',
                                                    height: 100,
                                                    width: 100,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 5,
                                                    ),
                                                    child: const Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          'Women Casula Cotton Tops ',
                                                          style: TextStyle(
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.w600,
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            top: 6,
                                                          ),
                                                          child: Text(
                                                            'No Brand,Free Size,Color:Black',
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            top: 26,
                                                          ),
                                                          child: Text(
                                                            'Rs:1,499',
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                              color: Color
                                                                  .fromARGB(
                                                                255,
                                                                220,
                                                                38,
                                                                38,
                                                              ),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            top: 4,
                                                          ),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Expanded(
                                                                child: Text(
                                                                  'Rs:2000',
                                                                  style:
                                                                      TextStyle(
                                                                    decoration:
                                                                        TextDecoration
                                                                            .lineThrough,
                                                                    fontSize:
                                                                        10,
                                                                    color: Color
                                                                        .fromARGB(
                                                                      255,
                                                                      138,
                                                                      133,
                                                                      133,
                                                                    ),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                                ),
                                                              ),
                                                              Text(
                                                                'Qty-1',
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 10,
                                                                  color: Color
                                                                      .fromARGB(
                                                                    255,
                                                                    138,
                                                                    133,
                                                                    133,
                                                                  ),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const Divider(
                                              color: Color.fromARGB(
                                                251,
                                                210,
                                                232,
                                                203,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.symmetric(vertical: 8),
                                  child: Text(
                                    'Standard Delivery',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Color.fromARGB(
                                        255,
                                        1,
                                        75,
                                        145,
                                      ),
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(bottom: 8),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          'Delivered By- May 30,2024',
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Color.fromARGB(
                                              255,
                                              159,
                                              148,
                                              148,
                                            ),
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(right: 17),
                                        child: Text(
                                          'Rs.70',
                                          style: TextStyle(
                                            fontSize: 10,
                                            color: Color.fromARGB(
                                              255,
                                              138,
                                              133,
                                              133,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
          const BILLSUMMARY(),
          Padding(
            padding: const EdgeInsets.only(
              top: 28,
              left: 8,
              right: 8,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(bottom: 6),
                  child: Text(
                    'Payment Method',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Color.fromARGB(255, 1, 75, 145),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Card(
                        child: SizedBox(
                          height: 62,
                          width: 156,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 17,
                              horizontal: 41,
                            ),
                            child: Image.asset(
                              'assets/images/khalti.jpg',
                              height: 28,
                              width: 73,
                            ),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Card(
                        child: SizedBox(
                          height: 62,
                          width: 156,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 17,
                              horizontal: 41,
                            ),
                            child: Image.asset(
                              'assets/images/esewa.png',
                              height: 28,
                              width: 73,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isSelect = !isSelect;
                    });
                  },
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 18, horizontal: 8),
                    child: Card(
                      child: SizedBox(
                        height: 50,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.circle,
                                    color:
                                        !isSelect ? Colors.grey : Colors.green,
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  const Text(
                                    'Cash on hand',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromARGB(255, 0, 0, 0),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 8),
                                child: Image.asset('assets/images/cash.png'),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class BILLSUMMARY extends StatelessWidget {
  const BILLSUMMARY({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(62, 210, 232, 203),
            blurRadius: 2.0,
            spreadRadius: 1.0,
            offset: Offset(
              -0.5,
              2,
            ),
          )
        ],
      ),
      child: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            side: BorderSide(
              color: Color.fromARGB(255, 210, 232, 203),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 7, left: 11, bottom: 8),
                child: Text(
                  'Billing Summary',
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromARGB(255, 1, 75, 145),
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Subtotal',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Discount',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Shipping',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Tax',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Rs.550',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            '10%',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Rs.100',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 10, left: 11),
                          child: Text(
                            'Rs.40',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 49, 48, 48),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Divider(
                color: Color.fromARGB(255, 218, 218, 218),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 7, left: 8),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Grand Total',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 220, 38, 38),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 11),
                      child: Text(
                        'Rs.600',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 220, 38, 38),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ProductListTile extends StatefulWidget {
  final ProductData productData;
  const ProductListTile({
    Key? key,
    required this.productData,
  }) : super(key: key);

  @override
  State<ProductListTile> createState() => _ProductListTileState();
}

class _ProductListTileState extends State<ProductListTile> {
  int quantity = 1;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    quantity = widget.productData.quantity;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Feedback.forTap(context);
      },
      child: Card(
        elevation: 0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(0),
        ),
        child: SizedBox(
          height: 140,
          child: Row(
            children: [
              Checkbox(
                value: isSelected,
                onChanged: (value) {
                  setState(() {
                    isSelected = value!;
                    if (cartBloc.productIds
                        .contains(widget.productData.product.id)) {
                      cartBloc.productIds.remove(widget.productData.product.id);
                    } else {
                      cartBloc.productIds.add(widget.productData.product.id);
                    }
                    cartBloc.getTotalPrice(cartBloc.productIds);
                  });
                },
                activeColor: Colors.green,
                fillColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.disabled)) {
                    return Colors.green.withOpacity(.32);
                  }
                  return Colors.green;
                }),
              ),
              Container(
                height: 80,
                width: 80,
                margin: const EdgeInsets.fromLTRB(13, 15, 0, 15),
                color: Colors.grey[200],
                child: ProductCachedNetworkImage(
                  product: widget.productData.product,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    top: 10.0,
                    bottom: 10.0,
                    right: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        widget.productData.product.name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'Rs. ${widget.productData.product.price}',
                        style: GoogleFonts.rubik(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: const Color(0xffDC2626),
                        ),
                      ),
                      Text(
                        'Varient : -------',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(fontSize: 12),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              widget.productData.product.seller!.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodySmall!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                          ),
                          // const Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: InkWell(
                              onTap: () async {
                                _logger.i(widget.productData.cartId);
                                if (quantity > 1) {
                                  AddToCartResponse response =
                                      await cartBloc.updateProductQuantity(
                                    widget.productData.cartId,
                                    quantity,
                                    ProductQuantityEvent.remove,
                                    context,
                                  );
                                  if (quantity > 1 && response.error == null) {
                                    quantity--;
                                    setState(() {});
                                  }
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(52),
                                  ),
                                ),
                                child: const Icon(Icons.remove, size: 18),
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            width: 30,
                            height: 20,
                            child: Text(quantity.toString()),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: InkWell(
                              onTap: () async {
                                AddToCartResponse response =
                                    await cartBloc.updateProductQuantity(
                                  widget.productData.cartId,
                                  quantity,
                                  ProductQuantityEvent.add,
                                  context,
                                );
                                if (response.error == null) {
                                  quantity++;
                                  setState(() {});
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(52),
                                  ),
                                ),
                                child: const Icon(Icons.add, size: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

// class ProductListTiles extends StatefulWidget {
//   final String productData;
//   const ProductListTiles({
//     Key? key,
//     required this.productData,
//   }) : super(key: key);

//   @override
//   State<ProductListTiles> createState() => _ProductListTilesState();
// }

// class _ProductListTilesState extends State<ProductListTiles> {
//   int quantity = 1;
//   bool isSelected = false;

//   @override
//   void initState() {
//     super.initState();
//     quantity = 0;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         Feedback.forTap(context);
//       },
//       child: Card(
//         elevation: 0,
//         color: Colors.white,
//         shape: RoundedRectangleBorder(
//           side: const BorderSide(color: Colors.white70, width: 1),
//           borderRadius: BorderRadius.circular(0),
//         ),
//         child: SizedBox(
//           height: 140,
//           child: Row(
//             children: [
//               Checkbox(
//                 value: isSelected,
//                 onChanged: (value) {
//                   setState(() {
//                     isSelected = value!;
//                     // if (cartBloc.productIds
//                     //     .contains(widget.productData.product.id)) {
//                     //   cartBloc.productIds.remove(widget.productData.product.id);
//                     // } else {
//                     //   cartBloc.productIds.add(widget.productData.product.id);
//                     // }
//                     // cartBloc.getTotalPrice(cartBloc.productIds);
//                   });
//                 },
//                 activeColor: Colors.green,
//                 fillColor: MaterialStateProperty.resolveWith<Color>((states) {
//                   if (states.contains(MaterialState.disabled)) {
//                     return Colors.green.withOpacity(.32);
//                   }
//                   return Colors.green;
//                 }),
//               ),
//               Container(
//                 height: 80,
//                 width: 80,
//                 margin: const EdgeInsets.fromLTRB(5, 15, 0, 15),
//                 color: Colors.grey[200],
//                 child: const SizedBox(height: 100, width: 100),
//               ),
//               Expanded(
//                 child: Padding(
//                   padding: const EdgeInsets.only(
//                     left: 15.0,
//                     top: 10.0,
//                     bottom: 10.0,
//                     right: 5,
//                   ),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     mainAxisAlignment: MainAxisAlignment.spaceAround,
//                     children: [
//                       const Text(
//                         ' widget. product .name test case',
//                         overflow: TextOverflow.ellipsis,
//                         maxLines: 1,
//                         style: TextStyle(
//                           fontSize: 16,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                       Text(
//                         'Rs. 126',
//                         style: GoogleFonts.rubik(
//                           fontSize: 14,
//                           fontWeight: FontWeight.w400,
//                           color: const Color(0xffDC2626),
//                         ),
//                       ),
//                       Text(
//                         'Varient : -------',
//                         style: Theme.of(context)
//                             .textTheme
//                             .headlineMedium!
//                             .copyWith(fontSize: 12),
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         children: [
//                           Expanded(
//                             child: Text(
//                               'Seller Name here T e s t ',
//                               maxLines: 1,
//                               overflow: TextOverflow.ellipsis,
//                               style: Theme.of(context)
//                                   .textTheme
//                                   .bodySmall!
//                                   .copyWith(fontWeight: FontWeight.normal),
//                             ),
//                           ),
//                           // const Spacer(),
//                           Padding(
//                             padding: const EdgeInsets.all(5.0),
//                             child: InkWell(
//                               onTap: () async {
//                                 // _logger.i(widget.productData.cartId);
//                                 // if (quantity > 1) {
//                                 //   AddToCartResponse response =
//                                 //       await cartBloc.updateProductQuantity(
//                                 //     widget.productData.cartId,
//                                 //     quantity,
//                                 //     ProductQuantityEvent.remove,
//                                 //     context,
//                                 //   );
//                                 //   if (quantity > 1 && response.error == null) {
//                                 //     quantity--;
//                                 //     setState(() {});
//                                 //   }
//                                 // }
//                               },
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                   border: Border.all(width: 1),
//                                   borderRadius: const BorderRadius.all(
//                                     Radius.circular(52),
//                                   ),
//                                 ),
//                                 child: const Icon(Icons.remove, size: 18),
//                               ),
//                             ),
//                           ),
//                           Container(
//                             alignment: Alignment.center,
//                             width: 30,
//                             height: 20,
//                             decoration: const BoxDecoration(
//                               color: Color.fromARGB(255, 223, 223, 223),
//                             ),
//                             child: Text(
//                               quantity.toString(),
//                               style: const TextStyle(
//                                 fontSize: 15,
//                               ),
//                             ),
//                           ),
//                           Container(
//                             padding: const EdgeInsets.all(5.0),
//                             child: InkWell(
//                               onTap: () async {
//                                 // AddToCartResponse response =
//                                 //     await cartBloc.updateProductQuantity(
//                                 //   widget.productData.cartId,
//                                 //   quantity,
//                                 //   ProductQuantityEvent.add,
//                                 //   context,
//                                 // );
//                                 // if (response.error == null) {
//                                 //   quantity++;
//                                 //   setState(() {});
//                                 // }
//                               },
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                   border: Border.all(width: 1),
//                                   borderRadius: const BorderRadius.all(
//                                     Radius.circular(52),
//                                   ),
//                                 ),
//                                 child: const Icon(Icons.add, size: 18),
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
