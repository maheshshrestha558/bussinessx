import 'package:flutter/material.dart';

import '../../../models/models.dart';
import '../../../route_names.dart';
import '../../home/components/components.dart';

class B2CProductListTile extends StatelessWidget {
  const B2CProductListTile({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Feedback.forTap(context);
        Navigator.pushNamed(
          context,
          RouteName.productDetailScreen,
          arguments: product,
        );
      },
      child: Card(
        elevation: 5,
        child: SizedBox(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  decoration: const BoxDecoration(),
                  height: MediaQuery.of(context).size.width / 4.7,
                  width: MediaQuery.of(context).size.width / 4.7,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: ProductCachedNetworkImage(
                      product: product,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 4,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            product.name,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 16,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          '2 item left',
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.red,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'RS. ${product.price}',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.red,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Visibility(
                                visible: false,
                                child: Expanded(
                                  child: Text(
                                    'RS. ${product.price}',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      decorationColor: const Color.fromARGB(
                                        255,
                                        196,
                                        196,
                                        196,
                                      ),
                                      fontSize: 12,
                                      color: Colors.grey.shade400,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(right: 5),
                          child: Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(right: 7),
                                child: CircleAvatar(
                                  backgroundColor:
                                      Color.fromARGB(255, 220, 38, 38),
                                  radius: 18,
                                  child: Icon(
                                    Icons.favorite_border,
                                    size: 15,
                                  ),
                                ),
                              ),
                              CircleAvatar(
                                radius: 18,
                                child: Icon(
                                  Icons.shopping_cart_outlined,
                                  size: 15,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
