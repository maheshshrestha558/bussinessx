import 'package:b2b_market/models/response/banner/banner_response.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';

import '../../../bloc/banner_bloc.dart';

class B2CBanners extends StatefulWidget {
  const B2CBanners({
    Key? key,
  }) : super(key: key);

  @override
  State<B2CBanners> createState() => _BannersState();
}

class _BannersState extends State<B2CBanners> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      height: 200,
      child: StreamBuilder<B2CBannerResponse>(
        stream: bannerBloc.b2cBanner.stream,
        builder: (context, snapshot) {
          // print('GG ${snapshot.data!.homeSlidingBanner}');
          var banners = snapshot.data?.homeSlidingBanner;
          if (banners != null && banners.isNotEmpty) {
            return CarouselSlider.builder(
              enableAutoSlider: true,
              unlimitedMode: true,
              itemCount: banners.length,
              slideBuilder: (index) {
                var imageThumbnail = banners[index].imageThumbnail;
                return BannerCachedNetworkImage(filePath: imageThumbnail);
              },
              slideIndicator: CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Colors.white,
                padding: const EdgeInsets.only(bottom: 18),
              ),
              autoSliderDelay: const Duration(seconds: 3),
            );
          }
          return SizedBox(
            width: double.infinity,
            child: Image.asset(
              'assets/homeimage/1.png',
              fit: BoxFit.cover,
            ),
          );
        },
      ),
    );
  }
}

class BannerCachedNetworkImage extends StatelessWidget {
  const BannerCachedNetworkImage({
    Key? key,
    required this.filePath,
  }) : super(key: key);

  final String filePath;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: CachedNetworkImage(
        imageUrl: filePath,
        fit: BoxFit.cover,
        errorWidget: (context, url, error) {
          return Align(
            alignment: Alignment.centerLeft,
            child: Image.asset(
              'assets/homeimage/1.png',
              fit: BoxFit.cover,
            ),
            // child: ShimmerWidget.rectangular(
            //   height: 200,
            //   // width: 550,
            // ),
          );
        },
      ),
    );
  }
}

class B2CSingleBanners extends StatefulWidget {
  const B2CSingleBanners({
    Key? key,
  }) : super(key: key);

  @override
  State<B2CSingleBanners> createState() => _BannerSingleState();
}

class _BannerSingleState extends State<B2CSingleBanners> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      height: 150,
      child: StreamBuilder<B2CBannerResponse>(
        stream: bannerBloc.b2cBanner.stream,
        builder: (context, snapshot) {
          var banners = snapshot.data?.homeBannerFirst;
          if (banners != null && banners.imageThumbnail.isNotEmpty) {
            return BannerCachedNetworkImage(filePath: banners.imageThumbnail);
          }
          return Align(
            alignment: Alignment.centerLeft,
            child: SizedBox(
              // height: 115,
              width: double.infinity,
              child: Image.asset(
                'assets/homeimage/2.png',
                fit: BoxFit.cover,
              ),
            ),
            // ShimmerWidget.rectangular(
            //   height: 150,
            // ),
          );
        },
      ),
    );
  }
}
