// import 'package:b2b_market/common/widgets/custom_app_bar.dart';
// import 'package:b2b_market/route_names.dart';
// import 'package:flutter/material.dart';

// class BuyerProductsScreen extends StatelessWidget {
//   // final int index;
//   const BuyerProductsScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: const CustomAppBar(),
//       key: key,
//       body: Container(
//         padding: const EdgeInsets.all(10.0),
//         color: const Color.fromARGB(255, 230, 230, 206),
//         // height: MediaQuery.of(context).size.height * 0.57,
//         child: GridView.builder(
//           shrinkWrap: true,
//           physics: const ClampingScrollPhysics(),
//           gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
//             maxCrossAxisExtent: MediaQuery.of(context).size.width / 2,
//             childAspectRatio: 0.75,
//             crossAxisSpacing: 15,
//             mainAxisSpacing: 20,
//           ),
//           itemCount: 1,
//           itemBuilder: (BuildContext ctx, index) {
//             return Container(
//               alignment: Alignment.center,
//               decoration: BoxDecoration(
//                 color: const Color.fromARGB(0, 255, 255, 255),
//                 borderRadius: BorderRadius.circular(5),
//               ),
//               child: ProductsItems(index: index),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }

// class ProductsItems extends StatelessWidget {
//   final int index;
//   const ProductsItems({Key? key, required this.index}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         Navigator.pushNamed(context, RouteName.buyerSingleProductsScreen);
//       },
//       child: SizedBox(
//         child: Column(
//           children: [
//             Card(
//               child: SizedBox(
//                 height: MediaQuery.of(context).size.height * 0.2,
//                 child: Hero(
//                   tag: 'product_image_tag',
//                   child: Image.asset('assets/images/icon-13.png'),
//                 ),
//               ),
//             ),
//             Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: const [
//                 Text(
//                   'jjklwawdkjl awkjd kjajkaw jkdaw jkjk wajawd awd a dwad aww aw wddw da wkd',
//                   style: TextStyle(fontSize: 14),
//                   overflow: TextOverflow.ellipsis,
//                   maxLines: 2,
//                   textAlign: TextAlign.center,
//                 ),
//                 SizedBox(
//                   height: 5,
//                 ),
//                 Text(
//                   'Rs. 2525',
//                   style: TextStyle(
//                     fontSize: 12,
//                     color: Color.fromARGB(255, 255, 7, 7),
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
