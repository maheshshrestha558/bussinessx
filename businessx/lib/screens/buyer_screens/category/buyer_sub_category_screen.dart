import 'package:b2b_market/bloc/category_bloc.dart';
import 'package:b2b_market/bloc/product_bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/models/child_category.dart' as childresponse;
import 'package:b2b_market/models/response/response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:flutter/material.dart';

import '../../../models/product.dart';

class BuyerSubCategoryScreen extends StatefulWidget {
  final SubCategory subCategory;
  const BuyerSubCategoryScreen({
    Key? key,
    required this.subCategory,
  }) : super(key: key);

  @override
  State<BuyerSubCategoryScreen> createState() => _BuyerSubCategoryScreenState();
}

class _BuyerSubCategoryScreenState extends State<BuyerSubCategoryScreen> {
  @override
  void initState() {
    super.initState();
    // categoryBloc.getSubCategories(widget.subCategory.id);
    // productBloc.fetchCategoryWiseProducts(widget.subCategory.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarForAllPage(
        navTitle: widget.subCategory.name,
        visible: true,
      ),
      // backgroundColor: const Color.fromARGB(255, 242, 248, 246),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            const ChildCategoriesSection(),
            const SubCategoryBanners(),
            // const SizedBox(
            //   height: 8,
            // ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 3),
              color: const Color.fromARGB(255, 229, 234, 250),
              child: StreamBuilder<List<Product>>(
                stream: productBloc.subCategoryWiseProducts,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var products = snapshot.data!;
                    if (products.isNotEmpty) {
                      return GridView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 5,
                          childAspectRatio: 0.65,
                        ),
                        itemCount: products.length,
                        itemBuilder: (context, index) {
                          var product = products[index];

                          return ProductForYou(
                            product: product,
                            heroType: 'all',
                          );
                        },
                      );
                    }
                  }
                  return Center(
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 10,
                        mainAxisExtent: 360,
                      ),
                      itemCount: 6,
                      itemBuilder: (context, index) => const Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [B2CProductShimmer()],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BannerSection extends StatelessWidget {
  const BannerSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      color: const Color.fromARGB(255, 222, 247, 246),
      // padding: const EdgeInsets.all(5),
      child: SizedBox(
        // height: 19.5,
        // width: 19.5,
        child: Image.asset('assets/images/icon-22.jpg'),
      ),
      // Placeholder(
      //   fallbackHeight: MediaQuery.of(context).size.height * 0.25,
      // ),
    );
  }
}

class ChildCategoriesSection extends StatelessWidget {
  const ChildCategoriesSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
        vertical: 2,
      ),
      height: 80,
      child: Center(
        child: StreamBuilder<ChildCategoryResponse>(
          stream: categoryBloc.childCategories,
          builder: (context, snapshot) {
            var childCategories =
                snapshot.data?.childCategoryData?.childCategories;

            if (childCategories != null && childCategories.isNotEmpty) {
              return ListView.builder(
                physics: const PageScrollPhysics(),
                itemCount: childCategories.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  var childCategory = childCategories[index];
                  return Row(
                    children: [
                      SubCategoryItem(
                        childCategory: childCategory,
                      ),
                    ],
                  );
                },
              );
            } else {
              return const Text('Something Went Wrong');
            }
          },
        ),
      ),
    );
  }
}

class SubCategoryItem extends StatelessWidget {
  final childresponse.ChildCategory childCategory;
  const SubCategoryItem({Key? key, required this.childCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getLeafCategories(childCategory.id);

        await productBloc.fetchChildCategoryWiseProducts(childCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerChildCategoryScreen,
            arguments: childCategory,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: SizedBox(
        width: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 25,
              backgroundColor: const Color.fromARGB(255, 248, 242, 226),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image.network(
                  childCategory.imageThumbnail,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 3,
            ),
            Text(
              childCategory.name,
              style: const TextStyle(fontSize: 10),
              overflow: TextOverflow.fade,
              textAlign: TextAlign.center,
              maxLines: 2,
            ),
          ],
        ),
      ),
    );
  }
}

class SubCategoryBanners extends StatelessWidget {
  const SubCategoryBanners({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 5),
      child: ListView.builder(
        shrinkWrap: true,
        // padding: const EdgeInsets.all(0),
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 3.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/calog.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 1.7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/cabig.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
