import 'package:b2b_market/bloc/category_bloc.dart';
import 'package:b2b_market/bloc/product_bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/models/response/response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:flutter/material.dart';

import '../../../models/product.dart';

class BuyerCategoryScreen extends StatefulWidget {
  final RootCategory rootCategory;
  const BuyerCategoryScreen({
    Key? key,
    required this.rootCategory,
  }) : super(key: key);

  @override
  State<BuyerCategoryScreen> createState() => _BuyerCategoryScreenState();
}

class _BuyerCategoryScreenState extends State<BuyerCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarForAllPage(
        navTitle: widget.rootCategory.name,
        visible: true,
      ),
      // backgroundColor: const Color.fromARGB(255, 172, 247, 222),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            const SubCategoriesSection(),
            const CategoryBanners(),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 3),
              color: const Color.fromARGB(255, 229, 234, 250),
              child: StreamBuilder<List<Product>>(
                stream: productBloc.categoryWiseProducts,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var products = snapshot.data!;
                    if (products.isNotEmpty) {
                      return GridView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 05,
                          childAspectRatio: 0.65,
                          // mainAxisExtent:
                          //     // MediaQuery.of(context).size.height / 2.9,

                          // MediaQuery.of(context).size.width / 1.25,
                        ),
                        itemCount: products.length,
                        itemBuilder: (context, index) {
                          var product = products[index];

                          return ProductForYou(
                            product: product,
                            heroType: 'all',
                          );
                        },
                      );
                    }
                  }
                  return Center(
                    child: GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 10,
                        mainAxisExtent: 360,
                      ),
                      itemCount: 6,
                      itemBuilder: (context, index) => const Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [B2CProductShimmer()],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SubCategoriesSection extends StatelessWidget {
  const SubCategoriesSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // decoration: const BoxDecoration(
      //   color: Color.fromARGB(255, 165, 243, 198),
      //   gradient: LinearGradient(
      //     begin: Alignment.topCenter,
      //     end: Alignment.bottomCenter,
      //     colors: [
      //       Color.fromARGB(255, 170, 247, 204),
      //       Color.fromARGB(255, 186, 250, 215),
      //     ],
      //   ),
      //   boxShadow: [
      //     BoxShadow(
      //       color: Color.fromARGB(255, 168, 238, 226),
      //       blurRadius: 2.0,
      //       spreadRadius: 0.5,
      //       offset: Offset(2.0, 2.0),
      //     )
      //   ],
      // ),
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
        vertical: 2,
      ),
      height: 80,
      child: Center(
        child: StreamBuilder<SubCategoriesResponse>(
          stream: categoryBloc.subCategories,
          builder: (context, snapshot) {
            var subCategories = snapshot.data?.data?.subCategories;

            if (subCategories != null && subCategories.isNotEmpty) {
              return ListView.builder(
                physics: const PageScrollPhysics(),
                itemCount: subCategories.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  var subCategory = subCategories[index];
                  return Row(
                    children: [
                      RootCategoryItem(
                        subCategory: subCategory,
                      ),
                    ],
                  );
                },
              );
            } else {
              return const Text('Something Went Wrong');
            }
          },
        ),
      ),
    );
  }
}

class RootCategoryItem extends StatelessWidget {
  final SubCategory subCategory;
  const RootCategoryItem({Key? key, required this.subCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        showCustomProgressDialog(context, 'Loading...');
        await productBloc.fetchSubCategoryWiseProducts(subCategory.id);
        var response = await categoryBloc.getChildCategories(subCategory.id);

        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSubCategoryScreen,
            arguments: subCategory,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: SizedBox(
        width: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 25,
              backgroundColor: const Color.fromARGB(255, 248, 242, 226),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30),
                child: Image.network(
                  subCategory.imageThumbnail,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              height: 3,
            ),
            Text(
              subCategory.name,
              style: const TextStyle(
                fontSize: 10,
              ),
              overflow: TextOverflow.fade,
              textAlign: TextAlign.center,
              maxLines: 2,
            ),
          ],
        ),
      ),
    );
  }
}

class CategoryTitleName extends StatelessWidget {
  const CategoryTitleName({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(left: 10),
      child: Text(
        'Electronics',
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.normal,
          color: Color.fromARGB(255, 1, 75, 145),
        ),
      ),
    );
  }
}

class CategoryBanners extends StatelessWidget {
  const CategoryBanners({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 5),
      child: ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.all(0),
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 3,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 3.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/calog.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 1.7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/cabig.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
