import 'package:b2b_market/bloc/bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/models/models.dart';
import 'package:b2b_market/models/response/response.dart' as childcat;
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:flutter/material.dart';

class BuyerChildCategoryScreen extends StatefulWidget {
  final ChildCategory childCategory;
  const BuyerChildCategoryScreen({
    Key? key,
    required this.childCategory,
  }) : super(key: key);

  @override
  State<BuyerChildCategoryScreen> createState() =>
      _BuyerChildCategoryScreenState();
}

class _BuyerChildCategoryScreenState extends State<BuyerChildCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarForAllPage(
        navTitle: widget.childCategory.name,
        visible: true,
      ),
      // backgroundColor: const Color.fromARGB(255, 172, 247, 222),
      body: ListView(
        children: [
          const LeafCategoriesSection(),
          const ChildCategoryBanners(),
          const SizedBox(
            height: 5,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
            color: const Color.fromARGB(255, 229, 234, 250),
            child: StreamBuilder<List<Product>>(
              stream: productBloc.childCategoryWiseProducts,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var products = snapshot.data!;

                  if (products.isNotEmpty) {
                    return GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 05,
                        childAspectRatio: 0.65,
                        // mainAxisExtent:
                        //     // MediaQuery.of(context).size.height / 2.9,

                        // MediaQuery.of(context).size.width / 1.25,
                      ),
                      itemCount: products.length,
                      itemBuilder: (context, index) {
                        var product = products[index];

                        return ProductForYou(
                          product: product,
                          heroType: 'all',
                        );
                      },
                    );
                  }
                }
                return Center(
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 10,
                      mainAxisExtent: 360,
                    ),
                    itemCount: 6,
                    itemBuilder: (context, index) => const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [B2CProductShimmer()],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class LeafCategoriesSection extends StatelessWidget {
  const LeafCategoriesSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 5,
        vertical: 0,
      ),
      height: 80,
      child: Center(
        child: StreamBuilder<childcat.LeafResponse>(
          stream: categoryBloc.leafCategories,
          builder: (context, snapshot) {
            var leafCategories = snapshot.data?.leaf?.leafCategory;

            if (leafCategories != null && leafCategories.isNotEmpty) {
              return ListView.builder(
                physics: const PageScrollPhysics(),
                itemCount: leafCategories.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  var leafCategory = leafCategories[index];
                  return Row(
                    children: [
                      LeafCategoryItem(
                        leafCategory: leafCategory,
                      ),
                    ],
                  );
                },
              );
            } else {
              return const Text('Something Went Wrong');
            }
          },
        ),
      ),
    );
  }
}

class LeafCategoryItem extends StatelessWidget {
  final childcat.LeafCategory leafCategory;
  const LeafCategoryItem({Key? key, required this.leafCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        showCustomProgressDialog(context, 'Loading...');
        await productBloc.fetchLeafCategoryWiseProducts(leafCategory.id);

        Navigator.of(context).pop();
        Navigator.pushNamed(
          context,
          RouteName.buyerLeafCategoryScreen,
          arguments: leafCategory,
        );
      },
      child: SizedBox(
        width: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 20,
              backgroundColor: const Color.fromARGB(255, 248, 242, 226),
              backgroundImage: NetworkImage(leafCategory.imageThumbnail),
            ),
            const SizedBox(
              height: 3,
            ),
            Text(
              leafCategory.name,
              style: const TextStyle(fontSize: 10),
              overflow: TextOverflow.fade,
              textAlign: TextAlign.center,
              maxLines: 2,
            ),
          ],
        ),
      ),
    );
  }
}

class ChildCategoryBanners extends StatelessWidget {
  const ChildCategoryBanners({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 12),
      child: ListView.builder(
        shrinkWrap: true,
        padding: const EdgeInsets.all(0),
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 3.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/calog.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 1.7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/cabig.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
