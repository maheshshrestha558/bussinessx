// ignore_for_file: must_be_immutable

import 'package:b2b_market/bloc/product_bloc.dart';
import 'package:b2b_market/models/response/response.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:b2b_market/screens/product/components/b2c_product_list_tile.dart';
import 'package:flutter/material.dart';

import '../../../models/product.dart';

List exerciseFilter = [
  'walking',
  'running',
  'cycling',
  'hiking',
  'hikjing',
  'hikjing',
  'hikjing',
  'hikging'
];
List title = [
  'walking',
  'running',
  'cycling',
  'hiking',
  'hikjing',
  'hikjing',
  'hikging'
];

List _isSelected = List.generate(exerciseFilter.length, (index) => false);

class BuyerLeafCategoryScreen extends StatefulWidget {
  final LeafCategory leafCategory;
  const BuyerLeafCategoryScreen({
    Key? key,
    required this.leafCategory,
  }) : super(key: key);

  @override
  State<BuyerLeafCategoryScreen> createState() =>
      _BuyerLeafCategoryScreenState();
}

final GlobalKey<ScaffoldState> _key = GlobalKey();

List items = [
  'Best Price',
  'Popularity',
  'Best Value',
  'User Chioces',
  'Premium Products',
  'Best Price'
];

class _BuyerLeafCategoryScreenState extends State<BuyerLeafCategoryScreen> {
  bool _isList = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarForAllPage(
        navTitle: widget.leafCategory.name,
        visible: true,
      ),
      endDrawer: Drawer(
        child: FilterChipParentCard(title: title.length),
      ),
      endDrawerEnableOpenDragGesture: false,
      drawerEnableOpenDragGesture: false,
      key: _key,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              height: 60,
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: ListView.separated(
                      shrinkWrap: true,
                      separatorBuilder: (context, index) => Container(
                        decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(255, 32, 110, 255), //New
                              blurRadius: 5.0,
                              offset: Offset(0, 0),
                            )
                          ],
                        ),
                      ),
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Align(
                          alignment: Alignment.center,
                          child: Container(
                            margin: const EdgeInsets.symmetric(
                              horizontal: 3,
                            ),
                            padding: const EdgeInsets.symmetric(
                              horizontal: 8,
                              vertical: 10,
                            ),
                            decoration: BoxDecoration(
                              color: const Color.fromARGB(237, 219, 236, 214),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Text(
                              items[index],
                              style: const TextStyle(
                                color: Color.fromARGB(221, 1, 75, 145),
                                fontStyle: FontStyle.normal, //New
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: 4,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _key.currentState!.openEndDrawer();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                        border: Border.all(width: 1, color: Colors.black),
                        gradient: const LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          tileMode: TileMode.clamp,
                          colors: [
                            Color.fromARGB(255, 203, 253, 218),
                            Color(0xff7CD0FF),
                          ],
                        ),
                      ),
                      child: const Icon(
                        size: 30,
                        Icons.filter_list_outlined,
                        color: Color(0xff1D4ED8),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _isList = !_isList;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        // color: Color.fromARGB(255, 82, 211, 146),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                        border: Border.all(width: 1, color: Colors.black),

                        gradient: const LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          tileMode: TileMode.clamp,
                          colors: [
                            Color.fromARGB(255, 203, 253, 218),
                            Color(0xff7CD0FF),
                          ],
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: _isList
                            ? const Icon(
                                Icons.grid_view_outlined,
                                color: Color(0xff1D4ED8),
                              )
                            : const Icon(
                                Icons.list,
                                color: Color(0xff1D4ED8),
                              ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  SizedBox(
                    child: StreamBuilder<List<Product>>(
                      stream: productBloc.leafCategoryWiseProducts,
                      builder: (context, snapshot) {
                        var products = snapshot.data;
                        if (products == null) {
                          return GridView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 1.0,
                              mainAxisExtent: 280,
                            ),
                            primary: false,
                            shrinkWrap: true,
                            itemCount: 6,
                            itemBuilder: (context, index) {
                              return const B2CProductShimmer();
                            },
                          );
                        }
                        if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        }
                        if (products.isEmpty) {
                          return const Text('No products found');
                        }
                        return _isList
                            ? GridView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                gridDelegate:
                                    const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 0,
                                  mainAxisSpacing: 5,
                                  childAspectRatio: 0.65,
                                ),
                                itemCount: products.length,
                                itemBuilder: (context, index) {
                                  var product = products[index];

                                  return ProductForYou(
                                    product: product,
                                    heroType: 'all',
                                  );
                                },
                              )
                            : ListView.builder(
                                primary: true,
                                shrinkWrap: true,
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 5,
                                  vertical: 5,
                                ),
                                physics: const BouncingScrollPhysics(),
                                itemCount: products.length,
                                itemBuilder: (context, index) {
                                  var product = products[index];

                                  return B2CProductListTile(
                                    product: product,
                                    heroType: '',
                                  );
                                },
                              );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FilterChipParentCard extends StatefulWidget {
  FilterChipParentCard({super.key, required this.title});
  int title;
  @override
  State<FilterChipParentCard> createState() => _FilterChipParentCardState();
}

class _FilterChipParentCardState extends State<FilterChipParentCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView.builder(
        itemCount: widget.title,
        itemBuilder: (BuildContext context, int index) {
          return Customchip(
            title: title,
            labelIndex: index,
          );
        },
      ),
    );
  }
}

class Customchip extends StatefulWidget {
  Customchip({
    required this.labelIndex,
    super.key,
    required this.title,
  });

  int labelIndex;

  List title;

  @override
  State<Customchip> createState() => _CustomchipState();
}

class _CustomchipState extends State<Customchip> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Text(
                title[widget.labelIndex].toString().toUpperCase(),
                style: const TextStyle(
                  color: Color.fromARGB(255, 1, 75, 145),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
            const SizedBox(height: 5.0),
            SizedBox(
              width: double.infinity,
              child: Center(
                child: Wrap(
                  runSpacing: 5,
                  spacing: 5.0,
                  children: List<Widget>.generate(
                    exerciseFilter.length,
                    (index) => FilterChip(
                      label: Text(
                        exerciseFilter[index],
                        style: const TextStyle(
                          color: Color.fromARGB(255, 48, 49, 50),
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      selected: _isSelected[index],
                      onSelected: (bool selected) {
                        setState(() {
                          _isSelected[index] = !_isSelected[index];
                        });
                      },
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10.0,
              child: Divider(
                color: Color.fromARGB(255, 217, 217, 217),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
