import 'package:b2b_market/models/response/banner/banner_response.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';

import '../../../bloc/banner_bloc.dart';
import '../../../common/shimmer_widget.dart';

class B2CCategoryBanner extends StatefulWidget {
  const B2CCategoryBanner({
    Key? key,
  }) : super(key: key);

  @override
  State<B2CCategoryBanner> createState() => _BannersState();
}

class _BannersState extends State<B2CCategoryBanner> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      height: 250,
      child: StreamBuilder<B2CBannerCategoryResponse>(
        stream: bannerBloc.b2cCategoryBanner.stream,
        builder: (context, snapshot) {
          var banners = snapshot.data?.categoryBanners;
          if (banners != null && banners.isNotEmpty) {
            return CarouselSlider.builder(
              enableAutoSlider: true,
              unlimitedMode: true,
              itemCount: banners.length,
              slideBuilder: (index) {
                var imageThumbnail = banners[index].imageThumbnail;
                return BannerCachedNetworkImage(filePath: imageThumbnail);
              },
              slideIndicator: CircularSlideIndicator(
                indicatorBackgroundColor: Colors.grey,
                currentIndicatorColor: Colors.white,
                padding: const EdgeInsets.only(bottom: 18),
              ),
              autoSliderDelay: const Duration(seconds: 5),
            );
          }
          return const Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 250,
              // width: 550,
            ),
          );
        },
      ),
    );
  }
}

class BannerCachedNetworkImage extends StatelessWidget {
  const BannerCachedNetworkImage({
    Key? key,
    required this.filePath,
  }) : super(key: key);

  final String filePath;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: filePath,
      fit: BoxFit.cover,
      errorWidget: (context, url, error) {
        return const Align(
          alignment: Alignment.centerLeft,
          child: ShimmerWidget.rectangular(
            height: 200,
            // width: 550,
          ),
        );
      },
    );
  }
}

class B2CSingleBanners extends StatefulWidget {
  const B2CSingleBanners({
    Key? key,
  }) : super(key: key);

  @override
  State<B2CSingleBanners> createState() => _BannerSingleState();
}

class _BannerSingleState extends State<B2CSingleBanners> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      height: 100,
      child: StreamBuilder<B2CBannerResponse>(
        stream: bannerBloc.b2cBanner.stream,
        builder: (context, snapshot) {
          var banners = snapshot.data?.homeBannerFirst;
          if (banners != null && banners.imageThumbnail.isNotEmpty) {
            return BannerCachedNetworkImage(filePath: banners.imageThumbnail);
          }
          return const Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 100,
              // width: 550,
            ),
          );
        },
      ),
    );
  }
}
