import 'package:b2b_market/bloc/banner_bloc.dart';
import 'package:b2b_market/bloc/category_bloc.dart';
import 'package:b2b_market/bloc/popular_product_bloc.dart';
import 'package:b2b_market/bloc/product_bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/models/response/category/root_categories_response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ShopSugestion extends StatefulWidget {
  const ShopSugestion({super.key});

  @override
  State<ShopSugestion> createState() => ShopSugestionState();
}

class ShopSugestionState extends State<ShopSugestion> {
  bool select = true;
  bool select1 = true;
  bool select2 = true;
  List<bool> isSelected = [true, false, false];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBarForAllPage(
        visible: true,
        navTitle: 'Near By Shop',
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: ListView(
          padding: const EdgeInsets.all(0),
          // shrinkWrap: true,
          children: [
            Card(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(right: 4),
                        child: Icon(
                          Icons.location_on,
                          size: 25,
                          color: Colors.green,
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 100,
                        child: const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Your Current Location',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color: Color.fromARGB(255, 121, 121, 121),
                              ),
                            ),
                            Text(
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              'Opposite Buddha Park,Swayambhu,kathmandu',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                top: 8,
              ),
              child: Text(
                'Select Category',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color.fromARGB(255, 1, 75, 145),
                ),
              ),
            ),
            const ShopCatagories(),
            const Divider(
              height: 1,
              color: Color.fromARGB(255, 217, 217, 217),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Row(
                children: [
                  const Expanded(
                    child: Text(
                      'Shops By Category',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color.fromARGB(255, 1, 75, 145),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: const Color.fromARGB(255, 1, 75, 145),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 8,
                        right: 8,
                        bottom: 2,
                        top: 2,
                      ),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/filter.png',
                            height: 18,
                            width: 18,
                          ),
                          const Text(
                            'Filters',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: Color.fromARGB(255, 1, 75, 145),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 60,
              child: CustomradioButton(),
            ),
            const ListOfShop(),
          ],
        ),
      ),
    );
  }
}

class ListOfShop extends StatefulWidget {
  const ListOfShop({super.key});

  @override
  State<ListOfShop> createState() => _ListOfShopState();
}

class _ListOfShopState extends State<ListOfShop> {
  bool liked = true;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 1,
      itemBuilder: (context, index) {
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.width / 4.5,
                      width: MediaQuery.of(context).size.width / 4.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          'assets/images/ah.jpg',
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          top: 5,
                          left: 5,
                          bottom: 5,
                          right: 5,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                const Expanded(
                                  child: Text(
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    'Maila dai ko pasal & bhati store enterprises',
                                    style: TextStyle(
                                      color: Color.fromARGB(255, 1, 75, 145),
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(3),
                                    ),
                                    border: Border.all(
                                      color: const Color.fromARGB(
                                        255,
                                        1,
                                        75,
                                        145,
                                      ),
                                    ),
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.all(3),
                                    child: Text(
                                      '200m away',
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 1, 75, 145),
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const Padding(
                              padding: EdgeInsets.only(bottom: 3, top: 3),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    size: 13,
                                    color: Colors.green,
                                  ),
                                  Expanded(
                                    child: Text(
                                      maxLines: 1,
                                      'Swayambhu,Opposite Buddha Park  ',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        'Rating:',
                                        style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 1, 75, 145),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: 3,
                                        ),
                                        child: Text(
                                          '4/5',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 30,
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          shrinkWrap: true,
                                          physics:
                                              const NeverScrollableScrollPhysics(),
                                          itemCount: 5,
                                          itemBuilder: (context, index) {
                                            return const Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Icon(
                                                  Icons.star,
                                                  color: Color.fromARGB(
                                                    255,
                                                    250,
                                                    179,
                                                    0,
                                                  ),
                                                  size: 15,
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      liked = !liked;
                                    });
                                  },
                                  child: Card(
                                    elevation: 6,
                                    color: liked
                                        ? const Color.fromARGB(
                                            255,
                                            210,
                                            232,
                                            203,
                                          )
                                        : Colors.blue,
                                    // color: const Color.fromARGB(255, 210, 232, 203),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            'assets/images/like.png',
                                            height: 10,
                                            width: 10,
                                          ),
                                          const Padding(
                                            padding: EdgeInsets.only(left: 4),
                                            child: Text(
                                              '6.3k',
                                              style: TextStyle(
                                                color: Color.fromARGB(
                                                  255,
                                                  1,
                                                  75,
                                                  145,
                                                ),
                                                fontSize: 10,
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class ShopCatagories extends StatefulWidget {
  const ShopCatagories({Key? key}) : super(key: key);

  @override
  State<ShopCatagories> createState() => _ShopCatagoriesState();
}

class _ShopCatagoriesState extends State<ShopCatagories> {
  final _scrollController = ScrollController();

  int index = 0;
  @override
  void initState() {
    super.initState();

    index = 0;

    _scrollController.addListener(() {});

    popularProductBloc.getPopularProduct();
  }

  getForward(int index) {
    _scrollController.position.pixels;
    Future.delayed(const Duration(milliseconds: 50), () {}).then((s) {
      _scrollController.animateTo(
        index * 80.5 * 4,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  getReverse(int index) {
    _scrollController.position.pixels;
    Future.delayed(const Duration(milliseconds: 50), () {}).then((s) {
      _scrollController.animateTo(
        index * 80.5 * 4,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<RootCategoriesResponse>(
      stream: categoryBloc.rootCategories.stream,
      builder: (context, snapshot) {
        var rootCategories = snapshot.data?.rootCategories?.rootCategory;
        if (rootCategories != null && rootCategories.isNotEmpty) {
          return SizedBox(
            height: 100,
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification notification) {
                if (notification is UserScrollNotification) {
                  if (notification.direction == ScrollDirection.forward) {
                    if (index > 0) {
                      setState(() {
                        index--;
                        getForward(index);
                      });
                    } else {}
                  } else if (notification.direction ==
                      ScrollDirection.reverse) {
                    var value = rootCategories.length / 4 - 2;

                    if (index > value) {
                    } else {
                      setState(() {
                        index++;
                        getReverse(index);
                      });
                    }
                  }
                }

                return true;
              },
              child: ListView.separated(
                separatorBuilder: (context, index) => Container(
                  width: 5,
                ),
                controller: _scrollController,

                // physics: const PageScrollPhysics(),
                itemCount: rootCategories.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return UserCategoryItem(
                    rootCategory: rootCategories[index],
                  );
                },
              ),
            ),
          );
        }

        return const SizedBox(height: 70, child: CategoryShimmer());
      },
    );
  }
}

class UserCategoryItem extends StatelessWidget {
  final RootCategory rootCategory;
  const UserCategoryItem({
    Key? key,
    required this.rootCategory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getSubCategories(rootCategory.id);
        await bannerBloc.getB2CCategoryBanner(rootCategory.id);
        await productBloc.fetchCategoryWiseProducts(rootCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerCategoryScreen,
            arguments: rootCategory,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: SizedBox(
        width: 50,
        // height: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              // height: 42,
              child: CircleAvatar(
                radius: 30,
                backgroundColor: const Color.fromARGB(255, 248, 242, 226),
                backgroundImage: NetworkImage(rootCategory.iconImageThumbnail),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomradioButton extends StatefulWidget {
  const CustomradioButton({super.key});

  @override
  State<CustomradioButton> createState() => _CustomradioButton();
}

class _CustomradioButton extends State<CustomradioButton> {
  int value = 0;
  Widget customRadioButton(
    String text,
    int index,
    BorderRadiusGeometry borderRadiusGeometry,
  ) {
    return GestureDetector(
      onTap: () {
        setState(() {
          value = index;
        });
      },
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          borderRadius: borderRadiusGeometry,
          border: Border.all(color: const Color.fromARGB(255, 210, 232, 203)),
          color: (value == index)
              ? const Color.fromARGB(255, 210, 232, 203)
              : Colors.white,
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            text,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              color: Color.fromARGB(255, 1, 75, 145),
              fontStyle: FontStyle.normal,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: customRadioButton(
                'Feature',
                1,
                const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  topLeft: Radius.circular(10),
                ),
              ),
            ),
            Expanded(
              child: customRadioButton('Distance', 2, BorderRadius.circular(0)),
            ),
            Expanded(
              child: customRadioButton(
                'Rating',
                3,
                const BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
