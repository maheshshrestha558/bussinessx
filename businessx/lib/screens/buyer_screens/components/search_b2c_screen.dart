import 'package:b2b_market/bloc/B2C/search_b2c_bloc.dart';
import 'package:b2b_market/common/widgets/page_title.dart';
import 'package:b2b_market/models/response/category_search_response.dart';
import 'package:b2b_market/models/response/product_search_response.dart';
import 'package:b2b_market/screens/buyer_screens/b2c_search/b2c_product_list_tile.dart';
import 'package:b2b_market/screens/category/leaf_categories_screen/components/leaf_category_card.dart';
import 'package:flutter/material.dart';

class SearchB2CScreen extends StatefulWidget {
  const SearchB2CScreen({Key? key}) : super(key: key);

  @override
  State<SearchB2CScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchB2CScreen> {
  final _searchController = TextEditingController();
  final _inputFocusNode = FocusNode();
  final _searchBloc = SearchB2CBloc();
  bool _showCategories = true;

  @override
  void dispose() {
    _inputFocusNode.dispose();
    _searchController.dispose();
    _searchBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          leading: const Icon(
            Icons.arrow_back_ios,
            color: Color.fromARGB(255, 1, 75, 145),
          ),
          title: const Text(
            'Search Products',
            style: TextStyle(
              color: Color.fromARGB(255, 1, 75, 145),
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.shopping_cart_outlined),
              color: const Color.fromARGB(255, 100, 177, 77),
              onPressed: () {},
            ),
            IconButton(
              icon: const Icon(Icons.notifications_none_outlined),
              color: const Color.fromARGB(255, 100, 177, 77),
              onPressed: () {},
            ),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(1, 4),
                    blurRadius: 6,
                    spreadRadius: 0,
                    color: Colors.grey.withOpacity(.4),
                  ),
                ],
              ),
              child: TextFormField(
                enableSuggestions: false,
                autocorrect: false,
                style: const TextStyle(
                  decoration: TextDecoration.none,
                  color: Color.fromARGB(255, 1, 75, 145),
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
                enabled: true,
                controller: _searchController,
                autofocus: true,
                focusNode: _inputFocusNode,
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  if (query.trim().isNotEmpty && query.trim().length > 2) {
                    _searchBloc.searchProducts(query.trim());
                    setState(() {
                      _showCategories = false;
                    });
                  }
                },
                onChanged: (query) {
                  _searchBloc.querySink.add(query.trim());

                  setState(() {});
                  _showCategories = true;
                },
                decoration: InputDecoration(
                  prefixIcon: GestureDetector(
                    onTap: () {
                      if (_searchController.text.trim().isNotEmpty &&
                          _searchController.text.trim().length > 2) {
                        _searchBloc
                            .searchProducts(_searchController.text.trim());
                        setState(() {
                          _showCategories = false;
                        });
                      }
                    },
                    child: const Icon(
                      Icons.search,
                      color: Color.fromARGB(255, 100, 177, 77),
                      size: 24,
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'Search Product/ Service',
                  hintStyle: const TextStyle(
                    decoration: TextDecoration.none,
                    fontSize: 16,
                    color: Color.fromARGB(255, 1, 75, 145),
                    fontWeight: FontWeight.w400,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  // enabledBorder: OutlineInputBorder(
                  //   borderRadius: BorderRadius.circular(20),
                  // ),
                  // focusedBorder: OutlineInputBorder(
                  //   borderRadius: BorderRadius.circular(20),
                  // ),
                ),
              ),
            ),
            const PageTitle(
              title: 'Search Results',
              showBackButton: false,
            ),
            Visibility(
              visible: _showCategories,
              replacement: Expanded(
                child: StreamBuilder<ProductSearchResponse>(
                  stream: _searchBloc.productSearchResponse.stream,
                  builder: (context, snapshot) {
                    var products = snapshot.data?.products;
                    if (products != null && products.isEmpty) {
                      return const Center(
                        child: Text(
                          'No results found',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      );
                    }
                    if (products != null && products.isNotEmpty) {
                      return ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        itemCount: products.length,
                        itemBuilder: (context, index) {
                          var product = products[index];
                          return B2CProductListTile(product: product);
                        },
                      );
                    }

                    return const Center(
                      child: Text('Enter keyword to search'),
                    );
                  },
                ),
              ),
              child: Expanded(
                child: StreamBuilder<LeafCategorySearchResponse?>(
                  stream: _searchBloc.leafCategorySearchResult,
                  builder: (context, snapshot) {
                    var leafCategories = snapshot.data?.leafCategories;
                    if (leafCategories != null && leafCategories.isEmpty) {
                      return const Center(
                        child: Text(
                          'No results found',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      );
                    }
                    if (leafCategories != null && leafCategories.isNotEmpty) {
                      return ListView.separated(
                        physics: const BouncingScrollPhysics(),
                        separatorBuilder: (__, _) => const Divider(),
                        itemCount: leafCategories.length,
                        itemBuilder: (context, index) {
                          var leafCategory = leafCategories[index];
                          return LeafCategoryCard.listTile(
                            leafCategory: leafCategory,
                          );
                        },
                      );
                    }
                    return const Center(
                      child: Text('Enter keyword to search'),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
