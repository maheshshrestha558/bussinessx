import 'package:b2b_market/screens/home/components/components.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../models/models.dart';

class B2CProductCachedNetworkImage extends StatelessWidget {
  const B2CProductCachedNetworkImage({
    Key? key,
    required this.product,
    required this.heroType,
  }) : super(key: key);

  final Product product;
  final String heroType;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: '${product.id}-$heroType',
      child: CachedNetworkImage(
        placeholder: (context, _) => ErrorAndPlaceholder(
          product.name,
        ),
        imageUrl: product.imageThumbnail,
        imageBuilder: (context, imageProvider) => Container(
          height: 120,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(4),
              topRight: Radius.circular(4),
            ),
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        errorWidget: (context, url, error) => ErrorAndPlaceholder(
          product.name,
        ),
      ),
    );
  }
}
