import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/cart_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      // flexibleSpace: Container(
      //   decoration: appBarDecoration,
      // ),
      title: Padding(
        padding: const EdgeInsets.only(left: 10, right: 0),
        child: SvgPicture.asset(
          'assets/images/business_logo.svg',
          width: 150,
          height: 50,
        ),
      ),
      actions: [
        IconButton(
          icon: const Icon(
            Icons.notifications_none_outlined,
            color: Colors.green,
            size: 25,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: const Icon(
            Icons.shopping_cart_outlined,
            size: 25,
          ),
          color: Colors.green,
          onPressed: () {
            Navigator.pushNamed(
              context,
              CartPage.routeName,
            );
          },
        ),
        const SizedBox(
          width: 10,
        ),
        // Padding(
        //   padding: const EdgeInsets.symmetric(vertical: 13.0, horizontal: 5),
        //   child: Container(
        //     decoration: BoxDecoration(
        //       gradient: const LinearGradient(
        //         begin: Alignment.topCenter,
        //         end: Alignment.bottomCenter,
        //         colors: [
        //           Color.fromARGB(255, 173, 255, 196),
        //           // Color.fromARGB(184, 248, 234, 233),
        //           Color.fromARGB(255, 165, 252, 255),
        //         ],
        //       ),
        //       borderRadius: BorderRadius.circular(12),
        //       // border: Border.all(width: 1),
        //     ),
        //     child: Container(
        //       margin: const EdgeInsets.all(2),
        //       decoration: BoxDecoration(
        //         borderRadius: BorderRadius.circular(12),
        //         color: const Color.fromARGB(255, 255, 255, 255),
        //       ),
        //       child: const Padding(
        //         padding: EdgeInsets.all(6.0),
        //         child: Text(
        //           '000.00',
        //           style: TextStyle(
        //             fontSize: 13,
        //             color: Color.fromARGB(255, 128, 41, 241),
        //           ),
        //         ),
        //       ),
        //     ),
        //   ),
        // )
      ],
      bottom: PreferredSize(
        preferredSize: preferredSize,
        child: const Padding(
          padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
          child: SearchField(),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(130);
}

class CustomAppBarForAllPage extends StatelessWidget
    implements PreferredSizeWidget {
  final String navTitle;
  final bool visible;
  const CustomAppBarForAllPage({
    Key? key,
    required this.navTitle,
    required this.visible,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: const Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Color.fromARGB(255, 1, 75, 145),
          size: 18,
        ),
      ),
      titleSpacing: 1,
      elevation: 5,
      automaticallyImplyLeading: true,
      // flexibleSpace: Container(
      //   decoration: appBarDecoration,
      // ),
      title: Padding(
        padding: const EdgeInsets.only(left: 0, right: 5),
        child: Text(
          navTitle,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            color: Color.fromARGB(255, 1, 75, 145),
            fontSize: 16,
          ),
        ),
      ),
      actions: [
        Visibility(
          visible: visible,
          child: IconButton(
            color: const Color.fromARGB(255, 100, 177, 77),
            icon: const Icon(
              Icons.search,
            ),
            onPressed: () {
              Navigator.pushNamed(context, RouteName.searchB2CScreen);
            },
          ),
        ),
        Visibility(
          visible: true,
          child: IconButton(
            color: const Color.fromARGB(255, 100, 177, 77),
            icon: const Icon(Icons.shopping_cart_outlined),
            onPressed: () {
              Navigator.pushNamed(
                context,
                CartPage.routeName,
              );
            },
          ),
        ),
        Visibility(
          visible: true,
          child: InkWell(
            onTap: () {},
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Icon(
                Icons.notifications_none_outlined,
                color: Color.fromARGB(255, 100, 177, 77),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(50);
}

class SearchField extends StatelessWidget {
  final String hintText;
  const SearchField({
    this.hintText = 'Search Product/ Service',
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, RouteName.searchB2CScreen);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Container(
          height: 50,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.blueAccent),
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                offset: const Offset(1, 5),
                blurRadius: 10,
                spreadRadius: 0,
                color: Colors.grey.withOpacity(.4),
              ),
            ],
          ),
          child: TextField(
            onTap: () {},
            enabled: false,
            decoration: InputDecoration(
              prefixIcon: const Icon(
                Icons.search,
                size: 30,
                color: Color.fromRGBO(76, 175, 80, 1),
              ),
              filled: true,
              fillColor: Colors.white,
              hintText: hintText,
              hintStyle: const TextStyle(
                color: Colors.blueAccent,
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
