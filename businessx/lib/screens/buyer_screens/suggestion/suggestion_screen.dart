import 'package:b2b_market/bloc/bloc.dart';
import 'package:b2b_market/models/models.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:flutter/material.dart';

class ProductSuggestionScreen extends StatefulWidget {
  const ProductSuggestionScreen({Key? key}) : super(key: key);

  @override
  State<ProductSuggestionScreen> createState() =>
      _ProductSuggestionScreenState();
}

class _ProductSuggestionScreenState extends State<ProductSuggestionScreen> {
  bool favorite = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: const CustomAppBarForAllPage(
        navTitle: 'Suggestion for you',
        visible: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width - 10,
              height: MediaQuery.of(context).size.height / 12,
              child: Image.asset(
                'assets/banner/suggestion_banner.jpg',
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            SingleChildScrollView(
              child: StreamBuilder<List<Product>>(
                stream: productBloc.featuredProducts,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var allProducts = snapshot.data!;
                    var suggestedProducts = snapshot.data;
                    if (suggestedProducts == null) {
                      return GridView.builder(
                        scrollDirection: Axis.vertical,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 1.0,
                          mainAxisExtent: 231,
                        ),
                        shrinkWrap: true,
                        itemCount: 6,
                        itemBuilder: (context, index) {
                          return const B2CProductShimmer();
                        },
                      );
                    }
                    if (suggestedProducts.isEmpty) {
                      return const Center(child: Text('No Any Products'));
                    }
                    return SizedBox(
                      child: GridView.builder(
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 0,
                          mainAxisSpacing: 5,
                          childAspectRatio: 0.65,
                        ),
                        itemCount: allProducts.length,
                        itemBuilder: (context, index) {
                          var product = allProducts[index];

                          return ProductForYou(
                            product: product,
                            heroType: 'all',
                          );
                        },
                      ),
                    );
                  }
                  if (snapshot.hasError) {
                    return Text(snapshot.error.toString());
                  }
                  return const Center(child: Text('No Any Suggestions'));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
