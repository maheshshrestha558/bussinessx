import 'package:b2b_market/bloc/bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/models/models.dart';
import 'package:b2b_market/models/response/product/popular_product_response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:flutter/material.dart';

class ProductCardB2C extends StatefulWidget {
  const ProductCardB2C({
    Key? key,
    required this.product,
    required this.heroType,
  }) : super(key: key);

  final String heroType;
  final Product product;

  @override
  State<ProductCardB2C> createState() => _ProductCardB2CState();
}

class _ProductCardB2CState extends State<ProductCardB2C> {
  bool favorite = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(widget.product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': widget.heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: ProductNewArrivalB2C(product: widget.product),
    );
  }
}

class ProductNewArrivalB2C extends StatelessWidget {
  const ProductNewArrivalB2C({
    Key? key,
    required this.product,
  }) : super(key: key);
  final String heroType = 'featured';
  final Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Card(
        elevation: 3,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Stack(
                // fit: StackFit.passthrough,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2.65,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      color: Colors.grey.shade300,
                    ),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child: Image.network(
                        product.imageThumbnail,
                        fit: BoxFit.cover,
                        height: MediaQuery.of(context).size.width / 2.65,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Positioned(
                      top: 5,
                      left: 5,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 5,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color.fromARGB(255, 1, 75, 145),
                        ),
                        child: const Center(
                          child: Text(
                            '60% Off',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: false,
                    child: Positioned(
                      bottom: 6,
                      right: 5,
                      child: GestureDetector(
                        onTap: () {},
                        child: const CircleAvatar(
                          backgroundColor: Colors.red,
                          maxRadius: 15,
                          child: Icon(
                            Icons.favorite_border_outlined,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Divider(
                height: 2,
                thickness: 5,
                color: Colors.grey.shade200,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            product.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          const Row(
                            children: [
                              Text(
                                '4/5',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Icon(
                                Icons.star,
                                color: Colors.amber,
                                size: 20,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const CircleAvatar(
                      backgroundColor: Color.fromARGB(255, 1, 75, 145),
                      maxRadius: 18,
                      child: Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.white,
                        size: 15,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Rs. ${product.price}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Visibility(
                            visible: true,
                            child: Text(
                              'Rs. ${product.price}',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                decoration: TextDecoration.lineThrough,
                                decorationColor: Colors.redAccent,
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                color: Colors.grey,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Center(
                        child: Container(
                          height: 25,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: const Color.fromARGB(255, 100, 177, 77),
                          ),
                          child: const Center(
                            child: Text(
                              'Buy',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ProductForYou extends StatefulWidget {
  const ProductForYou({
    super.key,
    required this.heroType,
    required this.product,
  });
  final String heroType;
  final Product product;

  @override
  State<ProductForYou> createState() => _ProductForYouState();
}

class _ProductForYouState extends State<ProductForYou> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(widget.product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': widget.heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: SizedBox(
        height: MediaQuery.of(context).size.height / 2.9,
        child: Card(
          elevation: 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Stack(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2.2,
                    height: MediaQuery.of(context).size.width / 2.3,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child: Image.network(
                        widget.product.imageThumbnail,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Positioned(
                      left: 5,
                      top: 5,
                      child: Container(
                        height: 25,
                        width: 70,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color.fromARGB(255, 1, 75, 145),
                        ),
                        child: const Center(
                          child: Text(
                            '60% Off',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Positioned(
                      bottom: 6,
                      right: 5,
                      child: GestureDetector(
                        onTap: () {},
                        child: const CircleAvatar(
                          backgroundColor: Colors.red,
                          maxRadius: 18,
                          child: Icon(
                            Icons.favorite_border_outlined,
                            color: Colors.white,
                            size: 16,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Divider(
                height: 2,
                thickness: 5,
                color: Colors.grey.shade200,
              ),
              // Padding(
              //   padding: const EdgeInsets.all(5.0),
              //   child: Column(
              //     mainAxisAlignment: MainAxisAlignment.end,
              //     children: [
              //     ],
              //   ),
              // )

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        widget.product.name,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          height: 1,
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 1, 75, 145),
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                    const CircleAvatar(
                      backgroundColor: Color.fromARGB(255, 1, 75, 145),
                      maxRadius: 17,
                      child: Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.white,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  children: [
                    Text(
                      '4.5',
                      style: TextStyle(
                        fontSize: 14,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                      size: 20,
                    ),
                    Text(
                      'Free delivery',
                      style: TextStyle(
                        fontSize: 12,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Rs. ${widget.product.price}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            'Rs. ${widget.product.price}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              decorationColor: Colors.red,
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              color: Colors.grey.shade500,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 28,
                      width: 65,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: const Color.fromARGB(255, 100, 177, 77),
                      ),
                      child: const Center(
                        child: Text(
                          'Buy',
                          style: TextStyle(
                            fontSize: 16,
                            fontStyle: FontStyle.normal,
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
