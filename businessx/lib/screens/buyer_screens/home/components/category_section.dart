import 'package:b2b_market/bloc/bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/models/response/category/root_categories_response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CategoriesSection extends StatefulWidget {
  const CategoriesSection({Key? key}) : super(key: key);

  @override
  State<CategoriesSection> createState() => _CategoriesSectionState();
}

class _CategoriesSectionState extends State<CategoriesSection> {
  final _scrollController = ScrollController();

  int index = 0;
  @override
  void initState() {
    super.initState();

    index = 0;

    _scrollController.addListener(() {});

    popularProductBloc.getPopularProduct();
  }

  getForward(int index) {
    _scrollController.position.pixels;
    Future.delayed(const Duration(milliseconds: 50), () {}).then((s) {
      _scrollController.animateTo(
        index * 80.5 * 4,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  getReverse(int index) {
    _scrollController.position.pixels;
    Future.delayed(const Duration(milliseconds: 50), () {}).then((s) {
      _scrollController.animateTo(
        index * 80.5 * 4,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<RootCategoriesResponse>(
      stream: categoryBloc.rootCategories.stream,
      builder: (context, snapshot) {
        var rootCategories = snapshot.data?.rootCategories?.rootCategory;
        if (rootCategories != null && rootCategories.isNotEmpty) {
          return SizedBox(
            height: 80,
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification notification) {
                if (notification is UserScrollNotification) {
                  if (notification.direction == ScrollDirection.forward) {
                    if (index > 0) {
                      setState(() {
                        index--;
                        getForward(index);
                      });
                    } else {}
                  } else if (notification.direction ==
                      ScrollDirection.reverse) {
                    var value = rootCategories.length / 4 - 2;

                    if (index > value) {
                    } else {
                      setState(() {
                        index++;
                        getReverse(index);
                      });
                    }
                  }
                }

                return true;
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: ListView.separated(
                  separatorBuilder: (context, index) => Container(),
                  controller: _scrollController,
                  itemCount: rootCategories.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return UserCategoryItem(
                      rootCategory: rootCategories[index],
                    );
                  },
                ),
              ),
            ),
          );
        }

        return const SizedBox(height: 80, child: CategoryShimmer());
      },
    );
  }
}

class UserCategoryItem extends StatelessWidget {
  final RootCategory rootCategory;
  const UserCategoryItem({
    Key? key,
    required this.rootCategory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getSubCategories(rootCategory.id);
        await bannerBloc.getB2CCategoryBanner(rootCategory.id);
        await productBloc.fetchCategoryWiseProducts(rootCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerCategoryScreen,
            arguments: rootCategory,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Row(
        children: [
          SizedBox(
            width: 60,
            child: Column(
              children: [
                CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30),
                    child: Image.network(
                      rootCategory.iconImageThumbnail,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 3,
                ),
                Text(
                  rootCategory.name,
                  style: const TextStyle(
                    fontSize: 10,
                  ),
                  overflow: TextOverflow.fade,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 5,
          ),
        ],
      ),
    );
  }
}
