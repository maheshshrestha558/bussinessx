import 'package:b2b_market/screens/buyer_screens/cart_page.dart';
import 'package:flutter/material.dart';

class EditPageScreen extends StatefulWidget {
  const EditPageScreen({super.key});

  @override
  State<EditPageScreen> createState() => _EditpageState();
}

class _EditpageState extends State<EditPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(
          Icons.arrow_back_ios,
          color: Color.fromARGB(255, 1, 75, 145),
        ),
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        title: const Text(
          'Profile',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: Color.fromARGB(255, 1, 75, 145),
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                CartPage.routeName,
              );
            },
            child: const Icon(
              Icons.shopping_cart_outlined,
              color: Color.fromARGB(255, 100, 177, 77),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () {},
            child: const Icon(
              Icons.notifications_none_sharp,
              color: Color.fromARGB(255, 100, 177, 77),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            const Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Profile',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 0, 0, 0),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Stack(
                  children: [
                    CircleAvatar(
                      backgroundColor: Color.fromARGB(255, 1, 75, 145),
                      radius: 60,
                      child: CircleAvatar(
                        radius: 56,
                        backgroundImage: AssetImage('assets/images/ah.jpg'),
                      ),
                    ),
                    Positioned(
                      bottom: 1,
                      right: 1,
                      child: CircleAvatar(
                        backgroundColor: Color.fromARGB(255, 100, 177, 77),
                        radius: 20,
                        child: Icon(
                          Icons.camera_alt_outlined,
                          size: 15,
                          color: Color.fromARGB(255, 255, 255, 255),
                        ),
                      ),
                    ),
                  ],
                ),
                // Container(
                //   decoration: BoxDecoration(
                //     borderRadius: BorderRadius.circular(20),
                //     border: Border.all(
                //       color: const Color.fromARGB(255, 1, 75, 145),
                //     ),
                //   ),
                //   child: const Padding(
                //     padding: EdgeInsets.symmetric(
                //       vertical: 8,
                //       horizontal: 13,
                //     ),
                //     child: Text(
                //       'Add Profile',
                //       style: TextStyle(
                //         fontSize: 14,
                //         fontWeight: FontWeight.w500,
                //         color: Color.fromARGB(255, 0, 0, 0),
                //       ),
                //     ),
                //   ),
                // )
              ],
            ),
            Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        ' Name (English) ',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 1, 75, 145),
                        ),
                      ),
                    ),
                    CustomTextormfield2(
                      hintTextFormField: 'Jhon Sapkota',
                    ),
                    const SizedBox(height: 10),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        '  Name (Nepali)',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ),
                    ),
                    CustomTextormfield2(hintTextFormField: 'Jhon Sapkota'),
                    const SizedBox(height: 10),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        ' Contact Number ',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ),
                    ),
                    CustomTextormfield2(
                      hintTextFormField: '+977-9851032333',
                    ),
                    const SizedBox(height: 10),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        ' Alternate Contact Number (Optional) ',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ),
                    ),
                    CustomTextormfield2(
                      hintTextFormField: '+977-9851032333',
                    ),
                    const SizedBox(height: 10),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        ' Address ',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 0, 0, 0),
                        ),
                      ),
                    ),
                    CustomTextormfield2(
                      hintTextFormField: 'Swoyambhu - 15, Kathmandu, Nepal',
                    ),
                    const SizedBox(height: 10),
                    const Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        ' Email Address',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    CustomTextormfield2(
                      hintTextFormField: 'northline.himalaya@gmail.com',
                    ),
                  ],
                ),
                const SizedBox(height: 100),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 1, 75, 145),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: const Center(
                      child: Text(
                        ' Save',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 100),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class CustomTextormfield2 extends StatefulWidget {
  CustomTextormfield2({
    super.key,
    required this.hintTextFormField,
  });
  String hintTextFormField;

  @override
  State<CustomTextormfield2> createState() => Customtextformfield2State();
}

class Customtextformfield2State extends State<CustomTextormfield2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: const Color.fromARGB(255, 217, 249, 235),
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextFormField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: widget.hintTextFormField,
          hintStyle: const TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w500,
            color: Color.fromARGB(255, 1, 75, 145),
          ),
        ),
      ),
    );
  }
}
