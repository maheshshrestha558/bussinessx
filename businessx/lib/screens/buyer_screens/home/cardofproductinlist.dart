import 'package:flutter/material.dart';

class SubCategorypage1 extends StatefulWidget {
  const SubCategorypage1({super.key});

  @override
  State<SubCategorypage1> createState() => _HomeScreenPageState();
}

class _HomeScreenPageState extends State<SubCategorypage1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height / 3,
              child: ListView.builder(
                itemCount: 10,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(9, 2, 0, 2),
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2.15,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                      ),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                height: MediaQuery.of(context).size.width / 2,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                  ),
                                  color: Colors.grey.shade300,
                                ),
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                  ),
                                  child: Image.asset(
                                    'assets/images/ah.jpg',
                                    fit: BoxFit.fitHeight,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: 30,
                                  width: 80,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color:
                                        const Color.fromARGB(255, 1, 75, 145),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      '60% Off',
                                      style: TextStyle(
                                        fontFamily: 'Rubik',
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontStyle: FontStyle.normal,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 6,
                                right: 1,
                                child: GestureDetector(
                                  onTap: () {},
                                  child: const CircleAvatar(
                                    backgroundColor: Colors.red,
                                    maxRadius: 20,
                                    child: Icon(
                                      Icons.favorite_border_outlined,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Divider(
                            height: 5,
                            thickness: 5,
                            color: Colors.grey.shade200,
                          ),
                          const Row(
                            children: [
                              Flexible(
                                flex: 2,
                                child: Text(
                                  'Crops field in man harbasting the crops',
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Color.fromARGB(255, 1, 75, 145),
                                    fontStyle: FontStyle.normal,
                                  ),
                                ),
                              ),
                              CircleAvatar(
                                backgroundColor:
                                    Color.fromARGB(255, 1, 75, 145),
                                maxRadius: 20,
                                child: Icon(
                                  Icons.shopping_cart_outlined,
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Row(
                                      children: [
                                        Text(
                                          '4.5',
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 4,
                                        ),
                                        Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                          size: 20,
                                        ),
                                        Text(
                                          '',
                                          style: TextStyle(
                                            fontSize: 12,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text(
                                          'Rs980000',
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontStyle: FontStyle.normal,
                                            color: Colors.redAccent,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 4,
                                        ),
                                        Expanded(
                                          child: Text(
                                            'Rs980000',
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              decoration:
                                                  TextDecoration.lineThrough,
                                              decorationColor: Colors.red,
                                              fontSize: 14,
                                              fontStyle: FontStyle.normal,
                                              color: Colors.grey.shade500,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 40,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color:
                                      const Color.fromARGB(255, 100, 177, 77),
                                ),
                                child: const Center(
                                  child: Text(
                                    'Buy',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontStyle: FontStyle.normal,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}


//  Card(
//         clipBehavior: Clip.antiAlias,
//         color: Colors.white,
//         child: Padding(
//           padding: EdgeInsets.all(
//             MediaQuery.of(context).size.width * 0.003,
//           ),
//           child: Container(
//             margin: const EdgeInsets.all(0),
//             decoration: BoxDecoration(
//               boxShadow: const [
//                 BoxShadow(
//                   blurStyle: BlurStyle.outer,
//                   color: Color.fromARGB(255, 166, 236, 202),

//                   offset: Offset(0, 3), //Offset
//                   blurRadius: 5.0,
//                   spreadRadius: 1.0,
//                 ),
//                 BoxShadow(
//                   // blurStyle: BlurStyle.outer,
//                   color: Color.fromARGB(255, 255, 255, 255),
//                   offset: Offset(0, 0), //Offset
//                   blurRadius: 0.0,
//                   spreadRadius: 0.0,
//                 ),
//               ],
//               borderRadius: BorderRadius.circular(20),
//             ),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Hero(
//                   tag: '${product.id}-$heroType',
//                   child: Flexible(
//                     flex: 6,
//                     child: SizedBox(
//                       width: MediaQuery.of(context).size.width,
//                       height: MediaQuery.of(context).size.width / 2,
//                       child: ClipRRect(
//                         borderRadius: const BorderRadius.only(
//                           topRight: Radius.circular(20),
//                           topLeft: Radius.circular(20),
//                         ),
//                         child: Image.network(
//                           product.imageThumbnail,
//                           fit: BoxFit.cover,
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 Flexible(
//                   flex: 3,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: [
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Expanded(
//                             child: Text(
//                               product.name,
//                               maxLines: 2,
//                               style: const TextStyle(
//                                 fontSize: 16,
//                                 fontStyle: FontStyle.normal,
//                                 fontWeight: FontWeight.w500,
//                                 color: Colors.blue,
//                               ),
//                             ),
//                           ),
//                           const Row(
//                             children: [
//                               Text(
//                                 '4.5',
//                                 style: TextStyle(
//                                   fontSize: 14,
//                                   fontStyle: FontStyle.normal,
//                                   fontWeight: FontWeight.w500,
//                                   color: Colors.blue,
//                                 ),
//                               ),
//                               SizedBox(
//                                 width: 2,
//                               ),
//                               Icon(size: 18, Icons.star, color: Colors.amber)
//                             ],
//                           ),
//                         ],
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.symmetric(horizontal: 05),
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: [
//                             Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 Text(
//                                   'Rs. ${product.price}',
//                                   style: const TextStyle(
//                                     fontSize: 14,
//                                     fontStyle: FontStyle.normal,
//                                     fontWeight: FontWeight.w500,
//                                     color: Colors.redAccent,
//                                   ),
//                                 ),
//                                 const SizedBox(
//                                   width: 5,
//                                 ),
//                                 const Text(
//                                   'Rs 9000',
//                                   style: TextStyle(
//                                     decoration: TextDecoration.lineThrough,
//                                     decorationColor: Colors.redAccent,
//                                     fontSize: 14,
//                                     fontStyle: FontStyle.normal,
//                                     fontWeight: FontWeight.w400,
//                                     color: Colors.grey,
//                                   ),
//                                 ),
//                               ],
//                             ),
//                             Row(
//                               children: [
//                                 SizedBox(
//                                   width: 20,
//                                   child: GestureDetector(
//                                     child: const Icon(
//                                       Icons.favorite_border,
//                                       color: Colors.redAccent,
//                                       size: 20,
//                                     ),
//                                   ),
//                                 ),
//                                 const SizedBox(
//                                   width: 10,
//                                 ),
//                                 SizedBox(
//                                   width: 20,
//                                   child: GestureDetector(
//                                     child: const Icon(
//                                       Icons.shopping_cart,
//                                       color: Colors.redAccent,
//                                       size: 20,
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
   
   