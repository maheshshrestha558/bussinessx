import 'package:b2b_market/bloc/B2C/b2c_banner_bloc.dart';
import 'package:b2b_market/bloc/bloc.dart';
import 'package:b2b_market/common/shimmer_widget.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/common/widgets/shimmer_widgets/circular_category_shimmer.dart';
import 'package:b2b_market/models/brand.dart';
import 'package:b2b_market/models/product.dart';
import 'package:b2b_market/models/response/brand/all_brands_response.dart';
import 'package:b2b_market/models/response/product/popular_product_response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/category_section.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:b2b_market/screens/home/components/components.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../b2c_banners.dart';

class BuyerHomeScreen extends StatefulWidget {
  const BuyerHomeScreen({Key? key}) : super(key: key);

  @override
  State<BuyerHomeScreen> createState() => _BuyerHomeScreenState();
}

class _BuyerHomeScreenState extends State<BuyerHomeScreen> {
  final controller = ScrollController();

  int itemCount = 2;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      if (controller.position.pixels == controller.position.maxScrollExtent) {
        setState(() {
          itemCount += 2;
        });
      }
    });
    productBloc.fetchNewArrivalProducts();
    productBloc.fetchFeaturedProducts();
    productBloc.fetchAllProducts();
    productBloc.fetchSuggestedProducts();
    categoryBloc.getRootCategories();
    b2cBrandBloc.fetchAll();
    bannerBloc.getB2CBanner();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: const Color.fromARGB(255, 172, 247, 222),
      backgroundColor: Colors.white,
      appBar: const CustomAppBar(),
      body: SafeArea(
        child: SingleChildScrollView(
          controller: controller,
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 6),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'All Categories',
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 16,
                  fontFamily: 'Rubik',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              const CategoriesSection(),
              const SizedBox(
                height: 10,
              ),
              const B2CBanners(),
              const SizedBox(
                height: 5,
              ),
              const B2CSingleBanners(),
              const SuggestionSection(),
              const BrandsSection(),
              const FeaturedSection(),
              const NewArrivalsSection(),
              JustForYouSection(
                itemCount: itemCount,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class JustForYouSection extends StatelessWidget {
  final int itemCount;
  const JustForYouSection({Key? key, required this.itemCount})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromARGB(0, 200, 243, 223),
      child: Column(
        children: [
          const ListTile(
            dense: true,
            title: Text(
              'Products For You',
              style: TextStyle(
                fontSize: 16,
                color: Color.fromARGB(255, 1, 75, 145),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w600,
              ),
            ),
            subtitle: Text(
              'See what\'s new in the market',
              style: TextStyle(
                fontSize: 14,
                color: Color.fromARGB(255, 100, 177, 77),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 3),
            color: const Color.fromARGB(255, 229, 234, 250),
            child: StreamBuilder<List<Product>>(
              stream: productBloc.allProducts,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var allProducts = snapshot.data!;
                  if (allProducts.isNotEmpty) {
                    return GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 05,
                        childAspectRatio: 0.65,
                        // mainAxisExtent:
                        //     // MediaQuery.of(context).size.height / 2.9,

                        // MediaQuery.of(context).size.width / 1.25,
                      ),
                      itemCount: allProducts.length < itemCount
                          ? allProducts.length
                          : itemCount,
                      itemBuilder: (context, index) {
                        var product = allProducts[index];

                        return ProductForYou(
                          product: product,
                          heroType: 'all',
                        );
                      },
                    );
                  }
                }
                return Center(
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 10,
                      mainAxisExtent: 360,
                    ),
                    itemCount: 6,
                    itemBuilder: (context, index) => const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [B2CProductShimmer()],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class ProductSuggestCardB2C extends StatelessWidget {
  const ProductSuggestCardB2C({
    Key? key,
    required this.product,
    this.imgHeight = 180,
    required this.heroType,
  }) : super(key: key);

  final double imgHeight;
  final String heroType;
  final Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // height: imgHeight,
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Card(
        clipBehavior: Clip.antiAlias,
        color: const Color.fromARGB(255, 224, 253, 239),
        child: Padding(
          padding: EdgeInsets.all(
            MediaQuery.of(context).size.width * 0.003,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: EdgeInsets.all(
                  MediaQuery.of(context).size.width * 0.003,
                ),
                child: Hero(
                  tag: '${product.id}-$heroType',
                  child: CachedNetworkImage(
                    // fit: BoxFit.fitHeight,
                    placeholder: (context, _) => B2CErrorAndPlaceholder(
                      product.name,
                    ),
                    imageUrl: product.imageThumbnail,
                    imageBuilder: (context, imageProvider) => Container(
                      height: imgHeight,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(4),
                          topRight: Radius.circular(4),
                        ),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => ErrorAndPlaceholder(
                      product.name,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.002,
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.010,
                ),
                child: Text(
                  product.name,
                  style: TextStyle(
                    fontSize: MediaQuery.of(context).size.height * 0.015,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.011,
                ),
                child: Text(
                  'Rs. ${product.price}',
                  style: TextStyle(
                    fontSize: MediaQuery.of(context).size.height * 0.015,
                    color: const Color.fromARGB(
                      255,
                      228,
                      42,
                      144,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class B2CProductShimmer extends StatelessWidget {
  const B2CProductShimmer({
    Key? key,
    this.imgHeight = 180,
    this.imgWidth = 180,
  }) : super(key: key);
  final double imgHeight;
  final double imgWidth;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: imgHeight,
              width: imgWidth,
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(left: 8, top: 2),
          child: Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 20,
              width: 150,
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(left: 8, top: 5),
          child: Align(
            alignment: Alignment.centerLeft,
            child: ShimmerWidget.rectangular(
              height: 15,
              width: 100,
            ),
          ),
        ),
      ],
    );
  }
}

class ProductFeaturedCardB2C extends StatelessWidget {
  const ProductFeaturedCardB2C({
    Key? key,
    required this.product,
  }) : super(key: key);
  final String heroType = 'featured';
  final Product product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Card(
        elevation: 3,
        child: Container(
          width: MediaQuery.of(context).size.width / 2.55 - 15,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2.65,
                    height: MediaQuery.of(context).size.width / 2.65,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      color: Colors.grey.shade300,
                    ),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child: Image.network(
                        product.imageThumbnail,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: true,
                    child: Positioned(
                      top: 5,
                      left: 5,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 5,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color.fromARGB(255, 1, 75, 145),
                        ),
                        child: const Center(
                          child: Text(
                            '60% Off',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: false,
                    child: Positioned(
                      bottom: 6,
                      right: 5,
                      child: GestureDetector(
                        onTap: () {},
                        child: const CircleAvatar(
                          backgroundColor: Colors.red,
                          maxRadius: 15,
                          child: Icon(
                            Icons.favorite_border_outlined,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Divider(
                height: 2,
                thickness: 5,
                color: Colors.grey.shade200,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            product.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                          const SizedBox(height: 5),
                          const Row(
                            children: [
                              Text(
                                '4/5',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Icon(
                                Icons.star,
                                color: Colors.amber,
                                size: 20,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const CircleAvatar(
                      backgroundColor: Color.fromARGB(255, 1, 75, 145),
                      maxRadius: 18,
                      child: Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.white,
                        size: 15,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Rs. ${product.price}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Visibility(
                            visible: true,
                            child: Text(
                              'Rs. ${product.price}',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                decoration: TextDecoration.lineThrough,
                                decorationColor: Colors.redAccent,
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                color: Colors.grey,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Center(
                        child: Container(
                          height: 25,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: const Color.fromARGB(255, 100, 177, 77),
                          ),
                          child: const Center(
                            child: Text(
                              'Buy',
                              style: TextStyle(
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class NewArrivalsSection extends StatelessWidget {
  const NewArrivalsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        const ListTile(
          title: Text(
            'New Arrivals',
            style: TextStyle(
              fontSize: 16,
              color: Color.fromARGB(255, 1, 75, 145),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Text(
            'See what\'s new in the market',
            style: TextStyle(
              fontSize: 14,
              color: Color.fromARGB(255, 100, 177, 77),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Container(
          color: const Color.fromARGB(179, 123, 212, 247),
          width: double.infinity,
          height: MediaQuery.of(context).size.height / 1.8,
          // 500,
          child: StreamBuilder<List<Product>>(
            stream: productBloc.newArrivalProducts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var newArrivalProducts = snapshot.data;
                if (newArrivalProducts != null &&
                    newArrivalProducts.isNotEmpty) {
                  return Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: GridView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 2,
                        mainAxisSpacing: 0,
                        childAspectRatio: 1.5,
                        // mainAxisExtent:
                        //     MediaQuery.of(context).size.width / 2.57,
                      ),
                      itemCount: newArrivalProducts.length,
                      itemBuilder: (context, index) => ProductCardB2C(
                        product: newArrivalProducts[index],
                        heroType: 'new',
                      ),
                    ),
                  );
                }
                return Center(
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 0,
                      mainAxisExtent: 231,
                    ),
                    itemCount: 6,
                    itemBuilder: (context, index) => const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [B2CProductShimmer()],
                    ),
                  ),
                );
              }
              if (snapshot.hasError) {
                return Center(
                  child: Text('${snapshot.error}'),
                );
              }
              return const Center(
                child: CircularProgressIndicator(
                  color: Color(0xff29ed92),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}

class FeaturedSection extends StatefulWidget {
  const FeaturedSection({
    Key? key,
  }) : super(key: key);

  @override
  State<FeaturedSection> createState() => _FeaturedSectionState();
}

class _FeaturedSectionState extends State<FeaturedSection>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    )..repeat(reverse: true);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          dense: true,
          title: const Text(
            'Featured Product ',
            style: TextStyle(
              fontSize: 16,
              color: Color.fromARGB(255, 1, 75, 145),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w500,
            ),
          ),
          subtitle: const Text(
            'Only for You',
            style: TextStyle(
              fontSize: 14,
              color: Color.fromARGB(255, 100, 177, 77),
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w500,
            ),
          ),
          trailing: GestureDetector(
            onTap: () async {
              showCustomProgressDialog(context, 'Loading...');
              var response = productBloc.suggestedProducts;
              Navigator.of(context).pop();
              if (response.hasValue) {
                Navigator.pushNamed(
                  context,
                  RouteName.buyerfeatureScreen,
                );
              } else {
                ShowToast.error(
                  ' response.error',
                );
              }
            },
            child: const Icon(Icons.arrow_forward_ios),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        const Products(),
      ],
    );
  }
}

class Products extends StatelessWidget {
  const Products({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Product>>(
      stream: productBloc.featuredProducts,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var newArrivalProducts = snapshot.data;
          if (newArrivalProducts != null && newArrivalProducts.isNotEmpty) {
            return Container(
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 223, 250, 239),
              ),
              height: MediaQuery.of(context).size.width / 1.53,
              padding: const EdgeInsets.all(5),
              child: ListView.builder(
                itemCount: newArrivalProducts.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return ProductFeaturedCardB2C(
                    product: newArrivalProducts[index],
                  );
                },
              ),
            );
          }
          return Expanded(
            child: ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(width: 10),
              itemCount: 10,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return const SizedBox(
                  height: 200,
                  width: 160,
                  child: B2CProductShimmer(
                    imgHeight: 139,
                    imgWidth: 160,
                  ),
                );
              },
            ),
          );
        }

        return const Center(
          child: Text('No Featured Found'),
        );
      },
    );
  }
}

class BrandsSection extends StatelessWidget {
  const BrandsSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        ListTile(
          dense: true,
          title: Text(
            'Shop by Brand',
            style: TextStyle(
              fontSize: 16,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w500,
              color: Colors.blueAccent,
            ),
          ),
          subtitle: Text(
            'Shop through your favourite brands',
            style: TextStyle(
              fontSize: 12,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w500,
              color: Color.fromARGB(255, 100, 177, 77),
            ),
          ),
        ),
        Brands(),
      ],
    );
  }
}

class Brands extends StatelessWidget {
  const Brands({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0),
      child: Container(
        color: const Color(0x42BBCAF3),
        height: 80,
        child: StreamBuilder<AllBrandsResponse>(
          stream: b2cBrandBloc.allBrandsResponse.stream,
          builder: (context, snapshot) {
            var brands = snapshot.data?.brands;
            if (brands != null && brands.isNotEmpty) {
              return ListView.separated(
                separatorBuilder: (context, index) => const SizedBox(width: 15),
                itemCount: brands.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  var brand = brands[index];
                  return InkWell(
                    onTap: () async {
                      Feedback.forTap(context);
                      showCustomProgressDialog(context, 'Loading...');
                      var response = await b2cBrandBloc.fetch(brand.id);
                      Navigator.of(context).pop();
                      if (response.error == null) {}
                      if (response.error != null) {
                        ShowToast.error(
                          response.error,
                        );
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15),
                      child: BrandItem(
                        brand: brand,
                      ),
                    ),
                  );
                },
              );
            }
            return const BrandShimmer();
          },
        ),
      ),
    );
  }
}

class BrandItem extends StatelessWidget {
  final Brand brand;
  const BrandItem({Key? key, required this.brand}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      minRadius: 22,
      backgroundImage: NetworkImage(
        brand.imageThumbnail,
      ),
    );
  }
}

class SuggestionSection extends StatelessWidget {
  const SuggestionSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ColoredBox(
          // color: const Color(0x2629DE92),
          color: Colors.white,
          child: ListTile(
            dense: true,
            title: const Text(
              'Suggested for You',
              style: TextStyle(
                fontSize: 16,
                color: Colors.blueAccent,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
              ),
            ),
            subtitle: const Text(
              'Based on your searches',
              style: TextStyle(
                fontSize: 14,
                color: Color.fromARGB(255, 100, 177, 77),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w500,
              ),
            ),
            trailing: GestureDetector(
              onTap: () async {
                showCustomProgressDialog(context, 'Loading...');
                var response = productBloc.suggestedProducts;
                Navigator.of(context).pop();
                if (response.hasValue) {
                  Navigator.pushNamed(
                    context,
                    RouteName.buyerSuggestionScreen,
                  );
                } else {
                  ShowToast.error(
                    ' response.error',
                  );
                }
              },
              child: const Icon(Icons.arrow_forward_ios),
            ),
          ),
        ),
        Container(
          color: const Color.fromARGB(255, 223, 250, 239),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: StreamBuilder<List<Product>>(
              stream: productBloc.featuredProducts,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var newArrivalProducts = snapshot.data;
                  if (newArrivalProducts != null &&
                      newArrivalProducts.isNotEmpty) {
                    return SizedBox(
                      width: double.infinity,
                      child: Row(
                        children: [
                          Flexible(
                            flex: 7,
                            child: MainProductCard(
                              product: newArrivalProducts[1],
                            ),
                          ),
                          Flexible(
                            flex: 4,
                            child: Column(
                              children: [
                                SubProductCard(
                                  product: newArrivalProducts[1],
                                ),
                                SubProductCard(
                                  product: newArrivalProducts[1],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      // child: Stack(
                      //   children: [
                      //     Positioned(
                      //       // left: 1,
                      // child: MainProductCard(
                      //   product: newArrivalProducts[1],
                      // ),
                      //     ),
                      //     Positioned(
                      //       right: 0,
                      //       top: 0,
                      //       child: SubProductCard(
                      //         product: newArrivalProducts[2],
                      //       ),
                      //     ),
                      //     Positioned(
                      //       bottom: 0,
                      //       right: 0,
                      //       child: SubProductCard(
                      //         product: newArrivalProducts[0],
                      //       ),
                      //     ),
                      //   ],
                      // ),
                    );

                    // return SizedBox(
                    //   height: MediaQuery.of(context).size.height / 2.3,
                    //   child: SingleChildScrollView(
                    //     physics: const NeverScrollableScrollPhysics(),
                    //     child: StaggeredGrid.count(
                    //       crossAxisCount: 8,
                    //       crossAxisSpacing: 0,
                    //       mainAxisSpacing: 0,
                    //       children: const [
                    //         StaggeredGridTile.count(
                    //           crossAxisCellCount: 5,
                    //           mainAxisCellCount: 7,
                    //           child: MainProductCard(),
                    //         ),
                    //         StaggeredGridTile.count(
                    //           crossAxisCellCount: 3,
                    //           mainAxisCellCount: 3.5,
                    //           child: SubProductCard(),
                    //         ),
                    //         StaggeredGridTile.count(
                    //           crossAxisCellCount: 3,
                    //           mainAxisCellCount: 3.5,
                    //           child: SubProductCard(),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // );
                  }
                }
                return Container(
                  padding: const EdgeInsets.only(right: 5),
                  child: StaggeredGrid.count(
                    crossAxisCount: 7,
                    // mainAxisSpacing: 5,
                    // crossAxisSpacing: 1,
                    children: [
                      StaggeredGridTile.count(
                        crossAxisCellCount: 5,
                        mainAxisCellCount: 6,
                        child: B2CProductShimmer(
                          imgHeight: MediaQuery.of(context).size.height * 0.28,
                          imgWidth: 280,
                        ),
                      ),
                      const StaggeredGridTile.count(
                        crossAxisCellCount: 2,
                        mainAxisCellCount: 3,
                        child: B2CProductShimmer(
                          imgHeight: 100,
                          imgWidth: 280,
                        ),
                      ),
                      const StaggeredGridTile.count(
                        crossAxisCellCount: 2,
                        mainAxisCellCount: 3,
                        child: B2CProductShimmer(
                          imgHeight: 100,
                          imgWidth: 280,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}

class ServicesSection extends StatelessWidget {
  const ServicesSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color.fromARGB(255, 248, 229, 229),
      height: MediaQuery.of(context).size.height * 0.22,
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              height: MediaQuery.of(context).size.height * 0.24,
              width: MediaQuery.of(context).size.width * 0.44,
              child:
                  Image.asset('assets/images/icon-22.jpg', fit: BoxFit.cover),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.093,
                    width: MediaQuery.of(context).size.width * 0.48,
                    child: Image.asset(
                      'assets/images/icon-19.jpg',
                      fit: BoxFit.fill,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.093,
                    width: MediaQuery.of(context).size.width * 0.48,
                    child: Image.asset(
                      'assets/images/icon-23.jpg',
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// class BannerSection extends StatelessWidget {
//   const BannerSection({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 5),
//         child: SizedBox(
//           child: Image.asset('assets/images/icon-22.jpg', fit: BoxFit.fill),
//         ),
//       ),
//     );
//   }
// }

class ShowCaseBanner extends StatelessWidget {
  const ShowCaseBanner({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: SizedBox(
        width: double.infinity,
        height: MediaQuery.of(context).size.height * 0.12,
        child: Image.asset('assets/images/icon-20.jpg', fit: BoxFit.fill),
      ),
    );
  }
}

class CategoryShimmer extends StatelessWidget {
  const CategoryShimmer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) => const SizedBox(width: 5),
      padding: const EdgeInsets.all(2),
      itemCount: 10,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) => const Column(
        children: [
          ShimmerWidget.circular(
            height: 42,
            width: 42,
          ),
          SizedBox(
            height: 5,
          ),
          ShimmerWidget.rectangular(
            height: 15,
            width: 60,
          )
        ],
      ),
    );
  }
}

class MainProductCard extends StatelessWidget {
  const MainProductCard({
    super.key,
    required this.product,
  });
  final String heroType = 'featured';
  final Product product;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Card(
        elevation: 3,
        child: Container(
          // height: MediaQuery.of(context).size.height / 2.4,
          // width: MediaQuery.of(context).size.width / 1.7,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              10,
            ),
            // color: const Color.fromARGB(255, 241, 118, 118),
          ),
          child: Column(
            children: [
              Stack(
                children: [
                  SizedBox(
                    height: 240,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child: Image.network(
                        product.imageThumbnail,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    left: 5,
                    top: 5,
                    child: Visibility(
                      visible: true,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 5,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color.fromARGB(255, 1, 75, 145),
                        ),
                        child: const Center(
                          child: Text(
                            '60% Off',
                            style: TextStyle(
                              fontFamily: 'Rubik',
                              fontSize: 12,
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 6,
                    right: 5,
                    child: GestureDetector(
                      onTap: () {},
                      child: const CircleAvatar(
                        backgroundColor: Colors.red,
                        maxRadius: 20,
                        child: Icon(
                          Icons.favorite_border_outlined,
                          color: Colors.white,
                          size: 15,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(
                height: 5,
                thickness: 5,
                color: Colors.grey.shade200,
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 2,
                          child: Text(
                            product.name,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: const CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 1, 75, 145),
                            maxRadius: 20,
                            child: Icon(
                              Icons.shopping_cart_outlined,
                              color: Colors.white,
                              size: 15,
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Container(
                      height: 20,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: Colors.black,
                        ),
                      ),
                      child: const Text(
                        'Free Delivery',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12,
                          fontStyle: FontStyle.normal,
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const FittedBox(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                    size: 15,
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    '4.5/5',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  Text(
                                    '(120)',
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            FittedBox(
                              child: Text(
                                'Rs. ${product.price}',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 16,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.redAccent,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ],
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 28,
                            width: 60,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: const Color.fromARGB(
                                255,
                                100,
                                177,
                                77,
                              ),
                            ),
                            child: const Center(
                              child: Text(
                                'Buy',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SubProductCard extends StatelessWidget {
  const SubProductCard({
    super.key,
    required this.product,
  });
  final String heroType = 'featured';
  final Product product;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Feedback.forTap(context);
        showCustomProgressDialog(context, 'Loading...');
        B2CSingleProductResponse response =
            await productBloc.getB2CProduct(product.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerSingleProductsScreen,
            arguments: {
              'product': response.product,
              'heroType': heroType,
            },
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Card(
        elevation: 3,
        child: Container(
          // height: MediaQuery.of(context).size.height / 4.9,
          // width: MediaQuery.of(context).size.width / 3.1,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              10,
            ),
            color: Colors.white,
          ),
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                    ),
                    height: 110,
                    // width: 100,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      child: Image.network(
                        product.imageThumbnail,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 2,
                          child: Text(
                            product.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                        const CircleAvatar(
                          backgroundColor: Color.fromARGB(255, 1, 75, 145),
                          minRadius: 16,
                          child: Icon(
                            Icons.shopping_cart_outlined,
                            color: Colors.white,
                            size: 13,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 1,
                          child: Text(
                            'Rs. ${product.price}',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                              fontSize: 12,
                              fontStyle: FontStyle.normal,
                              color: Colors.redAccent,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 20,
                            width: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: const Color.fromARGB(
                                255,
                                100,
                                177,
                                77,
                              ),
                            ),
                            child: const Center(
                              child: Text(
                                'Buy',
                                style: TextStyle(
                                  fontSize: 16,
                                  fontStyle: FontStyle.normal,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
