import 'package:b2b_market/bloc/B2C/cart_bloc.dart';
import 'package:b2b_market/common/enums.dart';
import 'package:b2b_market/models/response/add_to_cart_response.dart';
import 'package:b2b_market/models/response/cart_response.dart';
import 'package:b2b_market/screens/buyer_screens/checkout_page.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/singlepages/single_product_screen.dart';
import 'package:b2b_market/screens/home/components/components.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../common/logger/logger.dart';
import '../../models/response/total_price_response.dart';

final _logger = getLogger(CartPage);

class CartPage extends StatefulWidget {
  const CartPage({
    Key? key,
  }) : super(key: key);

  static const String routeName = '/cart';

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  // bool isSelect = true;
  @override
  void initState() {
    super.initState();
    cartBloc.getCart();
  }

  @override
  void dispose() {
    cartBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBarForAllPage(
        navTitle: 'Cart',
        visible: true,
      ),
      body: const Customcart(),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: StreamBuilder<TotalPriceResponse>(
          stream: cartBloc.totalPriceResponse.stream,
          builder: (context, snapshot) {
            var data = snapshot.data;
            if (data == null && data?.error != null) {
              CustomButtomCartButton(
                // widget: widget,
                leftText: 'Total:Rs.0',
                rightText: 'CheckOut',
                onPressedLeft: () {},
                onPressedRight: () {
                  Navigator.of(context).pushNamed(CheckOutPage.routeName);
                },
              );
            }

            return CustomButtomCartButton(
              leftText: 'Total:Rs.${data?.totalPrice ?? 0}',
              rightText: 'CheckOut',
              onPressedLeft: () {},
              onPressedRight: () {
                Navigator.of(context).pushNamed(CheckOutPage.routeName);
              },
            );
          },
        ),
      ),
    );
  }
}

class BILLSUMMARY extends StatelessWidget {
  const BILLSUMMARY({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Color.fromARGB(62, 210, 232, 203),
            blurRadius: 2.0,
            spreadRadius: 1.0,
            offset: Offset(
              -0.5,
              2,
            ),
          )
        ],
      ),
      // body: Center(
      //   child: StreamBuilder<CartResponse>(
      //     stream: cartBloc.cartResponse,
      //     builder: (context, snapshot) {
      //       List<ProductData>? productDataList = snapshot.data?.data;
      //       if (productDataList == null) {
      //         return const CircularProgressIndicator();
      //       }
      //       return ListView.builder(
      //         itemCount: productDataList.length,
      //         itemBuilder: (context, index) {
      //           ProductData productData = productDataList[index];
      //           _logger.i(productData);
      //           return ProductListTile(
      //             key: ValueKey(productData.quantity),
      //             productData: productData,
      //           );
      //         },
      //       );
      //     },
      //   ),
      // ),
    );
  }
}

class Customcart extends StatefulWidget {
  const Customcart({
    super.key,
  });

  @override
  State<Customcart> createState() => _CustomcartState();
}

int iiiii = 2;

class _CustomcartState extends State<Customcart> {
  bool clicked = true;
  List count = List.generate(iiiii, (i) => 0);
  List<bool> onclick = List.generate(iiiii, (i) => false);

  makeFalse() {
    setState(() {
      for (var i = 0; i < iiiii; i++) {
        onclick[i] = false;
      }
    });
  }

  makeTrue() {
    setState(() {
      for (var i = 0; i < iiiii; i++) {
        onclick[i] = true;
      }
    });
  }

  void add(int index) {
    setState(() {
      count[index]++;
    });
  }

  void sub(int index) {
    setState(() {
      if (count[index] <= 0) {
        count[index] = 0;
      } else {
        count[index]--;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return Material(
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 10, 8, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            if (onclick.contains(true)) {
                              if (onclick.contains(false)) {
                                makeTrue();
                              } else if (!onclick.contains(false)) {
                                makeFalse();
                              }
                            } else {
                              makeTrue();
                            }
                          });
                        },
                        child: CircleAvatar(
                          minRadius: 12,
                          backgroundColor: onclick.contains(true)
                              ? Colors.orange
                              : const Color.fromARGB(255, 208, 240, 212),
                          child: const Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      const Expanded(
                        child: Text(
                          'Mama Earth Vitamin C ',
                          style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(255, 1, 75, 145),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          size: 20,
                          Icons.arrow_forward_ios,
                          color: Color.fromARGB(255, 1, 75, 145),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.grey.shade200,
                    height: 2,
                    thickness: 2,
                  ),
                  SizedBox(
                    child: ListView.separated(
                      separatorBuilder: (context, index) {
                        return Divider(
                          color: Colors.grey.shade300,
                          height: 2,
                          thickness: 2,
                        );
                      },
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: iiiii,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: MediaQuery.of(context).size.width / 4.7 + 16,
                          padding: const EdgeInsets.symmetric(vertical: 8),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    onclick[index] = !onclick[index];
                                  });
                                },
                                child: CircleAvatar(
                                  minRadius: 12,
                                  backgroundColor: onclick[index]
                                      ? Colors.orange
                                      : const Color.fromARGB(
                                          255,
                                          208,
                                          240,
                                          212,
                                        ),
                                  child: const Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: MediaQuery.of(context).size.width / 4.7,
                                width: MediaQuery.of(context).size.width / 4.7,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Image.asset(
                                    'assets/homeimage/1.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    const Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            'Mama Earth vitamin C',
                                            maxLines: 3,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          '2 item left',
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.red,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        ConstrainedBox(
                                          constraints: const BoxConstraints(
                                            minWidth: 50,
                                            maxWidth: 130,
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              const Expanded(
                                                child: FittedBox(
                                                  child: Text(
                                                    'Rs.950000',
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Colors.red,
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 4,
                                              ),
                                              Expanded(
                                                child: FittedBox(
                                                  child: Text(
                                                    'Rs.950000',
                                                    maxLines: 1,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                      decoration: TextDecoration
                                                          .lineThrough,
                                                      decorationColor:
                                                          Colors.red,
                                                      fontSize: 14,
                                                      color:
                                                          Colors.grey.shade400,
                                                      fontStyle:
                                                          FontStyle.normal,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          height: 20,
                                          width: 80,
                                          decoration: BoxDecoration(
                                            color: Colors.grey.shade200,
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              GestureDetector(
                                                onTap: () => sub(index),
                                                child: Container(
                                                  height: 20,
                                                  width: 20,
                                                  decoration:
                                                      const BoxDecoration(
                                                    color: Colors.grey,
                                                    borderRadius:
                                                        BorderRadius.horizontal(
                                                      left: Radius.circular(
                                                        20,
                                                      ),
                                                    ),
                                                  ),
                                                  child: const Icon(
                                                    Icons.remove,
                                                    color: Colors.white,
                                                    size: 15,
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                // ignore: unnecessary_brace_in_string_interps
                                                '${count[index]}',
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () => add(index),
                                                child: Container(
                                                  height: 20,
                                                  width: 20,
                                                  decoration:
                                                      const BoxDecoration(
                                                    color: Color(0x7564B14D),
                                                    borderRadius:
                                                        BorderRadius.horizontal(
                                                      right: Radius.circular(
                                                        20,
                                                      ),
                                                    ),
                                                  ),
                                                  child: const Icon(
                                                    Icons.add,
                                                    color: Colors.white,
                                                    size: 18,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class ProductListTile extends StatefulWidget {
  final ProductData productData;
  const ProductListTile({
    Key? key,
    required this.productData,
  }) : super(key: key);

  @override
  State<ProductListTile> createState() => _ProductListTileState();
}

class _ProductListTileState extends State<ProductListTile> {
  int quantity = 1;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    quantity = widget.productData.quantity;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Feedback.forTap(context);
      },
      child: Card(
        elevation: 0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: const BorderSide(color: Colors.white70, width: 1),
          borderRadius: BorderRadius.circular(0),
        ),
        child: SizedBox(
          height: 140,
          child: Row(
            children: [
              Checkbox(
                value: isSelected,
                onChanged: (value) {
                  setState(() {
                    isSelected = value!;
                    if (cartBloc.productIds
                        .contains(widget.productData.product.id)) {
                      cartBloc.productIds.remove(widget.productData.product.id);
                    } else {
                      cartBloc.productIds.add(widget.productData.product.id);
                    }
                    cartBloc.getTotalPrice(cartBloc.productIds);
                  });
                },
                activeColor: Colors.green,
                fillColor: MaterialStateProperty.resolveWith<Color>((states) {
                  if (states.contains(MaterialState.disabled)) {
                    return Colors.green.withOpacity(.32);
                  }
                  return Colors.green;
                }),
              ),
              Container(
                height: 80,
                width: 80,
                margin: const EdgeInsets.fromLTRB(13, 15, 0, 15),
                color: Colors.grey[200],
                child: ProductCachedNetworkImage(
                  product: widget.productData.product,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    top: 10.0,
                    bottom: 10.0,
                    right: 5,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        widget.productData.product.name,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        'Rs. ${widget.productData.product.price}',
                        style: GoogleFonts.rubik(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: const Color(0xffDC2626),
                        ),
                      ),
                      Text(
                        'Varient : -------',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(fontSize: 12),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              widget.productData.product.seller!.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodySmall!
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                          ),
                          // const Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: InkWell(
                              onTap: () async {
                                _logger.i(widget.productData.cartId);
                                if (quantity > 1) {
                                  AddToCartResponse response =
                                      await cartBloc.updateProductQuantity(
                                    widget.productData.cartId,
                                    quantity,
                                    ProductQuantityEvent.remove,
                                    context,
                                  );
                                  if (quantity > 1 && response.error == null) {
                                    quantity--;
                                    setState(() {});
                                  }
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(52),
                                  ),
                                ),
                                child: const Icon(Icons.remove, size: 18),
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.center,
                            width: 30,
                            height: 20,
                            child: Text(quantity.toString()),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: InkWell(
                              onTap: () async {
                                AddToCartResponse response =
                                    await cartBloc.updateProductQuantity(
                                  widget.productData.cartId,
                                  quantity,
                                  ProductQuantityEvent.add,
                                  context,
                                );
                                if (response.error == null) {
                                  quantity++;
                                  setState(() {});
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(52),
                                  ),
                                ),
                                child: const Icon(Icons.add, size: 18),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

// class ProductListTiles extends StatefulWidget {
//   final String productData;
//   const ProductListTiles({
//     Key? key,
//     required this.productData,
//   }) : super(key: key);

//   @override
//   State<ProductListTiles> createState() => _ProductListTilesState();
// }

// class _ProductListTilesState extends State<ProductListTiles> {
//   int quantity = 1;
//   bool isSelected = false;

//   @override
//   void initState() {
//     super.initState();
//     quantity = 0;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         Feedback.forTap(context);
//       },
//       child: Card(
//         elevation: 0,
//         color: Colors.white,
//         shape: RoundedRectangleBorder(
//           side: const BorderSide(color: Colors.white70, width: 1),
//           borderRadius: BorderRadius.circular(0),
//         ),
//         child: SizedBox(
//           height: 140,
//           child: Row(
//             children: [
//               Checkbox(
//                 value: isSelected,
//                 onChanged: (value) {
//                   setState(() {
//                     isSelected = value!;
//                     // if (cartBloc.productIds
//                     //     .contains(widget.productData.product.id)) {
//                     //   cartBloc.productIds.remove(widget.productData.product.id);
//                     // } else {
//                     //   cartBloc.productIds.add(widget.productData.product.id);
//                     // }
//                     // cartBloc.getTotalPrice(cartBloc.productIds);
//                   });
//                 },
//                 activeColor: Colors.green,
//                 fillColor: MaterialStateProperty.resolveWith<Color>((states) {
//                   if (states.contains(MaterialState.disabled)) {
//                     return Colors.green.withOpacity(.32);
//                   }
//                   return Colors.green;
//                 }),
//               ),
//               Container(
//                 height: 80,
//                 width: 80,
//                 margin: const EdgeInsets.fromLTRB(5, 15, 0, 15),
//                 color: Colors.grey[200],
//                 child: const SizedBox(height: 100, width: 100),
//               ),
//               Expanded(
//                 child: Padding(
//                   padding: const EdgeInsets.only(
//                     left: 15.0,
//                     top: 10.0,
//                     bottom: 10.0,
//                     right: 5,
//                   ),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     mainAxisAlignment: MainAxisAlignment.spaceAround,
//                     children: [
//                       const Text(
//                         ' widget. product .name test case',
//                         overflow: TextOverflow.ellipsis,
//                         maxLines: 1,
//                         style: TextStyle(
//                           fontSize: 16,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                       Text(
//                         'Rs. 126',
//                         style: GoogleFonts.rubik(
//                           fontSize: 14,
//                           fontWeight: FontWeight.w400,
//                           color: const Color(0xffDC2626),
//                         ),
//                       ),
//                       Text(
//                         'Varient : -------',
//                         style: Theme.of(context)
//                             .textTheme
//                             .headlineMedium!
//                             .copyWith(fontSize: 12),
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.start,
//                         children: [
//                           Expanded(
//                             child: Text(
//                               'Seller Name here T e s t ',
//                               maxLines: 1,
//                               overflow: TextOverflow.ellipsis,
//                               style: Theme.of(context)
//                                   .textTheme
//                                   .bodySmall!
//                                   .copyWith(fontWeight: FontWeight.normal),
//                             ),
//                           ),
//                           // const Spacer(),
//                           Padding(
//                             padding: const EdgeInsets.all(5.0),
//                             child: InkWell(
//                               onTap: () async {
//                                 // _logger.i(widget.productData.cartId);
//                                 // if (quantity > 1) {
//                                 //   AddToCartResponse response =
//                                 //       await cartBloc.updateProductQuantity(
//                                 //     widget.productData.cartId,
//                                 //     quantity,
//                                 //     ProductQuantityEvent.remove,
//                                 //     context,
//                                 //   );
//                                 //   if (quantity > 1 && response.error == null) {
//                                 //     quantity--;
//                                 //     setState(() {});
//                                 //   }
//                                 // }
//                               },
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                   border: Border.all(width: 1),
//                                   borderRadius: const BorderRadius.all(
//                                     Radius.circular(52),
//                                   ),
//                                 ),
//                                 child: const Icon(Icons.remove, size: 18),
//                               ),
//                             ),
//                           ),
//                           Container(
//                             alignment: Alignment.center,
//                             width: 30,
//                             height: 20,
//                             decoration: const BoxDecoration(
//                               color: Color.fromARGB(255, 223, 223, 223),
//                             ),
//                             child: Text(
//                               quantity.toString(),
//                               style: const TextStyle(
//                                 fontSize: 15,
//                               ),
//                             ),
//                           ),
//                           Container(
//                             padding: const EdgeInsets.all(5.0),
//                             child: InkWell(
//                               onTap: () async {
//                                 // AddToCartResponse response =
//                                 //     await cartBloc.updateProductQuantity(
//                                 //   widget.productData.cartId,
//                                 //   quantity,
//                                 //   ProductQuantityEvent.add,
//                                 //   context,
//                                 // );
//                                 // if (response.error == null) {
//                                 //   quantity++;
//                                 //   setState(() {});
//                                 // }
//                               },
//                               child: Container(
//                                 decoration: BoxDecoration(
//                                   border: Border.all(width: 1),
//                                   borderRadius: const BorderRadius.all(
//                                     Radius.circular(52),
//                                   ),
//                                 ),
//                                 child: const Icon(Icons.add, size: 18),
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
