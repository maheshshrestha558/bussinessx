import 'package:b2b_market/bloc/banner_bloc.dart';
import 'package:b2b_market/bloc/category_bloc.dart';
import 'package:b2b_market/bloc/popular_product_bloc.dart';
import 'package:b2b_market/bloc/product_bloc.dart';
import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:b2b_market/models/product.dart';
import 'package:b2b_market/models/response/category/root_categories_response.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ShopdetailsScreen extends StatefulWidget {
  const ShopdetailsScreen({super.key});

  @override
  State<ShopdetailsScreen> createState() => _ShopdetailsScreenState();
}

class _ShopdetailsScreenState extends State<ShopdetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: const Icon(Icons.arrow_back_ios, color: Colors.blue),
        title: Row(
          children: [
            Container(
              height: 50,
              width: 50,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
              ),
              child: Image.asset('assets/homeimage/pasal.png'),
            ),
            const SizedBox(
              width: 10,
            ),
            const Text(
              'Maila Dai Ko Pasal',
              style: TextStyle(
                fontSize: 16,
                color: Color.fromARGB(240, 1, 75, 145),
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        actions: [
          Theme(
            data: Theme.of(context).copyWith(
              textTheme: const TextTheme().apply(bodyColor: Colors.black),
              dividerColor: Colors.white,
              iconTheme: const IconThemeData(color: Colors.white),
            ),
            child: PopupMenuButton<int>(
              color: Colors.black,
              itemBuilder: (context) => [
                const PopupMenuItem<int>(value: 0, child: Text('Setting')),
                const PopupMenuItem<int>(
                  value: 1,
                  child: Text('Privacy Policy page'),
                ),
                const PopupMenuDivider(),
                const PopupMenuItem<int>(
                  value: 2,
                  child: Row(
                    children: [
                      Icon(
                        Icons.logout,
                        color: Colors.red,
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      Text('Logout')
                    ],
                  ),
                ),
              ],
              onSelected: (item) => SelectedItem(context, item),
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 14.0),
            child: SizedBox(
              height: 140,
              child: Image.asset(
                'assets/homeimage/R.jpg',
                fit: BoxFit.fill,
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox(
                      height: 30,
                      width: 30,
                      child: Image.asset(
                        'assets/homeimage/loc.png',
                        fit: BoxFit.fill,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Kalanki Chok,Kathmandu.',
                          style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(240, 1, 75, 145),
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Ratings(
                          number: 4,
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: const Color.fromARGB(255, 1, 75, 145),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 5,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/person.png'),
                        const Padding(
                          padding: EdgeInsets.only(left: 4),
                          child: Text(
                            '1200+',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Text(
              'Category List',
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Color.fromARGB(244, 1, 75, 145),
                fontSize: 16,
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
            child: StoreProductCategory(),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
            child: Text(
              'Featured List',
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Color.fromARGB(244, 1, 75, 145),
                fontSize: 16,
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
            child: Shopbanners(),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Text(
              'Hot Deals',
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Color.fromARGB(244, 1, 75, 145),
                fontSize: 16,
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0.0),
            child: ShopProducts(),
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Text(
              'Products Only For You',
              textAlign: TextAlign.start,
              style: TextStyle(
                color: Color.fromARGB(244, 1, 75, 145),
                fontSize: 16,
                fontFamily: 'Rubik',
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: StreamBuilder<List<Product>>(
              stream: productBloc.allProducts,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var allProducts = snapshot.data!;
                  if (allProducts.isNotEmpty) {
                    return GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 0,
                        mainAxisSpacing: 05,
                        childAspectRatio: 0.65,
                        // mainAxisExtent:
                        //     // MediaQuery.of(context).size.height / 2.9,

                        // MediaQuery.of(context).size.width / 1.25,
                      ),
                      itemCount: allProducts.length,
                      itemBuilder: (context, index) {
                        var product = allProducts[index];

                        return ProductForYou(
                          product: product,
                          heroType: 'all',
                        );
                      },
                    );
                  }
                }
                return Center(
                  child: GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 10,
                      mainAxisExtent: 360,
                    ),
                    itemCount: 6,
                    itemBuilder: (context, index) => const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [B2CProductShimmer()],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void SelectedItem(BuildContext context, item) {
    switch (item) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Container()));
        break;
      case 1:
        print('Privacy Clicked');
        break;
      case 2:
        print('User Logged out');
        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Container()),
          (route) => false,
        );
        break;
    }
  }
}

// ignore: must_be_immutable
class Ratings extends StatefulWidget {
  Ratings({
    super.key,
    required this.number,
  });
  int number;
  @override
  State<Ratings> createState() => RatingsState();
}

class RatingsState extends State<Ratings> {
  @override
  Widget build(BuildContext context) {
    return Row(
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Text(
          'Rating:',
          style: TextStyle(
            fontSize: 16,
            color: Color.fromARGB(240, 1, 75, 145),
            fontWeight: FontWeight.w400,
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Text(
                '${widget.number} /5',
                style: const TextStyle(
                  fontSize: 14,
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(
              height: 20,
              child: Center(
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: widget.number,
                  itemBuilder: (context, index) {
                    return const Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.star,
                          color: Color.fromARGB(255, 250, 179, 0),
                          size: 18,
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

class StoreProductCategory extends StatefulWidget {
  const StoreProductCategory({Key? key}) : super(key: key);

  @override
  State<StoreProductCategory> createState() => _CategoriesSectionState();
}

class _CategoriesSectionState extends State<StoreProductCategory> {
  final _scrollController = ScrollController();

  int index = 0;
  @override
  void initState() {
    super.initState();

    index = 0;

    _scrollController.addListener(() {});

    popularProductBloc.getPopularProduct();
  }

  getForward(int index) {
    _scrollController.position.pixels;
    Future.delayed(const Duration(milliseconds: 50), () {}).then((s) {
      _scrollController.animateTo(
        index * 80.5 * 4,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  getReverse(int index) {
    _scrollController.position.pixels;
    Future.delayed(const Duration(milliseconds: 50), () {}).then((s) {
      _scrollController.animateTo(
        index * 80.5 * 4,
        duration: const Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<RootCategoriesResponse>(
      stream: categoryBloc.rootCategories.stream,
      builder: (context, snapshot) {
        var rootCategories = snapshot.data?.rootCategories?.rootCategory;
        if (rootCategories != null && rootCategories.isNotEmpty) {
          return SizedBox(
            height: 55,
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification notification) {
                if (notification is UserScrollNotification) {
                  if (notification.direction == ScrollDirection.forward) {
                    if (index > 0) {
                      setState(() {
                        index--;
                        getForward(index);
                      });
                    } else {}
                  } else if (notification.direction ==
                      ScrollDirection.reverse) {
                    var value = rootCategories.length / 4 - 2;

                    if (index > value) {
                    } else {
                      setState(() {
                        index++;
                        getReverse(index);
                      });
                    }
                  }
                }

                return true;
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: ListView.separated(
                  separatorBuilder: (context, index) => Container(),
                  controller: _scrollController,
                  itemCount: rootCategories.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return UserCategoryItem(
                      rootCategory: rootCategories[index],
                    );
                  },
                ),
              ),
            ),
          );
        }

        return const SizedBox(height: 80, child: CategoryShimmer());
      },
    );
  }
}

class UserCategoryItem extends StatelessWidget {
  final RootCategory rootCategory;
  const UserCategoryItem({
    Key? key,
    required this.rootCategory,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        showCustomProgressDialog(context, 'Loading...');
        var response = await categoryBloc.getSubCategories(rootCategory.id);
        await bannerBloc.getB2CCategoryBanner(rootCategory.id);
        await productBloc.fetchCategoryWiseProducts(rootCategory.id);
        Navigator.of(context).pop();
        if (response.error == null) {
          Navigator.pushNamed(
            context,
            RouteName.buyerCategoryScreen,
            arguments: rootCategory,
          );
        } else {
          ShowToast.error(
            response.error,
          );
        }
      },
      child: Row(
        children: [
          SizedBox(
            width: 60,
            child: Column(
              children: [
                CircleAvatar(
                  radius: 25,
                  backgroundColor: Colors.white,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30),
                    child: Image.network(
                      rootCategory.iconImageThumbnail,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 3,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class Shopbanners extends StatelessWidget {
  const Shopbanners({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
      child: ListView.builder(
        shrinkWrap: true,
        // padding: const EdgeInsets.all(0),
        physics: const NeverScrollableScrollPhysics(),
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 3.2,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/calog.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: MediaQuery.of(context).size.width / 3.2,
                    width: MediaQuery.of(context).size.width / 1.7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.1, 2),
                          blurRadius: 5,
                          spreadRadius: 1,
                        )
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(
                        'assets/homeimage/cabig.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ShopProducts extends StatelessWidget {
  const ShopProducts({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Product>>(
      stream: productBloc.featuredProducts,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var newArrivalProducts = snapshot.data;
          if (newArrivalProducts != null && newArrivalProducts.isNotEmpty) {
            return Container(
              // decoration: const BoxDecoration(
              //   color: Color.fromARGB(255, 223, 250, 239),
              // ),
              height: MediaQuery.of(context).size.width / 1.53,
              padding: const EdgeInsets.all(0),
              child: ListView.builder(
                itemCount: newArrivalProducts.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return ProductFeaturedCardB2C(
                    product: newArrivalProducts[index],
                  );
                },
              ),
            );
          }
          return Expanded(
            child: ListView.separated(
              separatorBuilder: (context, index) => const SizedBox(width: 10),
              itemCount: 10,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return const SizedBox(
                  height: 200,
                  width: 160,
                  child: B2CProductShimmer(
                    imgHeight: 139,
                    imgWidth: 160,
                  ),
                );
              },
            ),
          );
        }

        return const Center(
          child: Text('No Featured Found'),
        );
      },
    );
  }
}
