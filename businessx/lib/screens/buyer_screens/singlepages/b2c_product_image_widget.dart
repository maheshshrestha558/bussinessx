import 'package:b2b_market/models/response/product/popular_product_response.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';

import '../../../common/shimmer_widget.dart';

class B2CProductImageWidget extends StatelessWidget {
  const B2CProductImageWidget({
    Key? key,
    required this.product,
    required this.heroType,
  }) : super(key: key);

  final SingleProduct product;
  final String heroType;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.width,
      child: CarouselSlider.builder(
        enableAutoSlider: false,
        unlimitedMode: false,
        itemCount: product.files!.length,
        slideBuilder: (index) {
          var imageThumbnail = product.files![index].path;
          return Hero(
            tag: '${product.id}-$heroType',
            child: ProductImageCarousel(
              filePath: imageThumbnail ?? '',
            ),
          );
        },
        slideIndicator: CircularSlideIndicator(
          indicatorBackgroundColor: Colors.grey,
          currentIndicatorColor: Colors.white,
          padding: const EdgeInsets.only(bottom: 18),
        ),
      ),
    );
  }
}

class ProductImageCarousel extends StatefulWidget {
  const ProductImageCarousel({
    Key? key,
    required this.filePath,
  }) : super(key: key);

  final String filePath;

  @override
  State<ProductImageCarousel> createState() => _ProductImageCarouselState();
}

class _ProductImageCarouselState extends State<ProductImageCarousel> {
  bool select = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          height: MediaQuery.of(context).size.width,
          width: MediaQuery.of(context).size.width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: CachedNetworkImage(
              imageUrl: widget.filePath,
              fit: BoxFit.cover,
              errorWidget: (context, url, error) {
                return const Align(
                  alignment: Alignment.centerLeft,
                  child: ShimmerWidget.rectangular(
                    height: 320,
                    width: 320,
                  ),
                );
              },
            ),
          ),
        ),
        Positioned(
          bottom: 10,
          right: 10,
          child: GestureDetector(
            onTap: () {
              setState(() {
                select = !select;
              });
            },
            child: CircleAvatar(
              backgroundColor: !select ? Colors.grey : Colors.red,
              child: const Icon(
                Icons.favorite_border_outlined,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
