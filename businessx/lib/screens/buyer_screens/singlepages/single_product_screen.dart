// ignore_for_file: must_be_immutable

import 'package:b2b_market/models/product.dart';
import 'package:b2b_market/route_names.dart';
import 'package:b2b_market/screens/buyer_screens/components/custom_b2c_navbar.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/components/product_card_b2c.dart';
import 'package:b2b_market/screens/buyer_screens/singlepages/b2c_product_image_widget.dart';
import 'package:flutter/material.dart';

import '../../../bloc/B2C/b2c_banner_bloc.dart';
import '../../../bloc/banner_bloc.dart';
import '../../../bloc/category_bloc.dart';
import '../../../bloc/product_bloc.dart';
import '../../../models/response/product/popular_product_response.dart';

class SingleProductScreen extends StatefulWidget {
  final String heroType;
  final SingleProduct product;
  const SingleProductScreen({
    Key? key,
    required this.heroType,
    required this.product,
  }) : super(key: key);

  @override
  State<SingleProductScreen> createState() => _SingleProductScreenState();
}

class _SingleProductScreenState extends State<SingleProductScreen> {
  bool favorite = false;
  final controller = ScrollController();

  int itemCount = 2;

  @override
  void initState() {
    super.initState();
    controller.addListener(() {
      if (controller.position.pixels == controller.position.maxScrollExtent) {
        setState(() {
          itemCount += 2;
        });
      }
    });
    productBloc.fetchNewArrivalProducts();
    productBloc.fetchFeaturedProducts();
    productBloc.fetchAllProducts();
    productBloc.fetchSuggestedProducts();
    categoryBloc.getRootCategories();
    b2cBrandBloc.fetchAll();
    bannerBloc.getB2CBanner();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarForAllPage(
        navTitle: widget.product.name,
        visible: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.width,
                    child: B2CProductImageWidget(
                      product: widget.product,
                      heroType: widget.heroType,
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 14.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                flex: 2,
                                child: Text(
                                  widget.product.description.toString(),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: const TextStyle(
                                    fontSize: 17,
                                    color: Color.fromARGB(255, 1, 75, 145),
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              Text(
                                'Rs. ${widget.product.price.toString()}',
                                style: const TextStyle(
                                  fontSize: 16,
                                  color: Color.fromARGB(255, 220, 38, 38),
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              const SizedBox(width: 5),
                              Text(
                                'Rs. ${widget.product.price.toString()}',
                                style: const TextStyle(
                                  decoration: TextDecoration.lineThrough,
                                  fontSize: 14,
                                  color: Color.fromARGB(255, 165, 156, 156),
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: const Color.fromARGB(255, 219, 236, 214),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 16,
                    ),
                    child: Text(
                      'Description:',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color.fromARGB(255, 1, 75, 145),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: const Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 16,
                    ),
                    child: Text(
                      'A product description is a written copy that provides essential information about a product or service being sold online. It typically includes details such as features, benefits, specifications, and usage instructions.',
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 16,
                        color: Color.fromARGB(255, 1, 75, 145),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            ProductSpecification(product: widget.product),
            Container(
              width: MediaQuery.of(context).size.width,
              color: const Color.fromARGB(255, 219, 236, 214),
              child: const Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 12,
                  horizontal: 16,
                ),
                child: Text(
                  'Rating & Review:(125+)',
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromARGB(255, 1, 75, 145),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            Rating(
              number: 3,
            ),
            SellerSection(product: widget.product),
            Similarproduct(
              itemCount: itemCount,
            ),
          ],
        ),
      ),
    );
  }
}

class CustomButtomCartButton extends StatelessWidget {
  const CustomButtomCartButton({
    Key? key,
    // required this.widget,
    required this.leftText,
    required this.rightText,
    this.onPressedLeft,
    this.onPressedRight,
  }) : super(key: key);

  final String leftText;
  final String rightText;
  final void Function()? onPressedLeft;
  final void Function()? onPressedRight;
  // final SingleProductScreen widget;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: IntrinsicHeight(
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.center,

          children: [
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(5),
                    bottomRight: Radius.circular(5),
                  ),
                  // color: Color(0xff29DE92),
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    tileMode: TileMode.clamp,
                    colors: [
                      Color.fromARGB(255, 41, 100, 190),
                      // Color(0xff1D4ED8),
                      Color.fromARGB(255, 41, 155, 190),
                    ],
                  ),
                ),
                height: 50,
                child: TextButton(
                  onPressed: onPressedLeft,
                  child: Text(
                    leftText,
                    style: const TextStyle(
                      color: Color.fromARGB(255, 255, 255, 255),
                      shadows: <Shadow>[
                        Shadow(
                          offset: Offset(0.0, 0.0),
                          blurRadius: 8.0,
                          color: Color.fromARGB(255, 2, 12, 0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const VerticalDivider(
              thickness: 2,
              width: 2,
              indent: 2,
              endIndent: 2,
              color: Color.fromARGB(0, 0, 0, 0),
            ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(12),
                    bottomLeft: Radius.circular(5),
                  ),
                  // color: Color(0xff1D4ED8),
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    tileMode: TileMode.clamp,
                    colors: [
                      Color.fromARGB(255, 41, 155, 190),
                      // Color(0xff1D4ED8),
                      Color.fromARGB(255, 41, 210, 173),
                    ],
                  ),
                ),
                height: 50,
                child: TextButton(
                  onPressed: onPressedRight,
                  child: Text(
                    rightText,
                    style: const TextStyle(
                      color: Color.fromARGB(255, 255, 255, 255),
                      shadows: <Shadow>[
                        Shadow(
                          offset: Offset(0.0, 0.0),
                          blurRadius: 8.0,
                          color: Color.fromARGB(255, 8, 95, 0),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SellerSection extends StatelessWidget {
  const SellerSection({
    Key? key,
    required this.product,
  }) : super(key: key);
  final SingleProduct product;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: Color.fromARGB(255, 219, 236, 214),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        image: const DecorationImage(
                          image: AssetImage('assets/images/ah.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                      height: 45,
                      width: 45,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      ' ${product.seller.business!.enName.toString()}',
                      style: const TextStyle(
                        fontSize: 16,
                        color: Color.fromARGB(255, 1, 75, 145),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: const Color.fromARGB(255, 1, 75, 145),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 8,
                      horizontal: 8,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset('assets/images/person.png'),
                        const Padding(
                          padding: EdgeInsets.only(left: 4),
                          child: Text(
                            '1200+',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 18,
          ),
          child: Column(
            children: [
              const Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    height: 60,
                    width: 120,
                    child: Card(
                      child: Column(
                        children: [
                          Text(
                            '91+',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            'Positive Seller',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                    width: 120,
                    child: Card(
                      elevation: 3,
                      child: Column(
                        children: [
                          Text(
                            '100%',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            'Shipping on time',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60,
                    width: 120,
                    child: Card(
                      elevation: 3,
                      child: Column(
                        children: [
                          Text(
                            '100%',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            'Chat Replies',
                            style: TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 1, 75, 145),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 60,
                    width: 120,
                    child: Card(
                      // color: Color.fromARGB(255, 210, 232, 203),
                      elevation: 3,
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '+',
                              style: TextStyle(
                                fontSize: 16,
                                color: Color.fromARGB(255, 1, 75, 145),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Text(
                              'Follow',
                              style: TextStyle(
                                fontSize: 14,
                                color: Color.fromARGB(255, 1, 75, 145),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 20),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed(
                        RouteName.shopdetailsScreen,
                      );
                    },
                    child: const SizedBox(
                      height: 60,
                      width: 120,
                      child: Card(
                        elevation: 3,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(right: 10),
                              child: Image(
                                image: AssetImage('assets/images/shop.png'),
                                height: 18,
                                width: 18,
                              ),
                            ),
                            Text(
                              'Vist Store',
                              style: TextStyle(
                                fontSize: 14,
                                color: Color.fromARGB(255, 1, 75, 145),
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class Rating extends StatefulWidget {
  Rating({
    super.key,
    required this.number,
  });
  int number;
  @override
  State<Rating> createState() => RatingState();
}

class RatingState extends State<Rating> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 1,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'James Shrestha,2079-03-12',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromARGB(255, 0, 0, 0),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: widget.number,
                              itemBuilder: (context, index) {
                                return const Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.star,
                                      color: Color.fromARGB(255, 250, 179, 0),
                                      size: 18,
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                          Text(
                            '${widget.number} /5',
                            style: const TextStyle(
                              fontSize: 14,
                              color: Color.fromARGB(255, 0, 0, 0),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    'Amazing Product..Totally loved it..Surely going to purchase ',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 14,
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 70,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: 1,
                          itemBuilder: (context, index) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Card(
                                  child: Container(
                                    height: 60,
                                    width: 60,
                                    decoration: BoxDecoration(
                                      image: const DecorationImage(
                                        image:
                                            AssetImage('assets/images/ah.jpg'),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        )
      ],
    );
  }
}

class ProductSpecification extends StatelessWidget {
  const ProductSpecification({
    Key? key,
    required this.product,
  }) : super(key: key);
  final SingleProduct product;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            color: const Color.fromARGB(255, 219, 236, 214),
            child: const Padding(
              padding: EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 16,
              ),
              child: Text(
                'Specification :',
                style: TextStyle(
                  fontSize: 16,
                  color: Color.fromARGB(255, 1, 75, 145),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Concern',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          ' shoe',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        SizedBox(
                          width: 100,
                          child: Divider(
                            color: Color.fromARGB(255, 210, 232, 203),
                            thickness: 2,
                          ),
                        ),
                        SizedBox(
                          height: 18,
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Color',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                          width: 100,
                          height: 30,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(right: 3),
                                child: CircleAvatar(
                                  backgroundColor: Colors.blue,
                                  radius: 12,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 3),
                                child: CircleAvatar(
                                  backgroundColor: Colors.red,
                                  radius: 12,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 3),
                                child: CircleAvatar(
                                  backgroundColor: Colors.yellow,
                                  radius: 12,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 3),
                                child: CircleAvatar(
                                  backgroundColor: Colors.green,
                                  radius: 12,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 3),
                                child: CircleAvatar(
                                  backgroundColor: Colors.deepPurple,
                                  radius: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 100,
                          child: Divider(
                            color: Color.fromARGB(255, 210, 232, 203),
                            thickness: 2,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Fabric',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          ' Cotton',
                          style: TextStyle(
                            fontSize: 14,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        SizedBox(
                          width: 100,
                          child: Divider(
                            color: Color.fromARGB(255, 210, 232, 203),
                            thickness: 2,
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Size',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 5,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        SizedBox(
                          width: 100,
                          height: 30,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(left: 5.0),
                                child: CircleAvatar(
                                  radius: 12,
                                  backgroundColor:
                                      Color.fromARGB(242, 219, 236, 214),
                                  child: Text(
                                    'S',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0),
                                child: CircleAvatar(
                                  radius: 12,
                                  backgroundColor:
                                      Color.fromARGB(242, 219, 236, 214),
                                  child: Text(
                                    'L',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0),
                                child: CircleAvatar(
                                  radius: 12,
                                  backgroundColor:
                                      Color.fromARGB(242, 219, 236, 214),
                                  child: Text(
                                    'XL',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0),
                                child: CircleAvatar(
                                  radius: 12,
                                  backgroundColor:
                                      Color.fromARGB(242, 219, 236, 214),
                                  child: Text(
                                    'XXL',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 5.0),
                                child: CircleAvatar(
                                  radius: 12,
                                  backgroundColor:
                                      Color.fromARGB(242, 219, 236, 214),
                                  child: Text(
                                    'XXX',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 100,
                          child: Divider(
                            color: Color.fromARGB(255, 210, 232, 203),
                            thickness: 2,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          )
          // product.specifications != null && product.specifications!.isNotEmpty
          //     ? Padding(
          //         padding: const EdgeInsets.only(
          //           top: 14,
          //           bottom: 16,
          //           left: 27,
          //           right: 34,
          //         ),
          //         child: GridView.builder(
          //           padding: const EdgeInsets.all(0),
          //           gridDelegate:
          //               const SliverGridDelegateWithFixedCrossAxisCount(
          //             crossAxisCount: 2,
          //             mainAxisSpacing: 0,
          //             crossAxisSpacing: 30,
          //             mainAxisExtent: 73,
          //           ),
          //           shrinkWrap: true,
          //           physics: const NeverScrollableScrollPhysics(),
          //           itemCount: product.specifications!.length,
          //           itemBuilder: (context, index) {
          //             var specification = product.specifications!;
          //             return Column(
          //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 Text(
          //                   specification[index].name.toString(),
          //                   style: const TextStyle(
          //                     fontSize: 16,
          //                   ),
          //                   overflow: TextOverflow.ellipsis,
          //                   maxLines: 5,
          //                 ),
          //                 const SizedBox(
          //                   height: 5,
          //                 ),
          //                 Text(
          //                   ' ${specification[index].value.toString()}',
          //                   style: const TextStyle(
          //                     fontSize: 16,
          //                   ),
          //                   overflow: TextOverflow.ellipsis,
          //                   maxLines: 5,
          //                 ),
          //                 const Divider(
          //                   thickness: 1,
          //                   color: Color.fromARGB(255, 210, 232, 203),
          //                 )
          //               ],
          //             );
          //           },
          //         ),
          //       )
          //     : const Padding(
          //         padding: EdgeInsets.all(8),
          //         child: Text(
          //           'Oopss ! ! ! no specifications',
          //           style: TextStyle(
          //             fontSize: 16,
          //           ),
          //           overflow: TextOverflow.ellipsis,
          //           maxLines: 5,
          //         ),
          //       ),
        ],
      ),
    );
  }
}

class ProductDecription extends StatelessWidget {
  const ProductDecription({
    Key? key,
    required this.product,
  }) : super(key: key);

  final SingleProduct product;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 244, 250, 247),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      padding: const EdgeInsets.only(top: 8, bottom: 0, left: 0, right: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Align(
            alignment: Alignment.topLeft,
            child: Text(
              'Description :',
              style: TextStyle(
                fontSize: 14,
                color: Color.fromARGB(255, 0, 85, 134),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              product.description.toString(),
              style: const TextStyle(
                fontSize: 16,
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 5,
            ),
          ),
        ],
      ),
    );
  }
}

class FavouriteCategoryItem extends StatelessWidget {
  final int index;
  const FavouriteCategoryItem({Key? key, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // height: MediaQuery.of(context).size.height * 0.3,
      // width: MediaQuery.of(context).size.height * 0.2,
      // decoration: BoxDecoration(border: Border.all()),
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.12,
            child: Image.asset('assets/images/icon-13.png'),
          ),
          const Text(
            'HeadPhone',
            style: TextStyle(fontSize: 14),
          ),
        ],
      ),
    );
  }
}

class Similarproduct extends StatefulWidget {
  final int itemCount;
  const Similarproduct({super.key, required this.itemCount});

  @override
  State<Similarproduct> createState() => SimilarproductState();
}

class SimilarproductState extends State<Similarproduct> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          color: const Color.fromARGB(255, 219, 236, 214),
          child: const Padding(
            padding: EdgeInsets.symmetric(
              vertical: 12,
              horizontal: 16,
            ),
            child: Text(
              'Similar Products:',
              style: TextStyle(
                fontSize: 16,
                color: Color.fromARGB(255, 1, 75, 145),
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 10),
          color: const Color.fromARGB(255, 229, 234, 250),
          child: StreamBuilder<List<Product>>(
            stream: productBloc.allProducts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                var allProducts = snapshot.data!;
                if (allProducts.isNotEmpty) {
                  return GridView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 05,
                      childAspectRatio: 0.65,
                    ),
                    itemCount: allProducts.length,
                    itemBuilder: (context, index) {
                      var product = allProducts[index];

                      return ProductForYou(
                        product: product,
                        heroType: 'all',
                      );
                    },
                  );
                }
              }
              return Center(
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 0,
                    mainAxisSpacing: 10,
                    mainAxisExtent: 360,
                  ),
                  itemCount: 6,
                  itemBuilder: (context, index) => const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [B2CProductShimmer()],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
