import 'package:b2b_market/screens/buyer_screens/cart_page.dart';
import 'package:b2b_market/screens/buyer_screens/home/buyer_home_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/profile/profile_main_screen.dart';
import 'package:b2b_market/screens/buyer_screens/shop/shop_suggest.dart';
import 'package:flutter/material.dart';

class BuyerNavigationScreen extends StatefulWidget {
  const BuyerNavigationScreen({Key? key}) : super(key: key);

  static const routeName = 'BuyerNavigationScreen';

  @override
  State<BuyerNavigationScreen> createState() => _BuyerNavigationScreenState();
}

class _BuyerNavigationScreenState extends State<BuyerNavigationScreen> {
  int _currentPageIndex = 0;

  void _onDestinationSelected(int index) {
    setState(() {
      _currentPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: const Color.fromARGB(255, 217, 252, 233),
      body: Stack(
        children: [
          [
            const BuyerHomeScreen(),
            const ShopSugestion(),
            const CartPage(),
            const ProfileMainScreen(),
          ][_currentPageIndex],
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 60,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  tileMode: TileMode.clamp,
                  colors: [Color(0xff7CD0FF), Color(0xff6AFD93)],
                ),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              child: NavigationBar(
                labelBehavior:
                    NavigationDestinationLabelBehavior.onlyShowSelected,
                backgroundColor: const Color.fromARGB(0, 255, 255, 255),
                onDestinationSelected: _onDestinationSelected,
                selectedIndex: _currentPageIndex,
                destinations: [
                  NavigationDestination(
                    selectedIcon: Container(
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(87, 41, 222, 147),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: const Center(
                        child: Icon(Icons.home_outlined, color: Colors.white),
                      ),
                    ),
                    icon: const Icon(Icons.home),
                    label: 'Home',
                  ),
                  NavigationDestination(
                    selectedIcon: Container(
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(87, 41, 222, 147),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: const Center(
                        child: Icon(Icons.store_outlined, color: Colors.white),
                      ),
                    ),
                    icon: const Icon(Icons.store),
                    label: 'Store',
                  ),
                  NavigationDestination(
                    selectedIcon: Container(
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(87, 41, 222, 147),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: const Center(
                        child: Icon(
                          Icons.shopping_cart_outlined,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    icon: const Icon(Icons.shopping_cart),
                    label: 'Cart',
                  ),
                  NavigationDestination(
                    selectedIcon: Container(
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(87, 41, 222, 147),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: const Center(
                        child:
                            Icon(Icons.person_2_outlined, color: Colors.white),
                      ),
                    ),
                    icon: const Icon(Icons.person),
                    label: 'Profile',
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
