import 'package:flutter/material.dart';

import '../../common/widgets/widgets.dart';
import '../../models/business.dart';
import '../../route_names.dart';
import 'components/quick_links.dart';
import 'product_and_leads_stats.dart';

class DashboardScreen extends StatelessWidget {
  final Business business;

  const DashboardScreen({
    Key? key,
    required this.business,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        top: 5,
        bottom: 20,
      ),
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(Icons.arrow_back_ios),
                ),
                Text(
                  'Welcome Back,${business.ceoName}',
                  style: Theme.of(context).textTheme.headlineLarge,
                ),
              ],
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Text(
                business.name,
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
            const SizedBox(height: 20),
            const ProductAndLeadStats(),
            const SizedBox(height: 20),
            CustomOutlinedButton(
              onPressed: () => Navigator.pushNamed(
                context,
                RouteName.businessProfileScreen,
                arguments: business,
              ),
              title: 'Business Profile',
              color: const Color(0xff1D4ED8),
            ),
            const SizedBox(height: 20),
            Visibility(
              visible: business.verified == 1,
              child: CustomOutlinedButton(
                onPressed: () async {
                  Navigator.pushNamed(
                    context,
                    RouteName.storePageScreen,
                    arguments: business.verified,
                  );
                },
                title: 'Store Page',
                color: const Color(0xff29DE92),
              ),
            ),
            const SizedBox(height: 20),
            Text(
              'Quick Links',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(height: 20),
            QuickLinks(
              isSellerVerified: business.verified == 1,
            ),
          ],
        ),
      ),
    );
  }
}
