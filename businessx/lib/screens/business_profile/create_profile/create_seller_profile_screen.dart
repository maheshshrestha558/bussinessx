import 'dart:io';

import 'package:b2b_market/bloc/auth_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:image_picker/image_picker.dart';

import '../../../common/widgets/widgets.dart';
import 'create_seller_profile_second_screen.dart';

class CreateSellerProfileFirstScreen extends StatefulWidget {
  const CreateSellerProfileFirstScreen({Key? key}) : super(key: key);

  @override
  State<CreateSellerProfileFirstScreen> createState() =>
      _CreateSellerProfileFirstScreenState();
}

class _CreateSellerProfileFirstScreenState
    extends State<CreateSellerProfileFirstScreen> {
  final _enNameController = TextEditingController();
  final _ceoNameController = TextEditingController();
  final _contactNumberController = TextEditingController();
  final _alternateContactNumberController = TextEditingController();
  final _addressController = TextEditingController();
  final _emailController = TextEditingController();
  final _createSellerFormKey = GlobalKey<FormState>();

  final ImagePicker _picker = ImagePicker();

  File? image;
  String? name;
  String? contact;

  getInfo() async {
    // final SharedPreferences prefs = await SharedPreferences.getInstance();
    // name = prefs.getString('name');
    name = await authBloc.getName();
    contact = await authBloc.getContact();

    debugPrint('name $name');
    _ceoNameController.text = name ?? '';
    _contactNumberController.text = contact ?? '';
  }

  @override
  void initState() {
    super.initState();
    getInfo();
  }

  Future _img(ImageSource source) async {
    try {
      final image = await _picker.pickImage(source: source, imageQuality: 95);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image $e');
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Photo Library'),
                onTap: () {
                  _img(ImageSource.gallery);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () {
                  _img(ImageSource.camera);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          PageTitle(
            title: 'Business Profile',
            navbuttontext: 'Next',
            showTopNavButton: true,
            ontopNavButtonPressed: () {
              if (_createSellerFormKey.currentState!.validate()) {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (_) => CreateSellerProfileSecondScreen(
                      email: _emailController.text,
                      address: _addressController.text,
                      businessName: _enNameController.text,
                      ceoName: _ceoNameController.text,
                      contactNumber: _contactNumberController.text,
                      alternateContactNumber:
                          _alternateContactNumberController.text,
                      image: image,
                    ),
                  ),
                );
              }
            },
          ),
          Form(
            key: _createSellerFormKey,
            child: Expanded(
              child: ListView(
                physics: const BouncingScrollPhysics(),
                children: [
                  const SizedBox(height: 10),
                  Column(
                    children: [
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            _showPicker(context);
                          },
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                width: 2,
                                color: const Color(0xff77dce1),
                              ),
                            ),
                            child: image != null
                                ? ClipOval(
                                    child: Image.file(
                                      image!,
                                      width: 90,
                                      height: 90,
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : Container(
                                    height: 90,
                                    width: 90,
                                    decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Colors.grey[800],
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          foregroundColor: const Color.fromRGBO(29, 78, 216, 1),
                          backgroundColor: Colors.white,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                          ),
                          side: const BorderSide(
                            color: Color.fromRGBO(29, 78, 216, 1),
                            width: 3,
                          ),
                        ),
                        onPressed: () => _showPicker(context),
                        child: const Text('Add Logo'),
                      )
                    ],
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Business Name ',
                    controller: _enNameController,
                    validator: RequiredValidator(
                      errorText: 'Required',
                    ),
                  ),
                  const SizedBox(height: 10),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: "CEO's Name",
                    controller: _ceoNameController,
                    validator: RequiredValidator(
                      errorText: 'The ceo name field is required.',
                    ),
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Contact Number',
                    controller: _contactNumberController,
                    textInputType: TextInputType.phone,
                    validator: MultiValidator([
                      RequiredValidator(
                        errorText: 'Contact Number is required',
                      ),
                      LengthRangeValidator(
                        min: 10,
                        max: 10,
                        errorText: 'Contact Number must be 10 digits long',
                      )
                    ]),
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    textInputType: TextInputType.phone,
                    labelText: 'Alternate Contact Number(Optional)',
                    controller: _alternateContactNumberController,
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Address',
                    controller: _addressController,
                    validator: RequiredValidator(
                      errorText: 'The address field is required..',
                    ),
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Email Address',
                    controller: _emailController,
                    validator: MultiValidator([
                      RequiredValidator(errorText: 'Email is required'),
                      EmailValidator(errorText: 'Enter Valid Email address')
                    ]),
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
