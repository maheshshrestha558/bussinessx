import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../../../bloc/business_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/business_type.dart';
import '../../../models/response/response.dart';

class CreateSellerProfileSecondScreen extends StatefulWidget {
  final String? businessName;
  final String? ceoName;
  final String? contactNumber;
  final String? alternateContactNumber;
  final String? address;
  final String? email;
  final File? image;

  const CreateSellerProfileSecondScreen({
    Key? key,
    this.businessName,
    this.ceoName,
    this.contactNumber,
    this.alternateContactNumber,
    this.address,
    this.email,
    this.image,
  }) : super(key: key);

  @override
  State<CreateSellerProfileSecondScreen> createState() =>
      _CreateSellerProfileSecondScreenState();
}

class _CreateSellerProfileSecondScreenState
    extends State<CreateSellerProfileSecondScreen> {
  final _importCodeController = TextEditingController();
  final _panNumberController = TextEditingController();
  final _websiteUrlController = TextEditingController();
  final _facebookUrlController = TextEditingController();
  final _instaUrlController = TextEditingController();
  final _whatsappController = TextEditingController();
  final _establishedDateController = TextEditingController();

  final _createSellerProfileFormKey = GlobalKey<FormState>();

  BusinessType? _businessType;
  DateTime? _selectedEstablishedDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          PageTitle(
            title: 'Business Profile',
            navbuttontext: 'Create',
            showTopNavButton: true,
            ontopNavButtonPressed: () {
              if (_createSellerProfileFormKey.currentState!.validate()) {
                businessesBloc.createSeller(
                  context,
                  {
                    'en_name': widget.businessName,
                    'ceo_name': widget.ceoName,
                    'contact_number': widget.contactNumber,
                    'alternate_contact_number': widget.alternateContactNumber,
                    'address': widget.address,
                    'email': widget.email,
                    'pan': _panNumberController.text,
                    'import_export_code': _importCodeController.text,
                    'established_date': _establishedDateController.text,
                    'whatsapp_number': _whatsappController.text,
                    'website_url': _websiteUrlController.text,
                    'facebook_url': _facebookUrlController.text,
                    'instagram_url': _instaUrlController.text,
                    'business_type_id': _businessType?.id,
                  },
                  widget.image,
                );
              }
            },
          ),
          Form(
            key: _createSellerProfileFormKey,
            child: Expanded(
              child: ListView(
                physics: const BouncingScrollPhysics(),
                children: [
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'PAN Number',
                    controller: _panNumberController,
                    textInputType: TextInputType.number,
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Import/Export Code',
                    controller: _importCodeController,
                    textInputType: TextInputType.number,
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Established Date',
                    controller: _establishedDateController,
                    readOnly: true,
                    onTap: () async {
                      showCupertinoModalPopup(
                        context: context,
                        builder: (context) => Material(
                          child: SizedBox(
                            height: 200,
                            child: CupertinoDatePicker(
                              mode: CupertinoDatePickerMode.date,
                              dateOrder: DatePickerDateOrder.ymd,
                              maximumDate: DateTime.now()
                                  .subtract(const Duration(days: 1)),
                              initialDateTime: _selectedEstablishedDate ??
                                  DateTime.now()
                                      .subtract(const Duration(hours: 25)),
                              onDateTimeChanged: (dateTime) {
                                _establishedDateController.text =
                                    dateTime.toString().substring(0, 10);
                                _selectedEstablishedDate = dateTime;
                              },
                            ),
                          ),
                        ),
                      );
                    },
                    validator: RequiredValidator(
                      errorText: 'Required',
                    ),
                  ),
                  const SizedBox(height: 10),
                  const Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: CustomLabelText(labelText: 'Business Type'),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: StreamBuilder<AllBusinessTypeResponse>(
                      stream: businessesBloc.allBusinessTypes.stream,
                      builder: (context, snapshot) {
                        var businessTypes = snapshot.data?.businessTypes;
                        if (businessTypes != null && businessTypes.isNotEmpty) {
                          return ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButtonFormField<BusinessType>(
                              hint: const Text('Select One'),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineMedium!
                                  .copyWith(color: Colors.black),
                              validator: (value) => value == null
                                  ? 'The business type field is required'
                                  : null,
                              decoration: const InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  left: 5.0,
                                  right: 15.0,
                                  top: 10,
                                  bottom: 10,
                                ),
                                filled: true,
                                fillColor: Color(0xffffffff),
                                border: border,
                                focusedBorder: focusedBorder,
                                enabledBorder: enabledBorder,
                              ),
                              elevation: 0,
                              icon: const Icon(Icons.arrow_drop_down),
                              value: _businessType,
                              onChanged: (value) {
                                _businessType = value;

                                FocusScope.of(context).unfocus();

                                setState(() {});
                              },
                              items: businessTypes.map((value) {
                                return DropdownMenuItem(
                                  value: value,
                                  child: Text(value.name),
                                );
                              }).toList(),
                            ),
                          );
                        }

                        return Container();
                      },
                    ),
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'WhatsApp Number',
                    controller: _whatsappController,
                    textInputType: TextInputType.phone,
                    validator: MaxLengthValidator(
                      10,
                      errorText: 'WhatsApp Number must be 10 digits Number',
                    ),
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Website URL',
                    controller: _websiteUrlController,
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Facebook URL',
                    controller: _facebookUrlController,
                  ),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    labelText: 'Instagram URL',
                    controller: _instaUrlController,
                  ),
                  const SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
