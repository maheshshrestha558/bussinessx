import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../bloc/product_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/models.dart';
import '../../../route_names.dart';

class SellerProductListTile extends StatefulWidget {
  const SellerProductListTile({
    Key? key,
    required this.sellerProduct,
  }) : super(key: key);

  final SellerProduct sellerProduct;

  @override
  State<SellerProductListTile> createState() => _SellerProductListTileState();
}

class _SellerProductListTileState extends State<SellerProductListTile>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late bool buttonState = widget.sellerProduct.status == 1 ? true : false;

  @override
  void initState() {
    super.initState();
    _controller = BottomSheet.createAnimationController(this)
      ..reverseDuration = const Duration(milliseconds: 250);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          RouteName.editProductScreen,
          arguments: widget.sellerProduct,
        );
      },
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            color: Colors.white70,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(0),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 150,
              child: Row(
                children: [
                  Container(
                    height: 120,
                    width: 120,
                    margin: const EdgeInsets.fromLTRB(
                      13,
                      15,
                      0,
                      15,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.greenAccent,
                    ),
                    child: CachedNetworkImage(
                      placeholder: (__, _) {
                        return Center(
                          child: Text(
                            widget.sellerProduct.name[0].toUpperCase(),
                          ),
                        );
                      },
                      imageUrl: widget.sellerProduct.imageThumbnail,
                      imageBuilder: (_, imageProvider) {
                        return Container(
                          height: 120,
                          width: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                      errorWidget: (_, __, ___) {
                        return Center(
                          child: Text(
                            widget.sellerProduct.name[0].toUpperCase(),
                          ),
                        );
                      },
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 15.0,
                        top: 15.0,
                        bottom: 15.0,
                        right: 5,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            widget.sellerProduct.name,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: Theme.of(context).textTheme.headlineLarge,
                          ),
                          Text(
                            'Brand: ${widget.sellerProduct.brand.name}',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: Theme.of(context).textTheme.displayLarge,
                          ),
                          Text(
                            widget.sellerProduct.isVerified == 1
                                ? 'Verified'
                                : 'Not Verified',
                            style: GoogleFonts.rubik(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              color: widget.sellerProduct.isVerified == 1
                                  ? Colors.green
                                  : const Color.fromARGB(255, 238, 179, 89),
                            ),
                          ),
                          Text(
                            '${widget.sellerProduct.price}',
                            style: GoogleFonts.rubik(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: const Color(0xffDC2626),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      CupertinoSwitch(
                        value: buttonState,
                        onChanged: (value) {
                          int status = 1;
                          setState(() {
                            buttonState = value;
                            status = buttonState ? 1 : 0;

                            productBloc.setStatusOfProduct(
                              context,
                              widget.sellerProduct.id,
                              status,
                            );
                          });
                        },
                        trackColor: Colors.blueGrey[100],
                        activeColor: Colors.green,
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          color: Color.fromARGB(109, 255, 140, 140),
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: IconButton(
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                          onPressed: () {
                            showModalBottomSheet(
                              shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20),
                                ),
                              ),
                              context: context,
                              transitionAnimationController: _controller,
                              builder: (_) => Container(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 10,
                                  vertical: 20,
                                ),
                                height: 135,
                                child: Column(
                                  children: [
                                    const Text(
                                      'Do you want to delete the product?',
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500,
                                        color: Color.fromRGBO(52, 211, 153, 1),
                                      ),
                                    ),
                                    const Divider(),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        BottomSheetButton(
                                          backgroundColor: Colors.red,
                                          buttonLabel: 'Delete',
                                          onPressed: () {
                                            Navigator.pop(context);
                                            productBloc.deleteProduct(
                                              context,
                                              widget.sellerProduct.id,
                                            );
                                          },
                                        ),
                                        BottomSheetButton(
                                          backgroundColor: Colors.white,
                                          buttonLabel: 'Cancel',
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            widget.sellerProduct.isVerified == 1
                ? const Padding(padding: EdgeInsets.zero)
                : Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      'This product is on review.Please wait until verification process is done!!',
                      style: GoogleFonts.rubik(
                        fontSize: 10,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromARGB(255, 238, 179, 89),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
