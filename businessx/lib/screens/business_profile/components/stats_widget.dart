import 'package:flutter/material.dart';

class StatsWidget extends StatelessWidget {
  final int value;
  final String label;
  const StatsWidget({
    Key? key,
    required this.value,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          value.toString(),
          style: const TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
        ),
        Text(label),
      ],
    );
  }
}
