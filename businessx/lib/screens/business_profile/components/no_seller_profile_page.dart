import 'package:b2b_market/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../common/widgets/widgets.dart';
import '../../../repositories/repositories.dart';
import '../../../route_names.dart';

class NoSellerProfile extends StatefulWidget {
  final String? additionalMessage;
  final bool? visible;

  const NoSellerProfile({
    Key? key,
    this.additionalMessage,
    this.visible = false,
  }) : super(key: key);

  @override
  State<NoSellerProfile> createState() => _NoSellerProfileState();
}

class _NoSellerProfileState extends State<NoSellerProfile> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        const SizedBox(
          height: 10,
        ),
        Container(
          height: 60,
          padding: const EdgeInsets.fromLTRB(15.0, 10.0, 0.0, 15.0),
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                left: 0,
                child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(Icons.arrow_back_ios),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 28.0, right: 28.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '! Oopss !',
                style: GoogleFonts.rubik(
                  fontSize: 40,
                  fontWeight: FontWeight.w700,
                  color: const Color(0xff1D4ED8),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                'Looks like you don’t have a business profile.',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              const SizedBox(
                height: 20,
              ),
              Visibility(
                visible: widget.visible ?? false,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: Text(
                    widget.additionalMessage ?? '',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
              ),
              Text(
                'Create a business profile, and start selling at instant. It will just take a few mins to complete. ',
                style: Theme.of(context).textTheme.headlineMedium,
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 30,
              ),
              CustomOutlinedButton(
                title: 'Create Business Profile',
                color: const Color(0xff29DE92),
                onPressed: () {
                  !baseRepository.isUserLoggedIn
                      ? Navigator.pushReplacementNamed(
                          context,
                          SignUpScreen.routeName,
                        )
                      : Navigator.pushNamed(
                          context,
                          RouteName.createSellerProfileFirstScreen,
                        );
                },
              ),
              const SizedBox(
                height: 10,
              ),
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(
                  'Skip for now',
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium!
                      .copyWith(decoration: TextDecoration.underline),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
