import 'package:b2b_market/common/widgets/custom_progress_dialog.dart';
import 'package:b2b_market/common/widgets/custom_toast.dart';
import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../route_names.dart';
import '../packages/packages_screen.dart';
import 'quicklink.dart';

class QuickLinks extends StatelessWidget {
  final bool isSellerVerified;

  const QuickLinks({
    required this.isSellerVerified,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Wrap(
        runSpacing: 20,
        spacing: 40,
        children: [
          QuickLink(
            iconName: Icons.add,
            choiceName: 'Add Products',
            onPress: () {
              selectLeafCategoryBloc.setEditMode(false);
              Navigator.pushNamed(
                context,
                RouteName.selectRootCategoryScreen,
              );
            },
          ),
          QuickLink(
            iconName: Icons.ballot_rounded,
            choiceName: 'Buy Leads',
            onPress: () async {
              leadBloc.viewLead();
              await Navigator.pushNamed(
                context,
                RouteName.viewLeadScreen,
              );
            },
          ),
          QuickLink(
            iconName: Icons.visibility,
            choiceName: 'View Products',
            onPress: () async {
              Feedback.forTap(context);
              showCustomProgressDialog(context, 'Loading...');
              var response = await productBloc.fetchSellerProducts();
              Navigator.of(context).pop();
              if (response.error.isEmpty) {
                Navigator.pushNamed(
                  context,
                  RouteName.sellerProductsListScreen,
                );
              }
              if (response.error.isNotEmpty) {
                ShowToast.error(
                  response.error,
                );
              }
            },
          ),
          QuickLink(
            iconName: Icons.shopping_bag,
            choiceName: 'Packages',
            onPress: () {
              Navigator.pushNamed(
                context,
                PackagesScreen.routeName,
              );
            },
          ),
        ],
      ),
    );
  }
}
