import 'package:flutter/material.dart';

class QuickLink extends StatelessWidget {
  final IconData iconName;
  final String choiceName;
  final void Function()? onPress;

  const QuickLink({
    Key? key,
    required this.iconName,
    required this.choiceName,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Column(
        children: [
          Container(
            height: 80,
            width: 80,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.19),
                  spreadRadius: 0,
                  blurRadius: 20,
                  offset: Offset(0, 8), // changes position of shadow
                ),
              ],
            ),
            child: Icon(
              iconName,
              size: 40,
              color: const Color.fromRGBO(29, 78, 216, 1),
            ),
          ),
          const SizedBox(height: 10),
          Text(
            choiceName,
            style: Theme.of(context)
                .textTheme
                .titleLarge!
                .copyWith(color: const Color(0xff1D4ED8)),
          ),
        ],
      ),
    );
  }
}
