import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../bloc/lead_bloc.dart';
import '../../../common/decoration.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/lead/view_leads_response.dart';
import 'components/custom_lead_card.dart';

class ViewLeadsScreen extends StatefulWidget {
  const ViewLeadsScreen({Key? key}) : super(key: key);

  @override
  State<ViewLeadsScreen> createState() => _ViewLeadsScreenState();
}

class _ViewLeadsScreenState extends State<ViewLeadsScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  int? _userId;

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _sentScrollController = ScrollController();
  final _receivedScrollController = ScrollController();

  int page = 1;

  @override
  void initState() {
    SharedPreferences.getInstance().then(
      (pref) => {
        setState(() {
          _userId = pref.getInt('userID');
        })
      },
    );
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _sentScrollController.addListener(() {
      if (_sentScrollController.position.pixels ==
          _sentScrollController.position.maxScrollExtent) {
        if (page != leadBloc.lastPage) {
          page++;
          leadBloc.viewLead(page: page);
        }
      }
    });
    _receivedScrollController.addListener(() {
      if (_receivedScrollController.position.pixels ==
          _receivedScrollController.position.maxScrollExtent) {
        if (page != leadBloc.lastPage) {
          page++;
          leadBloc.viewLead(page: page);
        }
      }
    });
  }

  @override
  void dispose() {
    _sentScrollController.dispose();
    _receivedScrollController.dispose();
    leadBloc.drainLead();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          const PageTitle(title: 'Leads'),
          TabBar(
            controller: _tabController,
            labelColor: Colors.black,
            unselectedLabelColor: Colors.black,
            tabs: const [
              Tab(
                text: 'Sent',
              ),
              Tab(
                text: 'Received',
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            child: StreamBuilder<List<Lead>>(
              stream: leadBloc.lead.stream,
              builder: (context, snapshot) {
                var leads = snapshot.data;
                return Column(
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: deviceHeight > 700
                          ? deviceHeight / 1.35
                          : deviceHeight / 1.50,
                      width: double.maxFinite,
                      child: TabBarView(
                        controller: _tabController,
                        physics: const BouncingScrollPhysics(),
                        children: [
                          ListView.builder(
                            physics: const BouncingScrollPhysics(),
                            controller: _sentScrollController,
                            itemCount: leads?.length ?? 0,
                            itemBuilder: (context, index) {
                              var lead = leads![index];
                              if (_userId == lead.buyer?.id) {
                                return CustomLeadCard(
                                  lead: lead,
                                  //seller
                                );
                              }
                              return const SizedBox();
                            },
                          ),
                          ListView.builder(
                            physics: const BouncingScrollPhysics(),
                            controller: _receivedScrollController,
                            itemCount: leads?.length ?? 0,
                            itemBuilder: (context, index) {
                              var lead = leads![index];
                              if (_userId == lead.seller?.id) {
                                return CustomLeadCard(
                                  lead: lead,
                                  //buyer
                                );
                              }
                              return const SizedBox();
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
