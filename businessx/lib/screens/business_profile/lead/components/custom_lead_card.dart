import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../models/response/lead/view_leads_response.dart';
import '../../../chat/chat_screen.dart';

class CustomLeadCard extends StatelessWidget {
  const CustomLeadCard({
    Key? key,
    required this.lead,
  }) : super(key: key);

  final Lead lead;

  final rowSpacer = const TableRow(
    children: [
      SizedBox(
        height: 8,
      ),
      SizedBox(
        height: 8,
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: InkWell(
        onTap: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          var userID = prefs.getInt('userID');
          var token = prefs.getString('token');
          if (userID != null && token != null) {
            Navigator.push(
              context,
              CupertinoPageRoute(
                builder: (_) => ChatScreen(
                  lead: lead,
                  userId: userID,
                  token: token,
                  title: lead.seller?.business.name ?? '',
                ),
              ),
            );
          }
        },
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Table(
              children: [
                rowSpacer,
                TableRow(
                  children: [
                    Text(
                      'Seller:',
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: const Color(0xff393939)),
                    ),
                    Text(
                      lead.seller?.business.name ?? '',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    Text(
                      'Contact Number:',
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: const Color(0xff393939)),
                    ),
                    Text(
                      lead.seller?.business.contactNumber ?? '',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    Text(
                      'Email:',
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: const Color(0xff393939)),
                    ),
                    Text(
                      lead.seller?.business.email ?? '',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    Text(
                      'Address:',
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: const Color(0xff393939)),
                    ),
                    Text(
                      lead.seller?.business.address ?? '',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ),
                rowSpacer,
                TableRow(
                  children: [
                    Text(
                      'Requirements:',
                      style: Theme.of(context)
                          .textTheme
                          .titleSmall!
                          .copyWith(color: const Color(0xff393939)),
                    ),
                    Text(
                      lead.chatMessage ?? '',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
