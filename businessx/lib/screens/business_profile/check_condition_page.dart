import 'package:flutter/material.dart';

import '../../bloc/bloc.dart';
import '../../bloc/business_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/business/single_business_response.dart';
import 'components/no_seller_profile_page.dart';
import 'dashboard_screen.dart';

class CheckConditionPage extends StatefulWidget {
  const CheckConditionPage({Key? key}) : super(key: key);

  @override
  State<CheckConditionPage> createState() => _CheckConditionPageState();
}

class _CheckConditionPageState extends State<CheckConditionPage> {
  @override
  void initState() {
    super.initState();
    businessesBloc
      ..getBusinessTypes()
      ..getBusiness();
    leadBloc.drainLead();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: StreamBuilder<SingleBusinessResponse>(
        stream: businessesBloc.business.stream,
        builder: (context, AsyncSnapshot<SingleBusinessResponse> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(
                color: Color(0xff29ed92),
              ),
            );
          }
          var business = snapshot.data?.business;
          if (business != null) {
            return DashboardScreen(
              business: business,
            );
          }
          return const NoSellerProfile();
        },
      ),
    );
  }
}
