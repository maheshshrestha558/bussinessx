import 'package:flutter/material.dart';

import '../../../bloc/bloc.dart';
import '../../../bloc/dashboard_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/models.dart';
import '../components/seller_product_list_card.dart';

class SellerProductsListScreen extends StatefulWidget {
  const SellerProductsListScreen({Key? key}) : super(key: key);

  @override
  State<SellerProductsListScreen> createState() =>
      _SellerProductsListScreenState();
}

class _SellerProductsListScreenState extends State<SellerProductsListScreen> {
  @override
  initState() {
    super.initState();
    productBloc.fetchSellerProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          PageTitle(
            title: 'My Products',
            onBackPressed: () => dashBoardBloc.getDashboardInfo(),
          ),
          Expanded(
            child: StreamBuilder<List<SellerProduct>>(
              stream: productBloc.sellerProducts,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var sellerProducts = snapshot.data!;
                  if (sellerProducts.isNotEmpty) {
                    return ListView.builder(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      physics: const BouncingScrollPhysics(),
                      itemCount: sellerProducts.length,
                      itemBuilder: (context, index) {
                        var sellerProduct = sellerProducts[index];

                        return SellerProductListTile(
                          sellerProduct: sellerProduct,
                        );
                      },
                    );
                  }

                  if (sellerProducts.isEmpty) {
                    return const Center(
                      child: Text('No Products Available'),
                    );
                  }
                }
                if (snapshot.hasError) {
                  return Center(
                    child: Text('${snapshot.error}'),
                  );
                }
                return const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xff29ed92),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
