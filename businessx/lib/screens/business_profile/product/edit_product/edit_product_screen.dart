import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import '/bloc/bloc.dart';
import '/common/decoration.dart';
import '/common/widgets/widgets.dart';
import '/models/models.dart';
import '/models/response/response.dart';
import '../../../../models/product_image.dart';
import '../../../../route_names.dart';
import 'components/components.dart';

class EditProductScreen extends StatefulWidget {
  final SellerProduct sellerProduct;
  const EditProductScreen({Key? key, required this.sellerProduct})
      : super(key: key);

  @override
  State<EditProductScreen> createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  ProductCategory? _productCategory;
  Brand? _brand;
  List<ProductImage?> _productImages = [];
  final List<int> _ids = [];
  final List<String?> _values = [];

  List<Specification> _specifications = [];
  final _englishNameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _priceController = TextEditingController();
  final _unitController = TextEditingController();
  final _productCategoryController = TextEditingController();

  final ImagePicker _picker = ImagePicker();
  final List<File?> _images = List.filled(4, null);
  final List<dynamic> _imagePaths = [];

  Future _getImage(ImageSource source, index) async {
    try {
      final image = await _picker.pickImage(source: source, imageQuality: 95);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() {
        _imagePaths[index] = _images[index] = imageTemporary;
      });
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image $e');
    }
  }

  void _showPicker(context, index) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Photo Library'),
                onTap: () {
                  _getImage(ImageSource.gallery, index);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () {
                  _getImage(ImageSource.camera, index);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  initState() {
    super.initState();
    var sellerProduct = widget.sellerProduct;
    _productCategory = sellerProduct.productCategory;
    specificationsBloc.getSpecifications(_productCategory!.id);
    _englishNameController.text = sellerProduct.enName;
    _descriptionController.text = sellerProduct.description;
    _priceController.text = sellerProduct.price.toString();
    _unitController.text = sellerProduct.unit;
    _productCategoryController.text = sellerProduct.productCategory.name;
    _specifications = sellerProduct.specifications;
    _productImages = sellerProduct.productImages!;
    if (_productImages.length < 4) {
      for (var i = _productImages.length; i < 4; i++) {
        _productImages.add(ProductImage());
      }
    }

    for (var productImage in _productImages) {
      _imagePaths.add(productImage?.path?.split('/').last);
    }

    _brand = sellerProduct.brand;
  }

  @override
  dispose() {
    specificationsBloc.dispose();
    _englishNameController.dispose();
    _descriptionController.dispose();
    _productCategoryController.dispose();
    selectLeafCategoryBloc.categoryNotifier.value = '';
    _values.clear();
    _ids.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          PageTitle(
            title: 'Edit Product',
            ontopNavButtonPressed: () {
              List<Map<String, dynamic>> specificationsParams = [];
              for (var i = 0; i < _ids.length; i++) {
                specificationsParams.add({
                  'id': _ids[i],
                  'value': _values[i] ?? '',
                });
              }
              specificationsParams
                  .removeWhere((element) => element['value'].isEmpty);

              productBloc.setSpecifications(specificationsParams);
              productBloc.setProductNameEnglish(_englishNameController.text);
              productBloc.setDescription(_descriptionController.text);
              productBloc.setPrice(_priceController.text);
              productBloc.setUnit(_unitController.text);
              productBloc.setBrandId(_brand?.id);
              productBloc.updateProduct(
                context,
                widget.sellerProduct.id,
                _imagePaths,
              );
            },
            showTopNavButton: true,
            navbuttontext: 'Save',
          ),
          const SizedBox(height: 10),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 4,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      mainAxisExtent: MediaQuery.of(context).size.height / 6,
                    ),
                    itemBuilder: (context, index) {
                      return Center(
                        child: GestureDetector(
                          onTap: () => _showPicker(context, index),
                          onLongPress: () {
                            _images[index] = null;
                            _productImages[index] = ProductImage();
                            _imagePaths[index] = null;
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: _images[index] == null
                                ? SellerProductCachedImage(
                                    path: _productImages[index]?.path,
                                  )
                                : UserPickedImage(
                                    image: _images[index]!,
                                  ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 40),
                CustomTextFormField(
                  labelText: 'Enter Product Name in English',
                  controller: _englishNameController,
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      child: CustomTextFormField(
                        labelText: 'Price',
                        controller: _priceController,
                        textInputType: const TextInputType.numberWithOptions(
                          decimal: true,
                        ),
                        textInputFormatterList: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(
                            RegExp(r'^\d+\.?\d{0,3}'),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: CustomTextFormField(
                        labelText: 'Unit',
                        controller: _unitController,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 27, vertical: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CustomLabelText(labelText: 'Select Brand'),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 8),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: const Color.fromRGBO(218, 216, 216, 1),
                          ),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: StreamBuilder<AllBrandsResponse>(
                          stream: brandBloc.allBrandsResponse.stream,
                          builder: (context, snapshot) {
                            var brands = snapshot.data?.brands;
                            if (brands != null && brands.isNotEmpty) {
                              return ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<Brand>(
                                  hint: const Text('Select One'),
                                  underline: Container(),
                                  elevation: 0,
                                  isExpanded: true,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  value: _brand,
                                  onChanged: (value) {
                                    _brand = value;
                                    setState(() {});

                                    FocusScope.of(context).unfocus();
                                  },
                                  items: brands.map((value) {
                                    return DropdownMenuItem(
                                      value: value,
                                      child: Text(value.name),
                                    );
                                  }).toList(),
                                ),
                              );
                            }

                            return Container();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    selectLeafCategoryBloc.setEditMode(true);
                    _values.clear();
                    Navigator.pushNamed(
                      context,
                      RouteName.selectRootCategoryScreen,
                    );
                  },
                  child: ValueListenableBuilder(
                    builder: (context, value, _) {
                      var categoryName = value as String;
                      if (categoryName.isNotEmpty) {
                        _productCategoryController.text = categoryName;
                      }
                      if (categoryName.isEmpty) {
                        productBloc.setCategoryId(_productCategory!.id);
                      }
                      return CustomTextFormField(
                        labelText: 'Select Product Category',
                        controller: _productCategoryController,
                        enabled: false,
                      );
                    },
                    valueListenable: selectLeafCategoryBloc.categoryNotifier,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                CustomTextFormField(
                  textFieldHeight: 150,
                  labelText: 'Enter Product Description',
                  controller: _descriptionController,
                  maxLines: 10,
                ),
                const SizedBox(
                  height: 10,
                ),
                StreamBuilder<List<Specification>>(
                  stream: specificationsBloc.specifications.stream,
                  builder: (context, snapshot) {
                    _ids.clear();
                    var specifications = snapshot.data;
                    if (specifications != null && specifications.isNotEmpty) {
                      for (var specification in specifications) {
                        _ids.add(specification.id);
                        for (var specification in _specifications) {
                          if (specification.id == specification.id) {
                            specification.value = specification.value;
                          }
                        }
                        _values.add(specification.value);
                      }
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: specifications.length,
                        itemBuilder: (context, index) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Visibility(
                                visible: index == 0,
                                child: const Padding(
                                  padding:
                                      EdgeInsets.only(left: 20, bottom: 10),
                                  child: Text(
                                    'Product Specifications',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              CustomTextFormField(
                                key: Key(specifications[index].id.toString()),
                                labelText: specifications[index].name,
                                initialValue: specifications[index].value,
                                onChanged: (value) {
                                  _values[index] = value;
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }
                    return Container();
                  },
                ),
                const SizedBox(
                  height: 40,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
