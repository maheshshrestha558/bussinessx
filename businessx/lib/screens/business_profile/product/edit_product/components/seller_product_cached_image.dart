import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SellerProductCachedImage extends StatelessWidget {
  final String? path;
  const SellerProductCachedImage({
    Key? key,
    required this.path,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: CachedNetworkImage(
        placeholder: (context, url) => const FittedBox(
          child: CircularProgressIndicator(
            strokeWidth: 1,
          ),
        ),
        imageUrl: path ?? 'https://insightfulnepal.com/img/no-image.png',
        fit: BoxFit.fill,
        width: MediaQuery.of(context).size.width / 2,
        height: 100,
        errorWidget: (context, url, error) => const Icon(Icons.error),
      ),
    );
  }
}