import 'dart:io';

import 'package:flutter/material.dart';

class UserPickedImage extends StatelessWidget {
  final File image;
  const UserPickedImage({
    Key? key,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Image.file(
        image,
        fit: BoxFit.fill,
        height: 100,
        width: MediaQuery.of(context).size.width / 2,
      ),
    );
  }
}