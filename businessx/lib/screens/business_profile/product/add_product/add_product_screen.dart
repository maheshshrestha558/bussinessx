import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import '/bloc/bloc.dart';
import '/common/decoration.dart';
import '/common/widgets/widgets.dart';
import '/models/models.dart' hide Category;
import '/models/response/response.dart';
import '../../../../models/response/categories_response.dart';

class AddProductScreen extends StatefulWidget {
  final Category category;
  const AddProductScreen({required this.category, Key? key}) : super(key: key);

  @override
  State<AddProductScreen> createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  final _englishNameController = TextEditingController();
  final _priceController = TextEditingController();
  final _unitController = TextEditingController();
  final _controllers = <TextEditingController?>[];
  final _descriptionController = TextEditingController();

  final List<Specification> _specifications = [];

  Brand? _brand;
  final _values = <String?>[];
  final _keys = <int?>[];

  final ImagePicker _picker = ImagePicker();
  final List<File?> _images = List.filled(4, null);

  @override
  initState() {
    super.initState();
    _controllers.clear();
    brandBloc.fetchAll();
    _specifications.addAll(widget.category.specifications);
  }

  Future _img(ImageSource source, index) async {
    try {
      final image = await _picker.pickImage(source: source, imageQuality: 95);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() => _images[index] = imageTemporary);
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image $e');
    }
  }

  void _showPicker(context, index) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Photo Library'),
                onTap: () {
                  _img(ImageSource.gallery, index);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () {
                  _img(ImageSource.camera, index);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  dispose() {
    for (var controller in _controllers) {
      controller?.dispose();
    }
    _englishNameController.dispose();
    _descriptionController.dispose();
    _specifications.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PageTitle(
            title: 'Add Product',
            navbuttontext: 'ADD',
            showTopNavButton: true,
            ontopNavButtonPressed: () {
              for (var controller in _controllers) {
                _values.add(controller?.text);
              }
              List<Map<String, dynamic>> specifications = [];
              for (var i = 0; i < _keys.length; i++) {
                specifications.add({
                  'id': _keys[i],
                  'value': _values[i],
                });
              }

              specifications.removeWhere(
                (element) =>
                    element['value'] == null || element['value'].isEmpty,
              );

              productBloc
                ..setProductNameEnglish(_englishNameController.text)
                ..setPrice(_priceController.text)
                ..setUnit(_unitController.text)
                ..setBrandId(_brand?.id)
                ..setCategoryId(widget.category.id)
                ..setSpecifications(specifications)
                ..setDescription(_descriptionController.text)
                ..createProduct(context, _images);
            },
          ),
          const SizedBox(height: 20),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Text(
                    'Add Photos (Max 4 Photos)',
                    style: TextStyle(fontSize: 14, color: Colors.black54),
                  ),
                ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _images.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      mainAxisExtent: MediaQuery.of(context).size.height / 6,
                    ),
                    itemBuilder: (context, index) {
                      return Center(
                        child: GestureDetector(
                          onTap: () => _showPicker(context, index),
                          onLongPress: () {
                            _images[index] = null;
                            setState(() {});
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey, width: 1),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: _images[index] == null
                                ? const Center(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Add Photo',
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Text(
                                          'Long press to remove',
                                          style: TextStyle(
                                            fontSize: 10,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : Image.file(
                                    _images[index]!,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    fit: BoxFit.fill,
                                  ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20),
                CustomTextFormField(
                  controller: _englishNameController,
                  labelText: 'Name',
                ),
                const SizedBox(
                  height: 10,
                ),
                const SizedBox(
                  height: 10,
                ),
                CustomTextFormField(
                  controller: _priceController,
                  labelText: 'Price',
                  textInputType: const TextInputType.numberWithOptions(
                    decimal: true,
                  ),
                  textInputFormatterList: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,3}'))
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                CustomTextFormField(
                  controller: _unitController,
                  labelText: 'Unit',
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 27, vertical: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const CustomLabelText(labelText: 'Select Brand'),
                      const SizedBox(
                        height: 5.0,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 8),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: const Color.fromRGBO(218, 216, 216, 1),
                          ),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: StreamBuilder<AllBrandsResponse>(
                          stream: brandBloc.allBrandsResponse.stream,
                          builder: (context, snapshot) {
                            var brands = snapshot.data?.brands;
                            if (brands != null && brands.isNotEmpty) {
                              return ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<Brand>(
                                  hint: const Text('Select One'),
                                  underline: Container(),
                                  elevation: 0,
                                  isExpanded: true,
                                  icon: const Icon(Icons.arrow_drop_down),
                                  value: _brand,
                                  onChanged: (value) {
                                    _brand = value;

                                    FocusScope.of(context).unfocus();

                                    setState(() {});
                                  },
                                  items: brands.map((value) {
                                    return DropdownMenuItem(
                                      value: value,
                                      child: Text(value.name),
                                    );
                                  }).toList(),
                                ),
                              );
                            }

                            return Container();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                CustomTextFormField(
                  textFieldHeight: 150,
                  labelText: 'Enter Product Description',
                  controller: _descriptionController,
                  maxLines: 10,
                ),
                ListView.builder(
                  padding: const EdgeInsets.all(8),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: _specifications.length,
                  itemBuilder: (context, index) {
                    if (_specifications.isNotEmpty) {
                      _keys.add(_specifications[index].id);
                      debugPrint(_keys.toString());
                      var specification = _specifications[index];
                      _controllers.add(TextEditingController());
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: index == 0,
                            child: const Padding(
                              padding: EdgeInsets.only(left: 16, bottom: 10),
                              child: Text(
                                'Product Specification',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                          CustomTextFormField(
                            labelText: specification.name,
                            controller: _controllers[index],
                          )
                        ],
                      );
                    }

                    return const SizedBox.shrink();
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
