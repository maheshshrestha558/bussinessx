import 'package:flutter/material.dart';

import '../../../../bloc/bloc.dart';
import '../../../../common/widgets/widgets.dart';
import '../../../../models/response/categories_response.dart';
import '../../../../route_names.dart';
import 'components/category_list_tile.dart';

class SelectRootCategoryScreen extends StatefulWidget {
  const SelectRootCategoryScreen({Key? key}) : super(key: key);

  @override
  State<SelectRootCategoryScreen> createState() =>
      _SelectRootCategoryScreenState();
}

class _SelectRootCategoryScreenState extends State<SelectRootCategoryScreen> {
  late ScrollController _scrollController;
  int _page = 1;

  @override
  initState() {
    super.initState();
    _scrollController = ScrollController();
    selectRootCategoryBloc.getRootCategories(0, _page);
  }

  @override
  void didChangeDependencies() {
    _scrollController.addListener(() {
      double currentPosition = _scrollController.position.pixels;
      double maxScrollExtent = _scrollController.position.maxScrollExtent;
      if (currentPosition == maxScrollExtent) {
        if (selectRootCategoryBloc.lastPage != _page) {
          _page++;
          selectRootCategoryBloc.getRootCategories(0, _page);
        }
      }
    });
    super.didChangeDependencies();
  }

  @override
  dispose() {
    _scrollController.dispose();
    selectRootCategoryBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const PageTitle(
            title: 'Root Categories',
          ),
          Expanded(
            child: StreamBuilder<List<Category>>(
              stream: selectRootCategoryBloc.rootCategories.stream,
              builder: (context, snapshot) {
                var categories = snapshot.data;
                if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error.toString()));
                }
                if (categories == null) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (categories.isEmpty) {
                  return const Center(child: Text('No Categories Found'));
                }
                return ListView.builder(
                  controller: _scrollController,
                  physics: const BouncingScrollPhysics(),
                  itemCount: categories.length,
                  itemBuilder: (context, index) {
                    var category = categories[index];
                    return CategoryListTile(
                      category: category,
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          RouteName.selectSubCategoryScreen,
                          arguments: category,
                        );
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
