import 'package:flutter/material.dart';

import '../../../../bloc/bloc.dart';
import '../../../../common/decoration.dart';
import '../../../../common/widgets/widgets.dart';
import '../../../../models/response/categories_response.dart';
import '../../../../route_names.dart';
import 'components/category_list_tile.dart';

class SelectChildCategoryScreen extends StatefulWidget {
  final Category category;
  const SelectChildCategoryScreen({Key? key, required this.category})
      : super(key: key);

  @override
  State<SelectChildCategoryScreen> createState() =>
      _SelectChildCategoryScreenState();
}

class _SelectChildCategoryScreenState extends State<SelectChildCategoryScreen> {
  late ScrollController _scrollController;
  int _page = 1;

  @override
  initState() {
    super.initState();
    _scrollController = ScrollController();
    selectChildCategoryBloc.getChildCategories(widget.category.id, _page);
  }

  @override
  void didChangeDependencies() {
    _scrollController.addListener(() {
      double currentPosition = _scrollController.position.pixels;
      double maxScrollExtent = _scrollController.position.maxScrollExtent;
      if (currentPosition == maxScrollExtent) {
        if (selectChildCategoryBloc.lastPage != _page) {
          _page++;
          selectChildCategoryBloc.getChildCategories(widget.category.id, _page);
        }
      }
    });
    super.didChangeDependencies();
  }

  @override
  dispose() {
    _scrollController.dispose();
    selectChildCategoryBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Container(
            decoration: appBarDecoration,
          ),
          title: const CustomSearchBox(),
          titleSpacing: 0.2,
        ),
      ),
      body: Column(
        children: [
          const PageTitle(
            title: 'Child Categories',
          ),
          CategoryListTile(
            category: widget.category,
            isSelected: true,
            onTap: () {
              Navigator.pop(context);
            },
          ),
          Expanded(
            child: StreamBuilder<List<Category>>(
              stream: selectChildCategoryBloc.childCategories.stream,
              builder: (context, snapshot) {
                var categories = snapshot.data;
                if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error.toString()));
                }
                if (categories == null) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (categories.isEmpty) {
                  return const Center(child: Text('No Categories Found'));
                }
                return ListView.builder(
                  controller: _scrollController,
                  physics: const BouncingScrollPhysics(),
                  itemCount: categories.length,
                  itemBuilder: (context, index) {
                    var category = categories[index];
                    return CategoryListTile(
                      category: category,
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          RouteName.selectLeafCategoryScreen,
                          arguments: category,
                        );
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
