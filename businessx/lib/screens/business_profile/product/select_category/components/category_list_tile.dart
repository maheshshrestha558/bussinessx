import 'package:flutter/material.dart';

import '../../../../../models/response/categories_response.dart';

class CategoryListTile extends StatelessWidget {
  final Category category;
  final void Function()? onTap;
  final bool isSelected;
  const CategoryListTile({
    required this.category,
    required this.onTap,
    this.isSelected = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        selected: isSelected,
        selectedColor: const Color.fromARGB(255, 255, 255, 255),
        // tileColor: const Color.fromARGB(255, 255, 145, 145),
        selectedTileColor: const Color.fromARGB(255, 192, 194, 193),
        shape: isSelected
            ? const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                side: BorderSide(
                  color: Color.fromARGB(255, 139, 139, 139),
                  width: 1,
                ),
              )
            : null,
        trailing: isSelected
            ? const Icon(
                Icons.keyboard_arrow_left,
                size: 30,
              )
            : const Icon(Icons.keyboard_arrow_right),
        title: Text(
          category.name,
          style: TextStyle(fontSize: isSelected ? 16 : 12),
        ),
        onTap: onTap,
      ),
    );
  }
}
