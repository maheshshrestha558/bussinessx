import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../../models/business.dart';

class BusinessPicture extends StatelessWidget {
  const BusinessPicture({
    Key? key,
    required this.business,
  }) : super(key: key);

  final Business business;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: CachedNetworkImage(
        height: 200,
        placeholder: (context, url) => Center(
          child: Text(business.name[0]),
        ),
        imageUrl: business.imageThumbnail,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        errorWidget: (context, url, error) => Center(
          child: Text(business.name[0]),
        ),
      ),
    );
  }
}