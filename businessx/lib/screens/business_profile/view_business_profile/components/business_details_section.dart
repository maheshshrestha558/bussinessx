import 'package:flutter/material.dart';

import '../../../../bloc/business_bloc.dart';
import '../../../../common/widgets/widgets.dart';
import '../../../../models/business.dart';
import '../../../../models/response/response.dart';

class BusinessDetailsSection extends StatelessWidget {
  const BusinessDetailsSection({
    Key? key,
    required this.business,
  }) : super(key: key);

  final Business business;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.black),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DescriptionField(
              labelText: 'Business Name (English)',
              description: business.enName,
            ),
            DescriptionField(
              labelText: "CEO's Name",
              description: business.ceoName,
            ),
            DescriptionField(
              labelText: 'Contact Number',
              description: business.contactNumber,
            ),
            DescriptionField(
              labelText: 'Alternate Contact Number',
              description: business.alternateContactNumber,
            ),
            DescriptionField(
              labelText: 'Address',
              description: business.address,
            ),
            DescriptionField(
              labelText: 'Email Address',
              description: business.email,
            ),
            DescriptionField(
              labelText: 'Import/Export Code',
              description: business.importExportCode != null
                  ? business.importExportCode.toString()
                  : '- - - - -',
            ),
            DescriptionField(
              labelText: 'Pan no.',
              description:
                  business.pan != null ? business.pan.toString() : '- - - - -',
            ),
            DescriptionField(
              labelText: 'Website URL',
              description: business.websiteUrl,
            ),
            DescriptionField(
              labelText: 'Facebook URL',
              description: business.facebookUrl,
            ),
            DescriptionField(
              labelText: 'Instagram URL',
              description: business.instagramUrl,
            ),
            DescriptionField(
              labelText: 'Whatsapp Number',
              description: business.whatsappNumber,
            ),
            DescriptionField(
              labelText: 'Established Date',
              description: business.establishedDate.toString(),
            ),
            StreamBuilder<AllBusinessTypeResponse>(
              stream: businessesBloc.allBusinessTypes.stream,
              builder: (context, snapshot) {
                var businessTypes = snapshot.data?.businessTypes;
                if (businessTypes != null && businessTypes.isNotEmpty) {
                  var businessType = businessTypes.firstWhere(
                    (type) => type.id == business.businessTypeId,
                  );
                  return DescriptionField(
                    labelText: 'Business Type',
                    description: businessType.name,
                  );
                }
                return const DescriptionField(
                  labelText: 'Business Type',
                  description: '- - - - -',
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
