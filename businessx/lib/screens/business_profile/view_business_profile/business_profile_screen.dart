import 'package:flutter/material.dart';

import '../../../common/widgets/widgets.dart';
import '../../../models/models.dart';
import '../../../route_names.dart';
import 'components/business_details_section.dart';
import 'components/business_picture.dart';

class BusinessProfileScreen extends StatelessWidget {
  const BusinessProfileScreen({Key? key, required this.business})
      : super(key: key);
  final Business business;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(height: 10),
          PageTitle(
            title: business.name,
            showTopNavButton: true,
            navbuttontext: 'Edit',
            ontopNavButtonPressed: () {
              Navigator.pushNamed(
                context,
                RouteName.editBusinessProfileScreen,
                arguments: business,
              );
            },
          ),
          Expanded(
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                BusinessPicture(business: business),
                BusinessDetailsSection(
                  business: business,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
