import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

import '../../../bloc/business_bloc.dart';
import '../../../common/extensions/string_extensions.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/business.dart';
import '../../../models/business_type.dart';
import '../../../models/response/business_type/all_business_type_response.dart';

class EditBusinessProfile extends StatefulWidget {
  final Business business;
  const EditBusinessProfile({
    Key? key,
    required this.business,
  }) : super(key: key);

  @override
  State<EditBusinessProfile> createState() => _EditBusinessProfileState();
}

class _EditBusinessProfileState extends State<EditBusinessProfile> {
  final _enNameController = TextEditingController();
  final _ceoNameController = TextEditingController();
  final _contactNumberController = TextEditingController();
  final _alternateContactNumberController = TextEditingController();
  final _addressController = TextEditingController();
  final _emailController = TextEditingController();
  final _importCodeController = TextEditingController();
  final _panNumberController = TextEditingController();
  final _websiteUrlController = TextEditingController();
  final _facebookUrlController = TextEditingController();
  final _instaUrlController = TextEditingController();
  final _whatsappController = TextEditingController();
  final _establishedDateController = TextEditingController();
  DateTime? _selectedEstablishedDate;
  final _editSellerProfileFormKey = GlobalKey<FormState>();
  BusinessType? _businessType;

  final ImagePicker _picker = ImagePicker();

  File? image;

  Future _img(ImageSource source) async {
    try {
      final image = await _picker.pickImage(source: source, imageQuality: 95);
      if (image == null) return;
      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      debugPrint('Failed to pick image $e');
    }
  }

  void _showPicker(context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Photo Library'),
                onTap: () {
                  _img(ImageSource.gallery);
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_camera),
                title: const Text('Camera'),
                onTap: () {
                  _img(ImageSource.camera);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _enNameController.text = widget.business.enName;
    _ceoNameController.text = widget.business.ceoName;
    _contactNumberController.text = widget.business.contactNumber;
    _alternateContactNumberController.text =
        widget.business.alternateContactNumber.returnEmptyStringIfNull();
    _addressController.text = widget.business.address;
    _emailController.text = widget.business.email;
    _importCodeController.text = widget.business.importExportCode.toString();
    _panNumberController.text = widget.business.pan.toString();
    _websiteUrlController.text =
        widget.business.websiteUrl.returnEmptyStringIfNull();
    _facebookUrlController.text =
        widget.business.facebookUrl.returnEmptyStringIfNull();
    _instaUrlController.text =
        widget.business.instagramUrl.returnEmptyStringIfNull();
    _whatsappController.text =
        widget.business.whatsappNumber.returnEmptyStringIfNull();
    _selectedEstablishedDate = DateTime.parse(widget.business.establishedDate);
    _establishedDateController.text =
        _selectedEstablishedDate.toString().substring(0, 10);
  }

  @override
  void dispose() {
    _enNameController.dispose();
    _ceoNameController.dispose();
    _contactNumberController.dispose();
    _alternateContactNumberController.dispose();
    _addressController.dispose();
    _emailController.dispose();
    _importCodeController.dispose();
    _panNumberController.dispose();
    _websiteUrlController.dispose();
    _facebookUrlController.dispose();
    _instaUrlController.dispose();
    _whatsappController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(height: 10),
          PageTitle(
            title: 'Business Profile',
            showTopNavButton: true,
            navbuttontext: 'Save',
            ontopNavButtonPressed: () {
              if (_editSellerProfileFormKey.currentState!.validate()) {
                businessesBloc.updateBusiness(
                  context,
                  widget.business.id,
                  {
                    'name': _enNameController.text,
                    'en_name': _enNameController.text,
                    'ceo_name': _ceoNameController.text,
                    'contact_number': _contactNumberController.text,
                    'alternate_contact_number':
                        _alternateContactNumberController.text,
                    'address': _addressController.text,
                    'email': _emailController.text,
                    'pan': _panNumberController.text,
                    'import_export_code': _importCodeController.text,
                    'established_date': _establishedDateController.text,
                    'whatsapp_number': _whatsappController.text,
                    'website_url': _websiteUrlController.text,
                    'facebook_url': _facebookUrlController.text,
                    'instagram_url': _instaUrlController.text,
                    'business_type_id': _businessType != null
                        ? _businessType?.id
                        : widget.business.businessTypeId
                  },
                  image,
                );
              }
            },
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Form(
                key: _editSellerProfileFormKey,
                child: Expanded(
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    children: [
                      const SizedBox(height: 10),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            _showPicker(context);
                          },
                          child: Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                width: 2,
                                color: const Color(0xff77dce1),
                              ),
                            ),
                            child: image != null
                                ? ClipOval(
                                    child: Image.file(
                                      image!,
                                      width: 90,
                                      height: 90,
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : Center(
                                    child: Container(
                                      height: 90,
                                      width: 90,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: const Color(0xff77dce1),
                                          width: 2,
                                        ),
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                      ),
                                      child: CachedNetworkImage(
                                        placeholder: (context, url) => Center(
                                          child: Text(
                                            widget.business.enName
                                                .getCapitalizedFirstLetter(),
                                          ),
                                        ),
                                        imageUrl:
                                            widget.business.imageThumbnail,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          padding: const EdgeInsets.all(10),
                                          height: 90,
                                          width: 90,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Center(
                                          child: Text(
                                            widget.business.enName
                                                .getCapitalizedFirstLetter(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.only(left: 75.0, right: 75.0),
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            foregroundColor:
                                const Color.fromRGBO(29, 78, 216, 1),
                            backgroundColor: Colors.white,
                            shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                            ),
                            side: const BorderSide(
                              color: Color.fromRGBO(29, 78, 216, 1),
                              width: 3,
                            ),
                          ),
                          onPressed: () => _showPicker(context),
                          child: const Text('Add Logo'),
                        ),
                      ),
                      const SizedBox(height: 30),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomTextFormField(
                            labelText: 'Business Name (English)',
                            controller: _enNameController,
                          ),
                          const SizedBox(height: 5),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'CEO’s Name',
                            controller: _ceoNameController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Contact Number',
                            controller: _contactNumberController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Alternate Contact Number (Optional)',
                            controller: _alternateContactNumberController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Address',
                            controller: _addressController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Email Address',
                            controller: _emailController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Import/ Export Code',
                            controller: _importCodeController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Pan No.',
                            controller: _panNumberController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Website URL',
                            controller: _websiteUrlController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Facebook URL',
                            controller: _facebookUrlController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Instagram URL',
                            controller: _instaUrlController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Whatsapp Number',
                            controller: _whatsappController,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomTextFormField(
                            labelText: 'Established Date',
                            readOnly: true,
                            controller: _establishedDateController,
                            onTap: () {
                              showCupertinoModalPopup(
                                context: context,
                                builder: (context) => Material(
                                  child: SizedBox(
                                    height: 200,
                                    child: CupertinoDatePicker(
                                      mode: CupertinoDatePickerMode.date,
                                      dateOrder: DatePickerDateOrder.ymd,
                                      initialDateTime: _selectedEstablishedDate,
                                      maximumDate: DateTime.now(),
                                      onDateTimeChanged: (dateTime) {
                                        _establishedDateController.text =
                                            dateTime
                                                .toString()
                                                .substring(0, 10);
                                        _selectedEstablishedDate = dateTime;
                                      },
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 20,
                              vertical: 5,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const CustomLabelText(
                                  labelText: 'Business Type',
                                ),
                                const SizedBox(
                                  height: 5.0,
                                ),
                                StreamBuilder<AllBusinessTypeResponse>(
                                  stream:
                                      businessesBloc.allBusinessTypes.stream,
                                  builder: (context, snapshot) {
                                    var businessTypes =
                                        snapshot.data?.businessTypes;
                                    if (businessTypes != null &&
                                        businessTypes.isNotEmpty) {
                                      return ButtonTheme(
                                        alignedDropdown: true,
                                        child: DropdownButtonFormField<
                                            BusinessType>(
                                          hint: const Text('Select One'),
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineMedium!
                                              .copyWith(color: Colors.black),
                                          decoration: const InputDecoration(
                                            contentPadding: EdgeInsets.only(
                                              left: 5.0,
                                              right: 15.0,
                                              top: 10,
                                              bottom: 10,
                                            ),
                                            filled: true,
                                            fillColor: Color(0xffffffff),
                                            border: border,
                                            focusedBorder: focusedBorder,
                                            enabledBorder: enabledBorder,
                                          ),
                                          elevation: 0,
                                          isExpanded: true,
                                          icon:
                                              const Icon(Icons.arrow_drop_down),
                                          value: _businessType ??
                                              businessTypes.firstWhere(
                                                (element) =>
                                                    element.id ==
                                                    widget.business
                                                        .businessTypeId,
                                              ),
                                          onChanged: (value) {
                                            _businessType = value;
                                            setState(() {});
                                          },
                                          items: businessTypes.map((value) {
                                            return DropdownMenuItem(
                                              value: value,
                                              child: Text(value.name),
                                            );
                                          }).toList(),
                                        ),
                                      );
                                    }

                                    return Container();
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
