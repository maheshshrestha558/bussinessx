import 'package:flutter/material.dart';

import '../../../bloc/about_us_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/about_us/about_us_response.dart';
import '../../../route_names.dart';

class StorePageScreen extends StatefulWidget {
  final int verificationStatus;
  const StorePageScreen({Key? key, required this.verificationStatus})
      : super(key: key);

  @override
  State<StorePageScreen> createState() => _StorePageScreenState();
}

class _StorePageScreenState extends State<StorePageScreen> {
  AboutUsInfo? _aboutUsInfo;

  @override
  void initState() {
    super.initState();
    aboutUsBloc.getAboutUsInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(height: 10),
          PageTitle(
            title: 'Store Page',
            navbuttontext: 'Edit',
            ontopNavButtonPressed: () {
              Navigator.pushNamed(
                context,
                RouteName.editAboutUsScreen,
                arguments: _aboutUsInfo,
              );
            },
            showTopNavButton: true,
          ),
          widget.verificationStatus == 1
              ? StreamBuilder<AboutUsResponse>(
                  stream: aboutUsBloc.aboutUsInfo.stream,
                  builder: (context, AsyncSnapshot<AboutUsResponse> snapshot) {
                    if (!snapshot.hasData) {
                      return const Center(
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.green),
                        ),
                      );
                    }
                    _aboutUsInfo = snapshot.data?.aboutUsInfo;
                    if (snapshot.hasData) {
                      return Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 16.0,
                            vertical: 8,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: Colors.black,
                                width: 1,
                              ),
                            ),
                            child: ListView(
                              shrinkWrap: true,
                              physics: const BouncingScrollPhysics(),
                              children: [
                                const SizedBox(height: 20),
                                DescriptionField(
                                  labelText: 'Profile Heading',
                                  description: _aboutUsInfo?.profileHeading ??
                                      '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'Description',
                                  description:
                                      _aboutUsInfo?.profileDescription ??
                                          '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'No. of Employee',
                                  description: _aboutUsInfo?.numberOfEmployees
                                          .toString() ??
                                      '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'Annual Turnover',
                                  description: _aboutUsInfo?.annualTurnover ??
                                      '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'Packaging Description',
                                  description: _aboutUsInfo?.packagingMode ??
                                      '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'Payment Mode',
                                  description:
                                      _aboutUsInfo?.paymentMode ?? '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'Shipment Mode',
                                  description:
                                      _aboutUsInfo?.shipmentMode ?? '- - - - -',
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DescriptionField(
                                  labelText: 'Legal Status of Firm',
                                  description:
                                      _aboutUsInfo?.legalStatus ?? '- - - - -',
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    } else {
                      return const NoAvatarProfileShimmer();
                    }
                  },
                )
              : const Center(
                  child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Text(
                      'Need To be Verified Before Updating Store Page',
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
