import 'package:flutter/material.dart';

import '../../../bloc/about_us_bloc.dart';
import '../../../common/widgets/widgets.dart';
import '../../../models/response/about_us/about_us_response.dart';

class EditStorePageScreen extends StatefulWidget {
  final AboutUsInfo? aboutUsInfo;

  const EditStorePageScreen({
    Key? key,
    required this.aboutUsInfo,
  }) : super(key: key);

  @override
  State<EditStorePageScreen> createState() => _EditStorePageScreenState();
}

class _EditStorePageScreenState extends State<EditStorePageScreen> {
  late AboutUsInfo? _aboutUsInfo;
  final _profileHeadingController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _numberOfEmployeeController = TextEditingController();
  final _annualTurnOverController = TextEditingController();
  final _packagingDescriptionController = TextEditingController();
  final _paymentMethodController = TextEditingController();
  final _shipmentMethodController = TextEditingController();
  final _statusController = TextEditingController();

  final _aboutUsFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _aboutUsInfo = widget.aboutUsInfo;
    _profileHeadingController.text = _aboutUsInfo?.profileHeading ?? '';
    _descriptionController.text = _aboutUsInfo?.profileDescription ?? '';
    _numberOfEmployeeController.text =
        _aboutUsInfo?.numberOfEmployees?.toString() ?? '';
    _annualTurnOverController.text = _aboutUsInfo?.annualTurnover ?? '';
    _packagingDescriptionController.text = _aboutUsInfo?.packagingMode ?? '';
    _paymentMethodController.text = _aboutUsInfo?.paymentMode ?? '';
    _shipmentMethodController.text = _aboutUsInfo?.shipmentMode ?? '';
    _statusController.text = _aboutUsInfo?.legalStatus ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MyAppDrawer(),
      appBar: const CustomAppBar(),
      body: Column(
        children: [
          const SizedBox(height: 10),
          PageTitle(
            title: 'About Us',
            showTopNavButton: true,
            navbuttontext: 'Save',
            ontopNavButtonPressed: () {
              if (_aboutUsFormKey.currentState!.validate()) {
                aboutUsBloc.updateAboutUsInfo(
                  context,
                  {
                    'profile_heading': _profileHeadingController.text,
                    'profile_description': _descriptionController.text,
                    'number_of_employees': _numberOfEmployeeController.text,
                    'annual_turnover': _annualTurnOverController.text,
                    'packaging_mode': _packagingDescriptionController.text,
                    'payment_mode': _paymentMethodController.text,
                    'shipment_mode': _shipmentMethodController.text,
                    'legal_status': _statusController.text
                  },
                );
              }
            },
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Form(
                key: _aboutUsFormKey,
                child: Expanded(
                  child: ListView(
                    physics: const BouncingScrollPhysics(),
                    children: [
                      const SizedBox(height: 30),
                      CustomTextFormField(
                        labelText: 'Profile Heading',
                        controller: _profileHeadingController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        labelText: 'Enter Your Description',
                        maxLines: 10,
                        textFieldHeight: 200,
                        controller: _descriptionController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        labelText: 'No. of Employee',
                        textInputType: TextInputType.number,
                        controller: _numberOfEmployeeController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        labelText: 'Annual Turnover',
                        controller: _annualTurnOverController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        maxLines: 5,
                        textFieldHeight: 80,
                        controller: _packagingDescriptionController,
                        labelText: 'Enter Packaging Description',
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        labelText: 'Select Payment Modes',
                        controller: _paymentMethodController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        labelText: 'Select Shipment Modes',
                        controller: _shipmentMethodController,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      CustomTextFormField(
                        labelText: 'Legal Status of Firm',
                        controller: _statusController,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
