import '../../bloc/dashboard_bloc.dart';
import '../../common/widgets/widgets.dart';
import '../../models/response/dashboard_response.dart';
import 'package:flutter/material.dart';

import 'components/stats_widget.dart';

class ProductAndLeadStats extends StatefulWidget {
  const ProductAndLeadStats({
    Key? key,
  }) : super(key: key);

  @override
  State<ProductAndLeadStats> createState() => _ProductAndLeadStatsState();
}

class _ProductAndLeadStatsState extends State<ProductAndLeadStats> {
  @override
  void initState() {
    dashBoardBloc.getDashboardInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DashboardResponse>(
        stream: dashBoardBloc.dashboardInfo.stream,
        builder: (context, AsyncSnapshot<DashboardResponse> snapshot) {
          var info = snapshot.data?.dashboard;

          if (info != null) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                StatsWidget(
                  value: info.productsCount,
                  label: 'Products',
                ),
                StatsWidget(
                  value: info.productsViewCount,
                  label: 'Views',
                ),
                StatsWidget(
                  value: info.leadsCount,
                  label: 'Leads',
                ),
              ],
            );
          }

          return const ProductAndLeadStatsShimmer();
        },);
  }
}
