import 'package:flutter/material.dart';

import '../../../common/widgets/widgets.dart';
import '../../../models/packages.dart';

class SingleSubscriptionScreen extends StatefulWidget {
  final AllPackages packages;

  const SingleSubscriptionScreen({
    Key? key,
    required this.packages,
  }) : super(key: key);

  static const routeName = 'singleSubscriptionScreen';

  @override
  State<SingleSubscriptionScreen> createState() =>
      _SingleSubscriptionScreenState();
}

class _SingleSubscriptionScreenState extends State<SingleSubscriptionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      drawer: const MyAppDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            PageTitle(
              title: widget.packages.name!,
              navbuttontext: 'Call Us',
              showTopNavButton: true,
            ),
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 150,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          widget.packages.imageThumbnail!,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: ListView.builder(
                      // physics: const NeverScrollableScrollPhysics(),
                      itemCount: widget.packages.packageFeatures?.length,
                      itemBuilder: (context, index) {
                        var package = widget.packages.packageFeatures![index];
                        return Expanded(
                          child: ListTile(
                            dense: true,
                            // leading: const Icon(
                            //   Icons.arrow_right_alt,
                            //   size: 12,
                            // ),
                            title: Text(
                              package.enName!,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 5,
                              style: const TextStyle(
                                fontSize: 12,
                              ),
                            ),
                            subtitle: Text(
                              package.enName!,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
