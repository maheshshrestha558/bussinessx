// import 'dart:html';

import 'package:b2b_market/common/widgets/widgets.dart';
import 'package:b2b_market/models/packages.dart';
import 'package:b2b_market/route_names.dart';
import 'package:flutter/material.dart';

import '../../../bloc/package_bloc.dart';

class PackagesScreen extends StatefulWidget {
  const PackagesScreen({Key? key}) : super(key: key);

  static const routeName = 'packagesScreen';

  @override
  State<PackagesScreen> createState() => _PackagesScreenState();
}

class _PackagesScreenState extends State<PackagesScreen>
    with SingleTickerProviderStateMixin {
  final _packageBloc = PackageBloc();
  // final data = dataFromJson(jsonString);
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _packageBloc.getPackages();
    _tabController = TabController(initialIndex: 0, length: 2, vsync: this);
  }

  @override
  void dispose() {
    // _packageBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(),
      drawer: const MyAppDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const PageTitle(title: 'Packages'),
            StreamBuilder<PackagesResponse>(
              stream: _packageBloc.packagesResponse,
              builder: ((context, snapshot) {
                var packagesResponse = snapshot.data;
                if (packagesResponse == null) {
                  return const Center(child: CircularProgressIndicator());
                }
                if (packagesResponse.error != null) {
                  return Center(
                    child: Text(packagesResponse.error!),
                  );
                }

                return Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: SizedBox(
                          height: 45,
                          child: TabBar(
                            indicatorColor: Colors.transparent,
                            indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                30.0,
                              ),
                              color: const Color.fromRGBO(29, 78, 216, 1),
                            ),
                            labelColor: Colors.white,
                            unselectedLabelColor: Colors.black,
                            controller: _tabController,
                            tabs: const [
                              Tab(
                                child: Text(
                                  'Available Packages',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Tab(
                                child: Text(
                                  'Subscribed Packages',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: SizedBox(
                          width: double.maxFinite,
                          child: TabBarView(
                            controller: _tabController,
                            children: [
                              packagesResponse.allPackages.isNotEmpty
                                  ? AllPackageGridView(
                                      packagesResponse: packagesResponse,
                                    )
                                  : const Text('No Subscription Available'),
                              packagesResponse.subscribedPackagesData.isNotEmpty
                                  ? SubscribedPackages(
                                      packagesResponse: packagesResponse,
                                    )
                                  : const Text(
                                      'Subscribed packages not found. Please Subscribe First',
                                    ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ],
        ),
      ),
    );
  }
}

class SubscribedPackages extends StatelessWidget {
  const SubscribedPackages({
    Key? key,
    required this.packagesResponse,
  }) : super(key: key);

  final PackagesResponse? packagesResponse;

  @override
  Widget build(BuildContext context) {
    Color activeColor = const Color.fromARGB(255, 110, 236, 127);
    Color inActiveColor = const Color.fromARGB(211, 194, 253, 248);
    Color expiredColor = const Color.fromARGB(255, 243, 200, 80);

    final theme = Theme.of(context).copyWith(dividerColor: Colors.transparent);
    return SingleChildScrollView(
      child: SizedBox(
        child: Column(
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: packagesResponse!.subscribedPackagesData.length,
              itemBuilder: (context, index) {
                var subPackage =
                    packagesResponse!.subscribedPackagesData[index];
                String? name;
                for (var item in packagesResponse!.allPackages.toList()) {
                  if (item.id == subPackage.packageId) {
                    name = item.name;
                  }
                }

                return Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 5, bottom: 10),
                      child: Theme(
                        data: theme,
                        child: ExpansionTile(
                          iconColor: Colors.blueGrey,
                          textColor: Colors.black,
                          childrenPadding:
                              const EdgeInsets.symmetric(horizontal: 5),
                          collapsedBackgroundColor: (() {
                            if (subPackage.status == 'Active') {
                              return activeColor;
                            }
                            if (subPackage.status == 'Inactive') {
                              return inActiveColor;
                            }
                            if (subPackage.status == 'Expired') {
                              return expiredColor;
                            }
                          }()),
                          title: Center(
                            child: Text(
                              name.toString(),
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.start,
                            ),
                          ),
                          leading: Container(
                            decoration: BoxDecoration(
                              color: (() {
                                if (subPackage.status == 'Active') {
                                  return activeColor;
                                }
                                if (subPackage.status == 'Inactive') {
                                  return inActiveColor;
                                }
                                if (subPackage.status == 'Expired') {
                                  return expiredColor;
                                }
                              }()),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text(
                                subPackage.status.toString(),
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 15,
                                ),
                                textAlign: TextAlign.justify,
                              ),
                            ),
                          ),
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  tileMode: TileMode.clamp,
                                  colors: [
                                    (() {
                                      if (subPackage.status == 'Active') {
                                        return activeColor;
                                      }
                                      if (subPackage.status == 'Inactive') {
                                        return inActiveColor;
                                      } else {
                                        return expiredColor;
                                      }
                                    }()),
                                    const Color(0xfffafafa),
                                  ],
                                ),
                              ),
                              child: Column(
                                children: [
                                  ListTile(
                                    title: const Text(
                                      'Package Name',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                    trailing: Text(
                                      name.toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    title: const Text(
                                      'Start Date',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                    trailing: Text(
                                      '${subPackage.startDate!.year}-${subPackage.startDate!.month}-${subPackage.startDate!.day}',
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    title: const Text(
                                      'End Date',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                    trailing: Text(
                                      '${subPackage.endDate!.year}-${subPackage.endDate!.month}-${subPackage.endDate!.day}',
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  ListTile(
                                    title: Text(
                                      subPackage.subscriptionType!.name
                                          .toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 16,
                                      ),
                                    ),
                                    trailing: Text(
                                      'Rs.${subPackage.subtotal}',
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class AllPackageGridView extends StatelessWidget {
  const AllPackageGridView({
    Key? key,
    required this.packagesResponse,
  }) : super(key: key);

  final PackagesResponse? packagesResponse;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: packagesResponse!.allPackages.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          crossAxisCount: 2,
          mainAxisExtent: 300,
        ),
        itemBuilder: (context, index) {
          var package = packagesResponse!.allPackages[index];

          return Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.all(
                5,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0, left: 5),
                    child: Text(
                      package.name!,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  const Divider(
                    thickness: 1.5,
                    color: Colors.grey,
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 5,
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: package.subscriptionTypes!.length,
                      itemBuilder: (context, subtypeindex) {
                        return Row(
                          children: [
                            Text(
                              '${package.subscriptionTypes![subtypeindex].name}: ',
                              style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              package.subscriptionTypes![subtypeindex].value
                                  .toString(),
                              style: const TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 70,
                    child: ListView.builder(
                      semanticChildCount: 4,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: packagesResponse!
                                  .allPackages[index].packageFeatures!.length <
                              5
                          ? packagesResponse!
                              .allPackages[index].packageFeatures?.length
                          : 5,
                      itemBuilder: (context, inde) {
                        var package = packagesResponse!
                            .allPackages[index].packageFeatures![inde];
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Icon(
                              Icons.arrow_right_alt,
                              size: 12,
                            ),
                            const SizedBox(
                              width: 2,
                            ),
                            Expanded(
                              child: Text(
                                package.enName!,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                    ),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            RouteName.singlePackageSubscriptionScreen,
                            arguments: {
                              'packages': package,
                            },
                          );
                        },
                        child: const Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'Learn More',
                              style: TextStyle(fontSize: 15),
                            ),
                            SizedBox(
                              width: 15,
                              child: Icon(
                                Icons.chevron_right,
                                size: 18,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomElevatedButton(
                    buttonText: 'Subscribe',
                    leftPadding: 5,
                    rightPadding: 5,
                    onPressed: () {
                      var id = package.id;
                      packageBloc.postPackages(id!.toInt());
                    },
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
