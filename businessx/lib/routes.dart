import 'package:b2b_market/models/models.dart' as childcat;
import 'package:b2b_market/screens/buyer_screens/buyer_navigation_screen.dart';
import 'package:b2b_market/screens/buyer_screens/cart_page.dart';
import 'package:b2b_market/screens/buyer_screens/category/buyer_category_screen.dart';
import 'package:b2b_market/screens/buyer_screens/category/buyer_child_category_screen.dart';
import 'package:b2b_market/screens/buyer_screens/category/buyer_leaf_category_screen.dart';
import 'package:b2b_market/screens/buyer_screens/category/buyer_sub_category_screen.dart';
import 'package:b2b_market/screens/buyer_screens/components/search_b2c_screen.dart';
import 'package:b2b_market/screens/buyer_screens/home/feature/buyer_feature.dart';
import 'package:b2b_market/screens/buyer_screens/home/profile/edit_profille_screen.dart';
import 'package:b2b_market/screens/buyer_screens/singlepages/shopdetails_screen.dart';
import 'package:b2b_market/screens/buyer_screens/singlepages/single_product_screen.dart';
import 'package:b2b_market/screens/buyer_screens/suggestion/suggestion_screen.dart';
import 'package:flutter/cupertino.dart';

import 'common/enums.dart';
import 'models/business.dart';
import 'models/place.dart';
import 'models/product.dart';
import 'models/response/about_us/about_us_response.dart';
import 'models/response/categories_response.dart';
import 'models/response/response.dart';
import 'models/seller_product.dart';
import 'models/user.dart';
import 'route_names.dart';
import 'screens/brand/brand_product_screen.dart';
import 'screens/business_profile/create_profile/create_seller_profile_screen.dart';
import 'screens/business_profile/edit_profile/store_page_screen.dart';
import 'screens/business_profile/lead/view_leads_screen.dart';
import 'screens/business_profile/packages/packages_screen.dart';
import 'screens/business_profile/packages/single_subscription_screen.dart';
import 'screens/business_profile/view_business_profile/business_profile_screen.dart';
import 'screens/buyer_screens/checkout_page.dart';
import 'screens/category/sub_categories_screen.dart';
import 'screens/data_collection/b2b_pain_points_question_screen.dart';
import 'screens/data_collection/benefits_question_screen.dart';
import 'screens/data_collection/buying_pattern_screen.dart';
import 'screens/data_collection/general_presence_screen.dart';
import 'screens/data_collection/introduction_question_screen.dart';
import 'screens/data_collection/main_data_collection_page.dart';
import 'screens/data_collection/willingness_about_our_platform.dart';
import 'screens/favorite/favorite_condition_check.dart';
import 'screens/map_screen.dart';
import 'screens/screens.dart';
import 'screens/search/search_screen.dart';
import 'screens/seller_page/seller_all_popular_products.dart';
import 'screens/seller_page/seller_home_page.dart';

abstract class Routes {
  static CupertinoPageRoute cupertinoPageRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      // auth navigation

      case LoginScreen.routeName:
        return CupertinoPageRoute(
          builder: (_) => LoginScreen(
            userType: routeSettings.arguments as UserType,
          ),
        );

      case SignUpScreen.routeName:
        return CupertinoPageRoute(
          builder: (_) =>
              SignUpScreen(userType: routeSettings.arguments as UserType),
        );

      case RouteName.loginOptionsScreen:
        return CupertinoPageRoute(builder: (_) => const LoginOptionsScreen());
      case PasswordResetScreen.routeName:
        return CupertinoPageRoute(
          builder: (_) => PasswordResetScreen(
            contactNumber: routeSettings.arguments as String,
          ),
        );
      case RouteName.passwordResetConfirmationScreen:
        return CupertinoPageRoute(
          builder: (_) => const PasswordResetConfirmationScreen(),
        );

      case RouteName.otpVerificationScreen:
        return CupertinoPageRoute(
          builder: (_) => OTPVerificationScreen(
            credentials: routeSettings.arguments as LoginCredentials,
          ),
        );

      // category screens

      case RouteName.leafCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return LeafCategoryScreen(
              title: routeSettings.arguments as String,
            );
          },
        );

      case RouteName.subCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return SubCategoryScreen(
              categoryName: routeSettings.arguments as String,
            );
          },
        );

      case RouteName.childCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return ChildCategoryScreen(
              title: routeSettings.arguments as String,
            );
          },
        );

      case RouteName.navigationScreen:
        return CupertinoPageRoute(
          builder: (_) => NavigationScreen(
            selectedIndex: routeSettings.arguments as int? ?? 0,
          ),
        );

      case ForgotPasswordScreen.routeName:
        return CupertinoPageRoute(builder: (_) => ForgotPasswordScreen());

      case RouteName.changePasswordScreen:
        return CupertinoPageRoute(builder: (_) => const ChangePasswordScreen());
      case RouteName.settingScreen:
        return CupertinoPageRoute(builder: (_) => const SettingsScreen());
      case RouteName.checkConditionPage:
        return CupertinoPageRoute(builder: (_) => const CheckConditionPage());

      case RouteName.termsAndConditionScreen:
        return CupertinoPageRoute(builder: (_) => const TermsAndConditions());
      case RouteName.helpAndSupportScreen:
        return CupertinoPageRoute(builder: (_) => const HelpAndSupportScreen());

      case RouteName.checkFavouriteScreen:
        return CupertinoPageRoute(
          builder: (_) => const FavoriteConditionCheck(),
        );

      case RouteName.mainDataCollectionPage:
        return CupertinoPageRoute(
          builder: (_) => const MainDataCollectionPage(),
        );
      case IntroductionQuestionScreen.routeName:
        return CupertinoPageRoute(
          builder: (_) => const IntroductionQuestionScreen(),
        );

      case RouteName.generalPresenceQuestionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const GeneralPresenceQuestionScreen();
          },
        );

      case RouteName.buyingPatternQuestionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const BuyingPatternQuestionScreen();
          },
        );

      case RouteName.b2bPainPointsQuestionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const B2BPainPointsQuestionScreen();
          },
        );

      case RouteName.willingnessAboutOurPlatformQuestionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const WillingnessAboutOurPlatformScreen();
          },
        );

      case RouteName.benefitsQuestionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const BenefitsQuestionScreen();
          },
        );

      case RouteName.aboutUsScreen:
        return CupertinoPageRoute(builder: (_) => const AboutUsScreen());

      case RouteName.editAboutUsScreen:
        return CupertinoPageRoute(
          builder: (_) => EditStorePageScreen(
            aboutUsInfo: routeSettings.arguments as AboutUsInfo?,
          ),
        );

      case RouteName.searchScreen:
        return CupertinoPageRoute(builder: (_) => const SearchScreen());

      case RouteName.createSellerProfileFirstScreen:
        return CupertinoPageRoute(
          builder: (_) => const CreateSellerProfileFirstScreen(),
        );

      case RouteName.viewLeadScreen:
        return CupertinoPageRoute(builder: (_) => const ViewLeadsScreen());

      case RouteName.faqScreen:
        return CupertinoPageRoute(builder: (_) => const FaqScreen());

      case RouteName.sellerProductsListScreen:
        return CupertinoPageRoute(
          builder: (_) => const SellerProductsListScreen(),
        );

      case RouteName.sellerPopularProductsScreen:
        return CupertinoPageRoute(
          builder: (_) => const SellerPopularProductsScreen(),
        );

      case RouteName.sellerHomePage:
        return CupertinoPageRoute(
          builder: (_) => SellerHomePage(
            product: routeSettings.arguments as Product,
          ),
        );

      case RouteName.selectRootCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => const SelectRootCategoryScreen(),
        );

      case RouteName.selectSubCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => SelectSubCategoryScreen(
            category: routeSettings.arguments as Category,
          ),
        );

      case RouteName.selectChildCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => SelectChildCategoryScreen(
            category: routeSettings.arguments as Category,
          ),
        );

      case RouteName.selectLeafCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => SelectLeafCategoryScreen(
            category: routeSettings.arguments as Category,
          ),
        );

      case RouteName.addProductScreen:
        return CupertinoPageRoute(
          builder: (_) =>
              AddProductScreen(category: routeSettings.arguments as Category),
        );

      case RouteName.editProductScreen:
        return CupertinoPageRoute(
          settings: routeSettings,
          builder: (_) => EditProductScreen(
            sellerProduct: routeSettings.arguments as SellerProduct,
          ),
        );

      // brand product screens
      case RouteName.brandProductScreen:
        return CupertinoPageRoute(
          settings: routeSettings,
          builder: (_) => BrandProductScreen(
            brandID: routeSettings.arguments as int,
          ),
        );

      // notification
      case RouteName.notificationScreen:
        return CupertinoPageRoute(
          settings: routeSettings,
          builder: (_) => const NotificationScreen(),
        );

      // contact_us
      case RouteName.contactUs:
        return CupertinoPageRoute(
          builder: (_) => const ContactUsScreen(),
        );

      // edit_profile_screen
      case RouteName.editProfileScreen:
        return CupertinoPageRoute(
          builder: (_) => EditProfile(user: routeSettings.arguments as User),
        );

      case RouteName.submitRequirementScreen:
        return CupertinoPageRoute(
          builder: (_) {
            var product = routeSettings.arguments as Product;
            return SubmitRequirementScreen(product: product);
          },
        );

      case RouteName.businessProfileScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return BusinessProfileScreen(
              business: routeSettings.arguments as Business,
            );
          },
        );

      case RouteName.storePageScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return StorePageScreen(
              verificationStatus: routeSettings.arguments as int,
            );
          },
        );

      case RouteName.editBusinessProfileScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return EditBusinessProfile(
              business: routeSettings.arguments as Business,
            );
          },
        );

      case RouteName.productsScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return ProductsScreen(
              title: routeSettings.arguments as String,
            );
          },
        );

      case RouteName.productDetailScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return ProductDetailScreen(
              product: routeSettings.arguments as Product,
            );
          },
        );

      case PackagesScreen.routeName:
        return CupertinoPageRoute(
          builder: (_) {
            return const PackagesScreen();
          },
        );

      case RouteName.singlePackageSubscriptionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            var arguments = routeSettings.arguments as Map<String, dynamic>;
            return SingleSubscriptionScreen(
              packages: arguments['packages'],
            );
          },
        );

      case MapScreen.route:
        return CupertinoPageRoute(
          builder: (_) {
            var arguments = routeSettings.arguments as Place;
            return MapScreen(
              place: arguments,
            );
          },
        );

      // buyer section
      case BuyerNavigationScreen.routeName:
        return CupertinoPageRoute(
          builder: (_) {
            return const BuyerNavigationScreen();
          },
        );

      case RouteName.buyerCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return BuyerCategoryScreen(
              rootCategory: routeSettings.arguments as RootCategory,
            );
          },
        );

      case RouteName.buyerSubCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => BuyerSubCategoryScreen(
            subCategory: routeSettings.arguments as SubCategory,
          ),
        );
      case RouteName.buyerChildCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => BuyerChildCategoryScreen(
            childCategory: routeSettings.arguments as childcat.ChildCategory,
          ),
        );
      case RouteName.buyerLeafCategoryScreen:
        return CupertinoPageRoute(
          builder: (_) => BuyerLeafCategoryScreen(
            leafCategory: routeSettings.arguments as LeafCategory,
          ),
        );

      case RouteName.buyerSingleProductsScreen:
        return CupertinoPageRoute(
          builder: (_) {
            var arguments = routeSettings.arguments as Map<String, dynamic>;

            return SingleProductScreen(
              product: arguments['product'],
              heroType: arguments['heroType'],
            );
          },
        );
      case RouteName.searchB2CScreen:
        return CupertinoPageRoute(builder: (_) => const SearchB2CScreen());

      case RouteName.buyerSuggestionScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const ProductSuggestionScreen();
          },
        );
      case RouteName.buyerfeatureScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const ProductfeatureScreen();
          },
        );

      case CartPage.routeName:
        return CupertinoPageRoute(
          builder: (_) {
            return const CartPage();
          },
        );

      case RouteName.shopdetailsScreen:
        return CupertinoPageRoute(
          builder: (_) {
            return const ShopdetailsScreen();
          },
        );

      case CheckOutPage.routeName:
        return CupertinoPageRoute(
          builder: (_) {
            return const CheckOutPage();
          },
        );
      //customerprofile
      case RouteName.editPageScreen:
        return CupertinoPageRoute(
          builder: (_) => const EditPageScreen(),
        );

      default:
        return CupertinoPageRoute(builder: (_) => const NavigationScreen());
    }
  }
}
