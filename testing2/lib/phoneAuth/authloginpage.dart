import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:testing2/phoneAuth/optscreen.dart';

class AuthPage extends StatefulWidget {
  const AuthPage({super.key});

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final TextEditingController _phoneController = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void dispose() {
    _phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Phone Auth"),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              controller: _phoneController,
              decoration: InputDecoration(
                hintText: "Enter Phone Number",
                suffixIcon: const Icon(Icons.phone),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
              ),
            ),
          ),
          const SizedBox(height: 25),
          ElevatedButton(
            onPressed: () async {
              try {
                await _auth.verifyPhoneNumber(
                  phoneNumber: _phoneController.text,
                  verificationCompleted: (PhoneAuthCredential credential) {
                    // Handle auto verification completed
                    if (credential.smsCode != null) {
                      print("Verification Completed: ${credential.smsCode}");
                    } else {
                      print("Verification Completed: No SMS code");
                    }
                  },
                  verificationFailed: (FirebaseAuthException ex) {
                    // Handle verification failed
                    if (ex.message != null) {
                      print("Verification Failed: ${ex.message}");
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                            content:
                                Text("Verification Failed: ${ex.message}")),
                      );
                    } else {
                      print("Verification Failed: No message");
                    }
                  },
                  codeSent: (String verificationId, int? resendToken) {
                    print("Code Sent: $verificationId");
                    if (verificationId.isNotEmpty) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              Otpscreen(verifyedId: verificationId),
                        ),
                      );
                    } else {
                      print("Invalid verification ID");
                    }
                  },
                  codeAutoRetrievalTimeout: (String verificationId) {
                    // Handle auto retrieval timeout
                    print("Code Auto Retrieval Timeout: $verificationId");
                  },
                );
              } on FirebaseAuthException catch (e) {
                print("FirebaseAuthException: $e");
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text("FirebaseAuthException: $e")),
                );
              } catch (e) {
                print("Error: $e");
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text("Error: $e")),
                );
              }
            },
            child: const Text("Verify Phone Number"),
          ),
        ],
      ),
    );
  }
}
