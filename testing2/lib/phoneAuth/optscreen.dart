import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:testing2/main.dart';

// ignore: must_be_immutable
class Otpscreen extends StatefulWidget {
  String verifyedId;

  Otpscreen({super.key, required this.verifyedId});
  @override
  State<Otpscreen> createState() => _OtpscreenState();
}

class _OtpscreenState extends State<Otpscreen> {
  TextEditingController optController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('OTP Verification'),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: TextField(
              controller: optController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: "Enter Otp Code",
                  suffixIcon: const Icon(Icons.phone),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(24))),
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          ElevatedButton(
            onPressed: () async {
              try {
                PhoneAuthCredential credential = PhoneAuthProvider.credential(
                  verificationId: widget.verifyedId,
                  smsCode: optController.text.toString(),
                );

                await FirebaseAuth.instance.signInWithCredential(credential);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomeScreen()),
                );
              } on FirebaseAuthException catch (e) {
                print('Error: ${e.message}');
                // Handle error here, e.g. display an error message to the user
              } catch (e) {
                print('Error: $e');
                // Handle error here, e.g. display an error message to the user
              }
            },
            child: const Text("Continue"),
          )
        ],
      ),
    );
  }
}
