// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:todo_app/Repository/BaseRepository.dart';
import 'package:todo_app/model/todo_model.dart';

class TodoRepository {
  late final String appUrl;
  // late final Dio _dio;
  TodoRepository() {
    // _dio = baseRepository.dio;
    appUrl = baseRepository.appUrl;
  }

  // getTodolist() async {
  //   try {
  //     final response =
  //         await _dio.get('https://localhost:44354/api/TodoApi/GetallTodo');
  //     print("mahesh");
  //     print(response.data);
  //     return TodoResponse.fromJson(response.data);
  //   } catch (e) {
  //     print(e);
  //   }
  //   // catch (error, stacktrace) {
  //   //   debugPrint('Exception occurred: $error stackTrace: $stacktrace');
  //   //   return TodoModel.withError(baseRepository.handleError(error));
  //   // }
  // }

  fetchData() async {
    try {
      final response =
          await Dio().get('https://localhost:44354/api/TodoApi/GetallTodo');

      // Check if the response contains data
      if (response.data != null) {
        TodoResponse todoResponse = TodoResponse.fromJson(response.data);

        // Access the data
        print("Code: ${todoResponse.code}");
        print("Message: ${todoResponse.message}");

        // Iterate through response items
        for (TodoItem item in todoResponse.response) {
          print("Todo ID: ${item.id}, Todo: ${item.todo}");
        }
      } else {
        print("Empty response");
      }
    } catch (e) {
      print("Error: $e");
    }
  }
}




// Future<TodoModel> getdata() async {
//   final response = await http
//       .get(Uri.parse('https://localhost:44354/api/TodoApi/GetallTodo'));

//   if (response.statusCode == 200) {
//     // If the server did return a 200 OK response,
//     // then parse the JSON.
//     print(response.body);
//     return TodoModel.fromJson(jsonDecode(response.body));
//   } else {
//     // If the server did not return a 200 OK response,
//     // then throw an exception.
//     throw Exception('Failed to load album');
//   }
// }
