import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:todo_app/model/Error/Error.dart';

class BaseRepository {
  static const apiKey = 'AIzaSyA8uYcKYhrmYNTjorWnmcJ6potH3xY4QWg';
  // static const String _baseUrl = 'https://delivery-system.sidhasewa.com';
  // static const String _baseUrl = 'https://sawari.jyotishasolutions.com';
  // static const String _baseUrl = 'http://192.168.1.70:8080';

  // static const String _baseUrl = 'http://192.168.0.2:8000';
  static const String _baseUrl = 'https://localhost:44354';
  // static const String _baseUrl = 'http://dev-v2-sawari.jyotishasolutions.com';

  static const String _appUrl = '$_baseUrl/api/TodoApi';

  get appUrl => _appUrl;

  late Dio _dio;
  get dio => _dio;

  BaseRepository() {
    BaseOptions options = BaseOptions(
      receiveTimeout: const Duration(milliseconds: 5000),
      connectTimeout: const Duration(milliseconds: 30000),
    );
    _dio = Dio(options);
    _dio.interceptors.addAll([
      QueuedInterceptorsWrapper(
        onRequest: (options, handler) async {
          options.headers['Accept'] = 'application/json';
          options.headers['Content-Type'] = 'application/json';
          // options.headers['X-XSRF-TOKEN'] = '';

          // this is optional Added for testing
          // options.headers['Authorization'] =
          //     'Bearer ' '4|KpCOHAkDy6KjvkwuyphCQ2YdEB9HAqiSq2zFQEtn';

//
          if (getToken() != null && getToken() != '') {
            await getToken().then(
              (token) => {
                if (token != null)
                  options.headers['Authorization'] = 'Bearer ' + token
              },
            );
          }

          //
          //  options.headers[HttpHeaders.acceptHeader] = ContentType.json;
          // var token = await authBloc.getToken();
          // if (token != null) {
          //   options.headers[HttpHeaders.authorizationHeader] =
          //       'Bearer $token';
          //   options.headers[HttpHeaders.userAgentHeader] =
          //       'eeeinnovation369@gmail.com';
          // }
          // if (socketId != null) {
          //   options.headers['X-Socket-ID'] = socketId;
          // }
          handler.next(options);
        },
      ),
      LogInterceptor(requestBody: true, responseBody: true)
    ]);
  }

  getToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('token');
  }

  String handleError(error) {
    String? errorDescription;
    // List<String?> errorDescriptionList = [];
    if (error is DioError) {
      DioError dioError = error;
      switch (dioError.type) {
        case DioErrorType.cancel:
          errorDescription = 'Request to API server was cancelled';
          break;
        case DioErrorType.connectionTimeout:
          errorDescription = 'Connection timeout with API server';
          break;
        case DioErrorType.unknown:
          errorDescription = 'No internet connection';
          break;
        case DioErrorType.receiveTimeout:
          errorDescription = 'Receive timeout in connection with API server';
          break;
        case DioErrorType.badResponse:
          errorDescription =
              'Received invalid status code: ${dioError.response?.statusCode}';
          switch (dioError.response?.statusCode) {
            case 401:
              errorDescription = 'unauthenticated';
              break;
            case 422:
              print('object');
              if (dioError.response?.data['errors'] != null) {
                print('objects');
                // errMessageList = dioError.response?.data;
                if (dioError.response?.data != null) {
                  print('objectss');
                  errMessageList.message =
                      json.encode(dioError.response?.data['message']);
                  errorDescription =
                      json.encode(dioError.response?.data['message']);
                  // print(
                  //   json.encode(dioError.response?.data['errors']).toString(),
                  // );
                  if (dioError.response?.data['errors'] != null) {
                    // if (dioError.response?.data['errors']['email'] != null) {
                    //   errMessageList.email = json.encode(
                    //     dioError.response!.data['errors']['email'][0],
                    //   );
                    // }
                    if (dioError.response?.data['errors']['email'] != null) {
                      errMessageList.email = json.encode(
                        dioError.response!.data['errors']['email'][0],
                      );
                    }
                    if (dioError.response?.data['errors']['mobile'] != null) {
                      errMessageList.mobileNumber = json.encode(
                        dioError.response!.data['errors']['mobile'][0],
                      );
                    }
                    if (dioError.response?.data['errors']['name'] != null) {
                      errMessageList.name = json.encode(
                        dioError.response!.data['errors']['name'][0],
                      );
                    }
                    //  if (dioError.response?.data['errors']['mobile'] != null) {
                    //   errMessageList.signupMobile = json.encode(
                    //     dioError.response!.data['errors']['mobile'][0],
                    //   );
                    // }
                    // if (dioError.response?.data['errors']['email'] != null) {
                    //   errMessageList.signupEmail = json.encode(
                    //     dioError.response!.data['errors']['email'][0],
                    //   );
                    // }
                    if (dioError.response?.data['errors']['dob'] != null) {
                      errMessageList.dob = json.encode(
                        dioError.response!.data['errors']['dob'][0],
                      );
                      print(errMessageList.dob);
                    }
                    if (dioError.response?.data['errors']['name'] != null) {
                      errMessageList.pkgName = json.encode(
                        dioError.response!.data['errors']['name'][0],
                      );
                    }
                    if (dioError.response?.data['errors']
                            ['package_category_id'] !=
                        null) {
                      errMessageList.pkgCategoryId = json.encode(
                        dioError.response!.data['errors']['package_category_id']
                            [0],
                      );
                    }
                    if (dioError.response?.data['errors']['package_size_id'] !=
                        null) {
                      errMessageList.pkgSizeId = json.encode(
                        dioError.response!.data['errors']['package_size_id'][0],
                      );
                    }
                    if (dioError.response?.data['errors']['sender_address'] !=
                        null) {
                      errMessageList.senderAddress = json.encode(
                        dioError.response!.data['errors']['sender_address'][0],
                      );
                    }
                    if (dioError.response?.data['errors']['receiver_address'] !=
                        null) {
                      errMessageList.receiverAddress = json.encode(
                        dioError.response!.data['errors']['receiver_address']
                            [0],
                      );
                    }
                    if (dioError.response?.data['errors']['receiver_name'] !=
                        null) {
                      errMessageList.receiverName = json.encode(
                        dioError.response!.data['errors']['receiver_name'][0],
                      );
                    }
                    if (dioError.response?.data['errors']['receiver_mobile'] !=
                        null) {
                      errMessageList.receiverMobile = json.encode(
                        dioError.response!.data['errors']['receiver_mobile'][0],
                      );
                    }
                    if (dioError.response?.data['errors']
                            ['receiver_nick_name'] !=
                        null) {
                      errMessageList.receiverNickName = json.encode(
                        dioError.response!.data['errors']['receiver_nick_name']
                            [0],
                      );
                    }
                    if (dioError.response?.data['errors']['receiver_address'] !=
                        null) {
                      errMessageList.receiverAddress = json.encode(
                        dioError.response!.data['errors']['receiver_address']
                            [0],
                      );
                    }
                    if (dioError.response?.data['errors']
                            ['sender_receiver_distance'] !=
                        null) {
                      errMessageList.senderReceiverDistance = json.encode(
                        dioError.response!.data['errors']
                            ['sender_receiver_distance'][0],
                      );
                    }
                  }
                }
              }
              // Map<String, dynamic> errorData = json.decode(errorResponse);

              // if (errorData.containsKey('message')) {
              //   print('Error Message: ${errorData['message']}');
              // }

              // if (errorData.containsKey('errors')) {
              //   Map<String, dynamic> errors = errorData['errors'];

              //   errors.forEach((field, fieldErrors) {
              //     print('$field Errors:');
              //     if (fieldErrors is List<String>) {
              //       fieldErrors.forEach((error) {
              //         print('- $error');
              //       });
              //     }
              //   });
              // }

              break;
            case 500:
              if (dioError.response?.data['message'] != null) {
                errorDescription = dioError.response?.data['message'];
              } else {
                errorDescription = 'something went wrong on server';
              }
              break;
            case 404:
              if (dioError.response?.data['message'] != null) {
                errorDescription = dioError.response?.data['message'];
              } else {
                errorDescription = 'Page Not Found';
              }
          }
          break;
        case DioErrorType.sendTimeout:
          errorDescription = 'Send timeout in connection with API server';
          break;
        case DioErrorType.badCertificate:
          // TODO: Handle this case.
          break;
        case DioErrorType.connectionError:
          errorDescription = 'No internet connection';
          // TODO: Handle this case.
          break;
      }
    } else {
      errorDescription = 'Unexpected error occurred';
    }
    return errorDescription!;
  }
}

final BaseRepository baseRepository = BaseRepository();
