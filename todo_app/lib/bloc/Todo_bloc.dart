import 'package:rxdart/rxdart.dart';
import 'package:todo_app/Repository/TodoRepository.dart';
import 'package:todo_app/model/todo_model.dart';

class TodoBloc {
  final TodoRepository _TodoRepository = TodoRepository();
  final BehaviorSubject<TodoResponse> _todo = BehaviorSubject<TodoResponse>();

  gettodos() async {
    var response = await _TodoRepository.fetchData();
    print(response);
    // TodoResponse = response;
    _todo.sink.add(response);
  }

  BehaviorSubject<TodoResponse> get getvechileinfo => _todo;
}

final todoBloc = TodoBloc();
