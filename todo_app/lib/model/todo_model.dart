class TodoResponse {
  final String code;
  final String message;
  final List<TodoItem> response;

  TodoResponse({
    required this.code,
    required this.message,
    required this.response,
  });

  factory TodoResponse.fromJson(Map<String, dynamic> json) {
    return TodoResponse(
      code: json['code'],
      message: json['message'],
      response: (json['response'] as List<dynamic>)
          .map((item) => TodoItem.fromJson(item))
          .toList(),
    );
  }
}

class TodoItem {
  int id;
  String todo;

  TodoItem({
    required this.id,
    required this.todo,
  });

  factory TodoItem.fromJson(Map<String, dynamic> json) {
    return TodoItem(
      id: json['id'],
      todo: json['todo'],
    );
  }
}
