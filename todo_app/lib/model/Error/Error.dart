// To parse this JSON data, do
//
//     final error = errorFromJson(jsonString);

// Error errorFromJson(String str) => Error.fromJson(json.decode(str));

// String errorToJson(Error data) => json.encode(data.toJson());

class Error {
  String? message;
  String? email;
  String? mobileNumber;
  String? name;
  String? signupEmail;
  String? signupMobile;
  String? dob;

  String? pkgName;
  String? pkgCategoryId;
  String? pkgSizeId;
  String? senderAddress;
  String? receiverName;
  String? receiverMobile;
  String? receiverNickName;
  String? receiverAddress;
  String? senderReceiverDistance;
}

Error errMessageList = Error();
