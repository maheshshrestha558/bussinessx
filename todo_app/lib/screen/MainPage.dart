// ignore: file_names
// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/Constant/colors.dart';
import 'package:todo_app/Widgets/searchBox.dart';
import 'package:todo_app/Widgets/todo_item.dart';

import '../model/todo_model.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    // getdata();
    fetchData();
    super.initState();
  }

  Future<void> fetchData() async {
    try {
      final response =
          await Dio().get('https://localhost:44354/api/TodoApi/GetallTodo');

      // Check if the response contains data
      if (response.data != null) {
        TodoResponse todoResponse = TodoResponse.fromJson(response.data);

        // Access the data
        print("Code: ${todoResponse.code}");
        print("Message: ${todoResponse.message}");

        // Iterate through response items
        for (TodoItem item in todoResponse.response) {
          print("Todo ID: ${item.id}, Todo: ${item.todo}");
        }
      } else {
        print("Empty response");
      }
    } catch (e) {
      print("Error: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: tdBGColor,
      appBar: AppBar(
        backgroundColor: tdBGColor,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Icon(Icons.menu, color: tdBlack, size: 30),
            SizedBox(
              height: 40,
              width: 40,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(),
              ),
            )
          ],
        ),
      ),
      body: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 15,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const searchBox(),
                Container(
                  margin: const EdgeInsets.only(top: 50, bottom: 20),
                  child: const Text(
                    "All Todos",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                      color: tdBlack,
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: 10,
                    itemBuilder: (BuildContext context, int index) {
                      return const TodoItems();
                    },
                  ),
                ),
              ],
            ),
          ),
          const AddTodo()
        ],
      ),
    );
  }
}

class AddTodo extends StatelessWidget {
  const AddTodo({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(
                bottom: 20,
                right: 10,
                left: 20,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0, 0),
                      blurRadius: 10,
                      spreadRadius: 0,
                    )
                  ],
                  borderRadius: BorderRadius.circular(10)),
              child: const TextField(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    hintText: "Add new todo item",
                    border: InputBorder.none),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 20, right: 20),
            child: ElevatedButton(
                onPressed: () {
                  print(" new item added");
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: tdBlue,
                    elevation: 10,
                    minimumSize: const Size(40, 60)),
                child: const Text(
                  "+",
                  style: TextStyle(fontSize: 40, color: Colors.white),
                )),
          ),
        ],
      ),
    );
  }
}
