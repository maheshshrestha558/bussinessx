// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:form_field_validator/form_field_validator.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:locus/bloc/driverkyc/settings/brand_bloc.dart';
// import 'package:locus/bloc/driverkyc/settings/model_bloc.dart';
// import 'package:locus/bloc/driverkyc/settings/vechilecolor_bloc.dart';
// import 'package:locus/bloc/driverkyc/vechileinfo_bloc.dart';
// import 'package:locus/bloc/vehicle_bloc.dart';
// import 'package:locus/common/app_color/custom_app_color.dart';
// import 'package:locus/common/widgets/custom_app_bar.dart';
// import 'package:locus/common/widgets/custom_elevated_button.dart';
// import 'package:locus/common/widgets/custom_text_form_field.dart';
// import 'package:locus/models/driver/settings/brand_model.dart';
// import 'package:locus/models/driver/settings/setting_responses/color_map.dart';
// import 'package:locus/models/driver/settings/setting_responses/model_map.dart';

// class Vechileinfopage extends StatefulWidget {
//   const Vechileinfopage({Key? key}) : super(key: key);

//   @override
//   State<Vechileinfopage> createState() => _VechileinfopageState();
// }

// TextEditingController numberplatecontoller = TextEditingController();
// TextEditingController producationyearcontoller = TextEditingController();

// class _VechileinfopageState extends State<Vechileinfopage> {
//   final ImagePicker _picker = ImagePicker();

//   File? image;

//   File imageTemporary = File(
//     'assets/images/login_option_screen/openingimage.png',
//   );

//   String str = 'assets/images/login_option_screen/openingimage.png';

//   late File defaultFile;

//   String? imgTempPath;

//   Future _img(ImageSource source) async {
//     try {
//       final image = await _picker.pickImage(source: source, imageQuality: 95);
//       if (image == null) return;

//       imageTemporary = File(image.path);
//       setState(() {
//         imgTempPath = imageTemporary.path;
//         this.image = imageTemporary;
//         debugPrint(imgTempPath);
//       });
//     } on PlatformException catch (e) {
//       debugPrint('Failed to pick image $e');
//     }
//   }

//   void _showPicker(context) {
//     showModalBottomSheet(
//       context: context,
//       builder: (BuildContext bc) {
//         return Container(
//           color: const Color.fromARGB(255, 255, 255, 255),
//           child: SafeArea(
//             child: Wrap(
//               children: <Widget>[
//                 ListTile(
//                   leading: const Icon(Icons.photo_library),
//                   title: const Text('Photo Library'),
//                   onTap: () {
//                     _img(ImageSource.gallery);
//                     Navigator.of(context).pop();
//                   },
//                 ),
//                 ListTile(
//                   leading: const Icon(Icons.photo_camera),
//                   title: const Text('Camera'),
//                   onTap: () {
//                     _img(ImageSource.camera);
//                     Navigator.of(context).pop();
//                   },
//                 ),
//               ],
//             ),
//           ),
//         );
//       },
//     );
//   }

//   // final fullnameController = TextEditingController();

//   static Map<String, dynamic> brandoptions = {'Select Brand First': -1};
//   List<String> listdata = [
//     'select your vechile type',
//     'Car',
//     'Bike',
//     'Truck',
//     'SanoHatti'
//   ];

//   String vechilevalue = 'select your vechile type';
//   int _frequencyValue = -1;

//   int _modelValue = -1;
//   int _colorValue = -1;

//   String dropdownValue = 'Select Brand First';
//   String dropdownmodel = 'Select model First';
//   String dropdowncolor = 'Select color First';
//   bool brandVisible = false;
//   bool vechileVisible = false;
//   bool modelVisible = false;
//   bool colorVisible = false;
//   int? selectedOptionIndex;

//   @override
//   void initState() {
//     vehicleBloc.getVehicleType();
//     // brandbloc.getbrandinfo;
//     var datas = brandSetting.data;
//     brandoptions['Select Brand First'] = -1;
//     if (datas != null) {
//       for (var i = 0; i < datas.length; i++) {
//         if (datas[i].key != null && datas[i].id != null) {
//           print('Options1');
//           brandoptions[datas[i].value.toString()] = datas[i].id;
//         } else {
//           print('Options');
//         }
//       }
//     } else {
//       print('brandoptions');
//     }

//     super.initState();
//   }

//   final GlobalKey<FormState> vechileKey = GlobalKey<FormState>();

//   void _validateDropdown() async {
//     if (!vechileKey.currentState!.validate() ||
//         _frequencyValue <= 0 ||
//         _modelValue <= 0 ||
//         _colorValue <= 0) {
//       if (_frequencyValue <= 0) {
//         brandVisible = true;
//       }

//       if (_modelValue <= 0) {
//         modelVisible = true;
//       }
//       if (_colorValue <= 0) {
//         colorVisible = true;
//       }
//       Fluttertoast.showToast(
//         msg: 'User are requested to Complete Empty Space',
//         toastLength: Toast.LENGTH_SHORT,
//         gravity: ToastGravity.BOTTOM,
//         timeInSecForIosWeb: 3,
//         backgroundColor: Colors.red,
//         textColor: Colors.white,
//         fontSize: 16.0,
//       );
//     } else if (vechileKey.currentState!.validate()) {
//       await vechileBloc.postvechileinfo(context, {
//         'vehicle_type_id': selectedOptionIndex,
//         'brand_id': _frequencyValue,
//         'model_id': _modelValue,
//         'color_id': _colorValue,
//         'number_plate': numberplatecontoller.text,
//         'production_year': int.parse(producationyearcontoller.text)
//       });
//       Navigator.of(context).pop();
//       Navigator.of(context).pop();
//       Navigator.of(context).pop();
//       Navigator.of(context).pop();
//       setState(() {
//         numberplatecontoller.text = '';
//         producationyearcontoller.text = '';
//       });
//     }
//     setState(() {});
//   }

//   void selectDate(context) async {
//     DateTime currentDate = DateTime.now();

//     DateTime? picked = await showDatePicker(
//       initialDatePickerMode: DatePickerMode.year,
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(1900),
//       lastDate: currentDate,
//     );
//     if (picked != null && picked != producationyearcontoller.text) {
//       // Update the TextFormField with the selected date
//       // String formattedDate = DateFormat('yyyy').format(picked);
//       producationyearcontoller.text =
//           picked.year.toString(); // or any other desired date format
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: CustomAppBar(
//         title: 'Vehcile Information',
//         centerTitle: false,
//         leading: IconButton(
//           onPressed: () {
//             Navigator.pop(context);
//           },
//           icon: const Icon(Icons.arrow_back_ios_new),
//           color: Colors.black,
//         ),
//       ),
//       floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
//       floatingActionButton: Padding(
//         padding: const EdgeInsets.symmetric(
//           vertical: 00,
//           horizontal: 15,
//         ),
//         child: CustomElevatedButton(
//           buttonText: 'Continue',
//           onPressed: _validateDropdown,
//         ),
//       ),
//       body: SafeArea(
//         child: Form(
//           key: vechileKey,
//           child: ListView(
//             shrinkWrap: true,
//             // physics: const NeverScrollableScrollPhysics(),
//             children: [
//               const SizedBox(
//                 height: 15,
//               ),
//               const Padding(
//                 padding: EdgeInsets.symmetric(
//                   vertical: 10,
//                   horizontal: 15,
//                 ),
//                 child: Text(
//                   'Selete vehicle ',
//                   style: TextStyle(
//                     fontSize: 14,
//                     fontWeight: FontWeight.w400,
//                     fontStyle: FontStyle.normal,
//                     color: Appcolor.primaryBlue,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: StreamBuilder(
//                   stream: vehicleBloc.vehicleType,
//                   builder: (context, snapshot) {
//                     if (snapshot.data != null) {
//                       var vehciledata = snapshot.data;
//                       if (vehciledata != null) {
//                         return DropdownButtonFormField(
//                           decoration: const InputDecoration(
//                             enabledBorder: OutlineInputBorder(
//                               //<-- SEE HERE
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ),
//                             ),
//                             focusedBorder: OutlineInputBorder(
//                               //<-- SEE HERE
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(width: 1),
//                             ),
//                             filled: true,
//                             fillColor: Colors.white,
//                             contentPadding: EdgeInsets.symmetric(
//                               horizontal: 5,
//                               vertical: 15,
//                             ),
//                           ),
//                           dropdownColor: Colors.white,
//                           value: vechilevalue,
//                           onChanged: (String? newValue) {
//                             setState(() {
//                               vechilevalue = newValue!;
//                               selectedOptionIndex = listdata.indexOf(newValue);
//                             });
//                           },
//                           items: listdata
//                               .map<DropdownMenuItem<String>>((String value) {
//                             return DropdownMenuItem<String>(
//                               value: value,
//                               child: Text(
//                                 value,
//                                 style: const TextStyle(fontSize: 18),
//                               ),
//                             );
//                           }).toList(),
//                         );
//                       }
//                       {
//                         return const CircularProgressIndicator();
//                       }
//                     }
//                     return const CircularProgressIndicator();
//                   },
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: Visibility(
//                   visible: vechileVisible,
//                   child: const Text(
//                     'please select vechile name',
//                     style: TextStyle(
//                       color: Appcolor.bloodRed,
//                     ),
//                   ),
//                 ),
//               ),
//               ////////////
//               const Padding(
//                 padding: EdgeInsets.symmetric(
//                   vertical: 10,
//                   horizontal: 15,
//                 ),
//                 child: Text(
//                   'Selete Brand',
//                   style: TextStyle(
//                     fontSize: 14,
//                     fontWeight: FontWeight.w400,
//                     fontStyle: FontStyle.normal,
//                     color: Appcolor.primaryBlue,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: StreamBuilder(
//                   stream: brandbloc.getbrandinfo,
//                   builder: (context, snapshot) {
//                     if (snapshot.data != null) {
//                       var vehciledata = snapshot.data;
//                       if (vehciledata != null) {
//                         return DropdownButtonFormField(
//                           decoration: const InputDecoration(
//                             enabledBorder: OutlineInputBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ), //<-- SEE HERE
//                             ),
//                             focusedBorder: OutlineInputBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ), //<-- SEE HERE
//                             ),
//                             filled: true,
//                             fillColor: Colors.white,
//                             contentPadding: EdgeInsets.symmetric(
//                               horizontal: 5,
//                               vertical: 15,
//                             ),
//                           ),
//                           dropdownColor: Colors.white,
//                           items: brandoptions
//                               .map((name, value) {
//                                 return MapEntry(
//                                   name,
//                                   DropdownMenuItem<int>(
//                                     value: value,
//                                     child: Text(name),
//                                   ),
//                                 );
//                               })
//                               .values
//                               .toList(),
//                           value: _frequencyValue,
//                           onChanged: (int? vechilenewvalue) {
//                             if (vechilenewvalue != null) {
//                               _modelValue = -1;
//                               // _frequencyValue = -1;
//                               _colorValue = -1;
//                               setState(() {
//                                 _frequencyValue = vechilenewvalue;
//                                 modelMap.modelMaps = ModelMap().modelMaps;

//                                 // _frequencyValue = brandnewvalue;

//                                 modelbloc.getmodel(context, _frequencyValue);
//                                 print(_frequencyValue);
//                                 brandVisible = false;
//                               });
//                             }
//                           },
//                           validator: (value) {
//                             if (value == null) {
//                               return 'Please select an option';
//                             }
//                             return null;
//                           },
//                         );
//                       }
//                       {
//                         return const CircularProgressIndicator();
//                       }
//                     }
//                     {
//                       return DropdownButtonFormField(
//                         decoration: const InputDecoration(
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(18)),
//                             borderSide: BorderSide(width: 1),
//                           ),
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(18)),
//                             borderSide: BorderSide(width: 1),
//                           ),
//                           filled: true,
//                           fillColor: Colors.white,
//                           contentPadding: EdgeInsets.symmetric(
//                             horizontal: 5,
//                             vertical: 15,
//                           ),
//                         ),
//                         dropdownColor: Colors.white,
//                         value: dropdownmodel,
//                         onChanged: (String? newValue) {
//                           setState(() {
//                             dropdownmodel = newValue!;
//                           });
//                         },
//                         items: <String>['Select model First']
//                             .map<DropdownMenuItem<String>>((String value) {
//                           return DropdownMenuItem<String>(
//                             value: value,
//                             child: Text(
//                               value,
//                               style: const TextStyle(fontSize: 18),
//                             ),
//                           );
//                         }).toList(),
//                       );
//                     }
//                   },
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: Visibility(
//                   visible: brandVisible,
//                   child: const Text(
//                     'please select brand name',
//                     style: TextStyle(
//                       color: Appcolor.bloodRed,
//                     ),
//                   ),
//                 ),
//               ),
//               ////////////
//               const Padding(
//                 padding: EdgeInsets.symmetric(
//                   vertical: 10,
//                   horizontal: 15,
//                 ),
//                 child: Text(
//                   'Selete Model',
//                   style: TextStyle(
//                     fontSize: 14,
//                     fontWeight: FontWeight.w400,
//                     fontStyle: FontStyle.normal,
//                     color: Appcolor.primaryBlue,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: StreamBuilder(
//                   stream: modelbloc.gemodelinfo,
//                   builder: (context, snapshot) {
//                     if (snapshot.data != null) {
//                       var modeldata = snapshot.data;
//                       if (modeldata != null) {
//                         return DropdownButtonFormField(
//                           decoration: const InputDecoration(
//                             enabledBorder: OutlineInputBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ), //<-- SEE HERE
//                             ),
//                             focusedBorder: OutlineInputBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ), //<-- SEE HERE
//                             ),
//                             filled: true,
//                             fillColor: Colors.white,
//                             contentPadding: EdgeInsets.symmetric(
//                               horizontal: 5,
//                               vertical: 15,
//                             ),
//                           ),
//                           dropdownColor: Colors.white,
//                           items: modelMap.modelMaps
//                               .map((name, value) {
//                                 return MapEntry(
//                                   name,
//                                   DropdownMenuItem<int>(
//                                     value: value,
//                                     child: Text(name),
//                                   ),
//                                 );
//                               })
//                               .values
//                               .toList(),
//                           hint: const Text('data'),
//                           value: _modelValue,
//                           onChanged: (int? modelnewValue) {
//                             if (modelnewValue != null) {
//                               setState(() {
//                                 colormap.colormaps = ColorMap().colormaps;
//                                 _modelValue = modelnewValue;
//                                 brandVisible = false;
//                                 colorbloc.getcolor(context, _modelValue);
//                               });
//                             }
//                           },
//                         );
//                       }
//                     }
//                     {
//                       return DropdownButtonFormField(
//                         decoration: const InputDecoration(
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(18)),
//                             borderSide: BorderSide(width: 1),
//                           ),
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(18)),
//                             borderSide: BorderSide(width: 1),
//                           ),
//                           contentPadding: EdgeInsets.symmetric(
//                             horizontal: 5,
//                             vertical: 15,
//                           ),
//                           filled: true,
//                           fillColor: Colors.white,
//                         ),
//                         dropdownColor: Colors.white,
//                         value: dropdownmodel,
//                         onChanged: (String? newValue) {
//                           setState(() {
//                             dropdownmodel = newValue!;
//                           });
//                         },
//                         items: <String>['Select model First']
//                             .map<DropdownMenuItem<String>>((String value) {
//                           return DropdownMenuItem<String>(
//                             value: value,
//                             child: Text(
//                               value,
//                               style: const TextStyle(fontSize: 18),
//                             ),
//                           );
//                         }).toList(),
//                       );
//                     }
//                   },
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: Visibility(
//                   visible: modelVisible,
//                   child: const Text(
//                     'please select model name',
//                     style: TextStyle(
//                       color: Appcolor.bloodRed,
//                     ),
//                   ),
//                 ),
//               ),
//               //////////////////
//               const Padding(
//                 padding: EdgeInsets.symmetric(
//                   vertical: 10,
//                   horizontal: 15,
//                 ),
//                 child: Text(
//                   'Selete Color',
//                   style: TextStyle(
//                     fontSize: 14,
//                     fontWeight: FontWeight.w400,
//                     fontStyle: FontStyle.normal,
//                     color: Appcolor.primaryBlue,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: StreamBuilder(
//                   stream: colorbloc.getselectedcolor,
//                   builder: (context, snapshot) {
//                     if (snapshot.data != null) {
//                       var colordata = snapshot.data;
//                       if (colordata != null) {
//                         return DropdownButtonFormField(
//                           decoration: const InputDecoration(
//                             enabledBorder: OutlineInputBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ), //<-- SEE HERE
//                             ),
//                             focusedBorder: OutlineInputBorder(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(18)),
//                               borderSide: BorderSide(
//                                 width: 1,
//                               ), //<-- SEE HERE
//                             ),
//                             filled: true,
//                             fillColor: Colors.white,
//                             contentPadding: EdgeInsets.symmetric(
//                               horizontal: 5,
//                               vertical: 15,
//                             ),
//                           ),
//                           dropdownColor: Colors.white,
//                           items: colormap.colormaps
//                               .map((name, value) {
//                                 return MapEntry(
//                                   name,
//                                   DropdownMenuItem<int>(
//                                     value: value,
//                                     child: Text(name),
//                                   ),
//                                 );
//                               })
//                               .values
//                               .toList(),
//                           hint: const Text('Color'),
//                           value: _colorValue,
//                           onChanged: (int? newValue) {
//                             if (newValue != null) {
//                               setState(() {
//                                 colorVisible = false;
//                                 _colorValue = newValue;
//                               });
//                             }
//                             {}
//                           },
//                         );
//                       }
//                     }
//                     {
//                       return DropdownButtonFormField(
//                         decoration: const InputDecoration(
//                           enabledBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(18)),
//                             borderSide: BorderSide(width: 1),
//                           ),
//                           focusedBorder: OutlineInputBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(18)),
//                             borderSide: BorderSide(width: 1),
//                           ),
//                           filled: true,
//                           fillColor: Colors.white,
//                           contentPadding: EdgeInsets.symmetric(
//                             horizontal: 5,
//                             vertical: 15,
//                           ),
//                         ),
//                         dropdownColor: Colors.white,
//                         value: dropdowncolor,
//                         onChanged: (String? newValue) {
//                           setState(() {
//                             dropdowncolor = newValue!;
//                           });
//                         },
//                         items: <String>['Select color First']
//                             .map<DropdownMenuItem<String>>((String value) {
//                           return DropdownMenuItem<String>(
//                             value: value,
//                             child: Text(
//                               value,
//                               style: const TextStyle(fontSize: 18),
//                             ),
//                           );
//                         }).toList(),
//                       );
//                     }
//                   },
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: Visibility(
//                   visible: colorVisible,
//                   child: const Text(
//                     'please select color name',
//                     style: TextStyle(
//                       color: Appcolor.bloodRed,
//                     ),
//                   ),
//                 ),
//               ),
//               /////////////////
//               const SizedBox(
//                 height: 10,
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: CustomTextFormField(
//                   isRequired: true,
//                   hintText: 'eg:ba 02 kha 9989',
//                   labelText: 'Number Plate',
//                   controller: numberplatecontoller,
//                   validator: MultiValidator(
//                     [
//                       RequiredValidator(errorText: 'Number Plate is required'),
//                       // EmailValidator(errorText: 'Number Plate is required.'),
//                     ],
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 00,
//                   horizontal: 15,
//                 ),
//                 child: GestureDetector(
//                   onTap: () {
//                     selectDate(context);
//                   },
//                   child: CustomTextFormField(
//                     enabled: false,
//                     suffixIcon: IconButton(
//                       onPressed: () {
//                         selectDate(context);
//                       },
//                       icon: const Icon(
//                         Icons.calendar_month,
//                         size: 30,
//                         color: Colors.blue,
//                       ),
//                     ),
//                     isRequired: true,
//                     hintText: 'Plz click Here for Date',
//                     labelText: 'production year ',
//                     controller: producationyearcontoller,
//                     validator: MultiValidator(
//                       [
//                         RequiredValidator(
//                           errorText: 'production year is required',
//                         ),
//                         // EmailValidator(errorText: 'production year is required.'),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
// ///////////////
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   horizontal: 18,
//                   vertical: 10,
//                 ),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Text(
//                       'Upload your Vechile photo',
//                       textAlign: TextAlign.left,
//                       style: GoogleFonts.ubuntu(
//                         textStyle: const TextStyle(
//                           color: Color.fromARGB(255, 0, 0, 0),
//                           fontSize: 16,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                     ),
//                     const Icon(
//                       Icons.check_circle_outline,
//                       color: Appcolor.green,
//                     )
//                   ],
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.symmetric(
//                   vertical: 10,
//                   horizontal: 18,
//                 ),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     GestureDetector(
//                       onTap: () {
//                         setState(() {
//                           _showPicker(context);
//                         });
//                       },
//                       child: Center(
//                         child: SizedBox(
//                           child: Align(
//                             alignment: Alignment.center,
//                             child: DecoratedBox(
//                               decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.circular(5),
//                                 boxShadow: const [
//                                   BoxShadow(
//                                     color: Appcolor.grey,
//                                     blurStyle: BlurStyle.outer,
//                                     spreadRadius: 5.0,
//                                     blurRadius: 1.0,
//                                   )
//                                 ],
//                               ),
//                               child: SizedBox(
//                                 height:
//                                     MediaQuery.of(context).size.width / 2 - 40,
//                                 width:
//                                     MediaQuery.of(context).size.width / 2 - 40,
//                                 child: imgTempPath != null
//                                     ? Image.file(
//                                         File(imgTempPath.toString()),
//                                       )
//                                     : const Column(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.center,
//                                         children: [
//                                           Icon(
//                                             Icons.photo_camera_outlined,
//                                             color: Appcolor.orange,
//                                             size: 40,
//                                           ),
//                                           SizedBox(height: 10),
//                                           Text(
//                                             'insert 2nd and 3rd page photo of blue book',
//                                             maxLines: 1,
//                                             style: TextStyle(
//                                               fontSize: 14,
//                                               fontStyle: FontStyle.normal,
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 10,
//                     ),
//                     GestureDetector(
//                       onTap: () {
//                         setState(() {
//                           _showPicker(context);
//                         });
//                       },
//                       child: Center(
//                         child: SizedBox(
//                           child: Align(
//                             alignment: Alignment.center,
//                             child: DecoratedBox(
//                               decoration: BoxDecoration(
//                                 borderRadius: BorderRadius.circular(5),
//                                 boxShadow: const [
//                                   BoxShadow(
//                                     color: Appcolor.grey,
//                                     blurStyle: BlurStyle.outer,
//                                     spreadRadius: 5.0,
//                                     blurRadius: 1.0,
//                                   )
//                                 ],
//                               ),
//                               child: SizedBox(
//                                 height:
//                                     MediaQuery.of(context).size.width / 2 - 40,
//                                 width:
//                                     MediaQuery.of(context).size.width / 2 - 40,
//                                 child: imgTempPath != null
//                                     ? Image.file(
//                                         File(imgTempPath.toString()),
//                                       )
//                                     : const Column(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.center,
//                                         children: [
//                                           Icon(
//                                             Icons.photo_camera_outlined,
//                                             color: Appcolor.orange,
//                                             size: 40,
//                                           ),
//                                           SizedBox(height: 10),
//                                           Text(
//                                             'insert 2nd and 3rd page photo of blue book',
//                                             maxLines: 1,
//                                             style: TextStyle(
//                                               fontSize: 14,
//                                               fontStyle: FontStyle.normal,
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               const SizedBox(
//                 height: 18,
//               ),
//               GestureDetector(
//                 onTap: () {
//                   setState(() {
//                     _showPicker(context);
//                   });
//                 },
//                 child: Center(
//                   child: SizedBox(
//                     child: Align(
//                       alignment: Alignment.center,
//                       child: DecoratedBox(
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(5),
//                           boxShadow: const [
//                             BoxShadow(
//                               color: Appcolor.grey,
//                               blurStyle: BlurStyle.outer,
//                               spreadRadius: 5.0,
//                               blurRadius: 1.0,
//                             )
//                           ],
//                         ),
//                         child: SizedBox(
//                           height: MediaQuery.of(context).size.width - 40,
//                           width: MediaQuery.of(context).size.width - 40,
//                           child: imgTempPath != null
//                               ? Image.file(
//                                   File(imgTempPath.toString()),
//                                 )
//                               : const Column(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: [
//                                     Icon(
//                                       Icons.photo_camera_outlined,
//                                       color: Appcolor.orange,
//                                       size: 40,
//                                     ),
//                                     SizedBox(height: 10),
//                                     Text(
//                                       'insert 2nd and 3rd page photo of blue book',
//                                       maxLines: 1,
//                                       style: TextStyle(
//                                         fontSize: 14,
//                                         fontStyle: FontStyle.normal,
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//               const SizedBox(height: 50),

//               const Divider(
//                 color: Color.fromARGB(255, 219, 219, 219),
//                 thickness: 1,
//               ),

//               // const SizedBox(height: (100)),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
